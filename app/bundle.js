(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/// <reference path="../../../typings/underscore/underscore.d.ts"/>
/* global Firebase, SignaturePad */
/// <reference path="../../../typings/angularjs/angular.d.ts"/>

'use strict';

var _configRouteConfigJs = require('./config/route-config.js');

/**
 * Controllers
 */

var _controllersAppCtrlJs = require('./controllers/AppCtrl.js');

var _controllersLoginCtrlJs = require('./controllers/LoginCtrl.js');

var _controllersAdminCtrlJs = require('./controllers/AdminCtrl.js');

var _controllersAllReportsCtrlJs = require('./controllers/AllReportsCtrl.js');

var _controllersReportCtrlJs = require('./controllers/ReportCtrl.js');

var _controllersJobsCtrlJs = require('./controllers/JobsCtrl.js');

var _controllersCreateJobCtrlJs = require('./controllers/CreateJobCtrl.js');

var _controllersDangerSheetCtrlJs = require('./controllers/DangerSheetCtrl.js');

var _controllersWaterSheetCtrlJs = require('./controllers/WaterSheetCtrl.js');

var _controllersSideNavCtrlJs = require('./controllers/SideNavCtrl.js');

/**
 *Services 
 */

var _servicesSignPadSrvJs = require('./services/SignPadSrv.js');

var _servicesToastSrvJs = require('./services/ToastSrv.js');

var _servicesLoadReportsSrvJs = require('./services/LoadReportsSrv.js');

var _servicesFireBaseAuthSrvJs = require('./services/FireBaseAuthSrv.js');

var _servicesLoadingPopupSrvJs = require('./services/LoadingPopupSrv.js');

var _servicesLocalDatabaseSrvJs = require('./services/LocalDatabaseSrv.js');

var _servicesPointsDataSrvJs = require('./services/PointsDataSrv.js');

var _servicesOrientationSrvJs = require('./services/OrientationSrv.js');

var _servicesJobStatusSrvJs = require('./services/JobStatusSrv.js');

var _servicesPrintSrvJs = require('./services/PrintSrv.js');

var _servicesDateISOSrvJs = require('./services/DateISOSrv.js');

var app = angular.module('ConditionApp', ['firebase', 'ngMaterial', 'ngMessages', 'ui.router', 'angular-cache', 'ng.deviceDetector', 'angularMoment', 'angular-sortable-view', 'ngWebworker', 'ngIOS9UIWebViewPatch']);

app.config(_configRouteConfigJs.config);

app.run(function ($rootScope, $state, FireBaseRef) {

	$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page

	});

	$rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		document.querySelector('ui-view').scrollIntoView();
	});

	$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		console.log(error);
		if (error === "AUTH_REQUIRED") {
			$state.go("login");
		}
	});

	var connectedRef = new Firebase("https://wells-report-resume.firebaseio.com/.info/connected");
	connectedRef.on("value", function (snap) {
		if (snap.val() === true) {
			$rootScope.$broadcast('connected');
		} else {
			$rootScope.$broadcast('disconnected');
		}
	});
});
app.controller('AppCtrl', _controllersAppCtrlJs.AppCtrl);

app.controller('LoginCtrl', _controllersLoginCtrlJs.LoginCtrl);

app.controller('AdminCtrl', _controllersAdminCtrlJs.AdminCtrl);

app.controller('AllReportsCtrl', _controllersAllReportsCtrlJs.AllReportsCtrl);

app.controller('ReportCtrl', _controllersReportCtrlJs.ReportCtrl);

app.controller('JobsCtrl', _controllersJobsCtrlJs.JobsCtrl);

app.controller('CreateJobCtrl', _controllersCreateJobCtrlJs.CreateJobCtrl);

app.controller('DangerSheetCtrl', _controllersDangerSheetCtrlJs.DangerSheetCtrl);

app.controller('WaterSheetCtrl', _controllersWaterSheetCtrlJs.WaterSheetCtrl);

app.controller('SideNavCtrl', _controllersSideNavCtrlJs.SideNavCtrl);
app.service('SignPad', _servicesSignPadSrvJs.SignPadSrv);

app.service('Toast', _servicesToastSrvJs.ToastSrv);

app.service('LoadReportsSrv', _servicesLoadReportsSrvJs.LoadReportsSrv);

app.service('FireBaseAuth', _servicesFireBaseAuthSrvJs.FireBaseAuthSrv);

app.service('LoadingPopup', _servicesLoadingPopupSrvJs.LoadingPopupSrv);

app.service('LocalDatabase', _servicesLocalDatabaseSrvJs.LocalDatabaseSrv);

app.service('PointsData', _servicesPointsDataSrvJs.PointsDataSrv);

app.service('Orientation', _servicesOrientationSrvJs.OrientationSrv);

app.service('JobStatus', _servicesJobStatusSrvJs.JobStatusSrv);

app.service('Print', _servicesPrintSrvJs.PrintSrv);

app.service('DateISO', _servicesDateISOSrvJs.DateISOSrv);

/**
 * Firebase URL
 */
app.constant('FireBaseRef', {
	ref: new Firebase("https://wells-report-resume.firebaseio.com")
});

app.filter('jobFilter', function ($filter) {

	// In the return function, we must pass in a single parameter which will be the data we will work on.

	return function (arrayToParse, searchValue) {

		var output;

		if (searchValue) {

			if (searchValue.search('{') == 0 && searchValue.search('}') > 0) {
				(function () {
					//split the search text by comma to push array
					var searchParams = searchValue.split(',');
					//variable for storing the new object
					var searchValueObject = {};
					//loop through searchParams to split each array based colon
					searchParams.map(function (v, i, a) {
						var searchParam = v.split(':');
						var key = undefined,
						    value = undefined;
						//remove { or }

						try {
							key = searchParam[0].replace(/[{}]/, '');
							value = searchParam[1].replace(/[{}]/, '');
						} catch (error) {}

						//create a new object with key and value
						searchValueObject[key] = value;
					});

					try {
						output = $filter('filter')(arrayToParse, searchValueObject);
					} catch (error) {}
				})();
			} else {

				output = $filter('filter')(arrayToParse, searchValue);
			}
		} else {

			output = $filter('filter')(arrayToParse, searchValue);
		}

		return output;
	};
});
// We have the ability to support multiple other parameters that can be passed into the filter optionally

},{"./config/route-config.js":2,"./controllers/AdminCtrl.js":3,"./controllers/AllReportsCtrl.js":4,"./controllers/AppCtrl.js":5,"./controllers/CreateJobCtrl.js":6,"./controllers/DangerSheetCtrl.js":7,"./controllers/JobsCtrl.js":8,"./controllers/LoginCtrl.js":9,"./controllers/ReportCtrl.js":10,"./controllers/SideNavCtrl.js":11,"./controllers/WaterSheetCtrl.js":12,"./services/DateISOSrv.js":13,"./services/FireBaseAuthSrv.js":14,"./services/JobStatusSrv.js":15,"./services/LoadReportsSrv.js":16,"./services/LoadingPopupSrv.js":17,"./services/LocalDatabaseSrv.js":18,"./services/OrientationSrv.js":19,"./services/PointsDataSrv.js":20,"./services/PrintSrv.js":21,"./services/SignPadSrv.js":22,"./services/ToastSrv.js":23}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});
var config = (function config() {
	function config($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdGestureProvider, CacheFactoryProvider) {
		$mdGestureProvider.skipClickHijack();

		angular.extend(CacheFactoryProvider.defaults, {
			maxAge: 15 * 60 * 1000,
			storageMode: 'localStorage'
		});

		$stateProvider.state('login', {
			url: '/login',
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl as vm'

		}).state('logOut', {
			url: '/logOut',
			templateUrl: 'views/login.html',
			controller: function controller(FireBaseAuth) {
				FireBaseAuth.$unauth();
			}
		}).state('jobs', {
			url: '/jobs',
			templateUrl: 'views/jobs.html',
			controller: 'JobsCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		}).state('admin', {
			url: '/admin',
			templateUrl: 'views/admin.html',
			controller: 'AdminCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", function (Auth) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					return Auth.$requireAuth();
				}]
			}
		}).state('createJob', {
			url: '/createJob/:id',
			templateUrl: 'views/createJobs.html',
			controller: 'CreateJobCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		}).state('allReports', {
			url: '/allReports',
			templateUrl: 'views/allReports.html',
			controller: "AllReportsCtrl as vm",
			// controller: 'WaterSheetCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		}).state('allReports.report', {
			url: '/report/:id',
			templateUrl: 'views/reportSheet.html',
			controller: 'ReportCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		}).state('allReports.dangerSheet', {
			url: '/dangerSheet/:id',
			templateUrl: 'views/dangerSheet.html',
			controller: 'DangerSheetCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		}).state('allReports.waterSheet', {
			url: '/waterSheet/:id',
			templateUrl: 'views/waterSheet.html',
			controller: 'WaterSheetCtrl as vm',
			resolve: {
				// controller will not be loaded until $requireAuth resolves
				// Auth refers to our $firebaseAuth wrapper in the example above
				"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
					// $requireAuth returns a promise so the resolve waits for it to complete
					// If the promise is rejected, it will throw a $stateChangeError (see above)
					if (navigator.onLine) {
						return Auth.$requireAuth();
					} else {
						return LocalDatabase.$getAuth();
					}
				}]
			}
		});

		$urlRouterProvider.otherwise('login');

		$mdThemingProvider.theme('default').primaryPalette('light-blue').accentPalette('orange');
	}

	config.$inject = ['$stateProvider', '$urlRouterProvider', '$mdThemingProvider', '$mdGestureProvider', 'CacheFactoryProvider'];

	return config;
})();
exports.config = config;

},{}],3:[function(require,module,exports){
/**
 * @ngdoc controller
 * @name ConditionApp.controller.AdminCtrl
 * @description
 * Admin Controller admin.html
 * @requires currentAuth
 * @requires FireBaseRef
 * @requires $firebaseArray
 * @requires FireBaseAuth
 * @requires $mdDialog
 * @requires $mdToast
 * @requires $scope
 * @requires $firebaseObject
 * @requires $window
 * @requires Orientation
 * @requires deviceDetector
 * @requires LocalDatabase
 * @requires $rootScope
 * @requires DateISO
 * @ngInject
 *  
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var AdminCtrl = (function () {
	function AdminCtrl(currentAuth, FireBaseRef, $firebaseArray, FireBaseAuth, $mdDialog, $mdToast, $scope, $firebaseObject, $window, Orientation, deviceDetector, LocalDatabase, $rootScope, DateISO) {
		var _this = this;

		_classCallCheck(this, AdminCtrl);

		//converts local iso-string to handled timezone
		this.DateISO = DateISO;

		// Get a database reference
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.users = $firebaseArray(this.ref.child('users'));
		this.crews = $firebaseArray(this.ref.child('crews'));
		this.clients = $firebaseArray(this.ref.child('clients'));
		this.$mdDialog = $mdDialog;
		this.$mdToast = $mdToast;
		this.$scope = $scope;
		this.auth = FireBaseAuth;
		this.master = { email: '', password: '', name: '' };
		this.$firebaseObject = $firebaseObject;
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;

		// AdminCtrl.set$watch.apply(this);

		Orientation.sourcePage = 'admin';

		$window.onorientationchange = function () {
			console.log(Orientation.scrollHeight);
			_this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		};

		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}
	}

	_createClass(AdminCtrl, [{
		key: 'enroll',

		/**
   * @ngdoc method
   * @name enroll
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * Enroll new admin user
   * @param {HTMLFormElement} form new admin user HTML form
   * @param {Object} $event key pressed
   */

		value: function enroll(form, $event) {
			var _this2 = this;

			if ($event.keyCode != 13 && $event.keyCode != 0) {
				return;
			}

			this.auth.$createUser(this.user).then(function (userData) {
				console.log("User " + userData.uid + " created successfully!");

				_this2.showToast({ name: _this2.user.name });

				_this2.user.email = _this2.user.email.toLowerCase();

				_this2.users.$add(_this2.user);

				form.$setPristine();
				form.$setUntouched();
				_this2.user = angular.copy(_this2.master);
			})['catch'](function (error) {
				_this2.showAlert(error);
				console.error("Error: ", error);
			});
		}

		/**
   * @ngdoc method
   * @name _remove
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * Remove either the user or crew
   * @param {Object|string} itemToRemove The item to remove
   */

	}, {
		key: '_remove',
		value: function _remove(itemToRemove) {

			switch (this.selectedIndex) {
				case 1:
					AdminCtrl.removeUser.call(this, itemToRemove);
					break;
				case 2:
					AdminCtrl.removeCrew.call(this, itemToRemove);
					break;
				default:
					break;
			}
		}

		/**
   * @ngdoc method
   * @name _remove
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * Remove crew
   * @param {Object|string} itemToRemove The item to remove
   */

	}, {
		key: 'showAlert',

		/**
   * @ngdoc method
   * @name showAlert
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * show alert dialog
   * @param {string} error The error to display
   */

		value: function showAlert(error) {
			console.log(error);
			var alert = this.$mdDialog.alert().title('Error').content(error).ok('Close');

			this.$mdDialog.show(alert);
		}

		/**
   * @ngdoc method
   * @name _showReassignDialog
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * show reassign dialog.  Allows the admin to reassign all jobs to another user
   * @param {Object} $event Button click
   * @param {Object} obj The user to reassign
   */

	}, {
		key: '_showReassignDialog',
		value: function _showReassignDialog(userToReassign, $event) {
			var that = this;

			this.$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				templateUrl: 'views/reassignProjects.html',
				controller: function DialogController($mdDialog) {
					var _this3 = this;

					that.users.$loaded().then(function (users) {
						console.log(users);
						_this3.users = users;
					});

					// this.user = user;
					this.closeDialog = function () {
						$mdDialog.hide();
					};

					this.change = function (user) {
						that.clients.$loaded().then(function (clients) {
							clients.map(function (cv, i, a) {
								if (cv.techID === userToReassign.$id) {
									//client to update the tech for
									var client = that.$firebaseObject(that.ref.child('clients').child(cv.$id));
									client.$loaded().then(function (client) {
										console.log(client.tech);

										client.tech = user.name;
										client.techID = user.$id;
										client.$save().then(function (ref) {
											console.log("change successful");
											that.showToast({ msg: "Projects reassign to: " + user.name });
											$mdDialog.hide();
										}, function (error) {
											$mdDialog.hide();
											that.showAlert(error);
											console.error("Error:", error);
										});
									});
								}
							});
						});
					};
				},
				controllerAs: 'vm'
			});
		}

		/**
   * @ngdoc method
   * @name showEditDialog
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * show edit dialog.  Edit admin user
   * @param {Object} $event Button click
   * @param {Object} obj The user to edit
   */

	}, {
		key: 'showEditDialog',
		value: function showEditDialog($event, obj) {
			var that = this;
			var templateUrl = undefined;

			if (obj.data.name === 'All Crews') {
				return;
			}
			var enumTab = {
				editUser: 1,
				editCrew: 2
			};
			switch (this.selectedIndex) {
				case 1:
					templateUrl = 'views/editUser.html';
					break;
				case 2:
					templateUrl = 'views/addCrew.html';
					break;

				default:
					break;
			}
			this.$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				scope: this.$scope, // use parent scope in template
				preserveScope: true, // do not forget this if use parent scope
				templateUrl: templateUrl,
				controller: function DialogController($scope, $mdDialog) {
					var _this4 = this;

					var search = undefined,
					    model = undefined;

					//identifed which model information to load
					switch (that.selectedIndex) {
						case enumTab.editUser:
							// Load user info
							search = 'users';
							model = 'user';
							break;
						case enumTab.editCrew:
							// Load crew info
							search = 'crews';
							model = 'crew';
							this.btnName = false; // display Add
							break;

						default:
							break;
					}

					this[model] = that.$firebaseObject(that.ref.child(search).child(obj.data.$id));

					this.update = function (data, form) {

						form.$setPristine();
						form.$setUntouched();
						$mdDialog.hide();

						switch (that.selectedIndex) {
							case enumTab.editUser:
								updateUser(data);
								break;
							case enumTab.editCrew:
								updateCrew(data);
								break;

							default:
								break;
						}
					};

					function updateUser(user) {
						if (user.tempCrew) {
							var temp = user.crew;
							user.crew = user.tempCrew;
							user.tempCrew = temp;
							user.tempDate = that.DateISO.tomorrow;
						}

						if (user.newpassword) {
							that.auth.$changePassword({
								email: user.email,
								oldPassword: user.password,
								newPassword: user.newpassword
							}).then(function () {
								user.password = user.newpassword;
								delete user.newpassword;
								user.$save();
								user.$loaded().then(function (user) {
									that.LocalDatabase.put('allowed', user);
								});
								that.showToast({ msg: "Successfully Changed Data" });
								console.log("Password changed successfully!");
							})['catch'](function (error) {
								console.error("Error: ", error);
							});
						} else {
							delete user.newpassword;
							that.showToast({ msg: "Successfully Changed Data" });
							user.$save();
							that.$rootScope.$broadcast('successLogin', { user: user });
						}

						var newUser = {};
						_.extendOwn(newUser, user);
						delete newUser.$$conf;
						that.LocalDatabase.put('allowed', newUser);
					}

					function updateCrew(crew) {
						crew.$save();
					}

					this.closeDialog = function () {
						$mdDialog.hide();
					};

					this.remove = function (itemToRemove) {
						_this4.closeDialog();
						that._remove(itemToRemove);
					};

					this.showReassignDialog = function (userToReassign, form) {
						_this4.closeDialog();
						that._showReassignDialog(userToReassign, form);
					};

					this.removeTempCrew = function (user) {
						user.crew = user.tempCrew;
						user.tempDate = null;
						user.tempCrew = null;
					};
				},
				controllerAs: 'mdDialog'
			});
		}

		/**
   * @ngdoc method
   * @name showAddDialog
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * show add dialog.  Add admin user
   * @param {Object} $event The button clicked
   */

	}, {
		key: 'showAddDialog',
		value: function showAddDialog($event) {
			var that = this;

			var templateUrl = undefined;
			switch (this.selectedIndex) {
				case 1:

					break;
				case 2:
					templateUrl = 'views/addCrew.html';
					break;

				default:
					break;
			}
			this.$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				scope: this.$scope, // use parent scope in template
				preserveScope: true, // do not forget this if use parent scope
				// Since GreetingController is instantiated with ControllerAs syntax
				// AND we are passing the parent '$scope' to the dialog, we MUST
				// use 'vm.<xxx>' in the template markup
				templateUrl: templateUrl,
				controller: function DialogController($scope, $mdDialog) {

					this.btnName = true; // display Add

					this.closeDialog = function () {
						$mdDialog.hide();
					};

					this.add = function (item, form) {
						that.crews.$add(item);

						form.$setPristine();
						form.$setUntouched();
						$mdDialog.hide();
					};
				},
				controllerAs: 'mdDialog'
			});
		}

		/**
   * @ngdoc method
   * @name showToast
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * Show toast message at the bottom of the screen
   * @param {string} toast The message to display
   */

	}, {
		key: 'showToast',
		value: function showToast(toast) {
			var msg = toast.msg || 'User: ' + toast.name + ' has been enrolled';
			this.$mdToast.show(this.$mdToast.simple().content(msg));
		}
	}], [{
		key: 'set$watch',
		value: function set$watch() {
			var unwatch = this.users.$watch(function (data) {
				console.log("data changed!", data);
			});
		}
	}, {
		key: 'removeCrew',
		value: function removeCrew(itemToRemove) {
			var _this5 = this;

			var numOfMatches;

			this.clients.$loaded().then(function (clients) {
				numOfMatches = clients.filter(function (cv, i, a) {
					if (cv.crew == undefined) {
						return false;
					}
					return cv.crew === itemToRemove.name;
				});

				//if a project does not exist remove the user. Else display an error
				if (numOfMatches.length == 0) {
					itemToRemove.$remove().then(function () {
						_this5.showToast({ msg: "Crew removed successfully!" });
					})['catch'](function (error) {
						_this5.showAlert(error);
						console.error("Error: ", error);
					});
				} else {
					_this5.showAlert("Project(s) exist under the crew: " + itemToRemove.name + ", cannot delete crew");
					console.warn();
				}
			});
		}

		/**
   * @ngdoc method
   * @name _remove
   * @methodOf ConditionApp.controller.AdminCtrl
   * @description
   * Remove user
   * @param {Object|string} itemToRemove The item to remove
   */

	}, {
		key: 'removeUser',
		value: function removeUser(itemToRemove) {
			var _this6 = this;

			var numOfMatches = undefined;
			this.jobs = this.$firebaseArray(this.ref.child('job'));
			// this.clients = this.$firebaseArray(this.ref.child('clients'));
			//check if a project exist first before removing user
			this.jobs.$loaded().then(function (jobs) {

				_this6.clients.$loaded().then(function (clients) {
					numOfMatches = clients.filter(function (cv, i, a) {
						if (cv.techID == undefined) {
							return false;
						}

						var job = _.findWhere(jobs, { $id: cv.foreignKey });

						return cv.techID === itemToRemove.$id && job.status === 'Open';
					});

					//if a project does not exist remove the user. Else display an error
					if (numOfMatches.length == 0) {
						_this6.auth.$removeUser(itemToRemove).then(function () {
							itemToRemove.$remove().then(function (ref) {
								// data has been deleted locally and in the database
								_this6.showToast({ msg: "User removed successfully!" });
							}, function (error) {
								console.log("Error:", error);
							});
						})['catch'](function (error) {
							_this6.showAlert(error);
							console.error("Error: ", error);
						});
					} else {
						_this6.showAlert("Project(s) exist under the name: " + itemToRemove.name + ", cannot delete user");
						console.warn();
					}
				});
			});
		}
	}]);

	return AdminCtrl;
})();

exports.AdminCtrl = AdminCtrl;

},{}],4:[function(require,module,exports){
/**
 * All Reports Controller for allReports.html
 * @constructor
 * @ngInject
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var AllReportsCtrl = (function () {
	function AllReportsCtrl($state, $stateParams, $firebaseArray, LoadReportsSrv, $scope, $rootScope, LocalDatabase, LoadingPopup) {
		var _this = this;

		_classCallCheck(this, AllReportsCtrl);

		this.$rootScope = $rootScope;

		//counts the number condition reports added to the tab list
		this.countOfConditionReports = 0;

		//hides the add button for additional reports
		this.hideBtn = true;

		// access the data to know which reports to display
		if (navigator.onLine) {

			LoadReportsSrv.remote($scope).then(function (client) {
				_this.client = client;

				AllReportsCtrl.loadTabs.call(_this);
			});
		} else {
			LoadReportsSrv.local($scope).then(function (client) {
				//set the view model to client data from local database
				_this.client = client;

				AllReportsCtrl.loadTabs.call(_this);

				$scope.$watchCollection(function () {
					return client;
				}, function (n, o) {
					if (o !== n) {
						LocalDatabase.$save(n);
					}
				});
			});
		}
	}

	_createClass(AllReportsCtrl, [{
		key: 'addConditionReport',
		value: function addConditionReport() {
			if (this.selectedIndex = 0) {
				this.selectedIndex = this.currentIndex;
			} else {
				this.selectedIndex = this.currentIndex + this.countOfConditionReports;
			}

			// this.$rootScope.$broadcast('newConditionReport');
			if (!this.tabs) {
				return;
			}

			if (!this.client.condition) {
				this.tabs.splice(0, 0, {
					label: 'Condition Report',
					ngif: true,
					html: 'views/reportSheet.html'
				});
				this.client.condition = true;
				return;
			}
			this.countOfConditionReports++;

			if (this.countOfConditionReports <= 2) {
				this.tabs.splice(this.countOfConditionReports, 0, {
					label: 'Condition Report ' + this.countOfConditionReports,
					ngif: true,
					html: 'views/reportSheet.' + this.countOfConditionReports + '.html'
				});
			}

			if (this.countOfConditionReports == 2) {

				this.hideBtn = false;
			}
		}
	}, {
		key: 'addDangerReport',
		value: function addDangerReport() {
			this.tabs.push({
				label: 'Danger Report',
				ngif: true,
				html: 'views/dangerSheet.html'
			});

			this.client.danger = true;
		}
	}, {
		key: 'addWaterReport',
		value: function addWaterReport() {
			this.tabs.push({
				label: 'Water Report',
				ngif: true,
				html: 'views/waterSheet.html'
			});

			this.client.water = true;
		}
	}, {
		key: 'selectTab',
		value: function selectTab(tabName) {
			this.tabHTML = tabName;
		}
	}], [{
		key: 'loadTabs',
		value: function loadTabs() {
			this.tabs = [{
				label: 'Condition Report',
				ngif: this.client.condition,
				html: 'views/reportSheet.html'
			}, {
				label: 'Danger Report',
				ngif: this.client.danger,
				html: 'views/dangerSheet.html'
			}, {
				label: 'Water Report',
				ngif: this.client.water,
				html: 'views/waterSheet.html'
			}];

			if (this.client.conditionD) {
				this.tabs.splice(1, 0, {
					id: 'creport',
					label: 'Condition Report 1',
					ngif: this.client.condition,
					html: 'views/reportSheet.1.html'
				});
			}

			if (this.client.conditionG) {
				this.tabs.splice(2, 0, {
					id: 'creport',
					label: 'Condition Report 2',
					ngif: this.client.condition,
					html: 'views/reportSheet.2.html'
				});
			}

			var countExistConditionReports = _.where(this.tabs, { id: 'creport' });
			this.countOfConditionReports = countExistConditionReports.length;
			var setTab = _.findWhere(this.tabs, { ngif: true });
			if (this.countOfConditionReports == 2) {
				this.hideBtn = false;
			}

			this.selectTab(setTab.html);
		}
	}]);

	return AllReportsCtrl;
})();

exports.AllReportsCtrl = AllReportsCtrl;

},{}],5:[function(require,module,exports){
/**
 * App Controller for index.html
 * @param {!ui.router.$stae} $state
 * @param {} FireBaseAuth
 * @param {} LocalDatabase
 * @param {!angular.$rootScope} $rootScope
 * @param {} $firebaseArray
 * @constructor
 * @ngInject
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var AppCtrl = (function () {
	function AppCtrl($state, FireBaseAuth, LocalDatabase, $rootScope) {
		var _this = this;

		_classCallCheck(this, AppCtrl);

		this.$state = $state;
		this.master = { email: '', password: '' };
		this.auth = FireBaseAuth;
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;

		this.user = this.LocalDatabase.get('allowed');

		$rootScope.$on('successLogin', function (evt, data) {
			_this.user = _this.LocalDatabase.get('allowed');
		});
	}

	_createClass(AppCtrl, [{
		key: 'nav',
		value: function nav(site, $event) {
			if (site === 'logout') {
				console.log(site);
				this.auth.$unauth();
				this.$state.go('login');
				this.LocalDatabase.$unauth();
				this.user = '';
				return;
			}
			if (site === 'createJob') {
				this.$state.transitionTo(site, { id: '' });
			} else {
				this.$state.transitionTo(site);
			}
		}
	}]);

	return AppCtrl;
})();

exports.AppCtrl = AppCtrl;

},{}],6:[function(require,module,exports){
/**
 * Create Jobs Controller createJobs.html
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CreateJobCtrl = (function () {
	function CreateJobCtrl(FireBaseRef, currentAuth, $scope, $firebaseArray, $state, $mdDialog, $location, $anchorScroll, Toast, $stateParams, $firebaseObject, LoadingPopup, Orientation, $window, deviceDetector, PointsData, LocalDatabase) {
		var _this = this;

		_classCallCheck(this, CreateJobCtrl);

		//load states
		this.states = PointsData.states;

		// Get a database reference
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.$mdDialog = $mdDialog;
		var crewsList = $firebaseArray(this.ref.child('crews'));
		this.$firebaseObject = $firebaseObject;
		this.master = {};
		this.$state = $state;
		this.$location = $location;
		this.$anchorScroll = $anchorScroll;
		this.Toast = Toast;
		this.id = $stateParams.id;
		this.LoadingPopup = LoadingPopup;
		this.LocalDatabase = LocalDatabase;

		//clean the form
		this.cleanForm();

		if (this.id) {
			this.name = "Edit Job";
			this.btnName = "Save Job";
		} else {
			this.name = "Create Job";
			this.btnName = "Create Job";
			//default state field to PA
			this.client = { state: 'PA' };
		}

		crewsList.$loaded().then(function (items) {
			_this.crews = _.reject(items, function (value) {
				return value.name === "All Crews";
			});
		});

		if (this.id) {
			this.LoadingPopup.show();
			//list of all clients
			var clients = $firebaseArray(this.ref.child('clients'));
			//loop through clients for a match
			clients.$loaded().then(function (clients) {
				clients.map(function (cv, i, a) {
					if (cv.foreignKey === _this.id) {
						var job = _this.$firebaseObject(_this.ref.child('job').child(cv.foreignKey));
						job.$loaded().then(function (job) {
							cv.jobDate = new Date(job.jobDate);
							//pull the crew for client from JOB key
							cv.crew = job.crew;
							_this.client = cv;
						});
					}
				});
				_this.LoadingPopup.hide();
			});
		}

		Orientation.sourcePage = 'createJob';

		//set md-card-content css height property
		$window.onorientationchange = function () {
			_this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		};

		//fix the height on iphone 250px
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}
	}

	_createClass(CreateJobCtrl, [{
		key: "query",
		value: function query(searchText) {
			console.log(searchText);
			return _.where(this.states, { abbreviation: searchText });
		}
	}, {
		key: "createJob",
		value: function createJob(form) {
			var _this2 = this;

			//adding new job
			if (!this.id) {
				if (!this.client.condition && !this.client.danger && !this.client.water) {
					var alert = this.$mdDialog.alert({
						title: 'Error',
						content: 'You must choose a report to create',
						ok: 'Ok'
					});

					this.$mdDialog.show(alert);

					return;
				}
				var crewToAdd = this.$firebaseArray(this.ref.child('job'));
				var job = {
					name: this.client.name,
					crew: this.client.crew,
					status: 'Open',
					jobDate: this.client.jobDate.toISOString()
				};
				crewToAdd.$add(job).then(function (ref) {
					var clients = _this2.$firebaseArray(_this2.ref.child('clients'));
					var jobOrders = _this2.$firebaseArray(_this2.ref.child('jobOrder'));
					var id = ref.key();
					var user = _this2.LocalDatabase.get('allowed');
					_this2.client.foreignKey = id;
					//remove crew from client key.
					delete _this2.client.crew;

					clients.$add(_this2.client).then(function () {
						jobOrders.$loaded().then(function (jobOrder) {
							if (jobOrder.length !== 0) {
								var job = _this2.$firebaseObject(_this2.ref.child('job').child(id));
								//initial load of jobOrder table
								job.$loaded().then(function (_job) {
									jobOrder.$add({ foreignKey: _job.$id, order: jobOrder.length });
								});
							}
						});
					});

					_this2.Toast.showToast({ msg: 'Job Added' });

					if (user.crew !== 'All Crews') {
						var confirm = _this2.$mdDialog.confirm({
							title: 'Success',
							content: 'Would you like to open the new job',
							ok: 'Open Job',
							cancel: 'Create New Job'
						});

						_this2.$mdDialog.show(confirm).then(function (data) {
							_this2.$state.go('allReports.report', { id: id });
							_this2.cleanForm(form);
						}, function (data) {
							_this2.cleanForm(form);
						});
					} else {
						_this2.cleanForm(form);
					}
				});
			}
			//updating an existing job
			else {
					var client = this.$firebaseObject(this.ref.child('clients').child(this.client.$id));

					this.LoadingPopup.show();

					var name = this.client.name;
					var crew = this.client.crew;
					var jobDate = this.client.jobDate.toISOString();
					delete this.client.crew;

					client.$loaded().then(function (client) {
						angular.copy(_this2.client, client);
						client.$save().then(function (ref) {
							var job = _this2.$firebaseObject(_this2.ref.child('job').child(client.foreignKey));
							job.$loaded().then(function (job) {
								job.name = name;
								job.crew = crew;
								job.jobDate = jobDate;
								job.$save().then(function () {
									_this2.LoadingPopup.hide();
									_this2.$state.go('jobs');
								});
							});
						});
					});
				}
		}
	}, {
		key: "cleanForm",
		value: function cleanForm(form) {
			if (form) {
				form.$setPristine();
				form.$setUntouched();
				this.client = angular.copy(this.master);
				this.client = { state: 'PA' };
			}
			document.querySelector('.top').scrollIntoView();
		}
	}, {
		key: "cancel",
		value: function cancel(form) {
			form.$setPristine();
			form.$setUntouched();
			this.client = angular.copy(this.master);
			this.$state.go('jobs');
		}
	}]);

	return CreateJobCtrl;
})();

exports.CreateJobCtrl = CreateJobCtrl;

},{}],7:[function(require,module,exports){
/**
 * Danger Sheet Controller for index.html
 * @constructor
 * @ngInject
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

;

var DangerSheetCtrl = (function () {
	function DangerSheetCtrl(FireBaseRef, $firebaseObject, $state, /*currentAuth,*/$scope, $firebaseArray, $stateParams, LoadingPopup, LoadReportsSrv, SignPad, LocalDatabase, FireBaseAuth, $rootScope, Orientation, $window, deviceDetector, JobStatus, Print) {
		var _this = this;

		_classCallCheck(this, DangerSheetCtrl);

		//ui-router state service
		this.$state = $state;

		// Get a database reference
		this.ref = FireBaseRef.ref;

		//Signature Pad
		this.canvas = document.querySelector('canvas');
		this.SignPad = SignPad;

		this.LoadReportsSrv = LoadReportsSrv;

		this.FireBaseAuth = FireBaseAuth;

		this.$firebaseArray = $firebaseArray;

		this.LoadingPopup = LoadingPopup;

		this.LocalDatabase = LocalDatabase;

		this.$scope = $scope;

		this.JobStatus = JobStatus;

		this.Print = Print;

		//display loading window
		LoadingPopup.show();

		this.loadData();

		Orientation.sourcePage = 'report';

		$window.onorientationchange = function () {
			_this.scroll = Orientation.scrollHeight;
			_this.loadData();
		};

		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		this.$scope.$on('connected', function () {
			console.log('connected');
			_this.LocalDatabase.$syncToFirebase();
		});
	}

	_createClass(DangerSheetCtrl, [{
		key: 'loadData',
		value: function loadData() {
			var _this2 = this;

			//check if the system is online
			if (navigator.onLine) {

				this.LoadReportsSrv.remote(this.$scope).then(function (client) {
					_this2.client = client;

					_this2.LoadReportsSrv.remoteUsers().then(function (data) {
						_this2.users = data.users;
						if (!_this2.client.tech) {
							_this2.client.tech = data.tech.name;
						}

						_this2.printJob = data.tech.printJob;
						//hide loading window
						_this2.LoadingPopup.hide();
					});

					//set $watch on vm.client scope. Any changes to vm.client are saved
					_this2.LocalDatabase.$watch(_this2.client);

					setTimeout(function () {
						//Init Signature
						DangerSheetCtrl.initSignPad.call(_this2);
					}, 1);
				});
			} else {
				this.LoadReportsSrv.local(this.$scope).then(function (client) {
					//set the view model to client data from local database
					_this2.client = client;

					//load current admin user from local database
					var user = _this2.LocalDatabase.$getCurrentAuth();

					//set the tech based on the current user's email
					var users = _this2.LocalDatabase.get('users');
					_this2.users = users;

					//set the tech
					if (!_this2.client.tech) {
						_this2.client.tech = user.name;
						_this2.client.techID = user.$id;
					}

					//set $watch on vm.client scope. Any changes to vm.client are saved
					_this2.LocalDatabase.$watch(_this2.client);

					DangerSheetCtrl.initSignPad.call(_this2);

					//hide loading window
					_this2.LoadingPopup.hide();
				});
			}
		}
	}, {
		key: 'signature',
		value: function signature() {
			this.SignPad.clear();
		}
	}, {
		key: 'close',
		value: function close() {
			this.$state.go('jobs');
			this.Print.resetName();
			this.LocalDatabase.$unwatch();
		}
	}, {
		key: 'complete',
		value: function complete() {
			this.JobStatus.setStatus(this.client, 'Complete');
			this.close();
		}
	}, {
		key: 'print',
		value: function print() {
			this.JobStatus.setStatus(this.client, 'Printed');
			this.Print.titleName = { name: this.client.name, report: 'Danger Report' };
			this.Print.exec();
		}
	}], [{
		key: 'initSignPad',
		value: function initSignPad() {
			//Init Signature
			this.SignPad.init(this.canvas, this.client);
			this.SignPad.resizeCanvas();
			this.SignPad.getSignature();
		}
	}]);

	return DangerSheetCtrl;
})();

exports.DangerSheetCtrl = DangerSheetCtrl;

},{}],8:[function(require,module,exports){
/**
 * Jobs Controller jobs.html
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var JobsCtrl = (function () {
	function JobsCtrl(FireBaseRef, $firebaseObject, $state, currentAuth, $scope, $firebaseArray, Toast, LocalDatabase, $mdDialog, $window, Orientation, deviceDetector, $mdBottomSheet, DateISO, $interval, $filter) {
		var _this = this;

		_classCallCheck(this, JobsCtrl);

		//converts local iso-string to handled timezone
		this.DateISO = DateISO;

		this.$state = $state;
		// Get a database reference
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.$firebaseObject = $firebaseObject;
		this.Toast = Toast;
		this.LocalDatabase = LocalDatabase;
		this.$scope = $scope;
		this.$filter = $filter;

		//inject $mdBottomSheet service
		this.$mdBottomSheet = $mdBottomSheet;

		this.$interval = $interval;

		//holds a list of selected items
		this.selectedList = [];

		// Get signed in user's email address
		//check to see if the system is online
		if (navigator.onLine) {
			if (currentAuth != undefined) {
				this.adminEmail = currentAuth.password.email;
				this.loadJobs(false);
			}
		} else {
			this.adminEmail = this.LocalDatabase.$getCurrentAuth().email;
			this.loadJobs(true);
		}

		//set allowed button items
		this.user = this.LocalDatabase.get('allowed');
		if (this.user) {
			this.editJob = this.user.editJob;
			this.deleteJob = this.user.deleteJob;
			this.printJob = this.user.printJob;
		}

		this.$mdDialog = $mdDialog;

		//set orientation css file -
		Orientation.sourcePage = 'job';

		$window.onorientationchange = function () {
			_this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		};

		//set fix height for iPhone
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		//check to see if the temp crew date has passed
		// JobsCtrl.tempCrew.call(this);

		//check every 1s to see if temp crew date has passed
		/*this.checkTempCrew = this.$interval(() => {
  	JobsCtrl.tempCrew.call(this);
  }, 1000);*/

		this.$scope.$on('connected', function () {
			console.log('connected');
			_this.loadJobs(false);
			_this.LocalDatabase.$syncToFirebase();
		});

		//set $watch on vm.search scope. Any changes to vm.search are saved
		this.unwatch = this.$scope.$watchCollection(function () {
			return _this.search;
		}, function (n, o) {
			if (o !== n) {
				console.log(n);
				_this.onSearchHandler();
			}
		});

		this.LocalDatabase.$trackSiteLocation = { site: 'jobs' };
	}

	_createClass(JobsCtrl, [{
		key: 'loadJobs',

		/**
   * Load jobs data
   * @type {boolean} local True if loading from local database
   */
		value: function loadJobs(local) {
			var _this2 = this;

			if (!local) {
				//online run this code
				var users = this.$firebaseArray(this.ref.child('users'));
				//bind crew object to the $scope.crew to pull the value for crew
				users.$loaded().then(function (users) {

					users.map(function (currentValue, index, array) {
						var jobsList = [];
						var jobs = undefined;

						if (angular.lowercase(currentValue.email) === angular.lowercase(_this2.adminEmail)) {
							(function () {
								var jobOrder = _this2.$firebaseArray(_this2.ref.child('jobOrder'));
								jobs = _this2.$firebaseArray(_this2.ref.child('job'));
								jobs.$loaded().then(function (jobs) {

									if (currentValue.crew === 'All Crews') {
										//display all jobs for techs with crew st to All Crews

										jobOrder.$loaded().then(function (jobOrder) {
											JobsCtrl.sortJobs.call(_this2, { jobs: jobs, jobOrder: jobOrder });
											_this2.jobs = _this2.sortByJobDate;
										});

										// init search
										_this2.search = '';

										//display print
										_this2.printed = true;
									} else {
										//only have access to one crew

										//filter jobs based on crew
										jobsList = jobs.filter(function (cv, i, a) {
											return cv.crew === currentValue.crew;
										});

										if (jobsList.length == 0) {
											return;
										}

										//do not display print
										_this2.printed = false;

										jobOrder.$loaded().then(function (jobOrder) {
											JobsCtrl.sortJobs.call(_this2, { jobs: jobsList, jobOrder: jobOrder });
											// init search
											_this2.todaySearch();
										});
									}
								});
							})();
						}
					});
				});
			} else {
				//offline run this code
				var users = this.LocalDatabase.get('users');
				var user = _.findWhere(users, { email: angular.lowercase(this.adminEmail) });
				var jobs = this.LocalDatabase.get('jobs');
				var jobOrder = this.LocalDatabase.get('jobOrder');

				JobsCtrl.loadJobData.call(this, { currentValue: user, jobs: jobs, jobOrder: jobOrder });
			}
		}
	}, {
		key: 'open',
		value: function open(job) {

			this.$state.go('allReports.report', { id: job.$id });
		}
	}, {
		key: 'edit',
		value: function edit(job, $event) {
			var that = this;
			if (this.selectedList.length > 1) {
				this.$mdDialog.show({
					targetEvent: $event,
					clickOutsideToClose: true,
					templateUrl: "views/changeJobCrew.html",
					controller: function DialogController($scope, $mdDialog) {
						var _this3 = this;

						this.update = function (crew, form) {
							var jobs = that.$firebaseArray(that.ref.child('job'));
							jobs.$loaded().then(function (jobs) {
								that.selectedList.forEach(function (val, idx, arr) {
									var job = _.findWhere(jobs, { $id: val.$id });
									var jobIndex = _.findIndex(jobs, { $id: val.$id });
									job.crew = crew;
									jobs.$save(jobIndex);
								});
								form.$setPristine();
								form.$setUntouched();
								that.selectedList = [];
								$mdDialog.hide();
							});
						};
						var crews = that.$firebaseArray(that.ref.child('crews'));
						crews.$loaded().then(function (items) {
							_this3.crews = _.reject(items, function (value) {
								return value.name === "All Crews";
							});
						});

						this.closeDialog = function () {
							that.selectedList = [];
							$mdDialog.hide();
						};
					},
					controllerAs: 'mdDialog'
				});
			} else {
				this.$state.go('createJob', { id: job.$id });
			}
		}
	}, {
		key: 'delete',
		value: function _delete(job) {
			var _this4 = this;

			var clients = this.$firebaseArray(this.ref.child('clients'));
			var jobsToRemove = this.$firebaseArray(this.ref.child('job'));

			var confirm = this.$mdDialog.confirm().title('Warn').content('This data will be deleted permanently.').ok('YES DELETE').cancel('Cancel');

			this.$mdDialog.show(confirm).then(removeJob.bind(this), function () {
				_this4.job = undefined;
				_this4.selectedList = [];
			});

			function removeJob() {
				var _this5 = this;

				this.selectedList = [];

				clients.$loaded().then(function (item) {
					item.map(function (cv, i, a) {

						if (cv.foreignKey === job.$id) {
							clients.$remove(i).then(function () {
								// this.loadJobs();
								jobsToRemove.$loaded().then(function (_item) {
									_item.map(function (_cv, _i) {
										if (_cv.$id === job.$id) {
											jobsToRemove.$remove(_i).then(function () {
												var jobOrders = _this5.$firebaseArray(_this5.ref.child('jobOrder'));
												jobOrders.$loaded().then(function (_jobOrders) {
													_jobOrders.map(function (jobOrder, jobOrderIndex) {
														if (jobOrder.foreignKey === job.$id) {
															jobOrders.$remove(jobOrderIndex);
														}
													});
												});

												_this5.job = null;
												_this5.loadJobs(false, true);
												_this5.Toast.showToast({ msg: 'Job removed' });
											});
										}
									});
								}); //jobsToRemove
							}); //clients
						}
					}); //item
				});
				this.selectedList = [];
			} //removeJob
		}
		//delete

	}, {
		key: 'setSelectedJob',
		value: function setSelectedJob($event, job) {
			var _this6 = this;

			var isMatch = false;

			//set vm.job for the last selected item
			//not used when multiple items are selected
			this.job = job;
			//check to see if the job is already selected.
			//if selected deselect job
			this.selectedList.forEach(function (val, idx, arr) {
				if (val.$id === job.$id) {
					_this6.selectedList.splice(idx, 1);
					isMatch = true;
				}
			});

			if (!isMatch) {
				// push selected job ist onto the selectedList array
				this.selectedList.push(job);
			}
		}
	}, {
		key: 'disableOptions',
		value: function disableOptions() {
			if (this.selectedList.length == 0 || this.selectedList.length > 1) {
				return true;
			} else {
				return false;
			}
		}
	}, {
		key: 'setSelectedClass',
		value: function setSelectedClass(job) {
			var match = _.findWhere(this.selectedList, { $id: job.$id });

			return match ? true : false;
		}
	}, {
		key: 'todaySearch',
		value: function todaySearch() {
			// this.search = '{jobDate:' + this.today.substring(0, 10) + ',status:!printed}'
			this.search = this.today.substring(0, 10) + ',!printed';
		}
	}, {
		key: 'tomorrowSearch',
		value: function tomorrowSearch() {
			// this.search = '{jobDate:' + this.tomorrow.substring(0, 10) + ',status:!printed}'
			this.search = this.tomorrow.substring(0, 10) + ',!printed';
		}
	}, {
		key: 'onSortHandler',
		value: function onSortHandler($item, $partFrom, $partTo) {
			var _this7 = this;

			//pull the sort order for each job
			var jobOrders = this.$firebaseArray(this.ref.child('jobOrder'));

			jobOrders.$loaded().then(function (jobOrder) {

				//initial load of jobOrder table
				if (jobOrder.length === 0) {

					var jobs = _this7.$firebaseArray(_this7.ref.child('job'));
					jobs.$loaded().then(function (_jobs) {
						_jobs.forEach(function (_job, index) {
							jobOrder.$add({ foreignKey: _job.$id, order: index });
						});
					});

					//update  jobOrder for first sort attempt
					jobOrder.forEach(function (v, i) {
						var index = _.findIndex($partTo, { $id: v.foreignKey });
						//save index for FROM
						jobOrder[i].order = index;
						jobOrder.$save(i);
					});
				} else {
					jobOrder.forEach(function (v, i) {
						var index = _.findIndex($partTo, { $id: v.foreignKey });
						if (index !== -1) {
							//save index for FROM
							jobOrder[i].order = index;
							jobOrder.$save(i);
						}
					});
				}
			});
		}
	}, {
		key: 'onSearchHandler',
		value: function onSearchHandler() {
			var _this8 = this;

			var searchValue = this.search;
			var crews = this.LocalDatabase.get('crews');
			var statuses = [{ name: 'open' }, { name: 'printed' }, { name: 'complete' }];
			var operators = ['!'];
			if (searchValue) {

				if (searchValue.search(',') > 0) {
					(function () {
						//variable for storing the new object
						var searchValueObject = {};
						var searchParams = searchValue.split(',');
						searchParams.map(function (v, i, a) {

							var isDate = !(new Date(v.replace(' ', '')).toDateString() == 'Invalid Date');
							var isCrew = _.some(crews, function (crew) {
								return crew.name.toUpperCase() === v.toUpperCase();
							});
							var isStatus = _.some(statuses, function (status) {
								var _status;
								operators.map(function (ops, i, a) {
									_status = v.replace(ops, '');
								});
								return _status.toUpperCase() === status.name.toUpperCase();
							});

							if (isDate) {
								searchValueObject.jobDate = v;
							} else if (isCrew) {
								searchValueObject.crew = v;
							} else if (isStatus) {
								searchValueObject.status = v;
							} else {
								searchValueObject.name = v;
							}

							try {
								_this8.jobs = _this8.$filter('filter')(_this8.backUpJobs, searchValueObject);
							} catch (error) {
								console.log(error);
							}
						});
					})();
				} else {

					this.jobs = this.$filter('filter')(this.sortByJobDate, searchValue);
				}
			} else {
				//no search item

				this.jobs = this.sortByJobDate;
			}
		}
	}, {
		key: 'saveSearch',
		value: function saveSearch(searchValue) {
			var searches = this.$firebaseArray(this.ref.child('searches'));
			searches.$loaded().then(function (searches) {
				searches.$add(searchValue);
			});
		}
	}, {
		key: 'tomorrow',
		get: function get() {
			return this.DateISO.tomorrow;
		}
	}, {
		key: 'today',
		get: function get() {
			return this.DateISO.today;
		}
	}, {
		key: 'todaySetClass',
		get: function get() {
			// return '{jobDate:' + this.today.substring(0, 10) + ',status:!printed}'
			return this.today.substring(0, 10) + ',!printed';
		}
	}, {
		key: 'tomorrowSetClass',
		get: function get() {
			// return '{jobDate:' + this.tomorrow.substring(0, 10) + ',status:!printed}'
			return this.tomorrow.substring(0, 10) + ',!printed';
		}
	}], [{
		key: 'tempCrew',
		value: function tempCrew() {
			var _this9 = this;

			//update crew if the temp date has been passed
			if (this.user || this.user.tempDate) {
				var remoteUser = this.$firebaseObject(this.ref.child('users').child(this.user.$id));
				remoteUser.$loaded().then(function (_remoteUser) {
					var curDate = _this9.DateISO.today;
					var isAfter = moment(curDate).isAfter(_remoteUser.tempDate, 'day');
					var isSame = moment(_remoteUser.tempDate).isSame(curDate, 'day');
					if (_remoteUser.tempDate) {
						if (isAfter || isSame) {
							_remoteUser.crew = _this9.user.tempCrew;
							_remoteUser.tempDate = null;
							_remoteUser.tempCrew = null;
							_remoteUser.$save().then(function () {
								var newUser = {};
								_.extendOwn(newUser, _remoteUser);
								delete newUser.$$conf;
								_this9.LocalDatabase.put('allowed', newUser);
								_this9.loadJobs(false);
								_this9.$interval.cancel(_this9.checkTempCrew);
							});
						}
					}
				});
			} else {
				this.$interval.cancel(this.checkTempCrew);
			}
		}
	}, {
		key: 'sortJobs',
		value: function sortJobs(param) {
			//sort jobs by job Date first
			this.sortByJobDate = param.jobs.sort(function (a, b) {
				var d1 = new Date(a.jobDate);
				var d2 = new Date(b.jobDate);
				return d2.getTime() - d1.getTime();
			});

			// sort jobs by job order second
			var sort = _.sortBy(this.sortByJobDate, function (obj) {
				var j = _.findWhere(param.jobOrder, { foreignKey: obj.$id });
				if (j) {
					return j.order;
				}
			});

			this.backUpJobs = sort;

			return sort;
		}
	}, {
		key: 'loadJobData',
		value: function loadJobData(param) {
			var jobsList = [];

			if (param.currentValue.crew === 'All Crews') {

				//display all jobs for techs with crew set to All Crews

				this.jobs = JobsCtrl.sortJobs({ jobs: param.jobs, jobOrder: param.jobOrder });;

				// init search
				this.search = '';

				//display print button
				this.printed = true;
			} else {
				//filter jobs based on crew
				jobsList = param.jobs.filter(function (cv, i, a) {
					return cv.crew === param.currentValue.crew;
				});

				if (jobsList.length == 0) {
					return;
				}

				//do not display print button
				this.printed = false;

				this.jobs = JobsCtrl.sortJobs.call(this, { jobs: jobsList, jobOrder: param.jobOrder });

				// set search to today's date
				// this.search = this.today.substring(0,10)
				this.todaySearch();
			}
		}
	}]);

	return JobsCtrl;
})();

exports.JobsCtrl = JobsCtrl;

},{}],9:[function(require,module,exports){
/**
 * Login Controller for login.html
 * @param {!firebase.$firebaseAuth} $firebaseAuth 
 * @param {!angular.$mdDialog} $mdDialog
 * @param {!ui.router.$stae} $state
 * @param {} FireBaseRef
 * @param {} FireBaseAuth
 * @param {} LoadingPopup
 * @param {} LocalDatabase
 * @param {!angular.$rootScope} $rootScope
 * @param {} $firebaseArray
 * @param {!angular.Scope} $scope
 * @constructor
 * @ngInject
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LoginCtrl = (function () {
	function LoginCtrl($firebaseAuth, $mdDialog, $state, FireBaseRef, FireBaseAuth, LoadingPopup, LocalDatabase, $rootScope, $firebaseArray, $scope, DateISO, $firebaseObject) {
		_classCallCheck(this, LoginCtrl);

		this.$mdDialog = $mdDialog;
		this.$state = $state;
		this.master = { email: '', password: '' };
		this.auth = FireBaseAuth;
		this.LoadingPopup = LoadingPopup;
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.$scope = $scope;
		this.DateISO = DateISO;
		this.$firebaseObject = $firebaseObject;
		this.user = {};

		/**
   * @type {object} 
   */
		var user = this.LocalDatabase.get('allowed');
		if (user) {
			this.createJob = user.createJob;
			this.adminstration = user.adminstration;
			this.user.email = user.email;
			this.user.password = user.password;
		}

		var track = this.LocalDatabase.$trackSiteLocation;
		if (track) {
			if (track.site && !track.$id) {
				this.$state.go(track.site);
			} else {
				this.$state.go(track.site, { id: track.$id });
			}
		}
	}

	_createClass(LoginCtrl, [{
		key: 'showAlert',
		value: function showAlert(error, form) {
			/**
    * @type {!angular.$mdDialog}
    */
			var alert = this.$mdDialog.alert().title('Authentication failed:').content(error.code).ok('Close');

			this.$mdDialog.show(alert);
		}
	}, {
		key: 'cancel',
		value: function cancel(form) {
			this.user = angular.copy(this.master);
			form.$setPristine();
			form.$setUntouched();
		}
	}, {
		key: 'login',
		value: function login($event, form) {

			var that = this;

			if ($event.keyCode != 13 && $event.keyCode != 0) {
				return;
			}

			this.LoadingPopup.show();

			this.LocalDatabase.initalLoad();

			if (navigator.onLine) {
				this.auth.$authWithPassword(this.user).then(onSuccess.bind(this), onReject.bind(this));

				//set the current auth locally
				this.LocalDatabase.$authWithPassword(this.user);
			} else {
				this.LocalDatabase.$authWithPassword(this.user).then(onSuccess.bind(this), onReject.bind(this));
			}

			function onSuccess(authData) {
				var _this = this;

				var users, user, email;

				if (navigator.onLine) {
					users = that.$firebaseArray(that.ref.child('users'));

					users.$loaded().then(function (users) {
						var user = _.findWhere(users, { email: authData.password.email });
						if (user.tempDate) {

							var remoteUser = _this.$firebaseObject(_this.ref.child('users').child(user.$id));
							remoteUser.$loaded().then(function (_remoteUser) {
								var curDate = _this.DateISO.today;
								var isAfter = moment(curDate).isAfter(_remoteUser.tempDate, 'day');
								var isSame = moment(_remoteUser.tempDate).isSame(curDate, 'day');

								if (isAfter || isSame) {
									_remoteUser.crew = user.tempCrew;
									_remoteUser.tempDate = null;
									_remoteUser.tempCrew = null;
									_remoteUser.$save().then(function () {
										var newUser = {};
										_.extendOwn(newUser, _remoteUser);
										delete newUser.$$conf;
										_this.LocalDatabase.put('allowed', newUser);
									});
								} else {
									_this.LocalDatabase.put('allowed', user);
								}
							});
						} else {

							_this.LocalDatabase.put('allowed', user);
						}

						that.$rootScope.$broadcast('successLogin', { user: user });

						console.log(user);
					});
				} else {
					users = that.LocalDatabase.get('users');
					email = authData.email.toLowerCase();
					user = _.findWhere(users, { email: email });
					that.LocalDatabase.put('allowed', user);
					that.$rootScope.$broadcast('successLogin', { user: user });
				}

				console.log("Logged in as:", authData);
				LoginCtrl.clearForm.bind(that, form);
				that.LoadingPopup.hide();
				that.LocalDatabase.$setAuth('true');
				that.$state.go('jobs');
			}

			function onReject(error) {
				that.showAlert(error);
				console.error("Authentication failed:", error);
				LoginCtrl.clearForm.bind(that, form);
				that.LoadingPopup.hide();
				that.LocalDatabase.$setAuth('false');
			}
		}
	}], [{
		key: 'clearForm',
		value: function clearForm(form) {
			this.user = angular.copy(this.master);
			form.$setPristine();
			form.$setUntouched();
		}
	}]);

	return LoginCtrl;
})();

exports.LoginCtrl = LoginCtrl;

},{}],10:[function(require,module,exports){
/**
 * Report Controller reportSheet.html
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var ReportCtrl = (function () {
	function ReportCtrl(FireBaseRef, /*currentAuth*/$scope, $firebaseArray, PointsData, $state, LoadReportsSrv, SignPad, LocalDatabase, FireBaseAuth, LoadingPopup, Orientation, $window, deviceDetector, $firebaseObject, JobStatus, Print, $interval) {
		var _this = this;

		_classCallCheck(this, ReportCtrl);

		this.$scope = $scope;

		// Get a database reference
		this.ref = FireBaseRef.ref;

		//state service
		this.$state = $state;

		//Signature Pad
		this.canvas = document.querySelector('canvas');
		this.SignPad = SignPad;

		this.LoadReportsSrv = LoadReportsSrv;

		this.FireBaseAuth = FireBaseAuth;

		this.$firebaseArray = $firebaseArray;

		this.$firebaseObject = $firebaseObject;

		this.LoadingPopup = LoadingPopup;

		this.LocalDatabase = LocalDatabase;

		this.JobStatus = JobStatus;

		this.Print = Print;

		//display loading window
		LoadingPopup.show();

		this.loadData();

		Orientation.sourcePage = 'report';

		//update the orientation.  Set height scroll = Portrait Mode or  scrollLG = Landscape
		$window.onorientationchange = function () {
			_this.scroll = Orientation.scrollHeight;
			//resize signature area
			// ReportCtrl.initSignPad.call(this);
			_this.loadData();
			// $scope.$apply();
		};

		//set a fix height for device of iphone
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		//Load the Point of Visual Inspection information
		this.headers = PointsData.points;

		//load available options
		this.options = ['S - Satisfactory', 'X - Unsatisfactory', 'N/A - Not Applicable'];

		this.$scope.$on('connected', function () {
			console.log('connected');
			_this.LocalDatabase.$syncToFirebase();
		});

		this.LocalDatabase.$trackSiteLocation = { site: 'allReports.report', $id: this.$state.params.id };
	}

	_createClass(ReportCtrl, [{
		key: 'loadData',
		value: function loadData() {
			var _this2 = this;

			//check if the system is online
			if (navigator.onLine) {
				(function () {
					//access current signed in user data
					var currentAuth = _this2.FireBaseAuth.$getAuth();

					//load the client data
					_this2.LoadReportsSrv.remote(_this2.$scope).then(function (client) {
						_this2.client = client;

						//set the tech based on the current user's email
						var users = _this2.$firebaseArray(_this2.ref.child('users'));
						_this2.users = users;
						users.$loaded().then(function (users) {
							var user = _.findWhere(users, { email: currentAuth.password.email });
							if (!_this2.client.tech) {
								_this2.client.tech = user.name;
								_this2.client.techID = user.$id;
							}
							_this2.printJob = user.printJob;
							_this2.JobStatus.date(_this2.client).then(function (date) {
								_this2.completedDate = date;
							});
							//hide loading window
							_this2.LoadingPopup.hide();
						});

						/**
       *set $watch to add and remove classes to hide columns
       */
						_this2.hideOnPrint();

						//temp fix for signature issue
						setTimeout(function () {
							//Init Signature
							ReportCtrl.initSignPad.call(_this2);
						}, 1);

						//set $watch on vm.client scope. Any changes to vm.client are saved
						_this2.LocalDatabase.$watch(_this2.client);
					});
				})();
			} else {
				/**
     * load data from local database
     */
				this.LoadReportsSrv.local().then(function (client) {
					//set the view model to client data from local database
					_this2.client = client;

					//load current admin user from local database
					var user = _this2.LocalDatabase.$getCurrentAuth();

					//set the tech based on the current user's email
					var users = _this2.LocalDatabase.get('users');
					_this2.users = users;

					if (!_this2.client.tech) {
						_this2.client.tech = user.name;
						_this2.client.techID = user.$id;
					}

					_this2.printJob = user.printJob;
					_this2.JobStatus.date(_this2.client).then(function (date) {
						_this2.completedDate = date;
					});

					//set $watch on vm.client scope. Any changes to vm.client are saved
					_this2.LocalDatabase.$watch(_this2.client);

					//Init Signature
					ReportCtrl.initSignPad.call(_this2);

					//hide loading window
					_this2.LoadingPopup.hide();
				});
			}
		}
	}, {
		key: 'setTechID',
		value: function setTechID(tech) {
			var _this3 = this;

			var techs = this.$firebaseArray(this.ref.child('users'));
			techs.$loaded().then(function (techs) {
				var _tech = _.findWhere(techs, { name: tech });
				_this3.client.techID = _tech.$id;
			});
		}
	}, {
		key: 'signature',
		value: function signature() {
			this.SignPad.clear();
		}
	}, {
		key: 'close',
		value: function close() {
			/** function for close button */
			this.$state.go('jobs');
			this.Print.resetName();
			this.LocalDatabase.$unwatch();
		}
	}, {
		key: 'complete',
		value: function complete() {
			this.JobStatus.setStatus(this.client, 'Complete');
			this.close();
		}
	}, {
		key: 'print',
		value: function print() {
			this.JobStatus.setStatus(this.client, 'Printed');
			this.Print.titleName = { name: this.client.name, report: 'Conditional Report' };
			this.Print.exec();
		}
	}, {
		key: 'hideOnPrint',
		value: function hideOnPrint() {
			var _this4 = this;

			this.$scope.$watchCollection(function () {
				return _this4.client;
			}, function (n, o) {

				//list of condition headers to review
				var listOfConditions = [{ name: 'conditionA', hide: 'hideA' }, { name: 'conditionB', hide: 'hideB' }, { name: 'conditionC', hide: 'hideC' }, { name: 'conditionD', hide: 'hideD' }, { name: 'conditionE', hide: 'hideE' }, { name: 'conditionF', hide: 'hideF' }, { name: 'conditionG', hide: 'hideG' }, { name: 'conditionH', hide: 'hideH' }, { name: 'conditionI', hide: 'hideI' }];
				//checks to see if the  condition header exist.
				//does not print column with no condition header
				listOfConditions.forEach(function (element) {
					if (n[element.name]) {
						n[element.name].length > 0 ? _this4[element.hide] = false : _this4[element.hide] = true;
					} else {
						_this4[element.hide] = true;
					}
				});

				//hides the right border if column B and C are hidden
				_this4.borderA = _this4.hideB && _this4.hideC;
				_this4.borderD = _this4.hideE && _this4.hideF;
				_this4.borderG = _this4.hideH && _this4.hideI;

				//hides the right border if column C are hidden
				_this4.borderB = _this4.hideC;
				_this4.borderE = _this4.hideF;
				_this4.borderH = _this4.hideI;
			});
		}
	}], [{
		key: 'initSignPad',
		value: function initSignPad() {
			//Init Signature

			this.SignPad.init(this.canvas, this.client);
			this.SignPad.resizeCanvas();
			this.SignPad.getSignature();
		}
	}]);

	return ReportCtrl;
})();

exports.ReportCtrl = ReportCtrl;

},{}],11:[function(require,module,exports){
/**
 * Side Nav Controller for index.html
 * @constructor
 * @ngInject
 */
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SideNavCtrl = function SideNavCtrl() {
  _classCallCheck(this, SideNavCtrl);
};

exports.SideNavCtrl = SideNavCtrl;

},{}],12:[function(require,module,exports){
/**
 * Water Sheet Controller for index.html
 * @constructor
 * @ngInject
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var WaterSheetCtrl = (function () {
	function WaterSheetCtrl(FireBaseRef, $firebaseObject, $state, $scope, $firebaseArray, LoadReportsSrv, SignPad, LocalDatabase, LoadingPopup, Orientation, $window, deviceDetector, FireBaseAuth, JobStatus, Print) {
		var _this = this;

		_classCallCheck(this, WaterSheetCtrl);

		//ui-router state service
		this.$state = $state;

		// Get a database reference
		this.ref = FireBaseRef.ref;

		//Signature Pad
		this.canvas = document.querySelector('canvas');
		this.SignPad = SignPad;

		this.LoadReportsSrv = LoadReportsSrv;

		this.FireBaseAuth = FireBaseAuth;

		this.$firebaseArray = $firebaseArray;

		this.LoadingPopup = LoadingPopup;

		this.LocalDatabase = LocalDatabase;

		this.$scope = $scope;

		this.JobStatus = JobStatus;

		this.Print = Print;

		//display loading window
		LoadingPopup.show();

		this.loadData();

		Orientation.sourcePage = 'report';

		$window.onorientationchange = function () {
			_this.scroll = Orientation.scrollHeight;
			_this.loadData();
		};

		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone';
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		this.$scope.$on('connected', function () {
			console.log('connected');
			_this.LocalDatabase.$syncToFirebase();
		});
	}

	_createClass(WaterSheetCtrl, [{
		key: 'loadData',
		value: function loadData() {
			var _this2 = this;

			//check if the system is online
			if (navigator.onLine) {

				this.LoadReportsSrv.remote(this.$scope).then(function (client) {
					_this2.client = client;

					_this2.LoadReportsSrv.remoteUsers().then(function (data) {
						_this2.users = data.users;
						if (!_this2.client.tech) {
							_this2.client.tech = data.tech.name;
						}
						_this2.printJob = data.tech.printJob;
						//hide loading window
						_this2.LoadingPopup.hide();
					});

					//set $watch on vm.client scope. Any changes to vm.client are saved
					_this2.LocalDatabase.$watch(_this2.client);

					setTimeout(function () {
						//Init Signature
						WaterSheetCtrl.initSignPad.call(_this2);
					}, 1);
				});
			} else {
				this.LoadReportsSrv.local(this.$scope).then(function (client) {
					//set the view model to client data from local database
					_this2.client = client;

					//load current admin user from local database
					var user = _this2.LocalDatabase.$getCurrentAuth();

					//set the tech based on the current user's email
					var users = _this2.LocalDatabase.get('users');
					_this2.users = users;

					//set the tech
					if (!_this2.client.tech) {
						_this2.client.tech = user.name;
						_this2.client.techID = user.$id;
					}

					//set $watch on vm.client scope. Any changes to vm.client are saved
					_this2.LocalDatabase.$watch(_this2.client);

					WaterSheetCtrl.initSignPad.call(_this2);

					//hide loading window
					_this2.LoadingPopup.hide();
				});
			}
		}
	}, {
		key: 'signature',
		value: function signature() {
			this.SignPad.clear();
		}
	}, {
		key: 'close',
		value: function close() {
			this.$state.go('jobs');
			this.Print.resetName();
			this.LocalDatabase.$unwatch();
		}
	}, {
		key: 'complete',
		value: function complete() {
			this.JobStatus.setStatus(this.client, 'Complete');
			this.close();
		}
	}, {
		key: 'print',
		value: function print() {
			this.JobStatus.setStatus(this.client, 'Printed');
			this.Print.titleName = { name: this.client.name, report: 'Water Report' };
			this.Print.exec();
		}
	}], [{
		key: 'initSignPad',
		value: function initSignPad() {
			//Init Signature
			this.SignPad.init(this.canvas, this.client);
			this.SignPad.resizeCanvas();
			this.SignPad.getSignature();
		}
	}]);

	return WaterSheetCtrl;
})();

exports.WaterSheetCtrl = WaterSheetCtrl;

},{}],13:[function(require,module,exports){
/**
 * Login Controller for login.html
 * @constructor
 * @ngInject
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var DateISOSrv = (function () {
	function DateISOSrv() {
		_classCallCheck(this, DateISOSrv);

		//converts local iso-string to handled timezone
		this.tzoffset = new Date().getTimezoneOffset() * 60000; //offset in milliseconds
		this.localISOTime = new Date(Date.now() - this.tzoffset).toISOString().slice(0, -1);
	}

	_createClass(DateISOSrv, [{
		key: 'today',
		get: function get() {
			return this.localISOTime;
		}
	}, {
		key: 'tomorrow',
		get: function get() {
			//check to see if today is Friday
			//if friday set tomorrow button to Monday
			var today = new Date().getDay();
			switch (today) {
				case 5:
					return moment(new Date(this.localISOTime)).add(3, 'days').toISOString();
					break;
				case 6:
					return moment(new Date(this.localISOTime)).add(2, 'days').toISOString();
					break;
				case 7:
					return moment(new Date(this.localISOTime)).add(1, 'days').toISOString();
					break;
				default:
					return moment(new Date(this.localISOTime)).add(1, 'days').toISOString();
					break;
			}
		}
	}]);

	return DateISOSrv;
})();

exports.DateISOSrv = DateISOSrv;

},{}],14:[function(require,module,exports){
/**
 * FireBase Auth Service
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FireBaseAuthSrv = function FireBaseAuthSrv(FireBaseRef, $firebaseAuth) {
	_classCallCheck(this, FireBaseAuthSrv);

	this.FireBaseRef = FireBaseRef.ref;
	return $firebaseAuth(this.FireBaseRef);
}

// get ref() {
// 	return this.FireBaseRef;
// }
;

exports.FireBaseAuthSrv = FireBaseAuthSrv;

},{}],15:[function(require,module,exports){
/**
 * Job Status Service 
 * @constructor
 * @ngInject
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var JobStatusSrv = (function () {
	function JobStatusSrv(FireBaseRef, $firebaseObject, $q, LocalDatabase) {
		_classCallCheck(this, JobStatusSrv);

		// Get a database reference
		this.ref = FireBaseRef.ref;
		this.$firebaseObject = $firebaseObject;
		this.$q = $q;
		this.LocalDatabase = LocalDatabase;
	}

	_createClass(JobStatusSrv, [{
		key: 'setStatus',
		value: function setStatus(client, status) {
			if (navigator.onLine) {
				var job = this.$firebaseObject(this.ref.child('job').child(client.foreignKey));
				job.$loaded().then(function (job) {
					if (status === 'Complete') {
						job.completedDate = new Date().toISOString();
					}
					job.status = status;
					job.$save();
				});
			} else {
				this.LocalDatabase.$save(client, { isJob: true, status: status });
			}
		}
	}, {
		key: 'date',
		value: function date(client) {
			var _this = this;

			return this.$q(function (resolve, reject) {
				var job = _this.$firebaseObject(_this.ref.child('job').child(client.foreignKey));
				job.$loaded().then(function (job) {
					resolve(moment(job.completedDate).format('ddd, MMM Do YYYY'));
				});
			});
		}
	}]);

	return JobStatusSrv;
})();

exports.JobStatusSrv = JobStatusSrv;

},{}],16:[function(require,module,exports){
/**
 * Set Address Service
 * @constructor
 * @ngInject
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LoadReportsSrv = (function () {
	function LoadReportsSrv(FireBaseRef, $state, $stateParams, $firebaseArray, $firebaseObject, $q, LocalDatabase, $rootScope, FireBaseAuth) {
		_classCallCheck(this, LoadReportsSrv);

		this.FireBaseRef = FireBaseRef;
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$firebaseArray = $firebaseArray;
		this.$firebaseObject = $firebaseObject;
		this.$q = $q;
		this.LocalDatabase = LocalDatabase;
		this.FireBaseAuth = FireBaseAuth;
		// Get a database reference
		this.ref = FireBaseRef.ref;
	}

	_createClass(LoadReportsSrv, [{
		key: 'remote',
		value: function remote($scope) {
			var _this = this;

			return this.$q(function (resolve, reject) {

				//client name
				var id = _this.$stateParams.id;

				//get the client data
				var clients = _this.$firebaseArray(_this.ref.child('clients'));

				//load the client data
				clients.$loaded().then(function (data) {

					data.map(function (cv, i, a) {

						if (cv.foreignKey === id) {
							//get the client data i.e. name, adderss, city, state
							var client = _this.$firebaseObject(_this.ref.child('clients').child(cv.$id));

							client.$bindTo($scope, 'client').then(function () {
								resolve($scope.client);
							});

							/*						debugger;
       						
       						this.unwatch = client.$watch((data)=> {
       							console.log(data);
       							debugger
       							var client = this.$firebaseObject(this.ref.child('clients').child(data.key));
       							client.$loaded().then((client)=>{
       								this.LocalDatabase.$save(client)
       								console.log("data changed!",client);
       							})
       							
       							
       						});*/
						}
					});
				});
			});
		}
	}, {
		key: 'remoteStopWatch',
		value: function remoteStopWatch() {
			this.unwatch();
		}
	}, {
		key: 'remoteUsers',
		value: function remoteUsers() {
			var _this2 = this;

			return this.$q(function (resolve, reject) {
				var currentAuth = _this2.FireBaseAuth.$getAuth();
				var users = _this2.$firebaseArray(_this2.ref.child('users'));
				_this2.users = users;
				users.$loaded().then(function (users) {
					var user = _.findWhere(users, { email: currentAuth.password.email });
					resolve({ users: users, tech: user });
				});
			});
		}
	}, {
		key: 'local',
		value: function local() {
			var _this3 = this;

			return this.$q(function (resolve, reject) {

				//client name
				var id = _this3.$stateParams.id;

				var clients = _this3.LocalDatabase.get('clients');

				//load the client data
				clients.map(function (cv, i, a) {

					if (cv.foreignKey === id) {
						//get the client data i.e. name, adderss, city, state
						setTimeout(function () {
							resolve(cv);
						}, 0);
					}
				});
			});
		}
	}]);

	return LoadReportsSrv;
})();

exports.LoadReportsSrv = LoadReportsSrv;

},{}],17:[function(require,module,exports){
/**
 * Loading Popup Service
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LoadingPopupSrv = (function () {
	function LoadingPopupSrv($mdDialog, $q, $timeout) {
		_classCallCheck(this, LoadingPopupSrv);

		this.$mdDialog = $mdDialog;
		this.$q = $q;
		this.$timeout = $timeout;
	}

	_createClass(LoadingPopupSrv, [{
		key: 'show',
		value: function show() {

			this.$mdDialog.show({
				clickOutsideToClose: false,
				template: '<md-dialog aria-label="Loading dialog"> ' + '<md-dialog-content layout="row" layout-align="center center">' + '<div style="font-size:30px">Loading</div>' + '<md-progress-circular md-mode="indeterminate" md-diameter="25"></md-progress-circular>' + '</md-dialog-content></md-dialog>'
			});
		}
	}, {
		key: 'hide',
		value: function hide() {
			var _this = this;

			this.$timeout(function () {
				_this.$mdDialog.hide();
			}, 0);
		}
	}]);

	return LoadingPopupSrv;
})();

exports.LoadingPopupSrv = LoadingPopupSrv;

},{}],18:[function(require,module,exports){
/**
 * Local Database Service
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LocalDatabaseSrv = (function () {
	function LocalDatabaseSrv(CacheFactory, FireBaseRef, $firebaseArray, $firebaseObject, $q, $rootScope, $interval, DateISO) {
		_classCallCheck(this, LocalDatabaseSrv);

		//get firebase reference
		this.ref = FireBaseRef.ref;

		//set $firebaseArray
		this.$firebaseArray = $firebaseArray;

		//set $firebaseArray
		this.$firebaseObject = $firebaseObject;
		//set $q libary
		this.$q = $q;

		this.$rootScope = $rootScope;

		this.DateISO = DateISO;

		this.unwatch;

		// Check to make sure the cache doesn't already exist
		if (!CacheFactory.get('cr-cache')) {
			this.conditionReportCache = CacheFactory('cr-cache');
		} else {
			this.conditionReportCache = CacheFactory.createCache('cr-cache', {
				storagePrefix: 'ng-cache'
			});
		}
	}

	_createClass(LocalDatabaseSrv, [{
		key: 'initalLoad',
		// storageMode: 'sessionStorage'
		value: function initalLoad() {
			var _this = this;

			var users = this.$firebaseArray(this.ref.child('users'));
			users.$loaded().then(function (users) {
				_this.conditionReportCache.put('users', users);
			});

			var jobs = this.$firebaseArray(this.ref.child('job'));
			jobs.$loaded().then(function (jobs) {
				_this.conditionReportCache.put('jobs', jobs);
			});

			var jobOrder = this.$firebaseArray(this.ref.child('jobOrder'));
			jobOrder.$loaded().then(function (jobOrder) {
				_this.conditionReportCache.put('jobOrder', jobOrder);
			});

			var crews = this.$firebaseArray(this.ref.child('crews'));
			crews.$loaded().then(function (crews) {
				_this.conditionReportCache.put('crews', crews);
			});

			var clients = this.$firebaseArray(this.ref.child('clients'));
			clients.$loaded().then(function (clients) {
				/**
    * temp array to load into localstorage
    * @type {array.<objects>}
    */
				var _clients = [];
				/**
    * tracks when to start pushing items 
    * into the _clients array
    * @type {boolean} startArrayPush
    */
				var startArrayPush = false;
				/**
    * jobs array
    * @type {array.<objects>} jobs
    */
				var jobs = _this.get('jobs');
				/**
    * job that contains a job date after 3 days ago
    * @type {object} jobs
    */
				var job = _.find(jobs, function (job) {
					//pull jobs greater than today
					var threeDaysAgo = moment(new Date(_this.DateISO.today)).subtract(3, 'days');
					var _jobDate = moment(job.jobDate);
					return moment(threeDaysAgo).isBefore(_jobDate, 'day');
				});

				//loop through client
				for (var index = 0; index < clients.length; index++) {
					//pull the item from the clients array
					var element = clients[index];
					//push the item into the temp array
					if (startArrayPush) {
						_clients.push(element);
					}

					//set startArrayPush to true if job.$id matches
					//the element job id under foreignKey
					if (element.foreignKey === job.$id) {
						startArrayPush = true;
					}
				}

				_this.conditionReportCache.put('clients', _clients);
			});
		}
	}, {
		key: 'put',
		value: function put(id, data) {
			this.conditionReportCache.put(id, data);
		}
	}, {
		key: 'remove',
		value: function remove(id) {
			this.conditionReportCache.remove(id);
		}
	}, {
		key: 'get',
		value: function get(id) {
			return this.conditionReportCache.get(id);
		}
	}, {
		key: '$authWithPassword',
		value: function $authWithPassword(user) {
			var _this2 = this;

			var that = this;

			return this.$q(function (res, rej) {
				var users = _this2.get('users');
				var result = _.findWhere(users, { email: user.email.toLowerCase(), password: user.password });
				_this2.$setCurrentAuth(result);
				if (result) {
					res(result);
				} else {
					rej({ code: "Error with credentials" });
				}
			});
		}
	}, {
		key: '$setCurrentAuth',
		value: function $setCurrentAuth(user) {
			console.log(user);
			this.put('currentAuth', user);
		}
	}, {
		key: '$getCurrentAuth',
		value: function $getCurrentAuth() {
			return this.get('currentAuth');
		}
	}, {
		key: '$getAuth',
		value: function $getAuth() {
			var _this3 = this;

			return this.$q(function (res, rej) {
				var session = _this3.get('session');

				if (session != 'false') {
					res(true);
				} else {
					console.log(session);
					rej(false);
				}
			});
		}
	}, {
		key: '$setAuth',
		value: function $setAuth(data) {
			this.put('session', data);
		}
	}, {
		key: '$unauth',
		value: function $unauth() {
			this.$setAuth('false');
			this.remove('allowed');
			this.remove('currentAuth');
			this.remove('track');
		}
	}, {
		key: '$save',
		value: function $save(client, param) {
			//if param.isJob = true
			//updates the jobs cache
			if (param && param.isJob) {
				/**
     * List of jobs from cache
     * @type{array.<objects>} jobs 
     */
				var jobs = this.get('jobs');
				/**
     * the index of job in the jobs array
     * @type{number} index
     */
				var _index = _.findIndex(jobs, { $id: client.foreignKey });
				/**
     * the job object in the jobs array
     * @type{object} job
     */
				var job = _.findWhere(jobs, { $id: client.foreignKey });
				if (param.status === 'Complete') {
					job.completedDate = new Date().toISOString();
				}
				//set job status to the new status
				job.status = param.status;
				debugger;
				//update the job in the jobs array
				jobs.splice(_index, 1, job);
				//update jobs
				this.put('jobs', jobs);
				console.log(this.get('jobs'));
				return;
			}

			var clients = this.get('clients');
			var index = _.findIndex(clients, { $id: client.$id });
			// if no match then add the new record to the end of array
			if (index === -1) {
				clients.push(client);
			} else {
				clients.splice(index, 1, client);
			}

			this.put('clients', clients);
		}
	}, {
		key: '$watch',
		value: function $watch(client) {
			var _this4 = this;

			this.unwatch = this.$rootScope.$watchCollection(function () {
				return client;
			}, function (n, o) {
				if (o !== n) {
					console.log(n);
					n.$dirty = true;
					_this4.$save(n);
				}
			});
		}
	}, {
		key: '$unwatch',
		value: function $unwatch() {
			this.unwatch();
		}
	}, {
		key: '$syncToFirebase',
		value: function $syncToFirebase() {
			var _this5 = this;

			var clients = this.get('clients');
			var jobs = this.get('jobs');
			var clientsToUpdate = _.filter(clients, { $dirty: true });
			var jobsToUpdate = _.filter(jobs, { $dirty: true });

			clientsToUpdate.forEach(function (v, idx, arr) {
				var client = _this5.$firebaseObject(_this5.ref.child('clients').child(v.$id));
				delete v.$dirty;
				for (var key in v) {
					if (v.hasOwnProperty(key)) {
						client[key] = v[key];
					}
				}
				client.$save().then(function () {
					_this5.initalLoad();
				});
			});

			jobsToUpdate.forEach(function (v, idx, arr) {
				var job = _this5.$firebaseObject(_this5.ref.child('jobs').child(v.foreignKey));
				delete v.$dirty;
				for (var key in v) {
					if (v.hasOwnProperty(key)) {
						job[key] = v[key];
					}
				}
				job.$save().then(function () {
					_this5.initalLoad();
				});
			});

			this.initalLoad();
		}
	}, {
		key: '$trackSiteLocation',
		set: function set(param) {
			/**
    * location and id
    * @type {object.<site,id>} || {string}
    */
			this.put('track', param);
		},
		get: function get() {
			return this.get('track');
		}
	}]);

	return LocalDatabaseSrv;
})();

exports.LocalDatabaseSrv = LocalDatabaseSrv;

},{}],19:[function(require,module,exports){
/**
 * Orientation Service
 * @constructor
 * @ngInject
 */
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var OrientationSrv = (function () {
	function OrientationSrv($window, $rootScope, $q) {
		_classCallCheck(this, OrientationSrv);

		this.$window = $window;
		this.$rootScope = $rootScope;
	}

	_createClass(OrientationSrv, [{
		key: 'sourcePage',
		set: function set(source) {
			this.source = source;
		}
	}, {
		key: 'scrollHeight',
		get: function get() {
			var scroll = undefined;
			var landscape = undefined;
			var portrait = undefined;
			switch (this.source) {
				case 'report':
					landscape = 'report-landscape';
					portrait = 'report-portrait';
					break;
				case 'job':
					landscape = 'job-landscape';
					portrait = 'job-portrait';
					break;
				case 'createJob':
					landscape = 'create-job-landscape';
					portrait = 'create-job-portrait';
					break;
				case 'admin':
					landscape = 'admin-landscape';
					portrait = 'admin-portrait';
					break;

				default:
					landscape = 'landscape';
					portrait = 'portrait';
					break;
			}

			if (this.$window.orientation === 0 || this.$window.orientation == 180) {
				scroll = portrait;
			} else {
				scroll = landscape;
			}

			return scroll;
		}
	}]);

	return OrientationSrv;
})();

exports.OrientationSrv = OrientationSrv;

},{}],20:[function(require,module,exports){
/**
 * Points Data Service
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var PointsDataSrv = (function () {
	function PointsDataSrv() {
		_classCallCheck(this, PointsDataSrv);
	}

	_createClass(PointsDataSrv, [{
		key: 'points',
		get: function get() {
			return PointsDataSrv.points();
		}
	}, {
		key: 'states',
		get: function get() {
			return [{ name: 'ALABAMA', abbreviation: 'AL' }, { name: 'ALASKA', abbreviation: 'AK' }, { name: 'AMERICAN SAMOA', abbreviation: 'AS' }, { name: 'ARIZONA', abbreviation: 'AZ' }, { name: 'ARKANSAS', abbreviation: 'AR' }, { name: 'CALIFORNIA', abbreviation: 'CA' }, { name: 'COLORADO', abbreviation: 'CO' }, { name: 'CONNECTICUT', abbreviation: 'CT' }, { name: 'DELAWARE', abbreviation: 'DE' }, { name: 'DISTRICT OF COLUMBIA', abbreviation: 'DC' }, { name: 'FEDERATED STATES OF MICRONESIA', abbreviation: 'FM' }, { name: 'FLORIDA', abbreviation: 'FL' }, { name: 'GEORGIA', abbreviation: 'GA' }, { name: 'GUAM', abbreviation: 'GU' }, { name: 'HAWAII', abbreviation: 'HI' }, { name: 'IDAHO', abbreviation: 'ID' }, { name: 'ILLINOIS', abbreviation: 'IL' }, { name: 'INDIANA', abbreviation: 'IN' }, { name: 'IOWA', abbreviation: 'IA' }, { name: 'KANSAS', abbreviation: 'KS' }, { name: 'KENTUCKY', abbreviation: 'KY' }, { name: 'LOUISIANA', abbreviation: 'LA' }, { name: 'MAINE', abbreviation: 'ME' }, { name: 'MARSHALL ISLANDS', abbreviation: 'MH' }, { name: 'MARYLAND', abbreviation: 'MD' }, { name: 'MASSACHUSETTS', abbreviation: 'MA' }, { name: 'MICHIGAN', abbreviation: 'MI' }, { name: 'MINNESOTA', abbreviation: 'MN' }, { name: 'MISSISSIPPI', abbreviation: 'MS' }, { name: 'MISSOURI', abbreviation: 'MO' }, { name: 'MONTANA', abbreviation: 'MT' }, { name: 'NEBRASKA', abbreviation: 'NE' }, { name: 'NEVADA', abbreviation: 'NV' }, { name: 'NEW HAMPSHIRE', abbreviation: 'NH' }, { name: 'NEW JERSEY', abbreviation: 'NJ' }, { name: 'NEW MEXICO', abbreviation: 'NM' }, { name: 'NEW YORK', abbreviation: 'NY' }, { name: 'NORTH CAROLINA', abbreviation: 'NC' }, { name: 'NORTH DAKOTA', abbreviation: 'ND' }, { name: 'NORTHERN MARIANA ISLANDS', abbreviation: 'MP' }, { name: 'OHIO', abbreviation: 'OH' }, { name: 'OKLAHOMA', abbreviation: 'OK' }, { name: 'OREGON', abbreviation: 'OR' }, { name: 'PALAU', abbreviation: 'PW' }, { name: 'PENNSYLVANIA', abbreviation: 'PA' }, { name: 'PUERTO RICO', abbreviation: 'PR' }, { name: 'RHODE ISLAND', abbreviation: 'RI' }, { name: 'SOUTH CAROLINA', abbreviation: 'SC' }, { name: 'SOUTH DAKOTA', abbreviation: 'SD' }, { name: 'TENNESSEE', abbreviation: 'TN' }, { name: 'TEXAS', abbreviation: 'TX' }, { name: 'UTAH', abbreviation: 'UT' }, { name: 'VERMONT', abbreviation: 'VT' }, { name: 'VIRGIN ISLANDS', abbreviation: 'VI' }, { name: 'VIRGINIA', abbreviation: 'VA' }, { name: 'WASHINGTON', abbreviation: 'WA' }, { name: 'WEST VIRGINIA', abbreviation: 'WV' }, { name: 'WISCONSIN', abbreviation: 'WI' }, { name: 'WYOMING', abbreviation: 'WY' }];
		}
	}], [{
		key: 'points',
		value: function points() {
			return [{
				name: 'Animal Prevention (Cap)',
				model: 'AnimalPrevention'
			}, {
				name: 'Spark Arrestor (Cap)',
				model: 'Spark'
			}, {
				name: 'Crown/Splash',
				model: 'Crown'
			}, {
				name: 'Exterior Structure',
				model: 'Exterior'
			}, {
				name: 'Height Of Chimney',
				model: 'Height'
			}, {
				name: 'Flashing/Cricket',
				model: 'Flashing'
			}, {
				name: 'Lining/Flue',
				model: 'Lining'
			}, {
				name: 'Clearance To Combustibles',
				model: 'Clearance'
			}, {
				name: 'Firebox Area',
				model: 'Firebox'
			}, {
				name: 'StackPipe Or StovePipe',
				model: 'StackPipe'
			}, {
				name: 'Damper Operation',
				model: 'Damper'
			}, {
				name: 'Draft Test',
				model: 'Draft'
			}];
		}
	}]);

	return PointsDataSrv;
})();

exports.PointsDataSrv = PointsDataSrv;

},{}],21:[function(require,module,exports){
/**
 * Print Service
 * @constructor
 * @ngInject
 */
"use strict";
Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var PrintSrv = (function () {
	function PrintSrv() {
		_classCallCheck(this, PrintSrv);
	}

	/**
  * set title name used for saving the document
  * @param {object} file
  */

	_createClass(PrintSrv, [{
		key: 'resetName',
		value: function resetName() {
			document.querySelector('title').innerHTML = 'Wells Reports';
		}
	}, {
		key: 'exec',
		value: function exec() {
			window.print();
		}
	}, {
		key: 'titleName',
		set: function set(file) {
			document.querySelector('title').innerHTML = file.name + ' ' + file.report;
		}
	}]);

	return PrintSrv;
})();

exports.PrintSrv = PrintSrv;

},{}],22:[function(require,module,exports){
/* global SignaturePad */
/**
 * Signature Pad Service
 * @constructor
 * @ngInject
 */

"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SignPadSrv = (function () {
	function SignPadSrv($rootScope) {
		_classCallCheck(this, SignPadSrv);

		this.$rootScope = $rootScope;
	}

	_createClass(SignPadSrv, [{
		key: "init",
		value: function init(canvas, client) {
			var _this = this;

			this.canvas = canvas;
			this.client = client;
			this.signaturePad = new SignaturePad(canvas, {
				onEnd: function onEnd() {
					_this.save(client);
					_this.$rootScope.$apply();
				}
			});
		}
	}, {
		key: "resizeCanvas",
		value: function resizeCanvas() {
			// When zoomed out to less than 100%, for some very strange reason,
			// some browsers report devicePixelRatio as less than 1
			// and only part of the canvas is cleared then.
			var ratio = Math.max(window.devicePixelRatio || 1, 1);
			this.canvas.width = this.canvas.offsetWidth * ratio;
			this.canvas.height = this.canvas.offsetHeight * ratio;
			this.canvas.getContext("2d").scale(ratio, ratio);
		}
	}, {
		key: "save",
		value: function save(client) {
			var data = this.signaturePad.toDataURL();
			client.signature = data;
			console.log(data);
		}
	}, {
		key: "clear",
		value: function clear() {
			this.signaturePad.clear();
			this.client.signature = '';
		}
	}, {
		key: "getSignature",
		value: function getSignature() {
			if (this.client.signature) {
				this.signaturePad.fromDataURL(this.client.signature);
			}
		}
	}, {
		key: "clearResizeCanvas",
		value: function clearResizeCanvas() {
			this.signaturePad.off();
			this.canvas.width = null;
			this.canvas.height = null;
		}
	}]);

	return SignPadSrv;
})();

exports.SignPadSrv = SignPadSrv;

},{}],23:[function(require,module,exports){
/**
 * Toast Notification Service
 * @constructor
 * @ngInject
 */

'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var ToastSrv = (function () {
	function ToastSrv($mdToast) {
		_classCallCheck(this, ToastSrv);

		this.$mdToast = $mdToast;
	}

	_createClass(ToastSrv, [{
		key: 'showToast',
		value: function showToast(toast) {
			var msg = toast.msg || 'User: ' + toast.name + ' has been enrolled';
			this.$mdToast.show(this.$mdToast.simple().content(msg));
		}
	}]);

	return ToastSrv;
})();

exports.ToastSrv = ToastSrv;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Vzci9sb2NhbC9saWIvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsIi9Vc2Vycy9mYWxvc2FrZXJzL0RvY3VtZW50cy9XZWJTaXRlIFByb2plY3RzL1dvcmsgSW4gUHJvZ3Jlc3MvV2VsbHMgUmVwb3J0LVJlc3VtZS9hcHAvc2NyaXB0cy9zcmMvYXBwLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb25maWcvcm91dGUtY29uZmlnLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9BZG1pbkN0cmwuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL2NvbnRyb2xsZXJzL0FsbFJlcG9ydHNDdHJsLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9BcHBDdHJsLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9DcmVhdGVKb2JDdHJsLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9EYW5nZXJTaGVldEN0cmwuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL2NvbnRyb2xsZXJzL0pvYnNDdHJsLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9Mb2dpbkN0cmwuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL2NvbnRyb2xsZXJzL1JlcG9ydEN0cmwuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL2NvbnRyb2xsZXJzL1NpZGVOYXZDdHJsLmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9jb250cm9sbGVycy9XYXRlclNoZWV0Q3RybC5qcyIsIi9Vc2Vycy9mYWxvc2FrZXJzL0RvY3VtZW50cy9XZWJTaXRlIFByb2plY3RzL1dvcmsgSW4gUHJvZ3Jlc3MvV2VsbHMgUmVwb3J0LVJlc3VtZS9hcHAvc2NyaXB0cy9zcmMvc2VydmljZXMvRGF0ZUlTT1Nydi5qcyIsIi9Vc2Vycy9mYWxvc2FrZXJzL0RvY3VtZW50cy9XZWJTaXRlIFByb2plY3RzL1dvcmsgSW4gUHJvZ3Jlc3MvV2VsbHMgUmVwb3J0LVJlc3VtZS9hcHAvc2NyaXB0cy9zcmMvc2VydmljZXMvRmlyZUJhc2VBdXRoU3J2LmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9zZXJ2aWNlcy9Kb2JTdGF0dXNTcnYuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL3NlcnZpY2VzL0xvYWRSZXBvcnRzU3J2LmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9zZXJ2aWNlcy9Mb2FkaW5nUG9wdXBTcnYuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL3NlcnZpY2VzL0xvY2FsRGF0YWJhc2VTcnYuanMiLCIvVXNlcnMvZmFsb3Nha2Vycy9Eb2N1bWVudHMvV2ViU2l0ZSBQcm9qZWN0cy9Xb3JrIEluIFByb2dyZXNzL1dlbGxzIFJlcG9ydC1SZXN1bWUvYXBwL3NjcmlwdHMvc3JjL3NlcnZpY2VzL09yaWVudGF0aW9uU3J2LmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9zZXJ2aWNlcy9Qb2ludHNEYXRhU3J2LmpzIiwiL1VzZXJzL2ZhbG9zYWtlcnMvRG9jdW1lbnRzL1dlYlNpdGUgUHJvamVjdHMvV29yayBJbiBQcm9ncmVzcy9XZWxscyBSZXBvcnQtUmVzdW1lL2FwcC9zY3JpcHRzL3NyYy9zZXJ2aWNlcy9QcmludFNydi5qcyIsIi9Vc2Vycy9mYWxvc2FrZXJzL0RvY3VtZW50cy9XZWJTaXRlIFByb2plY3RzL1dvcmsgSW4gUHJvZ3Jlc3MvV2VsbHMgUmVwb3J0LVJlc3VtZS9hcHAvc2NyaXB0cy9zcmMvc2VydmljZXMvU2lnblBhZFNydi5qcyIsIi9Vc2Vycy9mYWxvc2FrZXJzL0RvY3VtZW50cy9XZWJTaXRlIFByb2plY3RzL1dvcmsgSW4gUHJvZ3Jlc3MvV2VsbHMgUmVwb3J0LVJlc3VtZS9hcHAvc2NyaXB0cy9zcmMvc2VydmljZXMvVG9hc3RTcnYuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7bUNDUXFCLDBCQUEwQjs7Ozs7O29DQTRDekIsMEJBQTBCOztzQ0FHeEIsNEJBQTRCOztzQ0FHNUIsNEJBQTRCOzsyQ0FHdkIsaUNBQWlDOzt1Q0FHckMsNkJBQTZCOztxQ0FHL0IsMkJBQTJCOzswQ0FHdEIsZ0NBQWdDOzs0Q0FHOUIsa0NBQWtDOzsyQ0FHbkMsaUNBQWlDOzt3Q0FHcEMsOEJBQThCOzs7Ozs7b0NBUS9CLDBCQUEwQjs7a0NBRzVCLHdCQUF3Qjs7d0NBR2xCLDhCQUE4Qjs7eUNBRzdCLCtCQUErQjs7eUNBRy9CLCtCQUErQjs7MENBRzlCLGdDQUFnQzs7dUNBR25DLDZCQUE2Qjs7d0NBRzVCLDhCQUE4Qjs7c0NBR2hDLDRCQUE0Qjs7a0NBR2hDLHdCQUF3Qjs7b0NBR3RCLDBCQUEwQjs7QUFoSG5ELElBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUMsVUFBVSxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLGVBQWUsRUFDN0csbUJBQW1CLEVBQUUsZUFBZSxFQUFFLHVCQUF1QixFQUFDLGFBQWEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7O0FBR3ZHLEdBQUcsQ0FBQyxNQUFNLDZCQUFRLENBQUM7O0FBR25CLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxVQUFVLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRTs7QUFFbEQsV0FBVSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxVQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFOzs7O0VBS3JHLENBQUMsQ0FBQzs7QUFFSCxXQUFVLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLFVBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRTs7O0FBR2hHLFVBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7RUFFbkQsQ0FBQyxDQUFDOztBQUVILFdBQVUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRTs7O0FBR3JHLFNBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsTUFBSSxLQUFLLEtBQUssZUFBZSxFQUFFO0FBQzlCLFNBQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7R0FDbkI7RUFDRCxDQUFDLENBQUM7O0FBRUgsS0FBSSxZQUFZLEdBQUcsSUFBSSxRQUFRLENBQUMsNERBQTRELENBQUMsQ0FBQztBQUM5RixhQUFZLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUksRUFBRTtBQUN4QyxNQUFJLElBQUksQ0FBQyxHQUFHLEVBQUUsS0FBSyxJQUFJLEVBQUU7QUFDeEIsYUFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztHQUNuQyxNQUFNO0FBQ04sYUFBVSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztHQUN0QztFQUNELENBQUMsQ0FBQztDQUVILENBQUMsQ0FBQTtBQU9GLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxnQ0FBVSxDQUFDOztBQUduQyxHQUFHLENBQUMsVUFBVSxDQUFDLFdBQVcsb0NBQVksQ0FBQzs7QUFHdkMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxXQUFXLG9DQUFZLENBQUM7O0FBR3ZDLEdBQUcsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLDhDQUFpQixDQUFDOztBQUdqRCxHQUFHLENBQUMsVUFBVSxDQUFDLFlBQVksc0NBQWEsQ0FBQzs7QUFHekMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLGtDQUFXLENBQUM7O0FBR3JDLEdBQUcsQ0FBQyxVQUFVLENBQUMsZUFBZSw0Q0FBZ0IsQ0FBQzs7QUFHL0MsR0FBRyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsZ0RBQWtCLENBQUM7O0FBR25ELEdBQUcsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLDhDQUFpQixDQUFDOztBQUdqRCxHQUFHLENBQUMsVUFBVSxDQUFDLGFBQWEsd0NBQWMsQ0FBQztBQVEzQyxHQUFHLENBQUMsT0FBTyxDQUFDLFNBQVMsbUNBQWEsQ0FBQzs7QUFHbkMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLCtCQUFXLENBQUM7O0FBRy9CLEdBQUcsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLDJDQUFpQixDQUFDOztBQUc5QyxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsNkNBQWtCLENBQUM7O0FBRzdDLEdBQUcsQ0FBQyxPQUFPLENBQUMsY0FBYyw2Q0FBa0IsQ0FBQzs7QUFHN0MsR0FBRyxDQUFDLE9BQU8sQ0FBQyxlQUFlLCtDQUFtQixDQUFDOztBQUcvQyxHQUFHLENBQUMsT0FBTyxDQUFDLFlBQVkseUNBQWdCLENBQUM7O0FBR3pDLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSwyQ0FBaUIsQ0FBQzs7QUFHM0MsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLHVDQUFlLENBQUM7O0FBR3ZDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTywrQkFBVyxDQUFDOztBQUcvQixHQUFHLENBQUMsT0FBTyxDQUFDLFNBQVMsbUNBQWEsQ0FBQzs7Ozs7QUFLbkMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUU7QUFDM0IsSUFBRyxFQUFFLElBQUksUUFBUSxDQUFDLDRDQUE0QyxDQUFDO0NBQy9ELENBQUMsQ0FBQTs7QUFFRixHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxVQUFVLE9BQU8sRUFBRTs7OztBQUkxQyxRQUFPLFVBQVUsWUFBWSxFQUFFLFdBQVcsRUFBRTs7QUFFM0MsTUFBSSxNQUFNLENBQUM7O0FBRVgsTUFBSSxXQUFXLEVBQUU7O0FBRWhCLE9BQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7OztBQUVoRSxTQUFJLFlBQVksR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDOztBQUUxQyxTQUFJLGlCQUFpQixHQUFHLEVBQUUsQ0FBQzs7QUFFM0IsaUJBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBSztBQUM3QixVQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQy9CLFVBQUksR0FBRyxZQUFBO1VBQUUsS0FBSyxZQUFBLENBQUM7OztBQUdmLFVBQUk7QUFDSCxVQUFHLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7QUFDekMsWUFBSyxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO09BQzNDLENBQUMsT0FBTyxLQUFLLEVBQUUsRUFFZjs7O0FBR0QsdUJBQWlCLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxDQUFDO01BQy9CLENBQUMsQ0FBQzs7QUFFSCxTQUFJO0FBQ0gsWUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztNQUM1RCxDQUFDLE9BQU8sS0FBSyxFQUFFLEVBRWY7O0lBSUQsTUFBTTs7QUFFTixVQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsQ0FBQTtJQUNyRDtHQUNELE1BQU07O0FBRU4sU0FBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLENBQUE7R0FDckQ7O0FBRUQsU0FBTyxNQUFNLENBQUM7RUFFZCxDQUFBO0NBRUQsQ0FBQyxDQUFDOzs7O0FDcExILFlBQVksQ0FBQzs7OztBQUNiLElBQUksTUFBTSxHQUFHLENBQUMsU0FBUyxNQUFNLEdBQUc7QUFDL0IsVUFBUyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLG9CQUFvQixFQUFFO0FBQ2pILG9CQUFrQixDQUFDLGVBQWUsRUFBRSxDQUFDOztBQUVyQyxTQUFPLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRTtBQUM3QyxTQUFNLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJO0FBQ3RCLGNBQVcsRUFBRSxjQUFjO0dBQzNCLENBQUMsQ0FBQzs7QUFFSCxnQkFBYyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7QUFDN0IsTUFBRyxFQUFFLFFBQVE7QUFDYixjQUFXLEVBQUUsa0JBQWtCO0FBQy9CLGFBQVUsRUFBRSxpQkFBaUI7O0dBRTdCLENBQUMsQ0FDQSxLQUFLLENBQUMsUUFBUSxFQUFFO0FBQ2hCLE1BQUcsRUFBRSxTQUFTO0FBQ2QsY0FBVyxFQUFFLGtCQUFrQjtBQUMvQixhQUFVLEVBQUUsb0JBQVUsWUFBWSxFQUFFO0FBQ25DLGdCQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDdkI7R0FDRCxDQUFDLENBQ0QsS0FBSyxDQUFDLE1BQU0sRUFBRTtBQUNkLE1BQUcsRUFBRSxPQUFPO0FBQ1osY0FBVyxFQUFFLGlCQUFpQjtBQUM5QixhQUFVLEVBQUUsZ0JBQWdCO0FBQzVCLFVBQU8sRUFBRTs7O0FBR1IsaUJBQWEsRUFBRSxDQUFDLGNBQWMsRUFBRSxlQUFlLEVBQUUsVUFBVSxJQUFJLEVBQUUsYUFBYSxFQUFFOzs7QUFHL0UsU0FBSSxTQUFTLENBQUMsTUFBTSxFQUFFO0FBQ3JCLGFBQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO01BQzNCLE1BQU07QUFDTixhQUFPLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztNQUNoQztLQUdELENBQUM7SUFDRjtHQUNELENBQUMsQ0FDRCxLQUFLLENBQUMsT0FBTyxFQUFFO0FBQ2YsTUFBRyxFQUFFLFFBQVE7QUFDYixjQUFXLEVBQUUsa0JBQWtCO0FBQy9CLGFBQVUsRUFBRSxpQkFBaUI7QUFDN0IsVUFBTyxFQUFFOzs7QUFHUixpQkFBYSxFQUFFLENBQUMsY0FBYyxFQUFFLFVBQVUsSUFBSSxFQUFFOzs7QUFHL0MsWUFBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7S0FDM0IsQ0FBQztJQUNGO0dBQ0QsQ0FBQyxDQUNELEtBQUssQ0FBQyxXQUFXLEVBQUU7QUFDbkIsTUFBRyxFQUFFLGdCQUFnQjtBQUNyQixjQUFXLEVBQUUsdUJBQXVCO0FBQ3BDLGFBQVUsRUFBRSxxQkFBcUI7QUFDakMsVUFBTyxFQUFFOzs7QUFHUixpQkFBYSxFQUFFLENBQUMsY0FBYyxFQUFFLGVBQWUsRUFBRSxVQUFVLElBQUksRUFBRSxhQUFhLEVBQUU7OztBQUcvRSxTQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7QUFDckIsYUFBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7TUFDM0IsTUFBTTtBQUNOLGFBQU8sYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO01BQ2hDO0tBQ0QsQ0FBQztJQUNGO0dBQ0QsQ0FBQyxDQUNELEtBQUssQ0FBQyxZQUFZLEVBQUU7QUFDcEIsTUFBRyxFQUFFLGFBQWE7QUFDbEIsY0FBVyxFQUFFLHVCQUF1QjtBQUNwQyxhQUFVLEVBQUUsc0JBQXNCOztBQUVsQyxVQUFPLEVBQUU7OztBQUdSLGlCQUFhLEVBQUUsQ0FBQyxjQUFjLEVBQUUsZUFBZSxFQUFFLFVBQVUsSUFBSSxFQUFFLGFBQWEsRUFBRTs7O0FBRy9FLFNBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUNyQixhQUFPLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztNQUMzQixNQUFNO0FBQ04sYUFBTyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7TUFDaEM7S0FDRCxDQUFDO0lBQ0Y7R0FDRCxDQUFDLENBQ0QsS0FBSyxDQUFDLG1CQUFtQixFQUFFO0FBQzNCLE1BQUcsRUFBRSxhQUFhO0FBQ2xCLGNBQVcsRUFBRSx3QkFBd0I7QUFDckMsYUFBVSxFQUFFLGtCQUFrQjtBQUM5QixVQUFPLEVBQUU7OztBQUdSLGlCQUFhLEVBQUUsQ0FBQyxjQUFjLEVBQUUsZUFBZSxFQUFFLFVBQVUsSUFBSSxFQUFFLGFBQWEsRUFBRTs7O0FBRy9FLFNBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUNyQixhQUFPLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztNQUMzQixNQUFNO0FBQ04sYUFBTyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7TUFDaEM7S0FDRCxDQUFDO0lBQ0Y7R0FDRCxDQUFDLENBQ0QsS0FBSyxDQUFDLHdCQUF3QixFQUFFO0FBQ2hDLE1BQUcsRUFBRSxrQkFBa0I7QUFDdkIsY0FBVyxFQUFFLHdCQUF3QjtBQUNyQyxhQUFVLEVBQUUsdUJBQXVCO0FBQ25DLFVBQU8sRUFBRTs7O0FBR1IsaUJBQWEsRUFBRSxDQUFDLGNBQWMsRUFBRSxlQUFlLEVBQUUsVUFBVSxJQUFJLEVBQUUsYUFBYSxFQUFFOzs7QUFHL0UsU0FBSSxTQUFTLENBQUMsTUFBTSxFQUFFO0FBQ3JCLGFBQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO01BQzNCLE1BQU07QUFDTixhQUFPLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztNQUNoQztLQUNELENBQUM7SUFDRjtHQUNELENBQUMsQ0FDRCxLQUFLLENBQUMsdUJBQXVCLEVBQUU7QUFDL0IsTUFBRyxFQUFFLGlCQUFpQjtBQUN0QixjQUFXLEVBQUUsdUJBQXVCO0FBQ3BDLGFBQVUsRUFBRSxzQkFBc0I7QUFDbEMsVUFBTyxFQUFFOzs7QUFHUixpQkFBYSxFQUFFLENBQUMsY0FBYyxFQUFFLGVBQWUsRUFBRSxVQUFVLElBQUksRUFBRSxhQUFhLEVBQUU7OztBQUcvRSxTQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7QUFDckIsYUFBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7TUFDM0IsTUFBTTtBQUNOLGFBQU8sYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO01BQ2hDO0tBQ0QsQ0FBQztJQUNGO0dBQ0QsQ0FBQyxDQUFBOztBQUdILG9CQUFrQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7QUFFdEMsb0JBQWtCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUNqQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQzVCLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztFQUMxQjs7QUFFRCxPQUFNLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLEVBQUUsb0JBQW9CLEVBQUUsb0JBQW9CLEVBQUUsc0JBQXNCLENBQUMsQ0FBQTs7QUFFN0gsUUFBTyxNQUFNLENBQUE7Q0FDYixDQUFBLEVBQUcsQ0FBQztRQUNHLE1BQU0sR0FBTixNQUFNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUMxSVIsU0FBUztBQUNILFVBRE4sU0FBUyxDQUNGLFdBQVcsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUN0RixNQUFNLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLGFBQWEsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFOzs7d0JBRi9GLFNBQVM7OztBQUtiLE1BQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBOzs7QUFHdEIsTUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDO0FBQzNCLE1BQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO0FBQ3JDLE1BQUksQ0FBQyxLQUFLLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDckQsTUFBSSxDQUFDLEtBQUssR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUNyRCxNQUFJLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0FBQ3pELE1BQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBQzNCLE1BQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ3pCLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0FBQ3JCLE1BQUksQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDO0FBQ3pCLE1BQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDO0FBQ3BELE1BQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO0FBQ3ZDLE1BQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0FBQ25DLE1BQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDOzs7O0FBSTdCLGFBQVcsQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDOztBQUVqQyxTQUFPLENBQUMsbUJBQW1CLEdBQUcsWUFBTTtBQUNuQyxVQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUNyQyxTQUFLLE1BQU0sR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO0FBQ3ZDLFNBQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztHQUNoQixDQUFBOztBQUVELE1BQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7QUFDdkMsT0FBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUE7R0FDN0IsTUFBTTtBQUNOLE9BQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztHQUN2QztFQUNEOztjQXJDSSxTQUFTOzs7Ozs7Ozs7Ozs7O1NBc0RSLGdCQUFDLElBQUksRUFBRSxNQUFNLEVBQUU7OztBQUNwQixPQUFJLE1BQU0sQ0FBQyxPQUFPLElBQUksRUFBRSxJQUFJLE1BQU0sQ0FBQyxPQUFPLElBQUksQ0FBQyxFQUFFO0FBQ2hELFdBQU07SUFDTjs7QUFFRCxPQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ25ELFdBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxHQUFHLEdBQUcsd0JBQXdCLENBQUMsQ0FBQzs7QUFFL0QsV0FBSyxTQUFTLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBSyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQzs7QUFFekMsV0FBSyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7QUFFaEQsV0FBSyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQUssSUFBSSxDQUFDLENBQUM7O0FBRTNCLFFBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztBQUNwQixRQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7QUFDckIsV0FBSyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDO0lBRXRDLENBQUMsU0FDSyxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2pCLFdBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ3RCLFdBQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUMsQ0FBQztHQUNKOzs7Ozs7Ozs7Ozs7O1NBVU0saUJBQUMsWUFBWSxFQUFFOztBQUVyQixXQUFRLElBQUksQ0FBQyxhQUFhO0FBQ3pCLFNBQUssQ0FBQztBQUNMLGNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztBQUM5QyxXQUFNO0FBQUEsQUFDUCxTQUFLLENBQUM7QUFDTCxjQUFTLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUM7QUFDOUMsV0FBTTtBQUFBLEFBQ1A7QUFDQyxXQUFNO0FBQUEsSUFDUDtHQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQStGUSxtQkFBQyxLQUFLLEVBQUU7QUFDaEIsVUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNuQixPQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUNoQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQ2QsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUNkLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7QUFFZCxPQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztHQUczQjs7Ozs7Ozs7Ozs7Ozs7U0FXa0IsNkJBQUMsY0FBYyxFQUFFLE1BQU0sRUFBRTtBQUMzQyxPQUFJLElBQUksR0FBRyxJQUFJLENBQUM7O0FBRWhCLE9BQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO0FBQ25CLGVBQVcsRUFBRSxNQUFNO0FBQ25CLHVCQUFtQixFQUFFLElBQUk7QUFDekIsZUFBVyxFQUFFLDZCQUE2QjtBQUMxQyxjQUFVLEVBQUUsU0FBUyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUU7OztBQUVoRCxTQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNwQyxhQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xCLGFBQUssS0FBSyxHQUFHLEtBQUssQ0FBQztNQUNuQixDQUFDLENBQUM7OztBQUdILFNBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWTtBQUM5QixlQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7TUFDakIsQ0FBQzs7QUFFRixTQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQzdCLFVBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsT0FBTyxFQUFLO0FBQ3hDLGNBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRTtBQUMvQixZQUFJLEVBQUUsQ0FBQyxNQUFNLEtBQUssY0FBYyxDQUFDLEdBQUcsRUFBRTs7QUFFckMsYUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUFDMUUsZUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUNqQyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7O0FBRXhCLGdCQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDdkIsZ0JBQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQTtBQUN4QixnQkFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQUcsRUFBSztBQUM1QixrQkFBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO0FBQ2hDLGVBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxHQUFHLEVBQUUsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7QUFDOUQsb0JBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztXQUNqQixFQUFFLFVBQVUsS0FBSyxFQUFFO0FBQ25CLG9CQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDakIsZUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUN0QixrQkFBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7V0FDL0IsQ0FBQyxDQUFDO1VBQ0gsQ0FBQyxDQUFBO1NBQ0Y7UUFDRCxDQUFDLENBQUE7T0FDRixDQUFDLENBQUM7TUFDSCxDQUFDO0tBQ0Y7QUFDRCxnQkFBWSxFQUFFLElBQUk7SUFDbEIsQ0FBQyxDQUFDO0dBQ0g7Ozs7Ozs7Ozs7Ozs7O1NBV2Esd0JBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRTtBQUMzQixPQUFJLElBQUksR0FBRyxJQUFJLENBQUM7QUFDaEIsT0FBSSxXQUFXLEdBQUcsU0FBUyxDQUFDOztBQUU1QixPQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtBQUNsQyxXQUFNO0lBQ047QUFDRCxPQUFJLE9BQU8sR0FBRztBQUNiLFlBQVEsRUFBRSxDQUFDO0FBQ1gsWUFBUSxFQUFFLENBQUM7SUFDWCxDQUFBO0FBQ0QsV0FBUSxJQUFJLENBQUMsYUFBYTtBQUN6QixTQUFLLENBQUM7QUFDTCxnQkFBVyxHQUFHLHFCQUFxQixDQUFBO0FBQ25DLFdBQU07QUFBQSxBQUNQLFNBQUssQ0FBQztBQUNMLGdCQUFXLEdBQUcsb0JBQW9CLENBQUE7QUFDbEMsV0FBTTs7QUFBQSxBQUVQO0FBQ0MsV0FBTTtBQUFBLElBQ1A7QUFDRCxPQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztBQUNuQixlQUFXLEVBQUUsTUFBTTtBQUNuQix1QkFBbUIsRUFBRSxJQUFJO0FBQ3pCLFNBQUssRUFBRSxJQUFJLENBQUMsTUFBTTtBQUNsQixpQkFBYSxFQUFFLElBQUk7QUFDbkIsZUFBVyxFQUFFLFdBQVc7QUFDeEIsY0FBVSxFQUFFLFNBQVMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRTs7O0FBR3hELFNBQUksTUFBTSxZQUFBO1NBQUUsS0FBSyxZQUFBLENBQUM7OztBQUdsQixhQUFRLElBQUksQ0FBQyxhQUFhO0FBQ3pCLFdBQUssT0FBTyxDQUFDLFFBQVE7O0FBQ3BCLGFBQU0sR0FBRyxPQUFPLENBQUE7QUFDaEIsWUFBSyxHQUFHLE1BQU0sQ0FBQTtBQUNkLGFBQU07QUFBQSxBQUNQLFdBQUssT0FBTyxDQUFDLFFBQVE7O0FBQ3BCLGFBQU0sR0FBRyxPQUFPLENBQUE7QUFDaEIsWUFBSyxHQUFHLE1BQU0sQ0FBQTtBQUNkLFdBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO0FBQ3BCLGFBQU07O0FBQUEsQUFFUDtBQUNDLGFBQU07QUFBQSxNQUNQOztBQUVELFNBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7O0FBRS9FLFNBQUksQ0FBQyxNQUFNLEdBQUcsVUFBVSxJQUFJLEVBQUUsSUFBSSxFQUFFOztBQUVuQyxVQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7QUFDcEIsVUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0FBQ3JCLGVBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7QUFFakIsY0FBUSxJQUFJLENBQUMsYUFBYTtBQUN6QixZQUFLLE9BQU8sQ0FBQyxRQUFRO0FBQ3BCLGtCQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDakIsY0FBTTtBQUFBLEFBQ1AsWUFBSyxPQUFPLENBQUMsUUFBUTtBQUNwQixrQkFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ2pCLGNBQU07O0FBQUEsQUFFUDtBQUNDLGNBQU07QUFBQSxPQUNQO01BRUQsQ0FBQzs7QUFFRixjQUFTLFVBQVUsQ0FBQyxJQUFJLEVBQUU7QUFDekIsVUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO0FBQ2xCLFdBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDcEIsV0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBQzFCLFdBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0FBQ3JCLFdBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUE7T0FDckM7O0FBRUQsVUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO0FBQ3JCLFdBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO0FBQ3pCLGFBQUssRUFBRSxJQUFJLENBQUMsS0FBSztBQUNqQixtQkFBVyxFQUFFLElBQUksQ0FBQyxRQUFRO0FBQzFCLG1CQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7UUFDN0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQ2IsWUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO0FBQ2pDLGVBQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztBQUN4QixZQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDYixZQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ25DLGFBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUN4QyxDQUFDLENBQUE7QUFDRixZQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLDJCQUEyQixFQUFFLENBQUMsQ0FBQztBQUNyRCxlQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFDOUMsQ0FBQyxTQUFNLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDbkIsZUFBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDO09BQ0gsTUFBTTtBQUNOLGNBQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztBQUN4QixXQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLDJCQUEyQixFQUFFLENBQUMsQ0FBQztBQUNyRCxXQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDYixXQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztPQUMzRDs7QUFFRCxVQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFDakIsT0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDM0IsYUFBTyxPQUFPLENBQUMsTUFBTSxDQUFDO0FBQ3RCLFVBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztNQUMzQzs7QUFFRCxjQUFTLFVBQVUsQ0FBQyxJQUFJLEVBQUU7QUFDekIsVUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO01BQ2I7O0FBRUQsU0FBSSxDQUFDLFdBQVcsR0FBRyxZQUFNO0FBQ3hCLGVBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztNQUNqQixDQUFDOztBQUVGLFNBQUksQ0FBQyxNQUFNLEdBQUcsVUFBQyxZQUFZLEVBQUs7QUFDL0IsYUFBSyxXQUFXLEVBQUUsQ0FBQztBQUNuQixVQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO01BRTNCLENBQUM7O0FBRUYsU0FBSSxDQUFDLGtCQUFrQixHQUFHLFVBQUMsY0FBYyxFQUFFLElBQUksRUFBSztBQUNuRCxhQUFLLFdBQVcsRUFBRSxDQUFDO0FBQ25CLFVBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7TUFFL0MsQ0FBQzs7QUFFRixTQUFJLENBQUMsY0FBYyxHQUFHLFVBQUMsSUFBSSxFQUFLO0FBQy9CLFVBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztBQUMxQixVQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtBQUNwQixVQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtNQUNwQixDQUFBO0tBQ0Q7QUFDRCxnQkFBWSxFQUFFLFVBQVU7SUFDeEIsQ0FBQyxDQUFDO0dBQ0E7Ozs7Ozs7Ozs7Ozs7U0FVUyx1QkFBQyxNQUFNLEVBQUU7QUFDckIsT0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDOztBQUVoQixPQUFJLFdBQVcsR0FBRyxTQUFTLENBQUM7QUFDNUIsV0FBUSxJQUFJLENBQUMsYUFBYTtBQUN6QixTQUFLLENBQUM7O0FBRUwsV0FBTTtBQUFBLEFBQ1AsU0FBSyxDQUFDO0FBQ0wsZ0JBQVcsR0FBRyxvQkFBb0IsQ0FBQTtBQUNsQyxXQUFNOztBQUFBLEFBRVA7QUFDQyxXQUFNO0FBQUEsSUFDUDtBQUNELE9BQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO0FBQ25CLGVBQVcsRUFBRSxNQUFNO0FBQ25CLHVCQUFtQixFQUFFLElBQUk7QUFDekIsU0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNO0FBQ2xCLGlCQUFhLEVBQUUsSUFBSTs7OztBQUluQixlQUFXLEVBQUUsV0FBVztBQUN4QixjQUFVLEVBQUUsU0FBUyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFOztBQUV4RCxTQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQTs7QUFFbkIsU0FBSSxDQUFDLFdBQVcsR0FBRyxZQUFZO0FBQzlCLGVBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztNQUNqQixDQUFDOztBQUVGLFNBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxJQUFJLEVBQUUsSUFBSSxFQUFFO0FBQ2hDLFVBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztBQUV0QixVQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7QUFDcEIsVUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0FBQ3JCLGVBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztNQUdqQixDQUFDO0tBQ0Y7QUFDRCxnQkFBWSxFQUFFLFVBQVU7SUFDeEIsQ0FBQyxDQUFDO0dBQ0E7Ozs7Ozs7Ozs7Ozs7U0FVSyxtQkFBQyxLQUFLLEVBQUU7QUFDaEIsT0FBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLEdBQUcsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQTtBQUNuRSxPQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0dBQ3hEOzs7U0F2YmUscUJBQUc7QUFDbEIsT0FBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDL0MsV0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQyxDQUFDO0dBQ0g7OztTQW1FZ0Isb0JBQUMsWUFBWSxFQUFFOzs7QUFDL0IsT0FBSSxZQUFZLENBQUM7O0FBRWpCLE9BQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsT0FBTyxFQUFLO0FBQ3hDLGdCQUFZLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFLO0FBQzNDLFNBQUksRUFBRSxDQUFDLElBQUksSUFBSSxTQUFTLEVBQUU7QUFDekIsYUFBTyxLQUFLLENBQUE7TUFDWjtBQUNELFlBQU8sRUFBRSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUMsSUFBSSxDQUFBO0tBQ3BDLENBQUMsQ0FBQTs7O0FBR0YsUUFBSSxZQUFZLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtBQUM3QixpQkFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQ2pDLGFBQUssU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLDRCQUE0QixFQUFFLENBQUMsQ0FBQztNQUN0RCxDQUFDLFNBQ0ssQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNqQixhQUFLLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUN0QixhQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztNQUNoQyxDQUFDLENBQUM7S0FDSixNQUFNO0FBQ04sWUFBSyxTQUFTLENBQUMsbUNBQW1DLEdBQUcsWUFBWSxDQUFDLElBQUksR0FBRyxzQkFBc0IsQ0FBQyxDQUFDO0FBQ2pHLFlBQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUNmO0lBQ0QsQ0FBQyxDQUFDO0dBR0g7Ozs7Ozs7Ozs7Ozs7U0FVZ0Isb0JBQUMsWUFBWSxFQUFFOzs7QUFDL0IsT0FBSSxZQUFZLEdBQUcsU0FBUyxDQUFDO0FBQzdCLE9BQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzs7QUFHdkQsT0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7O0FBRWxDLFdBQUssT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQU8sRUFBSztBQUN4QyxpQkFBWSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBSztBQUMzQyxVQUFJLEVBQUUsQ0FBQyxNQUFNLElBQUksU0FBUyxFQUFFO0FBQzNCLGNBQU8sS0FBSyxDQUFBO09BQ1o7O0FBRUQsVUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUMsRUFBQyxHQUFHLEVBQUMsRUFBRSxDQUFDLFVBQVUsRUFBQyxDQUFDLENBQUM7O0FBRWhELGFBQVEsRUFBRSxDQUFDLE1BQU0sS0FBSyxZQUFZLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDO01BQ2hFLENBQUMsQ0FBQTs7O0FBR0YsU0FBSSxZQUFZLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtBQUM3QixhQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQU07QUFDOUMsbUJBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxHQUFHLEVBQUs7O0FBRXBDLGVBQUssU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLDRCQUE0QixFQUFFLENBQUMsQ0FBQztRQUN0RCxFQUFFLFVBQVUsS0FBSyxFQUFFO0FBQ25CLGVBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQztPQUNILENBQUMsU0FDSyxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ2pCLGNBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ3RCLGNBQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO09BQ2hDLENBQUMsQ0FBQztNQUNKLE1BQU07QUFDTixhQUFLLFNBQVMsQ0FBQyxtQ0FBbUMsR0FBRyxZQUFZLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLENBQUM7QUFDakcsYUFBTyxDQUFDLElBQUksRUFBRSxDQUFDO01BQ2Y7S0FDRCxDQUFDLENBQUM7SUFDSCxDQUFDLENBQUE7R0FDRjs7O1FBekxJLFNBQVM7OztRQWdlUCxTQUFTLEdBQVQsU0FBUzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDbGZYLGNBQWM7QUFDUixVQUROLGNBQWMsQ0FDUCxNQUFNLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFOzs7d0JBRDlHLGNBQWM7O0FBRWxCLE1BQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDOzs7QUFHN0IsTUFBSSxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQzs7O0FBR2pDLE1BQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOzs7QUFHcEIsTUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFOztBQUVyQixpQkFBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDOUMsVUFBSyxNQUFNLEdBQUcsTUFBTSxDQUFDOztBQUVyQixrQkFBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLE9BQU0sQ0FBQztJQUVuQyxDQUFDLENBQUE7R0FDRixNQUFNO0FBQ04saUJBQWMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTSxFQUFLOztBQUU3QyxVQUFLLE1BQU0sR0FBRyxNQUFNLENBQUM7O0FBRXJCLGtCQUFjLENBQUMsUUFBUSxDQUFDLElBQUksT0FBTSxDQUFDOztBQUVuQyxVQUFNLENBQUMsZ0JBQWdCLENBQUMsWUFBWTtBQUFFLFlBQU8sTUFBTSxDQUFBO0tBQUUsRUFBRSxVQUFDLENBQUMsRUFBRSxDQUFDLEVBQUs7QUFDaEUsU0FBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO0FBQ1osbUJBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7TUFDdEI7S0FDRCxDQUFDLENBQUM7SUFFSCxDQUFDLENBQUM7R0FDSDtFQUNEOztjQWxDSSxjQUFjOztTQWdGRCw4QkFBRztBQUNwQixPQUFJLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxFQUFFO0FBQzNCLFFBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUN2QyxNQUFNO0FBQ04sUUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztJQUN0RTs7O0FBR0QsT0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7QUFDZixXQUFNO0lBQ047O0FBRUQsT0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFO0FBQzNCLFFBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFDdEIsVUFBSyxFQUFFLGtCQUFrQjtBQUN6QixTQUFJLEVBQUUsSUFBSTtBQUNWLFNBQUksRUFBRSx3QkFBd0I7S0FDOUIsQ0FBQyxDQUFBO0FBQ0YsUUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0FBQzdCLFdBQU07SUFDTjtBQUNELE9BQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDOztBQUUvQixPQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxDQUFDLEVBQUU7QUFDdEMsUUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUMsRUFBRTtBQUNqRCxVQUFLLEVBQUUsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHVCQUF1QjtBQUN6RCxTQUFJLEVBQUUsSUFBSTtBQUNWLFNBQUksRUFBRSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsT0FBTztLQUNuRSxDQUFDLENBQUE7SUFDRjs7QUFFRCxPQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxDQUFDLEVBQUU7O0FBRXRDLFFBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBRXJCO0dBQ0Q7OztTQUVjLDJCQUFHO0FBQ2pCLE9BQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQ2QsU0FBSyxFQUFFLGVBQWU7QUFDdEIsUUFBSSxFQUFFLElBQUk7QUFDVixRQUFJLEVBQUUsd0JBQXdCO0lBQzlCLENBQUMsQ0FBQzs7QUFFSCxPQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7R0FDMUI7OztTQUVhLDBCQUFHO0FBQ2hCLE9BQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQ2QsU0FBSyxFQUFFLGNBQWM7QUFDckIsUUFBSSxFQUFFLElBQUk7QUFDVixRQUFJLEVBQUUsdUJBQXVCO0lBQzdCLENBQUMsQ0FBQTs7QUFFRixPQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7R0FDekI7OztTQUVRLG1CQUFDLE9BQU8sRUFBRTtBQUNsQixPQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztHQUN2Qjs7O1NBeEdjLG9CQUFHO0FBQ2pCLE9BQUksQ0FBQyxJQUFJLEdBQUcsQ0FDWDtBQUNDLFNBQUssRUFBRSxrQkFBa0I7QUFDekIsUUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztBQUMzQixRQUFJLEVBQUUsd0JBQXdCO0lBQzlCLEVBQUU7QUFDRixTQUFLLEVBQUUsZUFBZTtBQUN0QixRQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO0FBQ3hCLFFBQUksRUFBRSx3QkFBd0I7SUFDOUIsRUFBRTtBQUNGLFNBQUssRUFBRSxjQUFjO0FBQ3JCLFFBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUs7QUFDdkIsUUFBSSxFQUFFLHVCQUF1QjtJQUM3QixDQUFDLENBQUE7O0FBRUgsT0FBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRTtBQUMzQixRQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQ3RCLE9BQUUsRUFBRSxTQUFTO0FBQ2IsVUFBSyxFQUFFLG9CQUFvQjtBQUMzQixTQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO0FBQzNCLFNBQUksRUFBRSwwQkFBMEI7S0FDaEMsQ0FBQyxDQUFBO0lBQ0Y7O0FBRUQsT0FBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRTtBQUMzQixRQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQ3RCLE9BQUUsRUFBRSxTQUFTO0FBQ2IsVUFBSyxFQUFFLG9CQUFvQjtBQUMzQixTQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO0FBQzNCLFNBQUksRUFBRSwwQkFBMEI7S0FDaEMsQ0FBQyxDQUFBO0lBQ0Y7O0FBRUQsT0FBSSwwQkFBMEIsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQTtBQUN0RSxPQUFJLENBQUMsdUJBQXVCLEdBQUcsMEJBQTBCLENBQUMsTUFBTSxDQUFDO0FBQ2pFLE9BQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQ3BELE9BQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLENBQUMsRUFBRTtBQUN0QyxRQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUNyQjs7QUFFRCxPQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztHQUM1Qjs7O1FBOUVJLGNBQWM7OztRQThJWixjQUFjLEdBQWQsY0FBYzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDeEloQixPQUFPO0FBQ0QsVUFETixPQUFPLENBQ0EsTUFBTSxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsVUFBVSxFQUFFOzs7d0JBRHhELE9BQU87O0FBRVgsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsTUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxDQUFDO0FBQzFDLE1BQUksQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFBO0FBQ3hCLE1BQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0FBQ25DLE1BQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDOztBQUU3QixNQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDOztBQUU5QyxZQUFVLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxVQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUs7QUFDN0MsU0FBSyxJQUFJLEdBQUcsTUFBSyxhQUFhLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0dBQzlDLENBQUMsQ0FBQTtFQUNGOztjQWJJLE9BQU87O1NBZVQsYUFBQyxJQUFJLEVBQUUsTUFBTSxFQUFFO0FBQ2pCLE9BQUksSUFBSSxLQUFLLFFBQVEsRUFBRTtBQUN0QixXQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLFFBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDcEIsUUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDeEIsUUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUM3QixRQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztBQUNmLFdBQU07SUFDTjtBQUNELE9BQUksSUFBSSxLQUFLLFdBQVcsRUFBRTtBQUN6QixRQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUUzQyxNQUFNO0FBQ04sUUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0I7R0FFRDs7O1FBL0JJLE9BQU87OztRQW1DTCxPQUFPLEdBQVAsT0FBTzs7Ozs7Ozs7QUN6Q2YsWUFBWSxDQUFDOzs7Ozs7Ozs7SUFDUCxhQUFhO0FBQ1AsVUFETixhQUFhLENBQ04sV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUN6RixhQUFhLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUNqRSxXQUFXLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFFOzs7d0JBSDdELGFBQWE7OztBQU1qQixNQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7OztBQUdoQyxNQUFJLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUM7QUFDM0IsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7QUFDckMsTUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7QUFDM0IsTUFBSSxTQUFTLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDeEQsTUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7QUFDdkMsTUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7QUFDakIsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsTUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7QUFDM0IsTUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7QUFDbkMsTUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7QUFDbkIsTUFBSSxDQUFDLEVBQUUsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFBO0FBQ3pCLE1BQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO0FBQ2pDLE1BQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDOzs7QUFHbkMsTUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDOztBQUdqQixNQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7QUFDWixPQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztBQUN2QixPQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQztHQUMxQixNQUFNO0FBQ04sT0FBSSxDQUFDLElBQUksR0FBRyxZQUFZLENBQUM7QUFDekIsT0FBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUM7O0FBRTVCLE9BQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7R0FDOUI7O0FBRUQsV0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUNuQyxTQUFLLEtBQUssR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxVQUFVLEtBQUssRUFBRTtBQUM3QyxXQUFPLEtBQUssQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDO0lBQ2xDLENBQUMsQ0FBQztHQUNILENBQUMsQ0FBQzs7QUFFSCxNQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7QUFDWixPQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDOztBQUV6QixPQUFJLE9BQU8sR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs7QUFFeEQsVUFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLE9BQU8sRUFBSztBQUNuQyxXQUFPLENBQUMsR0FBRyxDQUFDLFVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUs7QUFDekIsU0FBSSxFQUFFLENBQUMsVUFBVSxLQUFLLE1BQUssRUFBRSxFQUFFO0FBQzlCLFVBQUksR0FBRyxHQUFHLE1BQUssZUFBZSxDQUFDLE1BQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7QUFDM0UsU0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQUcsRUFBSztBQUMzQixTQUFFLENBQUMsT0FBTyxHQUFHLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7QUFFbkMsU0FBRSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFBO0FBQ2xCLGFBQUssTUFBTSxHQUFHLEVBQUUsQ0FBQztPQUNqQixDQUFDLENBQUE7TUFDRjtLQUVELENBQUMsQ0FBQztBQUNILFVBQUssWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUMsQ0FBQztHQUNIOztBQUVELGFBQVcsQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDOzs7QUFHckMsU0FBTyxDQUFDLG1CQUFtQixHQUFHLFlBQU07QUFDbkMsU0FBSyxNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztBQUN2QyxTQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7R0FDaEIsQ0FBQTs7O0FBR0QsTUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLFFBQVEsRUFBRTtBQUN2QyxPQUFJLENBQUMsTUFBTSxHQUFHLGVBQWUsQ0FBQTtHQUM3QixNQUFNO0FBQ04sT0FBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO0dBQ3ZDO0VBQ0Q7O2NBL0VJLGFBQWE7O1NBaUZiLGVBQUMsVUFBVSxFQUFFO0FBQ2pCLFVBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDeEIsVUFBTyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztHQUMxRDs7O1NBRVEsbUJBQUMsSUFBSSxFQUFFOzs7O0FBRWYsT0FBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUU7QUFDYixRQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFO0FBQ3hFLFNBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO0FBQ2hDLFdBQUssRUFBRSxPQUFPO0FBQ2QsYUFBTyxFQUFFLG9DQUFvQztBQUM3QyxRQUFFLEVBQUUsSUFBSTtNQUNSLENBQUMsQ0FBQzs7QUFFSCxTQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTs7QUFFMUIsWUFBTTtLQUNOO0FBQ0QsUUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0FBQzNELFFBQUksR0FBRyxHQUFHO0FBQ1QsU0FBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSTtBQUN0QixTQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJO0FBQ3RCLFdBQU0sRUFBRSxNQUFNO0FBQ2QsWUFBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRTtLQUMxQyxDQUFBO0FBQ0QsYUFBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxHQUFHLEVBQUs7QUFDakMsU0FBSSxPQUFPLEdBQUcsT0FBSyxjQUFjLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7QUFDN0QsU0FBSSxTQUFTLEdBQUcsT0FBSyxjQUFjLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7QUFDaEUsU0FBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO0FBQ25CLFNBQUksSUFBSSxHQUFHLE9BQUssYUFBYSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUM3QyxZQUFLLE1BQU0sQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDOztBQUU1QixZQUFPLE9BQUssTUFBTSxDQUFDLElBQUksQ0FBQzs7QUFFeEIsWUFBTyxDQUFDLElBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQ3BDLGVBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDdEMsV0FBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtBQUMxQixZQUFJLEdBQUcsR0FBRyxPQUFLLGVBQWUsQ0FBQyxPQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7O0FBRWhFLFdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDNUIsaUJBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7U0FDaEUsQ0FBQyxDQUFDO1FBQ0g7T0FDRCxDQUFDLENBQUM7TUFDSCxDQUFDLENBQUM7O0FBRUgsWUFBSyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7O0FBRzNDLFNBQUksSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7QUFDOUIsVUFBSSxPQUFPLEdBQUcsT0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDO0FBQ3BDLFlBQUssRUFBRSxTQUFTO0FBQ2hCLGNBQU8sRUFBRSxvQ0FBb0M7QUFDN0MsU0FBRSxFQUFFLFVBQVU7QUFDZCxhQUFNLEVBQUUsZ0JBQWdCO09BQ3hCLENBQUMsQ0FBQzs7QUFFSCxhQUFLLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQzNDLGNBQUssTUFBTSxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ2hELGNBQUssU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO09BQ3JCLEVBQUUsVUFBQyxJQUFJLEVBQUs7QUFDWixjQUFLLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztPQUVyQixDQUFDLENBQUM7TUFDSCxNQUFNO0FBQ04sYUFBSyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7TUFDckI7S0FDRCxDQUFDLENBQUE7SUFDRjs7UUFFSTtBQUNKLFNBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs7QUFFcEYsU0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7QUFFekIsU0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDNUIsU0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDNUIsU0FBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDaEQsWUFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzs7QUFFeEIsV0FBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUNqQyxhQUFPLENBQUMsSUFBSSxDQUFDLE9BQUssTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ2xDLFlBQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxHQUFHLEVBQUs7QUFDNUIsV0FBSSxHQUFHLEdBQUcsT0FBSyxlQUFlLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUMvRSxVQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBRyxFQUFLO0FBQzNCLFdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLFdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLFdBQUcsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0FBQ3RCLFdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBTTtBQUN0QixnQkFBSyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDekIsZ0JBQUssTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN2QixDQUFDLENBQUM7UUFDSCxDQUFDLENBQUE7T0FDRixDQUFDLENBQUM7TUFDSCxDQUFDLENBQUM7S0FDSDtHQUNEOzs7U0FFUSxtQkFBQyxJQUFJLEVBQUU7QUFDZixPQUFJLElBQUksRUFBRTtBQUNULFFBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztBQUNwQixRQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7QUFDckIsUUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN4QyxRQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDO0lBQzlCO0FBQ0QsV0FBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztHQUVoRDs7O1NBRUssZ0JBQUMsSUFBSSxFQUFFO0FBQ1osT0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0FBQ3BCLE9BQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztBQUNyQixPQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3hDLE9BQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0dBQ3ZCOzs7UUFwTUksYUFBYTs7O1FBdU1YLGFBQWEsR0FBYixhQUFhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4TXJCLENBQUM7O0lBQ0ssZUFBZTtBQUNULFVBRE4sZUFBZSxDQUNSLFdBQVcsRUFBRSxlQUFlLEVBQUUsTUFBTSxrQkFBbUIsTUFBTSxFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQ3RHLFlBQVksRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUM5RSxXQUFXLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFOzs7d0JBSHBELGVBQWU7OztBQU1uQixNQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQzs7O0FBR3JCLE1BQUksQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQzs7O0FBRzNCLE1BQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQyxNQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzs7QUFFdkIsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7O0FBRXJDLE1BQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDOztBQUVqQyxNQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQzs7QUFFckMsTUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUE7O0FBRWhDLE1BQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDOztBQUVuQyxNQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQzs7QUFFckIsTUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7O0FBRTNCLE1BQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDOzs7QUFHbkIsY0FBWSxDQUFDLElBQUksRUFBRSxDQUFDOztBQUVwQixNQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7O0FBRWhCLGFBQVcsQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDOztBQUVsQyxTQUFPLENBQUMsbUJBQW1CLEdBQUcsWUFBTTtBQUNuQyxTQUFLLE1BQU0sR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO0FBQ3ZDLFNBQUssUUFBUSxFQUFFLENBQUE7R0FDZixDQUFBOztBQUVELE1BQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7QUFDdkMsT0FBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUE7R0FDN0IsTUFBTTtBQUNOLE9BQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztHQUN2Qzs7QUFFRCxNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsWUFBTTtBQUNsQyxVQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pCLFNBQUssYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDO0dBQ3JDLENBQUMsQ0FBQztFQUVIOztjQXRESSxlQUFlOztTQStEWixvQkFBRzs7OztBQUVWLE9BQUksU0FBUyxDQUFDLE1BQU0sRUFBRTs7QUFFckIsUUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN4RCxZQUFLLE1BQU0sR0FBRyxNQUFNLENBQUM7O0FBRXJCLFlBQUssY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNoRCxhQUFLLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0FBQ3hCLFVBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxJQUFJLEVBQUU7QUFDdEIsY0FBSyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO09BQ2xDOztBQUVELGFBQUssUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDOztBQUVuQyxhQUFLLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztNQUN6QixDQUFDLENBQUE7OztBQUdGLFlBQUssYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDOztBQUV2QyxlQUFVLENBQUMsWUFBTTs7QUFFaEIscUJBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxRQUFNLENBQUM7TUFDdkMsRUFBRSxDQUFDLENBQUMsQ0FBQztLQUNOLENBQUMsQ0FBQTtJQUNGLE1BQU07QUFDTixRQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTSxFQUFLOztBQUV2RCxZQUFLLE1BQU0sR0FBRyxNQUFNLENBQUM7OztBQUdyQixTQUFJLElBQUksR0FBRyxPQUFLLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQTs7O0FBRy9DLFNBQUksS0FBSyxHQUFHLE9BQUssYUFBYSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUM1QyxZQUFLLEtBQUssR0FBRyxLQUFLLENBQUM7OztBQUduQixTQUFJLENBQUMsT0FBSyxNQUFNLENBQUMsSUFBSSxFQUFFO0FBQ3RCLGFBQUssTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQzdCLGFBQUssTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO01BQzlCOzs7QUFHRCxZQUFLLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBSyxNQUFNLENBQUMsQ0FBQzs7QUFFdkMsb0JBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxRQUFNLENBQUM7OztBQUd2QyxZQUFLLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUN6QixDQUFDLENBQUE7SUFDRjtHQUNEOzs7U0FFUSxxQkFBRztBQUNYLE9BQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7R0FDckI7OztTQUVJLGlCQUFHO0FBQ1AsT0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdkIsT0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztBQUN2QixPQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO0dBQzlCOzs7U0FFTyxvQkFBRztBQUNWLE9BQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7QUFDbEQsT0FBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0dBQ2I7OztTQUVJLGlCQUFHO0FBQ1AsT0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztBQUNqRCxPQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFFLENBQUM7QUFDM0UsT0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztHQUNsQjs7O1NBakZpQix1QkFBRzs7QUFFcEIsT0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDNUMsT0FBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztBQUM1QixPQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDO0dBQzVCOzs7UUE3REksZUFBZTs7O1FBNEliLGVBQWUsR0FBZixlQUFlOzs7Ozs7OztBQzdJdkIsWUFBWSxDQUFDOzs7Ozs7Ozs7SUFDUCxRQUFRO0FBQ0YsVUFETixRQUFRLENBQ0QsV0FBVyxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQ3BGLEtBQUssRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUNyRSxjQUFjLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUU7Ozt3QkFIekMsUUFBUTs7O0FBTVosTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7O0FBRXRCLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDOztBQUVyQixNQUFJLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUM7QUFDM0IsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7QUFDckMsTUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7QUFDdkMsTUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7QUFDbkIsTUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7QUFDbkMsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7OztBQUd2QixNQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQzs7QUFFckMsTUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7OztBQUczQixNQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzs7OztBQUt2QixNQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7QUFDckIsT0FBSSxXQUFXLElBQUksU0FBUyxFQUFFO0FBQzdCLFFBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7QUFDN0MsUUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQjtHQUNELE1BQU07QUFDTixPQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLENBQUMsS0FBSyxDQUFDO0FBQzdELE9BQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7R0FDcEI7OztBQUdELE1BQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDOUMsTUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO0FBQ2QsT0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztBQUNqQyxPQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO0FBQ3JDLE9BQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7R0FDbkM7O0FBRUQsTUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7OztBQUczQixhQUFXLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQTs7QUFFOUIsU0FBTyxDQUFDLG1CQUFtQixHQUFHLFlBQU07QUFDbkMsU0FBSyxNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztBQUN2QyxTQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7R0FDaEIsQ0FBQTs7O0FBR0QsTUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLFFBQVEsRUFBRTtBQUN2QyxPQUFJLENBQUMsTUFBTSxHQUFHLGVBQWUsQ0FBQTtHQUM3QixNQUFNO0FBQ04sT0FBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO0dBQ3ZDOzs7Ozs7Ozs7O0FBV0QsTUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFlBQU07QUFDbEMsVUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN6QixTQUFLLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUNyQixTQUFLLGFBQWEsQ0FBQyxlQUFlLEVBQUUsQ0FBQztHQUNyQyxDQUFDLENBQUM7OztBQUdILE1BQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFNO0FBQUUsVUFBTyxNQUFLLE1BQU0sQ0FBQTtHQUFFLEVBQUUsVUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFLO0FBQ25GLE9BQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtBQUNaLFdBQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDZixVQUFLLGVBQWUsRUFBRSxDQUFDO0lBQ3ZCO0dBQ0QsQ0FBQyxDQUFDOztBQUdILE1BQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUE7RUFFeEQ7O2NBMUZJLFFBQVE7Ozs7Ozs7U0E4SEwsa0JBQUMsS0FBSyxFQUFFOzs7QUFDZixPQUFJLENBQUMsS0FBSyxFQUFFOztBQUNYLFFBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs7QUFFekQsU0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSzs7QUFFL0IsVUFBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFLO0FBQ3pDLFVBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztBQUNsQixVQUFJLElBQUksR0FBRyxTQUFTLENBQUM7O0FBRXJCLFVBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFLLFVBQVUsQ0FBQyxFQUFFOztBQUNqRixZQUFJLFFBQVEsR0FBRyxPQUFLLGNBQWMsQ0FBQyxPQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUMvRCxZQUFJLEdBQUcsT0FBSyxjQUFjLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7QUFDbEQsWUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSzs7QUFFN0IsYUFBSSxZQUFZLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTs7O0FBR3RDLGtCQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBUSxFQUFLO0FBQ3JDLG1CQUFRLENBQUMsUUFBUSxDQUFDLElBQUksU0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7QUFDakUsa0JBQUssSUFBSSxHQUFHLE9BQUssYUFBYSxDQUFDO1dBQy9CLENBQUMsQ0FBQTs7O0FBR0YsaUJBQUssTUFBTSxHQUFHLEVBQUUsQ0FBQzs7O0FBR2pCLGlCQUFLLE9BQU8sR0FBRyxJQUFJLENBQUM7VUFDcEIsTUFBTTs7OztBQUdOLGtCQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQzFDLGtCQUFPLEVBQUUsQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLElBQUksQ0FBQTtXQUNwQyxDQUFDLENBQUM7O0FBRUgsY0FBSSxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtBQUN6QixrQkFBTTtXQUNOOzs7QUFHRCxpQkFBSyxPQUFPLEdBQUcsS0FBSyxDQUFDOztBQUVyQixrQkFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNyQyxtQkFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLFNBQU8sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDOztBQUVyRSxrQkFBSyxXQUFXLEVBQUUsQ0FBQztXQUNuQixDQUFDLENBQUE7VUFDRjtTQUNELENBQUMsQ0FBQTs7T0FDRjtNQUNELENBQUMsQ0FBQTtLQUVGLENBQUMsQ0FBQztJQUNILE1BQU07O0FBQ04sUUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDNUMsUUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQzdFLFFBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzFDLFFBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDOztBQUVsRCxZQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFFeEY7R0FDRDs7O1NBMERHLGNBQUMsR0FBRyxFQUFFOztBQUVULE9BQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0dBRXJEOzs7U0FFRyxjQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUU7QUFDakIsT0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLE9BQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO0FBQ2pDLFFBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO0FBQ25CLGdCQUFXLEVBQUUsTUFBTTtBQUNuQix3QkFBbUIsRUFBRSxJQUFJO0FBQ3pCLGdCQUFXLEVBQUUsMEJBQTBCO0FBQ3ZDLGVBQVUsRUFBRSxTQUFTLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUU7OztBQUV4RCxVQUFJLENBQUMsTUFBTSxHQUFHLFVBQUMsSUFBSSxFQUFFLElBQUksRUFBSztBQUM3QixXQUFJLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7QUFDdEQsV0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUM3QixZQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFLO0FBQzVDLGFBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO0FBQzdDLGFBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO0FBQ2xELFlBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2hCLGFBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDckIsQ0FBQyxDQUFBO0FBQ0YsWUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0FBQ3BCLFlBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztBQUNyQixZQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztBQUN2QixpQkFBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBRWpCLENBQUMsQ0FBQTtPQUVGLENBQUE7QUFDRCxVQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDekQsV0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUMvQixjQUFLLEtBQUssR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxVQUFDLEtBQUssRUFBSztBQUN2QyxlQUFPLEtBQUssQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDO1FBQ2xDLENBQUMsQ0FBQztPQUNILENBQUMsQ0FBQTs7QUFFRixVQUFJLENBQUMsV0FBVyxHQUFHLFlBQU07QUFDeEIsV0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7QUFDdkIsZ0JBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztPQUNqQixDQUFDO01BQ0Y7QUFDRCxpQkFBWSxFQUFFLFVBQVU7S0FDeEIsQ0FBQyxDQUFDO0lBQ0gsTUFBTTtBQUNOLFFBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM3QztHQUNEOzs7U0FFSyxpQkFBQyxHQUFHLEVBQUU7OztBQUNYLE9BQUksT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztBQUM3RCxPQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7O0FBRTlELE9BQUksT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQ3BDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FDYixPQUFPLENBQUMsd0NBQXdDLENBQUMsQ0FDakQsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUNoQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7O0FBSW5CLE9BQUksQ0FBQyxTQUFTLENBQ1osSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLFlBQU07QUFDL0MsV0FBSyxHQUFHLEdBQUcsU0FBUyxDQUFDO0FBQ3JCLFdBQUssWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN2QixDQUFDLENBQUM7O0FBRUosWUFBUyxTQUFTLEdBQUc7OztBQUNwQixRQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzs7QUFFdkIsV0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNoQyxTQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUs7O0FBRXRCLFVBQUksRUFBRSxDQUFDLFVBQVUsS0FBSyxHQUFHLENBQUMsR0FBRyxFQUFFO0FBQzlCLGNBQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQU07O0FBRTdCLG9CQUFZLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQ3RDLGNBQUssQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHLEVBQUUsRUFBRSxFQUFLO0FBQ3RCLGNBQUksR0FBRyxDQUFDLEdBQUcsS0FBSyxHQUFHLENBQUMsR0FBRyxFQUFFO0FBQ3hCLHVCQUFZLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQ25DLGdCQUFJLFNBQVMsR0FBRyxPQUFLLGNBQWMsQ0FBQyxPQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUNoRSxxQkFBUyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFVBQVUsRUFBSztBQUN4Qyx1QkFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFDLFFBQVEsRUFBRSxhQUFhLEVBQUs7QUFDM0Msa0JBQUksUUFBUSxDQUFDLFVBQVUsS0FBSyxHQUFHLENBQUMsR0FBRyxFQUFFO0FBQ3BDLHdCQUFTLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2VBQ2pDO2NBQ0QsQ0FBQyxDQUFDO2FBQ0gsQ0FBQyxDQUFDOztBQUVILG1CQUFLLEdBQUcsR0FBRyxJQUFJLENBQUM7QUFDaEIsbUJBQUssUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztBQUMzQixtQkFBSyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFFN0MsQ0FBQyxDQUFDO1dBQ0g7VUFDRCxDQUFDLENBQUM7U0FDSCxDQUFDLENBQUM7UUFDSCxDQUFDLENBQUM7T0FDSDtNQUNELENBQUMsQ0FBQztLQUNILENBQUMsQ0FBQztBQUNILFFBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCO0dBQ0Q7Ozs7O1NBRWEsd0JBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRTs7O0FBQzNCLE9BQUksT0FBTyxHQUFHLEtBQUssQ0FBQzs7OztBQUlwQixPQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQzs7O0FBR2YsT0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBSztBQUM1QyxRQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsRUFBRTtBQUN4QixZQUFLLFlBQVksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ2pDLFlBQU8sR0FBRyxJQUFJLENBQUM7S0FDZjtJQUNELENBQUMsQ0FBQTs7QUFFRixPQUFJLENBQUMsT0FBTyxFQUFFOztBQUViLFFBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzVCO0dBR0Q7OztTQUVhLDBCQUFHO0FBQ2hCLE9BQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUNsRSxXQUFPLElBQUksQ0FBQztJQUNaLE1BQ0k7QUFDSixXQUFPLEtBQUssQ0FBQztJQUNiO0dBQ0Q7OztTQUVlLDBCQUFDLEdBQUcsRUFBRTtBQUNyQixPQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7O0FBRTdELFVBQU8sQUFBQyxLQUFLLEdBQUksSUFBSSxHQUFHLEtBQUssQ0FBQTtHQUM3Qjs7O1NBZVUsdUJBQUc7O0FBRWIsT0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFBO0dBRXZEOzs7U0FPYSwwQkFBRzs7QUFFaEIsT0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFBO0dBRTFEOzs7U0FFWSx1QkFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRTs7OztBQUV4QyxPQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7O0FBRWhFLFlBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7OztBQUd0QyxRQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOztBQUUxQixTQUFJLElBQUksR0FBRyxPQUFLLGNBQWMsQ0FBQyxPQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztBQUN0RCxTQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQzlCLFdBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFLO0FBQzlCLGVBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztPQUN0RCxDQUFDLENBQUM7TUFDSCxDQUFDLENBQUM7OztBQUdILGFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFLO0FBQzFCLFVBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDOztBQUV4RCxjQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztBQUMxQixjQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO01BQ2xCLENBQUMsQ0FBQztLQUNILE1BQU07QUFDTixhQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBSztBQUMxQixVQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztBQUN4RCxVQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTs7QUFFakIsZUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7QUFDMUIsZUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztPQUNsQjtNQUNELENBQUMsQ0FBQztLQUNIO0lBQ0QsQ0FBQyxDQUFDO0dBQ0g7OztTQUVjLDJCQUFHOzs7QUFFakIsT0FBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztBQUM5QixPQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUM1QyxPQUFJLFFBQVEsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUE7QUFDNUUsT0FBSSxTQUFTLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUN0QixPQUFJLFdBQVcsRUFBRTs7QUFFaEIsUUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTs7O0FBRWhDLFVBQUksaUJBQWlCLEdBQUcsRUFBRSxDQUFDO0FBQzNCLFVBQUksWUFBWSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDMUMsa0JBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBSzs7QUFFN0IsV0FBSSxNQUFNLEdBQUcsRUFBRSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxJQUFJLGNBQWMsQ0FBQSxBQUFDLENBQUM7QUFDOUUsV0FBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsVUFBVSxJQUFJLEVBQUU7QUFDMUMsZUFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtRQUNsRCxDQUFDLENBQUM7QUFDSCxXQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxVQUFVLE1BQU0sRUFBRTtBQUNqRCxZQUFJLE9BQU8sQ0FBQTtBQUNYLGlCQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFDbEMsZ0JBQU8sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUM3QixDQUFDLENBQUE7QUFDRixlQUFPLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFBO1FBQzFELENBQUMsQ0FBQzs7QUFFSCxXQUFJLE1BQU0sRUFBRTtBQUNYLHlCQUFpQixDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDOUIsTUFDSSxJQUFJLE1BQU0sRUFBRTtBQUNoQix5QkFBaUIsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLE1BQ0ksSUFBSSxRQUFRLEVBQUU7QUFDbEIseUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUM3QixNQUNJO0FBQ0oseUJBQWlCLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztRQUMzQjs7QUFFRCxXQUFJO0FBQ0gsZUFBSyxJQUFJLEdBQUcsT0FBSyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBSyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUV2RSxDQUFDLE9BQU8sS0FBSyxFQUFFO0FBQ2YsZUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQjtPQUVELENBQUMsQ0FBQTs7S0FDRixNQUFNOztBQUVOLFNBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFBO0tBQ25FO0lBR0QsTUFBTTs7O0FBRU4sUUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFBO0lBQzlCO0dBQ0Q7OztTQUVTLG9CQUFDLFdBQVcsRUFBRTtBQUN2QixPQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUE7QUFDOUQsV0FBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLFFBQVEsRUFBRTtBQUMzQyxZQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQzNCLENBQUMsQ0FBQTtHQUNGOzs7T0FsSVcsZUFBRztBQUNkLFVBQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7R0FDN0I7OztPQUVRLGVBQUc7QUFDWCxVQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO0dBQzFCOzs7T0FFZ0IsZUFBRzs7QUFFbkIsVUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFBO0dBQ2hEOzs7T0FRbUIsZUFBRzs7QUFFdEIsVUFBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFBO0dBQ25EOzs7U0FqVWMsb0JBQUc7Ozs7QUFFakIsT0FBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO0FBQ3BDLFFBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUNwRixjQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsV0FBVyxFQUFLO0FBQzFDLFNBQUksT0FBTyxHQUFHLE9BQUssT0FBTyxDQUFDLEtBQUssQ0FBQTtBQUNoQyxTQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDbkUsU0FBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ2pFLFNBQUksV0FBVyxDQUFDLFFBQVEsRUFBRTtBQUN6QixVQUFJLE9BQU8sSUFBSSxNQUFNLEVBQUU7QUFDdEIsa0JBQVcsQ0FBQyxJQUFJLEdBQUcsT0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBQ3RDLGtCQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtBQUMzQixrQkFBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUE7QUFDM0Isa0JBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBTTtBQUM5QixZQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7QUFDakIsU0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7QUFDbEMsZUFBTyxPQUFPLENBQUMsTUFBTSxDQUFDO0FBQ3RCLGVBQUssYUFBYSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDM0MsZUFBSyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDckIsZUFBSyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQUssYUFBYSxDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFBO09BQ0Y7TUFDRDtLQUNELENBQUMsQ0FBQTtJQUNGLE1BQU07QUFDTixRQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUM7R0FFRDs7O1NBc0VjLGtCQUFDLEtBQUssRUFBRTs7QUFFdEIsT0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFDcEQsUUFBSSxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQzdCLFFBQUksRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUM3QixXQUFPLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkMsQ0FBQyxDQUFDOzs7QUFHSCxPQUFJLElBQUksR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsVUFBQyxHQUFHLEVBQUs7QUFDaEQsUUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEVBQUUsVUFBVSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0FBQzdELFFBQUksQ0FBQyxFQUFFO0FBQ04sWUFBTyxDQUFDLENBQUMsS0FBSyxDQUFDO0tBQ2Y7SUFDRCxDQUFDLENBQUM7O0FBRUgsT0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7O0FBRXZCLFVBQU8sSUFBSSxDQUFDO0dBQ1o7OztTQUVpQixxQkFBQyxLQUFLLEVBQUU7QUFDekIsT0FBSSxRQUFRLEdBQUcsRUFBRSxDQUFDOztBQUVsQixPQUFJLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTs7OztBQUk1QyxRQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQzs7O0FBRy9FLFFBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDOzs7QUFHakIsUUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDcEIsTUFBTTs7QUFFTixZQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRTtBQUNoRCxZQUFPLEVBQUUsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUE7S0FDMUMsQ0FBQyxDQUFDOztBQUVILFFBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7QUFDekIsWUFBTTtLQUNOOzs7QUFHRCxRQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzs7QUFFckIsUUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQzs7OztBQUl2RixRQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbkI7R0FDRDs7O1FBcFBJLFFBQVE7OztRQTZnQk4sUUFBUSxHQUFSLFFBQVE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDbmdCVixTQUFTO0FBQ0gsVUFETixTQUFTLENBQ0YsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFDdEUsWUFBWSxFQUFFLGFBQWEsRUFBRSxVQUFVLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFO3dCQUZ2RixTQUFTOztBQUdiLE1BQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBQzNCLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0FBQ3JCLE1BQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztBQUMxQyxNQUFJLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQTtBQUN4QixNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztBQUNqQyxNQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztBQUNuQyxNQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztBQUM3QixNQUFJLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUM7QUFDM0IsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUE7QUFDcEMsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDdkIsTUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7QUFDdkMsTUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7Ozs7O0FBS2YsTUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDN0MsTUFBSSxJQUFJLEVBQUU7QUFDVCxPQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7QUFDaEMsT0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0FBQ3hDLE9BQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7QUFDN0IsT0FBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztHQUNuQzs7QUFFRCxNQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDO0FBQ2xELE1BQUksS0FBSyxFQUFFO0FBQ1YsT0FBSSxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRTtBQUM3QixRQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFFLENBQUM7SUFDNUIsTUFBSztBQUNMLFFBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUcsRUFBRSxFQUFFLEVBQUUsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDL0M7R0FDRDtFQUdEOztjQXRDSSxTQUFTOztTQXdDTCxtQkFBQyxLQUFLLEVBQUUsSUFBSSxFQUFFOzs7O0FBSXRCLE9BQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQ2hDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUMvQixPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUNuQixFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7O0FBRWQsT0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7R0FDM0I7OztTQUVLLGdCQUFDLElBQUksRUFBRTtBQUNaLE9BQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdEMsT0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0FBQ3BCLE9BQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztHQUVyQjs7O1NBRUksZUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFFOztBQUVuQixPQUFJLElBQUksR0FBRyxJQUFJLENBQUM7O0FBRWhCLE9BQUksTUFBTSxDQUFDLE9BQU8sSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLE9BQU8sSUFBSSxDQUFDLEVBQUU7QUFDaEQsV0FBTTtJQUNOOztBQUVELE9BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7O0FBRXpCLE9BQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7O0FBRWhDLE9BQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUNyQixRQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7QUFHbEQsUUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDaEQsTUFBTTtBQUNOLFFBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7SUFDakQ7O0FBRUQsWUFBUyxTQUFTLENBQUMsUUFBUSxFQUFFOzs7QUFDNUIsUUFBSSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQzs7QUFFdkIsUUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFO0FBQ3JCLFVBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7O0FBRXJELFVBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDL0IsVUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0FBQ2xFLFVBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7QUFFbEIsV0FBSSxVQUFVLEdBQUcsTUFBSyxlQUFlLENBQUMsTUFBSyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMvRSxpQkFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFdBQVcsRUFBSztBQUMxQyxZQUFJLE9BQU8sR0FBRyxNQUFLLE9BQU8sQ0FBQyxLQUFLLENBQUE7QUFDaEMsWUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ25FLFlBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQzs7QUFFakUsWUFBSSxPQUFPLElBQUksTUFBTSxFQUFFO0FBQ3RCLG9CQUFXLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7QUFDakMsb0JBQVcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO0FBQzNCLG9CQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtBQUMzQixvQkFBVyxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQzlCLGNBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztBQUNqQixXQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztBQUNsQyxpQkFBTyxPQUFPLENBQUMsTUFBTSxDQUFDO0FBQ3RCLGdCQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1VBQzNDLENBQUMsQ0FBQTtTQUNGLE1BQU07QUFDTixlQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3hDO1FBRUQsQ0FBQyxDQUFBO09BQ0YsTUFBTTs7QUFFTixhQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ3hDOztBQUVELFVBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDOztBQUUzRCxhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO01BQ2xCLENBQUMsQ0FBQTtLQUNGLE1BQU07QUFDTixVQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDeEMsVUFBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDckMsU0FBSSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7QUFDNUMsU0FBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ3hDLFNBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0tBRTNEOztBQUVELFdBQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0FBQ3ZDLGFBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztBQUNyQyxRQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0FBQ3pCLFFBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3BDLFFBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZCOztBQUVELFlBQVMsUUFBUSxDQUFDLEtBQUssRUFBRTtBQUN4QixRQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ3RCLFdBQU8sQ0FBQyxLQUFLLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDL0MsYUFBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQ3JDLFFBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDekIsUUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDckM7R0FHRDs7O1NBRWUsbUJBQUMsSUFBSSxFQUFFO0FBQ3RCLE9BQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdEMsT0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0FBQ3BCLE9BQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztHQUVyQjs7O1FBMUpJLFNBQVM7OztRQThKUCxTQUFTLEdBQVQsU0FBUzs7Ozs7Ozs7QUN6S2pCLFlBQVksQ0FBQzs7Ozs7Ozs7O0lBQ1AsVUFBVTtBQUNKLFVBRE4sVUFBVSxDQUNILFdBQVcsaUJBQWtCLE1BQU0sRUFBRSxjQUFjLEVBQzlELFVBQVUsRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUN4RSxZQUFZLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUNuRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTs7O3dCQUp6QixVQUFVOztBQU1kLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDOzs7QUFHckIsTUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDOzs7QUFHM0IsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7OztBQUdyQixNQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDL0MsTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7O0FBRXZCLE1BQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDOztBQUVyQyxNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQzs7QUFFakMsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7O0FBRXJDLE1BQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFBOztBQUV0QyxNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQTs7QUFFaEMsTUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7O0FBRW5DLE1BQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDOztBQUUzQixNQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzs7O0FBR25CLGNBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7QUFFcEIsTUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDOztBQUVoQixhQUFXLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQzs7O0FBR2xDLFNBQU8sQ0FBQyxtQkFBbUIsR0FBRyxZQUFNO0FBQ25DLFNBQUssTUFBTSxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7OztBQUd2QyxTQUFLLFFBQVEsRUFBRSxDQUFDOztHQUVoQixDQUFBOzs7QUFHRCxNQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO0FBQ3ZDLE9BQUksQ0FBQyxNQUFNLEdBQUcsZUFBZSxDQUFBO0dBQzdCLE1BQU07QUFDTixPQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7R0FDdkM7OztBQUdELE1BQUksQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQzs7O0FBR2pDLE1BQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRSxvQkFBb0IsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDOztBQUVsRixNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsWUFBTTtBQUNsQyxVQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pCLFNBQUssYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDO0dBQ3JDLENBQUMsQ0FBQzs7QUFFSCxNQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsSUFBSSxFQUFFLG1CQUFtQixFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQTtFQUNqRzs7Y0FyRUksVUFBVTs7U0ErRVAsb0JBQUc7Ozs7QUFHVixPQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7OztBQUVyQixTQUFJLFdBQVcsR0FBRyxPQUFLLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7O0FBRy9DLFlBQUssY0FBYyxDQUFDLE1BQU0sQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN4RCxhQUFLLE1BQU0sR0FBRyxNQUFNLENBQUM7OztBQUdyQixVQUFJLEtBQUssR0FBRyxPQUFLLGNBQWMsQ0FBQyxPQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUN6RCxhQUFLLEtBQUssR0FBRyxLQUFLLENBQUM7QUFDbkIsV0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQUssRUFBSztBQUMvQixXQUFJLElBQUksR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7QUFDckUsV0FBSSxDQUFDLE9BQUssTUFBTSxDQUFDLElBQUksRUFBRTtBQUN0QixlQUFLLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztBQUM3QixlQUFLLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUM5QjtBQUNELGNBQUssUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7QUFDOUIsY0FBSyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQUssTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQy9DLGVBQUssYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUM7O0FBRUgsY0FBSyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7T0FDekIsQ0FBQyxDQUFBOzs7OztBQUtGLGFBQUssV0FBVyxFQUFFLENBQUM7OztBQUduQixnQkFBVSxDQUFDLFlBQU07O0FBRWhCLGlCQUFVLENBQUMsV0FBVyxDQUFDLElBQUksUUFBTSxDQUFDO09BQ2xDLEVBQUUsQ0FBQyxDQUFDLENBQUM7OztBQUdOLGFBQUssYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDO01BRXZDLENBQUMsQ0FBQTs7SUFDRixNQUFNOzs7O0FBSU4sUUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNLEVBQUs7O0FBRTVDLFlBQUssTUFBTSxHQUFHLE1BQU0sQ0FBQzs7O0FBR3JCLFNBQUksSUFBSSxHQUFHLE9BQUssYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFBOzs7QUFHL0MsU0FBSSxLQUFLLEdBQUcsT0FBSyxhQUFhLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQzVDLFlBQUssS0FBSyxHQUFHLEtBQUssQ0FBQzs7QUFFbkIsU0FBSSxDQUFDLE9BQUssTUFBTSxDQUFDLElBQUksRUFBRTtBQUN0QixhQUFLLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztBQUM3QixhQUFLLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztNQUM5Qjs7QUFFRCxZQUFLLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0FBQzlCLFlBQUssU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUMvQyxhQUFLLGFBQWEsR0FBRyxJQUFJLENBQUM7TUFDMUIsQ0FBQyxDQUFDOzs7QUFHSCxZQUFLLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBSyxNQUFNLENBQUMsQ0FBQzs7O0FBR3ZDLGVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxRQUFNLENBQUM7OztBQUdsQyxZQUFLLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUN6QixDQUFDLENBQUE7SUFDRjtHQUNEOzs7U0FFUSxtQkFBQyxJQUFJLEVBQUU7OztBQUNmLE9BQUksS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUN6RCxRQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFJO0FBQzlCLFFBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7QUFDL0MsV0FBSyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUM7SUFDL0IsQ0FBQyxDQUFBO0dBQ0Y7OztTQUVRLHFCQUFHO0FBQ1gsT0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztHQUNyQjs7O1NBRUksaUJBQUc7O0FBQ1AsT0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdkIsT0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQztBQUN2QixPQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO0dBQzlCOzs7U0FFTyxvQkFBRztBQUNWLE9BQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7QUFDbEQsT0FBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0dBQ2I7OztTQUVJLGlCQUFHO0FBQ1AsT0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztBQUNqRCxPQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQztBQUNoRixPQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0dBQ2xCOzs7U0FFVSx1QkFBRzs7O0FBRWIsT0FBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFNO0FBQUUsV0FBTyxPQUFLLE1BQU0sQ0FBQTtJQUFFLEVBQ3hELFVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBSzs7O0FBR1QsUUFBSSxnQkFBZ0IsR0FBRyxDQUN0QixFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUVyQyxDQUFDOzs7QUFHRixvQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUs7QUFDckMsU0FBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO0FBQ3BCLEFBQUMsT0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFJLE9BQUssT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssR0FBRyxPQUFLLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUE7TUFDckYsTUFBTTtBQUNOLGFBQUssT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQztNQUMxQjtLQUNELENBQUMsQ0FBQzs7O0FBR0gsV0FBSyxPQUFPLEdBQUksT0FBSyxLQUFLLElBQUksT0FBSyxLQUFLLEFBQUMsQ0FBQztBQUMxQyxXQUFLLE9BQU8sR0FBSSxPQUFLLEtBQUssSUFBSSxPQUFLLEtBQUssQUFBQyxDQUFDO0FBQzFDLFdBQUssT0FBTyxHQUFJLE9BQUssS0FBSyxJQUFJLE9BQUssS0FBSyxBQUFDLENBQUM7OztBQUkxQyxXQUFLLE9BQU8sR0FBSSxPQUFLLEtBQUssQUFBQyxDQUFDO0FBQzVCLFdBQUssT0FBTyxHQUFJLE9BQUssS0FBSyxBQUFDLENBQUM7QUFDNUIsV0FBSyxPQUFPLEdBQUksT0FBSyxLQUFLLEFBQUMsQ0FBQztJQUc1QixDQUFDLENBQUM7R0FDSjs7O1NBOUppQix1QkFBRzs7O0FBR3BCLE9BQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzVDLE9BQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUM7QUFDNUIsT0FBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztHQUM1Qjs7O1FBN0VJLFVBQVU7OztRQXdPUixVQUFVLEdBQVYsVUFBVTs7Ozs7Ozs7Ozs7Ozs7OztJQ3pPWixXQUFXLEdBQ0wsU0FETixXQUFXLEdBQ0g7d0JBRFIsV0FBVztDQUdmOztRQUdLLFdBQVcsR0FBWCxXQUFXOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDTFosY0FBYztBQUNSLFVBRE4sY0FBYyxDQUNQLFdBQVcsRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQ3ZFLGNBQWMsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFDcEQsV0FBVyxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUU7Ozt3QkFIbEUsY0FBYzs7O0FBTWxCLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDOzs7QUFHckIsTUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDOzs7QUFHM0IsTUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9DLE1BQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDOztBQUV2QixNQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQzs7QUFFckMsTUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7O0FBRWpDLE1BQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDOztBQUVyQyxNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQTs7QUFFaEMsTUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7O0FBRW5DLE1BQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDOztBQUVyQixNQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQzs7QUFFM0IsTUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7OztBQUduQixjQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7O0FBRXBCLE1BQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7QUFFaEIsYUFBVyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7O0FBRWxDLFNBQU8sQ0FBQyxtQkFBbUIsR0FBRyxZQUFNO0FBQ25DLFNBQUssTUFBTSxHQUFHLFdBQVcsQ0FBQyxZQUFZLENBQUM7QUFDdkMsU0FBSyxRQUFRLEVBQUUsQ0FBQztHQUNoQixDQUFBOztBQUVELE1BQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7QUFDdkMsT0FBSSxDQUFDLE1BQU0sR0FBRyxlQUFlLENBQUE7R0FDN0IsTUFBTTtBQUNOLE9BQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztHQUN2Qzs7QUFFRCxNQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsWUFBTTtBQUNsQyxVQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pCLFNBQUssYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDO0dBQ3JDLENBQUMsQ0FBQztFQUNIOztjQXJESSxjQUFjOztTQThEWCxvQkFBRzs7OztBQUVWLE9BQUksU0FBUyxDQUFDLE1BQU0sRUFBRTs7QUFFckIsUUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN4RCxZQUFLLE1BQU0sR0FBRyxNQUFNLENBQUM7O0FBRXJCLFlBQUssY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNoRCxhQUFLLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0FBQ3hCLFVBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxJQUFJLEVBQUU7QUFDdEIsY0FBSyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO09BQ2xDO0FBQ0QsYUFBSyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7O0FBRW5DLGFBQUssWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO01BQ3pCLENBQUMsQ0FBQTs7O0FBR0YsWUFBSyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQUssTUFBTSxDQUFDLENBQUM7O0FBRXZDLGVBQVUsQ0FBQyxZQUFNOztBQUVoQixvQkFBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLFFBQU0sQ0FBQztNQUN0QyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ04sQ0FBQyxDQUFBO0lBQ0YsTUFBTTtBQUNOLFFBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNLEVBQUs7O0FBRXZELFlBQUssTUFBTSxHQUFHLE1BQU0sQ0FBQzs7O0FBR3JCLFNBQUksSUFBSSxHQUFHLE9BQUssYUFBYSxDQUFDLGVBQWUsRUFBRSxDQUFDOzs7QUFHaEQsU0FBSSxLQUFLLEdBQUcsT0FBSyxhQUFhLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQzVDLFlBQUssS0FBSyxHQUFHLEtBQUssQ0FBQzs7O0FBR25CLFNBQUksQ0FBQyxPQUFLLE1BQU0sQ0FBQyxJQUFJLEVBQUU7QUFDdEIsYUFBSyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7QUFDN0IsYUFBSyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7TUFDOUI7OztBQUdELFlBQUssYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFLLE1BQU0sQ0FBQyxDQUFDOztBQUV2QyxtQkFBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLFFBQU0sQ0FBQzs7O0FBR3RDLFlBQUssWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0tBQ3pCLENBQUMsQ0FBQTtJQUNGO0dBQ0Q7OztTQUVRLHFCQUFHO0FBQ1gsT0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztHQUNyQjs7O1NBRUksaUJBQUc7QUFDUCxPQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUN2QixPQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDO0FBQ3ZCLE9BQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7R0FDOUI7OztTQUVPLG9CQUFHO0FBQ1YsT0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztBQUNsRCxPQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7R0FDYjs7O1NBRUksaUJBQUc7QUFDUCxPQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ2pELE9BQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQUUsQ0FBQztBQUMxRSxPQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0dBRWxCOzs7U0FqRmlCLHVCQUFHOztBQUVwQixPQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QyxPQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDO0FBQzVCLE9BQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUM7R0FDNUI7OztRQTVESSxjQUFjOzs7UUEwSVosY0FBYyxHQUFkLGNBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQzNJaEIsVUFBVTtBQUNKLFVBRE4sVUFBVSxHQUNEO3dCQURULFVBQVU7OztBQUdkLE1BQUksQ0FBQyxRQUFRLEdBQUcsQUFBQyxJQUFJLElBQUksRUFBRSxDQUFFLGlCQUFpQixFQUFFLEdBQUcsS0FBSyxDQUFDO0FBQ3pELE1BQUksQ0FBQyxZQUFZLEdBQUcsQUFBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFFLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUN0Rjs7Y0FMSSxVQUFVOztPQU9OLGVBQUc7QUFDWCxVQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7R0FDekI7OztPQUVXLGVBQUc7OztBQUdkLE9BQUksS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUE7QUFDL0IsV0FBUSxLQUFLO0FBQ1osU0FBSyxDQUFDO0FBQ0wsWUFBTyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUN4RSxXQUFNO0FBQUEsQUFDUCxTQUFLLENBQUM7QUFDTCxZQUFPLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBQ3hFLFdBQU07QUFBQSxBQUNQLFNBQUssQ0FBQztBQUNMLFlBQU8sTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDeEUsV0FBTTtBQUFBLEFBQ1A7QUFDQyxZQUFPLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBQ3hFLFdBQU07QUFBQSxJQUNQO0dBQ0Q7OztRQTdCSSxVQUFVOzs7UUFnQ1IsVUFBVSxHQUFWLFVBQVU7Ozs7Ozs7O0FDaENsQixZQUFZLENBQUM7Ozs7Ozs7SUFDUCxlQUFlLEdBQ1QsU0FETixlQUFlLENBQ1IsV0FBVyxFQUFFLGFBQWEsRUFBQzt1QkFEbEMsZUFBZTs7QUFFbkIsS0FBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDO0FBQ25DLFFBQU8sYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztDQUN2Qzs7Ozs7OztRQU9LLGVBQWUsR0FBZixlQUFlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNaaEIsWUFBWTtBQUNOLFVBRE4sWUFBWSxDQUNMLFdBQVcsRUFBRSxlQUFlLEVBQUUsRUFBRSxFQUFFLGFBQWEsRUFBRTt3QkFEeEQsWUFBWTs7O0FBR2hCLE1BQUksQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQztBQUMzQixNQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztBQUN2QyxNQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztBQUNiLE1BQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0VBQ25DOztjQVBJLFlBQVk7O1NBU1IsbUJBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRTtBQUN6QixPQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7QUFDckIsUUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7QUFDL0UsT0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQUcsRUFBSztBQUMzQixTQUFJLE1BQU0sS0FBSyxVQUFVLEVBQUU7QUFDMUIsU0FBRyxDQUFDLGFBQWEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO01BQzdDO0FBQ0QsUUFBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDcEIsUUFBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ1osQ0FBQyxDQUFBO0lBQ0YsTUFBTTtBQUNOLFFBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxFQUFDLEtBQUssRUFBQyxJQUFJLEVBQUMsTUFBTSxFQUFDLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDN0Q7R0FDRDs7O1NBRUcsY0FBQyxNQUFNLEVBQUU7OztBQUNaLFVBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUs7QUFDbkMsUUFBSSxHQUFHLEdBQUcsTUFBSyxlQUFlLENBQUMsTUFBSyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUMvRSxPQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBRyxFQUFLO0FBQzNCLFlBQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7S0FDOUQsQ0FBQyxDQUFBO0lBQ0YsQ0FBQyxDQUFBO0dBQ0Y7OztRQS9CSSxZQUFZOzs7UUFpQ1YsWUFBWSxHQUFaLFlBQVk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNoQ2QsY0FBYztBQUNSLFVBRE4sY0FBYyxDQUNQLFdBQVcsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFDNUQsZUFBZSxFQUFFLEVBQUUsRUFBRSxhQUFhLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRTt3QkFGMUQsY0FBYzs7QUFHbEIsTUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7QUFDL0IsTUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsTUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7QUFDakMsTUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7QUFDckMsTUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7QUFDdkMsTUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7QUFDYixNQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztBQUNuQyxNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQzs7QUFFakMsTUFBSSxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDO0VBQzNCOztjQWJJLGNBQWM7O1NBZWIsZ0JBQUMsTUFBTSxFQUFFOzs7QUFFZCxVQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFLOzs7QUFHbkMsUUFBSSxFQUFFLEdBQUcsTUFBSyxZQUFZLENBQUMsRUFBRSxDQUFDOzs7QUFHOUIsUUFBSSxPQUFPLEdBQUcsTUFBSyxjQUFjLENBQUMsTUFBSyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7OztBQUc3RCxXQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLOztBQUVoQyxTQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUs7O0FBRXRCLFVBQUksRUFBRSxDQUFDLFVBQVUsS0FBSyxFQUFFLEVBQUU7O0FBRXpCLFdBQUksTUFBTSxHQUFHLE1BQUssZUFBZSxDQUFDLE1BQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7O0FBRTNFLGFBQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFNO0FBQzNDLGVBQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdkIsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7T0FlSDtNQUNELENBQUMsQ0FBQTtLQUNGLENBQUMsQ0FBQTtJQUNGLENBQUMsQ0FBQztHQUNIOzs7U0FFYywyQkFBRTtBQUNoQixPQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7R0FDZjs7O1NBRVUsdUJBQUc7OztBQUNiLFVBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUs7QUFDbkMsUUFBSSxXQUFXLEdBQUcsT0FBSyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7QUFDL0MsUUFBSSxLQUFLLEdBQUcsT0FBSyxjQUFjLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDekQsV0FBSyxLQUFLLEdBQUcsS0FBSyxDQUFBO0FBQ2xCLFNBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxLQUFLLEVBQUs7QUFDL0IsU0FBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0FBQ3JFLFlBQU8sQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7S0FFdEMsQ0FBQyxDQUFBO0lBQ0YsQ0FBQyxDQUFDO0dBQ0g7OztTQUVJLGlCQUFHOzs7QUFDUCxVQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFLOzs7QUFHbkMsUUFBSSxFQUFFLEdBQUcsT0FBSyxZQUFZLENBQUMsRUFBRSxDQUFDOztBQUU5QixRQUFJLE9BQU8sR0FBRyxPQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7OztBQUdoRCxXQUFPLENBQUMsR0FBRyxDQUFDLFVBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUs7O0FBRXpCLFNBQUksRUFBRSxDQUFDLFVBQVUsS0FBSyxFQUFFLEVBQUU7O0FBRXpCLGdCQUFVLENBQUMsWUFBTTtBQUNoQixjQUFPLENBQUMsRUFBRSxDQUFDLENBQUE7T0FDWCxFQUFFLENBQUMsQ0FBQyxDQUFDO01BSU47S0FDRCxDQUFDLENBQUE7SUFDRixDQUFDLENBQUE7R0FFRjs7O1FBakdJLGNBQWM7OztRQW9HWixjQUFjLEdBQWQsY0FBYzs7Ozs7Ozs7QUNyR3RCLFlBQVksQ0FBQzs7Ozs7Ozs7O0lBQ1AsZUFBZTtBQUNULFVBRE4sZUFBZSxDQUNSLFNBQVMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFO3dCQURoQyxlQUFlOztBQUVuQixNQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtBQUMxQixNQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztBQUNiLE1BQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0VBQ3pCOztjQUxJLGVBQWU7O1NBT2hCLGdCQUFHOztBQUVOLE9BQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO0FBQ25CLHVCQUFtQixFQUFFLEtBQUs7QUFDMUIsWUFBUSxFQUFFLDBDQUEwQyxHQUNwRCwrREFBK0QsR0FDL0QsMkNBQTJDLEdBQzNDLHdGQUF3RixHQUN4RixrQ0FBa0M7SUFDbEMsQ0FBQyxDQUFDO0dBQ0g7OztTQUVHLGdCQUFHOzs7QUFDTixPQUFJLENBQUMsUUFBUSxDQUFDLFlBQU07QUFDbkIsVUFBSyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEIsRUFBRSxDQUFDLENBQUMsQ0FBQTtHQUNMOzs7UUF2QkksZUFBZTs7O1FBeUJiLGVBQWUsR0FBZixlQUFlOzs7Ozs7OztBQzFCdkIsWUFBWSxDQUFDOzs7Ozs7Ozs7SUFDUCxnQkFBZ0I7QUFDVixVQUROLGdCQUFnQixDQUNULFlBQVksRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFDckUsRUFBRSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFO3dCQUZoQyxnQkFBZ0I7OztBQUlwQixNQUFJLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUM7OztBQUczQixNQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQzs7O0FBR3JDLE1BQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDOztBQUV2QyxNQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQTs7QUFFWixNQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQzs7QUFFN0IsTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7O0FBRXZCLE1BQUksQ0FBQyxPQUFPLENBQUM7OztBQUdiLE1BQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxFQUFFO0FBQ2xDLE9BQUksQ0FBQyxvQkFBb0IsR0FBRyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7R0FDckQsTUFBTTtBQUNOLE9BQUksQ0FBQyxvQkFBb0IsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRTtBQUNoRSxpQkFBYSxFQUFFLFVBQVU7SUFFekIsQ0FBQyxDQUFBO0dBQ0Y7RUFDRDs7Y0E3QkksZ0JBQWdCOzs7U0ErQlgsc0JBQUc7OztBQUVaLE9BQUksS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQTtBQUN4RCxRQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQy9CLFVBQUssb0JBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5QyxDQUFDLENBQUM7O0FBRUgsT0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO0FBQ3JELE9BQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDN0IsVUFBSyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUMsQ0FBQzs7QUFFSCxPQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUE7QUFDOUQsV0FBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBSztBQUNyQyxVQUFLLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQyxDQUFDOztBQUVILE9BQUksS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQTtBQUN4RCxRQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFLO0FBQy9CLFVBQUssb0JBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5QyxDQUFDLENBQUE7O0FBRUYsT0FBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBO0FBQzVELFVBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPLEVBQUs7Ozs7O0FBS25DLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQzs7Ozs7O0FBTWxCLFFBQUksY0FBYyxHQUFHLEtBQUssQ0FBQzs7Ozs7QUFLM0IsUUFBSSxJQUFJLEdBQUcsTUFBSyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7O0FBSzVCLFFBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQUMsR0FBRyxFQUFLOztBQUUvQixTQUFJLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBSyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzVFLFNBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDbkMsWUFBTyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUN0RCxDQUFDLENBQUM7OztBQUdILFNBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFOztBQUVwRCxTQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7O0FBRTdCLFNBQUksY0FBYyxFQUFFO0FBQ25CLGNBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7TUFDdkI7Ozs7QUFJRCxTQUFJLE9BQU8sQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDLEdBQUcsRUFBRTtBQUNuQyxvQkFBYyxHQUFHLElBQUksQ0FBQztNQUN0QjtLQUNEOztBQUVELFVBQUssb0JBQW9CLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUVuRCxDQUFDLENBQUE7R0FFRjs7O1NBRUUsYUFBQyxFQUFFLEVBQUUsSUFBSSxFQUFFO0FBQ2IsT0FBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUE7R0FDdkM7OztTQUVLLGdCQUFDLEVBQUUsRUFBRTtBQUNWLE9BQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUE7R0FDcEM7OztTQUVFLGFBQUMsRUFBRSxFQUFFO0FBQ1AsVUFBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0dBQ3pDOzs7U0FFZ0IsMkJBQUMsSUFBSSxFQUFFOzs7QUFDdkIsT0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDOztBQUVoQixVQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBQyxHQUFHLEVBQUUsR0FBRyxFQUFLO0FBQzVCLFFBQUksS0FBSyxHQUFHLE9BQUssR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQzlCLFFBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0FBQzlGLFdBQUssZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzdCLFFBQUksTUFBTSxFQUFFO0FBQ1gsUUFBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ1osTUFBTTtBQUNOLFFBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSx3QkFBd0IsRUFBRSxDQUFDLENBQUM7S0FDeEM7SUFFRCxDQUFDLENBQUM7R0FFSDs7O1NBRWMseUJBQUMsSUFBSSxFQUFFO0FBQ3JCLFVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDbEIsT0FBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7R0FDOUI7OztTQUVjLDJCQUFHO0FBQ2pCLFVBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQTtHQUM5Qjs7O1NBRU8sb0JBQUc7OztBQUNWLFVBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUs7QUFDNUIsUUFBSSxPQUFPLEdBQUcsT0FBSyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7O0FBRWpDLFFBQUksT0FBTyxJQUFJLE9BQU8sRUFBRTtBQUN2QixRQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDVCxNQUFNO0FBQ04sWUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNyQixRQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7S0FDVjtJQUVELENBQUMsQ0FBQTtHQUNGOzs7U0FFTyxrQkFBQyxJQUFJLEVBQUU7QUFDZCxPQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztHQUMxQjs7O1NBRU0sbUJBQUc7QUFDVCxPQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ3ZCLE9BQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDdkIsT0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUMzQixPQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0dBQ3BCOzs7U0FFSSxlQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUU7OztBQUdwQixPQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFOzs7OztBQUt6QixRQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7OztBQUs1QixRQUFJLE1BQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQzs7Ozs7QUFLMUQsUUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7QUFDeEQsUUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtBQUNoQyxRQUFHLENBQUMsYUFBYSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7S0FDN0M7O0FBRUQsT0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0FBQzFCLGFBQVM7O0FBRVQsUUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFLLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDOztBQUUzQixRQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztBQUN2QixXQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztBQUM5QixXQUFNO0lBQ047O0FBR0QsT0FBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztBQUNsQyxPQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQzs7QUFFdEQsT0FBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7QUFDakIsV0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNyQixNQUFNO0FBQ04sV0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDOztBQUVELE9BQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0dBQzdCOzs7U0FFSyxnQkFBQyxNQUFNLEVBQUU7OztBQUNkLE9BQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZO0FBQUUsV0FBTyxNQUFNLENBQUE7SUFBRSxFQUFFLFVBQUMsQ0FBQyxFQUFFLENBQUMsRUFBSztBQUN4RixRQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7QUFDWixZQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2YsTUFBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7QUFDaEIsWUFBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FDZDtJQUNELENBQUMsQ0FBQztHQUNIOzs7U0FFTyxvQkFBRztBQUNWLE9BQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztHQUNmOzs7U0FFYywyQkFBRzs7O0FBQ2pCLE9BQUksT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDbEMsT0FBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QixPQUFJLGVBQWUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQzFELE9BQUksWUFBWSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7O0FBRXBELGtCQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUs7QUFDeEMsUUFBSSxNQUFNLEdBQUcsT0FBSyxlQUFlLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMxRSxXQUFPLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDaEIsU0FBSyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7QUFDbEIsU0FBSSxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQzFCLFlBQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7TUFFckI7S0FDRDtBQUNELFVBQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBTTtBQUN6QixZQUFLLFVBQVUsRUFBRSxDQUFDO0tBQ2xCLENBQUMsQ0FBQTtJQUVGLENBQUMsQ0FBQTs7QUFFRixlQUFZLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUs7QUFDckMsUUFBSSxHQUFHLEdBQUcsT0FBSyxlQUFlLENBQUMsT0FBSyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztBQUMzRSxXQUFPLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDaEIsU0FBSyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUU7QUFDbEIsU0FBSSxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQzFCLFNBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7TUFFbEI7S0FDRDtBQUNELE9BQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBTTtBQUN0QixZQUFLLFVBQVUsRUFBRSxDQUFDO0tBQ2xCLENBQUMsQ0FBQTtJQUVGLENBQUMsQ0FBQTs7QUFFRixPQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7R0FDbEI7OztPQUVxQixhQUFDLEtBQUssRUFBRTs7Ozs7QUFLN0IsT0FBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7R0FDekI7T0FFcUIsZUFBRztBQUN4QixVQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7R0FDekI7OztRQXBSSSxnQkFBZ0I7OztRQXNSZCxnQkFBZ0IsR0FBaEIsZ0JBQWdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUN2UmxCLGNBQWM7QUFDUixVQUROLGNBQWMsQ0FDUCxPQUFPLEVBQUUsVUFBVSxFQUFFLEVBQUUsRUFBRTt3QkFEaEMsY0FBYzs7QUFFbEIsTUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDdkIsTUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7RUFDN0I7O2NBSkksY0FBYzs7T0FNTCxhQUFDLE1BQU0sRUFBRTtBQUN0QixPQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztHQUNyQjs7O09BRWUsZUFBRztBQUNsQixPQUFJLE1BQU0sR0FBRyxTQUFTLENBQUM7QUFDdkIsT0FBSSxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBQzFCLE9BQUksUUFBUSxHQUFHLFNBQVMsQ0FBQztBQUN6QixXQUFRLElBQUksQ0FBQyxNQUFNO0FBQ2xCLFNBQUssUUFBUTtBQUNaLGNBQVMsR0FBRyxrQkFBa0IsQ0FBQTtBQUM5QixhQUFRLEdBQUcsaUJBQWlCLENBQUE7QUFDNUIsV0FBTTtBQUFBLEFBQ1AsU0FBSyxLQUFLO0FBQ1QsY0FBUyxHQUFHLGVBQWUsQ0FBQTtBQUMzQixhQUFRLEdBQUcsY0FBYyxDQUFBO0FBQ3pCLFdBQU07QUFBQSxBQUNQLFNBQUssV0FBVztBQUNmLGNBQVMsR0FBRyxzQkFBc0IsQ0FBQTtBQUNsQyxhQUFRLEdBQUcscUJBQXFCLENBQUE7QUFDaEMsV0FBTTtBQUFBLEFBQ1AsU0FBSyxPQUFPO0FBQ1gsY0FBUyxHQUFHLGlCQUFpQixDQUFBO0FBQzdCLGFBQVEsR0FBRyxnQkFBZ0IsQ0FBQTtBQUMzQixXQUFNOztBQUFBLEFBRVA7QUFDQyxjQUFTLEdBQUcsV0FBVyxDQUFBO0FBQ3ZCLGFBQVEsR0FBRyxVQUFVLENBQUE7QUFDckIsV0FBTTtBQUFBLElBQ1A7O0FBRUQsT0FBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLElBQUksR0FBRyxFQUFFO0FBQ3RFLFVBQU0sR0FBRyxRQUFRLENBQUM7SUFDbEIsTUFBTTtBQUNOLFVBQU0sR0FBRyxTQUFTLENBQUM7SUFDbkI7O0FBRUQsVUFBTyxNQUFNLENBQUM7R0FDZDs7O1FBN0NJLGNBQWM7OztRQStDWixjQUFjLEdBQWQsY0FBYzs7Ozs7Ozs7QUMvQ3RCLFlBQVksQ0FBQzs7Ozs7Ozs7O0lBQ1AsYUFBYTtBQUNQLFVBRE4sYUFBYSxHQUNKO3dCQURULGFBQWE7RUFHakI7O2NBSEksYUFBYTs7T0FLUixlQUFHO0FBQ1osVUFBTyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUE7R0FDN0I7OztPQUVTLGVBQUc7QUFDWixVQUFPLENBQ04sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdkMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdEMsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUM5QyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN2QyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUMxQyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUMzQyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxzQkFBc0IsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQ3BELEVBQUUsSUFBSSxFQUFFLGdDQUFnQyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDOUQsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdkMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdkMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDcEMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdEMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDckMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDeEMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdkMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDcEMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdEMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDeEMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDekMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDckMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUNoRCxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUM3QyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN6QyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUMzQyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN2QyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN0QyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUM3QyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUMxQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUMxQyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN4QyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQzlDLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQzVDLEVBQUUsSUFBSSxFQUFFLDBCQUEwQixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDeEQsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDcEMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDeEMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDdEMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDckMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDNUMsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDM0MsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsRUFDNUMsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUM5QyxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUM1QyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN6QyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUNyQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUNwQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxFQUN2QyxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQzlDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQ3hDLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQzFDLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQzdDLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQ3pDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQ3ZDLENBQUM7R0FDRjs7O1NBRVksa0JBQUc7QUFDZixVQUFPLENBQUM7QUFDUCxRQUFJLEVBQUUseUJBQXlCO0FBQy9CLFNBQUssRUFBRSxrQkFBa0I7SUFDekIsRUFBRTtBQUNELFFBQUksRUFBRSxzQkFBc0I7QUFDNUIsU0FBSyxFQUFFLE9BQU87SUFDZCxFQUFFO0FBQ0YsUUFBSSxFQUFFLGNBQWM7QUFDcEIsU0FBSyxFQUFFLE9BQU87SUFDZCxFQUFFO0FBQ0YsUUFBSSxFQUFFLG9CQUFvQjtBQUMxQixTQUFLLEVBQUUsVUFBVTtJQUNqQixFQUFFO0FBQ0YsUUFBSSxFQUFFLG1CQUFtQjtBQUN6QixTQUFLLEVBQUUsUUFBUTtJQUNmLEVBQUU7QUFDRixRQUFJLEVBQUUsa0JBQWtCO0FBQ3hCLFNBQUssRUFBRSxVQUFVO0lBQ2pCLEVBQUU7QUFDRixRQUFJLEVBQUUsYUFBYTtBQUNuQixTQUFLLEVBQUUsUUFBUTtJQUNmLEVBQUU7QUFDRixRQUFJLEVBQUUsMkJBQTJCO0FBQ2pDLFNBQUssRUFBRSxXQUFXO0lBQ2xCLEVBQUU7QUFDRixRQUFJLEVBQUUsY0FBYztBQUNwQixTQUFLLEVBQUUsU0FBUztJQUNoQixFQUFFO0FBQ0YsUUFBSSxFQUFFLHdCQUF3QjtBQUM5QixTQUFLLEVBQUUsV0FBVztJQUNsQixFQUFFO0FBQ0YsUUFBSSxFQUFFLGtCQUFrQjtBQUN4QixTQUFLLEVBQUUsUUFBUTtJQUNmLEVBQUU7QUFDRixRQUFJLEVBQUUsWUFBWTtBQUNsQixTQUFLLEVBQUUsT0FBTztJQUNkLENBQUMsQ0FBQztHQUNKOzs7UUEvR0ksYUFBYTs7O1FBa0hYLGFBQWEsR0FBYixhQUFhOzs7Ozs7OztBQ25IckIsWUFBWSxDQUFDOzs7Ozs7Ozs7SUFDUCxRQUFRO0FBQ0YsVUFETixRQUFRLEdBQ0M7d0JBRFQsUUFBUTtFQUdaOzs7Ozs7O2NBSEksUUFBUTs7U0FjSixxQkFBRztBQUNYLFdBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQztHQUM1RDs7O1NBRUcsZ0JBQUc7QUFDTixTQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7R0FDZjs7O09BVlksYUFBQyxJQUFJLEVBQUU7QUFDbkIsV0FBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztHQUMxRTs7O1FBWkksUUFBUTs7O1FBdUJOLFFBQVEsR0FBUixRQUFROzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ3RCVixVQUFVO0FBQ0osVUFETixVQUFVLENBQ0gsVUFBVSxFQUFFO3dCQURuQixVQUFVOztBQUVkLE1BQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0VBQzdCOztjQUhJLFVBQVU7O1NBS1gsY0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFOzs7QUFDcEIsT0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsT0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7QUFDckIsT0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7QUFDNUMsU0FBSyxFQUFFLGlCQUFNO0FBQ1osV0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDakIsV0FBSyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7S0FDekI7SUFDRCxDQUFDLENBQUM7R0FDSDs7O1NBRVcsd0JBQUc7Ozs7QUFJZCxPQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDdEQsT0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0FBQ3BELE9BQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztBQUN0RCxPQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0dBQ2pEOzs7U0FFRyxjQUFDLE1BQU0sRUFBRTtBQUNaLE9BQUksSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUM7QUFDekMsU0FBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7QUFDeEIsVUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztHQUNsQjs7O1NBRUksaUJBQUc7QUFDUCxPQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLE9BQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztHQUMzQjs7O1NBRVcsd0JBQUc7QUFDZCxPQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFO0FBQzFCLFFBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckQ7R0FDRDs7O1NBRWdCLDZCQUFHO0FBQ25CLE9BQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUM7QUFDeEIsT0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0FBQ3pCLE9BQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztHQUMxQjs7O1FBL0NJLFVBQVU7OztRQWlEUixVQUFVLEdBQVYsVUFBVTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ2xEWixRQUFRO0FBQ0YsVUFETixRQUFRLENBQ0QsUUFBUSxFQUFDO3dCQURoQixRQUFROztBQUVaLE1BQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0VBQ3pCOztjQUhJLFFBQVE7O1NBS0osbUJBQUMsS0FBSyxFQUFFO0FBQ2hCLE9BQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxJQUFJLEdBQUcsb0JBQW9CLENBQUE7QUFDbkUsT0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztHQUN4RDs7O1FBUkksUUFBUTs7O1FBVVAsUUFBUSxHQUFSLFFBQVEiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4uLy4uLy4uL3R5cGluZ3MvdW5kZXJzY29yZS91bmRlcnNjb3JlLmQudHNcIi8+XG4vKiBnbG9iYWwgRmlyZWJhc2UsIFNpZ25hdHVyZVBhZCAqL1xuLy8vIDxyZWZlcmVuY2UgcGF0aD1cIi4uLy4uLy4uL3R5cGluZ3MvYW5ndWxhcmpzL2FuZ3VsYXIuZC50c1wiLz5cblxuXG52YXIgYXBwID0gYW5ndWxhci5tb2R1bGUoJ0NvbmRpdGlvbkFwcCcsIFsnZmlyZWJhc2UnLCAnbmdNYXRlcmlhbCcsICduZ01lc3NhZ2VzJywgJ3VpLnJvdXRlcicsICdhbmd1bGFyLWNhY2hlJyxcblx0J25nLmRldmljZURldGVjdG9yJywgJ2FuZ3VsYXJNb21lbnQnLCAnYW5ndWxhci1zb3J0YWJsZS12aWV3JywnbmdXZWJ3b3JrZXInLCAnbmdJT1M5VUlXZWJWaWV3UGF0Y2gnXSk7XG5cbmltcG9ydCB7Y29uZmlnfSBmcm9tICcuL2NvbmZpZy9yb3V0ZS1jb25maWcuanMnXG5hcHAuY29uZmlnKGNvbmZpZyk7XG5cblxuYXBwLnJ1bihmdW5jdGlvbiAoJHJvb3RTY29wZSwgJHN0YXRlLCBGaXJlQmFzZVJlZikge1xuXG5cdCRyb290U2NvcGUuJG9uKFwiJHN0YXRlQ2hhbmdlU3RhcnRcIiwgZnVuY3Rpb24gKGV2ZW50LCB0b1N0YXRlLCB0b1BhcmFtcywgZnJvbVN0YXRlLCBmcm9tUGFyYW1zLCBlcnJvcikge1xuXHRcdC8vIFdlIGNhbiBjYXRjaCB0aGUgZXJyb3IgdGhyb3duIHdoZW4gdGhlICRyZXF1aXJlQXV0aCBwcm9taXNlIGlzIHJlamVjdGVkXG5cdFx0Ly8gYW5kIHJlZGlyZWN0IHRoZSB1c2VyIGJhY2sgdG8gdGhlIGhvbWUgcGFnZVxuXHRcdFxuXHRcdFxuXHR9KTtcblxuXHQkcm9vdFNjb3BlLiRvbihcIiRzdGF0ZUNoYW5nZVN1Y2Nlc3NcIiwgZnVuY3Rpb24gKGV2ZW50LCB0b1N0YXRlLCB0b1BhcmFtcywgZnJvbVN0YXRlLCBmcm9tUGFyYW1zKSB7XG5cdFx0Ly8gV2UgY2FuIGNhdGNoIHRoZSBlcnJvciB0aHJvd24gd2hlbiB0aGUgJHJlcXVpcmVBdXRoIHByb21pc2UgaXMgcmVqZWN0ZWRcblx0XHQvLyBhbmQgcmVkaXJlY3QgdGhlIHVzZXIgYmFjayB0byB0aGUgaG9tZSBwYWdlXG5cdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcigndWktdmlldycpLnNjcm9sbEludG9WaWV3KCk7XG5cblx0fSk7XG5cblx0JHJvb3RTY29wZS4kb24oXCIkc3RhdGVDaGFuZ2VFcnJvclwiLCBmdW5jdGlvbiAoZXZlbnQsIHRvU3RhdGUsIHRvUGFyYW1zLCBmcm9tU3RhdGUsIGZyb21QYXJhbXMsIGVycm9yKSB7XG5cdFx0Ly8gV2UgY2FuIGNhdGNoIHRoZSBlcnJvciB0aHJvd24gd2hlbiB0aGUgJHJlcXVpcmVBdXRoIHByb21pc2UgaXMgcmVqZWN0ZWRcblx0XHQvLyBhbmQgcmVkaXJlY3QgdGhlIHVzZXIgYmFjayB0byB0aGUgaG9tZSBwYWdlXG5cdFx0Y29uc29sZS5sb2coZXJyb3IpXG5cdFx0aWYgKGVycm9yID09PSBcIkFVVEhfUkVRVUlSRURcIikge1xuXHRcdFx0JHN0YXRlLmdvKFwibG9naW5cIik7XG5cdFx0fVxuXHR9KTtcblxuXHR2YXIgY29ubmVjdGVkUmVmID0gbmV3IEZpcmViYXNlKFwiaHR0cHM6Ly93ZWxscy1yZXBvcnQtcmVzdW1lLmZpcmViYXNlaW8uY29tLy5pbmZvL2Nvbm5lY3RlZFwiKTtcblx0Y29ubmVjdGVkUmVmLm9uKFwidmFsdWVcIiwgZnVuY3Rpb24gKHNuYXApIHtcblx0XHRpZiAoc25hcC52YWwoKSA9PT0gdHJ1ZSkge1xuXHRcdFx0JHJvb3RTY29wZS4kYnJvYWRjYXN0KCdjb25uZWN0ZWQnKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0JHJvb3RTY29wZS4kYnJvYWRjYXN0KCdkaXNjb25uZWN0ZWQnKTtcblx0XHR9XG5cdH0pO1xuXG59KVxuXG4vKipcbiAqIENvbnRyb2xsZXJzXG4gKi9cblxuaW1wb3J0IHtBcHBDdHJsfSBmcm9tICcuL2NvbnRyb2xsZXJzL0FwcEN0cmwuanMnXG5hcHAuY29udHJvbGxlcignQXBwQ3RybCcsIEFwcEN0cmwpO1xuXG5pbXBvcnQge0xvZ2luQ3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9Mb2dpbkN0cmwuanMnXG5hcHAuY29udHJvbGxlcignTG9naW5DdHJsJywgTG9naW5DdHJsKTtcblxuaW1wb3J0IHtBZG1pbkN0cmx9IGZyb20gJy4vY29udHJvbGxlcnMvQWRtaW5DdHJsLmpzJ1xuYXBwLmNvbnRyb2xsZXIoJ0FkbWluQ3RybCcsIEFkbWluQ3RybCk7XG5cbmltcG9ydCB7QWxsUmVwb3J0c0N0cmx9IGZyb20gJy4vY29udHJvbGxlcnMvQWxsUmVwb3J0c0N0cmwuanMnXG5hcHAuY29udHJvbGxlcignQWxsUmVwb3J0c0N0cmwnLCBBbGxSZXBvcnRzQ3RybCk7XG5cbmltcG9ydCB7UmVwb3J0Q3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9SZXBvcnRDdHJsLmpzJ1xuYXBwLmNvbnRyb2xsZXIoJ1JlcG9ydEN0cmwnLCBSZXBvcnRDdHJsKTtcblxuaW1wb3J0IHtKb2JzQ3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9Kb2JzQ3RybC5qcydcbmFwcC5jb250cm9sbGVyKCdKb2JzQ3RybCcsIEpvYnNDdHJsKTtcblxuaW1wb3J0IHtDcmVhdGVKb2JDdHJsfSBmcm9tICcuL2NvbnRyb2xsZXJzL0NyZWF0ZUpvYkN0cmwuanMnXG5hcHAuY29udHJvbGxlcignQ3JlYXRlSm9iQ3RybCcsIENyZWF0ZUpvYkN0cmwpO1xuXG5pbXBvcnQge0RhbmdlclNoZWV0Q3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9EYW5nZXJTaGVldEN0cmwuanMnXG5hcHAuY29udHJvbGxlcignRGFuZ2VyU2hlZXRDdHJsJywgRGFuZ2VyU2hlZXRDdHJsKTtcblxuaW1wb3J0IHtXYXRlclNoZWV0Q3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9XYXRlclNoZWV0Q3RybC5qcydcbmFwcC5jb250cm9sbGVyKCdXYXRlclNoZWV0Q3RybCcsIFdhdGVyU2hlZXRDdHJsKTtcblxuaW1wb3J0IHtTaWRlTmF2Q3RybH0gZnJvbSAnLi9jb250cm9sbGVycy9TaWRlTmF2Q3RybC5qcydcbmFwcC5jb250cm9sbGVyKCdTaWRlTmF2Q3RybCcsIFNpZGVOYXZDdHJsKTtcblxuXG4vKipcbiAqU2VydmljZXMgXG4gKi9cblxuaW1wb3J0IHtTaWduUGFkU3J2fSBmcm9tICcuL3NlcnZpY2VzL1NpZ25QYWRTcnYuanMnXG5hcHAuc2VydmljZSgnU2lnblBhZCcsIFNpZ25QYWRTcnYpO1xuXG5pbXBvcnQge1RvYXN0U3J2fSBmcm9tICcuL3NlcnZpY2VzL1RvYXN0U3J2LmpzJ1xuYXBwLnNlcnZpY2UoJ1RvYXN0JywgVG9hc3RTcnYpO1xuXG5pbXBvcnQge0xvYWRSZXBvcnRzU3J2fSBmcm9tICcuL3NlcnZpY2VzL0xvYWRSZXBvcnRzU3J2LmpzJ1xuYXBwLnNlcnZpY2UoJ0xvYWRSZXBvcnRzU3J2JywgTG9hZFJlcG9ydHNTcnYpO1xuXG5pbXBvcnQge0ZpcmVCYXNlQXV0aFNydn0gZnJvbSAnLi9zZXJ2aWNlcy9GaXJlQmFzZUF1dGhTcnYuanMnXG5hcHAuc2VydmljZSgnRmlyZUJhc2VBdXRoJywgRmlyZUJhc2VBdXRoU3J2KTtcblxuaW1wb3J0IHtMb2FkaW5nUG9wdXBTcnZ9IGZyb20gJy4vc2VydmljZXMvTG9hZGluZ1BvcHVwU3J2LmpzJ1xuYXBwLnNlcnZpY2UoJ0xvYWRpbmdQb3B1cCcsIExvYWRpbmdQb3B1cFNydik7XG5cbmltcG9ydCB7TG9jYWxEYXRhYmFzZVNydn0gZnJvbSAnLi9zZXJ2aWNlcy9Mb2NhbERhdGFiYXNlU3J2LmpzJ1xuYXBwLnNlcnZpY2UoJ0xvY2FsRGF0YWJhc2UnLCBMb2NhbERhdGFiYXNlU3J2KTtcblxuaW1wb3J0IHtQb2ludHNEYXRhU3J2fSBmcm9tICcuL3NlcnZpY2VzL1BvaW50c0RhdGFTcnYuanMnXG5hcHAuc2VydmljZSgnUG9pbnRzRGF0YScsIFBvaW50c0RhdGFTcnYpO1xuXG5pbXBvcnQge09yaWVudGF0aW9uU3J2fSBmcm9tICcuL3NlcnZpY2VzL09yaWVudGF0aW9uU3J2LmpzJ1xuYXBwLnNlcnZpY2UoJ09yaWVudGF0aW9uJywgT3JpZW50YXRpb25TcnYpO1xuXG5pbXBvcnQge0pvYlN0YXR1c1Nydn0gZnJvbSAnLi9zZXJ2aWNlcy9Kb2JTdGF0dXNTcnYuanMnXG5hcHAuc2VydmljZSgnSm9iU3RhdHVzJywgSm9iU3RhdHVzU3J2KTtcblxuaW1wb3J0IHtQcmludFNydn0gZnJvbSAnLi9zZXJ2aWNlcy9QcmludFNydi5qcydcbmFwcC5zZXJ2aWNlKCdQcmludCcsIFByaW50U3J2KTtcblxuaW1wb3J0IHtEYXRlSVNPU3J2fSBmcm9tICcuL3NlcnZpY2VzL0RhdGVJU09TcnYuanMnXG5hcHAuc2VydmljZSgnRGF0ZUlTTycsIERhdGVJU09TcnYpO1xuXG4vKipcbiAqIEZpcmViYXNlIFVSTFxuICovXG5hcHAuY29uc3RhbnQoJ0ZpcmVCYXNlUmVmJywge1xuXHRyZWY6IG5ldyBGaXJlYmFzZShcImh0dHBzOi8vd2VsbHMtcmVwb3J0LXJlc3VtZS5maXJlYmFzZWlvLmNvbVwiKSBcbn0pXG5cbmFwcC5maWx0ZXIoJ2pvYkZpbHRlcicsIGZ1bmN0aW9uICgkZmlsdGVyKSB7XG5cblx0Ly8gSW4gdGhlIHJldHVybiBmdW5jdGlvbiwgd2UgbXVzdCBwYXNzIGluIGEgc2luZ2xlIHBhcmFtZXRlciB3aGljaCB3aWxsIGJlIHRoZSBkYXRhIHdlIHdpbGwgd29yayBvbi5cblx0Ly8gV2UgaGF2ZSB0aGUgYWJpbGl0eSB0byBzdXBwb3J0IG11bHRpcGxlIG90aGVyIHBhcmFtZXRlcnMgdGhhdCBjYW4gYmUgcGFzc2VkIGludG8gdGhlIGZpbHRlciBvcHRpb25hbGx5XG5cdHJldHVybiBmdW5jdGlvbiAoYXJyYXlUb1BhcnNlLCBzZWFyY2hWYWx1ZSkge1xuXG5cdFx0dmFyIG91dHB1dDtcblxuXHRcdGlmIChzZWFyY2hWYWx1ZSkge1xuXG5cdFx0XHRpZiAoc2VhcmNoVmFsdWUuc2VhcmNoKCd7JykgPT0gMCAmJiBzZWFyY2hWYWx1ZS5zZWFyY2goJ30nKSA+IDApIHtcblx0XHRcdFx0Ly9zcGxpdCB0aGUgc2VhcmNoIHRleHQgYnkgY29tbWEgdG8gcHVzaCBhcnJheVxuXHRcdFx0XHRsZXQgc2VhcmNoUGFyYW1zID0gc2VhcmNoVmFsdWUuc3BsaXQoJywnKTtcblx0XHRcdFx0Ly92YXJpYWJsZSBmb3Igc3RvcmluZyB0aGUgbmV3IG9iamVjdFxuXHRcdFx0XHRsZXQgc2VhcmNoVmFsdWVPYmplY3QgPSB7fTtcblx0XHRcdFx0Ly9sb29wIHRocm91Z2ggc2VhcmNoUGFyYW1zIHRvIHNwbGl0IGVhY2ggYXJyYXkgYmFzZWQgY29sb25cblx0XHRcdFx0c2VhcmNoUGFyYW1zLm1hcCgodiwgaSwgYSkgPT4ge1xuXHRcdFx0XHRcdGxldCBzZWFyY2hQYXJhbSA9IHYuc3BsaXQoJzonKTtcblx0XHRcdFx0XHRsZXQga2V5LCB2YWx1ZTtcblx0XHRcdFx0XHQvL3JlbW92ZSB7IG9yIH1cblx0XHRcdFx0XHRcblx0XHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdFx0a2V5ID0gc2VhcmNoUGFyYW1bMF0ucmVwbGFjZSgvW3t9XS8sICcnKTtcblx0XHRcdFx0XHRcdHZhbHVlID0gc2VhcmNoUGFyYW1bMV0ucmVwbGFjZSgvW3t9XS8sICcnKTtcblx0XHRcdFx0XHR9IGNhdGNoIChlcnJvcikge1xuXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdC8vY3JlYXRlIGEgbmV3IG9iamVjdCB3aXRoIGtleSBhbmQgdmFsdWVcblx0XHRcdFx0XHRzZWFyY2hWYWx1ZU9iamVjdFtrZXldID0gdmFsdWU7XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0b3V0cHV0ID0gJGZpbHRlcignZmlsdGVyJykoYXJyYXlUb1BhcnNlLCBzZWFyY2hWYWx1ZU9iamVjdCk7XG5cdFx0XHRcdH0gY2F0Y2ggKGVycm9yKSB7XG5cblx0XHRcdFx0fVxuXG5cblxuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHRvdXRwdXQgPSAkZmlsdGVyKCdmaWx0ZXInKShhcnJheVRvUGFyc2UsIHNlYXJjaFZhbHVlKVxuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cblx0XHRcdG91dHB1dCA9ICRmaWx0ZXIoJ2ZpbHRlcicpKGFycmF5VG9QYXJzZSwgc2VhcmNoVmFsdWUpXG5cdFx0fVxuXG5cdFx0cmV0dXJuIG91dHB1dDtcblxuXHR9XG5cbn0pOyIsIlwidXNlIHN0cmljdFwiO1xudmFyIGNvbmZpZyA9IChmdW5jdGlvbiBjb25maWcoKSB7XG5cdGZ1bmN0aW9uIGNvbmZpZygkc3RhdGVQcm92aWRlciwgJHVybFJvdXRlclByb3ZpZGVyLCAkbWRUaGVtaW5nUHJvdmlkZXIsICRtZEdlc3R1cmVQcm92aWRlciwgQ2FjaGVGYWN0b3J5UHJvdmlkZXIpIHtcblx0XHQkbWRHZXN0dXJlUHJvdmlkZXIuc2tpcENsaWNrSGlqYWNrKCk7XG5cblx0XHRhbmd1bGFyLmV4dGVuZChDYWNoZUZhY3RvcnlQcm92aWRlci5kZWZhdWx0cywge1xuXHRcdFx0bWF4QWdlOiAxNSAqIDYwICogMTAwMCxcblx0XHRcdHN0b3JhZ2VNb2RlOiAnbG9jYWxTdG9yYWdlJ1xuXHRcdH0pO1xuXG5cdFx0JHN0YXRlUHJvdmlkZXIuc3RhdGUoJ2xvZ2luJywge1xuXHRcdFx0dXJsOiAnL2xvZ2luJyxcblx0XHRcdHRlbXBsYXRlVXJsOiAndmlld3MvbG9naW4uaHRtbCcsXG5cdFx0XHRjb250cm9sbGVyOiAnTG9naW5DdHJsIGFzIHZtJ1xuXG5cdFx0fSlcblx0XHRcdC5zdGF0ZSgnbG9nT3V0Jywge1xuXHRcdFx0XHR1cmw6ICcvbG9nT3V0Jyxcblx0XHRcdFx0dGVtcGxhdGVVcmw6ICd2aWV3cy9sb2dpbi5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogZnVuY3Rpb24gKEZpcmVCYXNlQXV0aCkge1xuXHRcdFx0XHRcdEZpcmVCYXNlQXV0aC4kdW5hdXRoKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQuc3RhdGUoJ2pvYnMnLCB7XG5cdFx0XHRcdHVybDogJy9qb2JzJyxcblx0XHRcdFx0dGVtcGxhdGVVcmw6ICd2aWV3cy9qb2JzLmh0bWwnLFxuXHRcdFx0XHRjb250cm9sbGVyOiAnSm9ic0N0cmwgYXMgdm0nLFxuXHRcdFx0XHRyZXNvbHZlOiB7XG5cdFx0XHRcdFx0Ly8gY29udHJvbGxlciB3aWxsIG5vdCBiZSBsb2FkZWQgdW50aWwgJHJlcXVpcmVBdXRoIHJlc29sdmVzXG5cdFx0XHRcdFx0Ly8gQXV0aCByZWZlcnMgdG8gb3VyICRmaXJlYmFzZUF1dGggd3JhcHBlciBpbiB0aGUgZXhhbXBsZSBhYm92ZVxuXHRcdFx0XHRcdFwiY3VycmVudEF1dGhcIjogW1wiRmlyZUJhc2VBdXRoXCIsIFwiTG9jYWxEYXRhYmFzZVwiLCBmdW5jdGlvbiAoQXV0aCwgTG9jYWxEYXRhYmFzZSkge1xuXHRcdFx0XHRcdFx0Ly8gJHJlcXVpcmVBdXRoIHJldHVybnMgYSBwcm9taXNlIHNvIHRoZSByZXNvbHZlIHdhaXRzIGZvciBpdCB0byBjb21wbGV0ZVxuXHRcdFx0XHRcdFx0Ly8gSWYgdGhlIHByb21pc2UgaXMgcmVqZWN0ZWQsIGl0IHdpbGwgdGhyb3cgYSAkc3RhdGVDaGFuZ2VFcnJvciAoc2VlIGFib3ZlKVxuXHRcdFx0XHRcdFx0aWYgKG5hdmlnYXRvci5vbkxpbmUpIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIEF1dGguJHJlcXVpcmVBdXRoKCk7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gTG9jYWxEYXRhYmFzZS4kZ2V0QXV0aCgpO1xuXHRcdFx0XHRcdFx0fVxuXG5cblx0XHRcdFx0XHR9XVxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdFx0LnN0YXRlKCdhZG1pbicsIHtcblx0XHRcdFx0dXJsOiAnL2FkbWluJyxcblx0XHRcdFx0dGVtcGxhdGVVcmw6ICd2aWV3cy9hZG1pbi5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ0FkbWluQ3RybCBhcyB2bScsXG5cdFx0XHRcdHJlc29sdmU6IHtcblx0XHRcdFx0XHQvLyBjb250cm9sbGVyIHdpbGwgbm90IGJlIGxvYWRlZCB1bnRpbCAkcmVxdWlyZUF1dGggcmVzb2x2ZXNcblx0XHRcdFx0XHQvLyBBdXRoIHJlZmVycyB0byBvdXIgJGZpcmViYXNlQXV0aCB3cmFwcGVyIGluIHRoZSBleGFtcGxlIGFib3ZlXG5cdFx0XHRcdFx0XCJjdXJyZW50QXV0aFwiOiBbXCJGaXJlQmFzZUF1dGhcIiwgZnVuY3Rpb24gKEF1dGgpIHtcblx0XHRcdFx0XHRcdC8vICRyZXF1aXJlQXV0aCByZXR1cm5zIGEgcHJvbWlzZSBzbyB0aGUgcmVzb2x2ZSB3YWl0cyBmb3IgaXQgdG8gY29tcGxldGVcblx0XHRcdFx0XHRcdC8vIElmIHRoZSBwcm9taXNlIGlzIHJlamVjdGVkLCBpdCB3aWxsIHRocm93IGEgJHN0YXRlQ2hhbmdlRXJyb3IgKHNlZSBhYm92ZSlcblx0XHRcdFx0XHRcdHJldHVybiBBdXRoLiRyZXF1aXJlQXV0aCgpO1xuXHRcdFx0XHRcdH1dXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQuc3RhdGUoJ2NyZWF0ZUpvYicsIHtcblx0XHRcdFx0dXJsOiAnL2NyZWF0ZUpvYi86aWQnLFxuXHRcdFx0XHR0ZW1wbGF0ZVVybDogJ3ZpZXdzL2NyZWF0ZUpvYnMuaHRtbCcsXG5cdFx0XHRcdGNvbnRyb2xsZXI6ICdDcmVhdGVKb2JDdHJsIGFzIHZtJyxcblx0XHRcdFx0cmVzb2x2ZToge1xuXHRcdFx0XHRcdC8vIGNvbnRyb2xsZXIgd2lsbCBub3QgYmUgbG9hZGVkIHVudGlsICRyZXF1aXJlQXV0aCByZXNvbHZlc1xuXHRcdFx0XHRcdC8vIEF1dGggcmVmZXJzIHRvIG91ciAkZmlyZWJhc2VBdXRoIHdyYXBwZXIgaW4gdGhlIGV4YW1wbGUgYWJvdmVcblx0XHRcdFx0XHRcImN1cnJlbnRBdXRoXCI6IFtcIkZpcmVCYXNlQXV0aFwiLCBcIkxvY2FsRGF0YWJhc2VcIiwgZnVuY3Rpb24gKEF1dGgsIExvY2FsRGF0YWJhc2UpIHtcblx0XHRcdFx0XHRcdC8vICRyZXF1aXJlQXV0aCByZXR1cm5zIGEgcHJvbWlzZSBzbyB0aGUgcmVzb2x2ZSB3YWl0cyBmb3IgaXQgdG8gY29tcGxldGVcblx0XHRcdFx0XHRcdC8vIElmIHRoZSBwcm9taXNlIGlzIHJlamVjdGVkLCBpdCB3aWxsIHRocm93IGEgJHN0YXRlQ2hhbmdlRXJyb3IgKHNlZSBhYm92ZSlcblx0XHRcdFx0XHRcdGlmIChuYXZpZ2F0b3Iub25MaW5lKSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBBdXRoLiRyZXF1aXJlQXV0aCgpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIExvY2FsRGF0YWJhc2UuJGdldEF1dGgoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XVxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdFx0LnN0YXRlKCdhbGxSZXBvcnRzJywge1xuXHRcdFx0XHR1cmw6ICcvYWxsUmVwb3J0cycsXG5cdFx0XHRcdHRlbXBsYXRlVXJsOiAndmlld3MvYWxsUmVwb3J0cy5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogXCJBbGxSZXBvcnRzQ3RybCBhcyB2bVwiLFxuXHRcdFx0XHQvLyBjb250cm9sbGVyOiAnV2F0ZXJTaGVldEN0cmwgYXMgdm0nLFxuXHRcdFx0XHRyZXNvbHZlOiB7XG5cdFx0XHRcdFx0Ly8gY29udHJvbGxlciB3aWxsIG5vdCBiZSBsb2FkZWQgdW50aWwgJHJlcXVpcmVBdXRoIHJlc29sdmVzXG5cdFx0XHRcdFx0Ly8gQXV0aCByZWZlcnMgdG8gb3VyICRmaXJlYmFzZUF1dGggd3JhcHBlciBpbiB0aGUgZXhhbXBsZSBhYm92ZVxuXHRcdFx0XHRcdFwiY3VycmVudEF1dGhcIjogW1wiRmlyZUJhc2VBdXRoXCIsIFwiTG9jYWxEYXRhYmFzZVwiLCBmdW5jdGlvbiAoQXV0aCwgTG9jYWxEYXRhYmFzZSkge1xuXHRcdFx0XHRcdFx0Ly8gJHJlcXVpcmVBdXRoIHJldHVybnMgYSBwcm9taXNlIHNvIHRoZSByZXNvbHZlIHdhaXRzIGZvciBpdCB0byBjb21wbGV0ZVxuXHRcdFx0XHRcdFx0Ly8gSWYgdGhlIHByb21pc2UgaXMgcmVqZWN0ZWQsIGl0IHdpbGwgdGhyb3cgYSAkc3RhdGVDaGFuZ2VFcnJvciAoc2VlIGFib3ZlKVxuXHRcdFx0XHRcdFx0aWYgKG5hdmlnYXRvci5vbkxpbmUpIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIEF1dGguJHJlcXVpcmVBdXRoKCk7XG5cdFx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gTG9jYWxEYXRhYmFzZS4kZ2V0QXV0aCgpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1dXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0XHQuc3RhdGUoJ2FsbFJlcG9ydHMucmVwb3J0Jywge1xuXHRcdFx0XHR1cmw6ICcvcmVwb3J0LzppZCcsXG5cdFx0XHRcdHRlbXBsYXRlVXJsOiAndmlld3MvcmVwb3J0U2hlZXQuaHRtbCcsXG5cdFx0XHRcdGNvbnRyb2xsZXI6ICdSZXBvcnRDdHJsIGFzIHZtJyxcblx0XHRcdFx0cmVzb2x2ZToge1xuXHRcdFx0XHRcdC8vIGNvbnRyb2xsZXIgd2lsbCBub3QgYmUgbG9hZGVkIHVudGlsICRyZXF1aXJlQXV0aCByZXNvbHZlc1xuXHRcdFx0XHRcdC8vIEF1dGggcmVmZXJzIHRvIG91ciAkZmlyZWJhc2VBdXRoIHdyYXBwZXIgaW4gdGhlIGV4YW1wbGUgYWJvdmVcblx0XHRcdFx0XHRcImN1cnJlbnRBdXRoXCI6IFtcIkZpcmVCYXNlQXV0aFwiLCBcIkxvY2FsRGF0YWJhc2VcIiwgZnVuY3Rpb24gKEF1dGgsIExvY2FsRGF0YWJhc2UpIHtcblx0XHRcdFx0XHRcdC8vICRyZXF1aXJlQXV0aCByZXR1cm5zIGEgcHJvbWlzZSBzbyB0aGUgcmVzb2x2ZSB3YWl0cyBmb3IgaXQgdG8gY29tcGxldGVcblx0XHRcdFx0XHRcdC8vIElmIHRoZSBwcm9taXNlIGlzIHJlamVjdGVkLCBpdCB3aWxsIHRocm93IGEgJHN0YXRlQ2hhbmdlRXJyb3IgKHNlZSBhYm92ZSlcblx0XHRcdFx0XHRcdGlmIChuYXZpZ2F0b3Iub25MaW5lKSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBBdXRoLiRyZXF1aXJlQXV0aCgpO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIExvY2FsRGF0YWJhc2UuJGdldEF1dGgoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XVxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdFx0LnN0YXRlKCdhbGxSZXBvcnRzLmRhbmdlclNoZWV0Jywge1xuXHRcdFx0XHR1cmw6ICcvZGFuZ2VyU2hlZXQvOmlkJyxcblx0XHRcdFx0dGVtcGxhdGVVcmw6ICd2aWV3cy9kYW5nZXJTaGVldC5odG1sJyxcblx0XHRcdFx0Y29udHJvbGxlcjogJ0RhbmdlclNoZWV0Q3RybCBhcyB2bScsXG5cdFx0XHRcdHJlc29sdmU6IHtcblx0XHRcdFx0XHQvLyBjb250cm9sbGVyIHdpbGwgbm90IGJlIGxvYWRlZCB1bnRpbCAkcmVxdWlyZUF1dGggcmVzb2x2ZXNcblx0XHRcdFx0XHQvLyBBdXRoIHJlZmVycyB0byBvdXIgJGZpcmViYXNlQXV0aCB3cmFwcGVyIGluIHRoZSBleGFtcGxlIGFib3ZlXG5cdFx0XHRcdFx0XCJjdXJyZW50QXV0aFwiOiBbXCJGaXJlQmFzZUF1dGhcIiwgXCJMb2NhbERhdGFiYXNlXCIsIGZ1bmN0aW9uIChBdXRoLCBMb2NhbERhdGFiYXNlKSB7XG5cdFx0XHRcdFx0XHQvLyAkcmVxdWlyZUF1dGggcmV0dXJucyBhIHByb21pc2Ugc28gdGhlIHJlc29sdmUgd2FpdHMgZm9yIGl0IHRvIGNvbXBsZXRlXG5cdFx0XHRcdFx0XHQvLyBJZiB0aGUgcHJvbWlzZSBpcyByZWplY3RlZCwgaXQgd2lsbCB0aHJvdyBhICRzdGF0ZUNoYW5nZUVycm9yIChzZWUgYWJvdmUpXG5cdFx0XHRcdFx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gQXV0aC4kcmVxdWlyZUF1dGgoKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBMb2NhbERhdGFiYXNlLiRnZXRBdXRoKCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fV1cblx0XHRcdFx0fVxuXHRcdFx0fSlcblx0XHRcdC5zdGF0ZSgnYWxsUmVwb3J0cy53YXRlclNoZWV0Jywge1xuXHRcdFx0XHR1cmw6ICcvd2F0ZXJTaGVldC86aWQnLFxuXHRcdFx0XHR0ZW1wbGF0ZVVybDogJ3ZpZXdzL3dhdGVyU2hlZXQuaHRtbCcsXG5cdFx0XHRcdGNvbnRyb2xsZXI6ICdXYXRlclNoZWV0Q3RybCBhcyB2bScsXG5cdFx0XHRcdHJlc29sdmU6IHtcblx0XHRcdFx0XHQvLyBjb250cm9sbGVyIHdpbGwgbm90IGJlIGxvYWRlZCB1bnRpbCAkcmVxdWlyZUF1dGggcmVzb2x2ZXNcblx0XHRcdFx0XHQvLyBBdXRoIHJlZmVycyB0byBvdXIgJGZpcmViYXNlQXV0aCB3cmFwcGVyIGluIHRoZSBleGFtcGxlIGFib3ZlXG5cdFx0XHRcdFx0XCJjdXJyZW50QXV0aFwiOiBbXCJGaXJlQmFzZUF1dGhcIiwgXCJMb2NhbERhdGFiYXNlXCIsIGZ1bmN0aW9uIChBdXRoLCBMb2NhbERhdGFiYXNlKSB7XG5cdFx0XHRcdFx0XHQvLyAkcmVxdWlyZUF1dGggcmV0dXJucyBhIHByb21pc2Ugc28gdGhlIHJlc29sdmUgd2FpdHMgZm9yIGl0IHRvIGNvbXBsZXRlXG5cdFx0XHRcdFx0XHQvLyBJZiB0aGUgcHJvbWlzZSBpcyByZWplY3RlZCwgaXQgd2lsbCB0aHJvdyBhICRzdGF0ZUNoYW5nZUVycm9yIChzZWUgYWJvdmUpXG5cdFx0XHRcdFx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gQXV0aC4kcmVxdWlyZUF1dGgoKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdHJldHVybiBMb2NhbERhdGFiYXNlLiRnZXRBdXRoKCk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fV1cblx0XHRcdFx0fVxuXHRcdFx0fSlcblxuXG5cdFx0JHVybFJvdXRlclByb3ZpZGVyLm90aGVyd2lzZSgnbG9naW4nKTtcblxuXHRcdCRtZFRoZW1pbmdQcm92aWRlci50aGVtZSgnZGVmYXVsdCcpXG5cdFx0XHQucHJpbWFyeVBhbGV0dGUoJ2xpZ2h0LWJsdWUnKVxuXHRcdFx0LmFjY2VudFBhbGV0dGUoJ29yYW5nZScpO1xuXHR9XG5cblx0Y29uZmlnLiRpbmplY3QgPSBbJyRzdGF0ZVByb3ZpZGVyJywgJyR1cmxSb3V0ZXJQcm92aWRlcicsICckbWRUaGVtaW5nUHJvdmlkZXInLCAnJG1kR2VzdHVyZVByb3ZpZGVyJywgJ0NhY2hlRmFjdG9yeVByb3ZpZGVyJ11cblxuXHRyZXR1cm4gY29uZmlnXG59KSgpO1xuZXhwb3J0IHtjb25maWd9IiwiLyoqXG4gKiBAbmdkb2MgY29udHJvbGxlclxuICogQG5hbWUgQ29uZGl0aW9uQXBwLmNvbnRyb2xsZXIuQWRtaW5DdHJsXG4gKiBAZGVzY3JpcHRpb25cbiAqIEFkbWluIENvbnRyb2xsZXIgYWRtaW4uaHRtbFxuICogQHJlcXVpcmVzIGN1cnJlbnRBdXRoXG4gKiBAcmVxdWlyZXMgRmlyZUJhc2VSZWZcbiAqIEByZXF1aXJlcyAkZmlyZWJhc2VBcnJheVxuICogQHJlcXVpcmVzIEZpcmVCYXNlQXV0aFxuICogQHJlcXVpcmVzICRtZERpYWxvZ1xuICogQHJlcXVpcmVzICRtZFRvYXN0XG4gKiBAcmVxdWlyZXMgJHNjb3BlXG4gKiBAcmVxdWlyZXMgJGZpcmViYXNlT2JqZWN0XG4gKiBAcmVxdWlyZXMgJHdpbmRvd1xuICogQHJlcXVpcmVzIE9yaWVudGF0aW9uXG4gKiBAcmVxdWlyZXMgZGV2aWNlRGV0ZWN0b3JcbiAqIEByZXF1aXJlcyBMb2NhbERhdGFiYXNlXG4gKiBAcmVxdWlyZXMgJHJvb3RTY29wZVxuICogQHJlcXVpcmVzIERhdGVJU09cbiAqIEBuZ0luamVjdFxuICogIFxuICovXG5cbmNsYXNzIEFkbWluQ3RybCB7XG5cdGNvbnN0cnVjdG9yKGN1cnJlbnRBdXRoLCBGaXJlQmFzZVJlZiwgJGZpcmViYXNlQXJyYXksIEZpcmVCYXNlQXV0aCwgJG1kRGlhbG9nLCAkbWRUb2FzdCxcblx0XHQkc2NvcGUsICRmaXJlYmFzZU9iamVjdCwgJHdpbmRvdywgT3JpZW50YXRpb24sIGRldmljZURldGVjdG9yLCBMb2NhbERhdGFiYXNlLCAkcm9vdFNjb3BlLCBEYXRlSVNPKSB7XG5cblx0XHQvL2NvbnZlcnRzIGxvY2FsIGlzby1zdHJpbmcgdG8gaGFuZGxlZCB0aW1lem9uZVxuXHRcdHRoaXMuRGF0ZUlTTyA9IERhdGVJU09cblx0XHRcblx0XHQvLyBHZXQgYSBkYXRhYmFzZSByZWZlcmVuY2UgXG5cdFx0dGhpcy5yZWYgPSBGaXJlQmFzZVJlZi5yZWY7XG5cdFx0dGhpcy4kZmlyZWJhc2VBcnJheSA9ICRmaXJlYmFzZUFycmF5O1xuXHRcdHRoaXMudXNlcnMgPSAkZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgndXNlcnMnKSk7XG5cdFx0dGhpcy5jcmV3cyA9ICRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdjcmV3cycpKTtcblx0XHR0aGlzLmNsaWVudHMgPSAkZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnY2xpZW50cycpKTtcblx0XHR0aGlzLiRtZERpYWxvZyA9ICRtZERpYWxvZztcblx0XHR0aGlzLiRtZFRvYXN0ID0gJG1kVG9hc3Q7XG5cdFx0dGhpcy4kc2NvcGUgPSAkc2NvcGU7XG5cdFx0dGhpcy5hdXRoID0gRmlyZUJhc2VBdXRoO1xuXHRcdHRoaXMubWFzdGVyID0geyBlbWFpbDogJycsIHBhc3N3b3JkOiAnJywgbmFtZTogJycgfTtcblx0XHR0aGlzLiRmaXJlYmFzZU9iamVjdCA9ICRmaXJlYmFzZU9iamVjdDtcblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UgPSBMb2NhbERhdGFiYXNlO1xuXHRcdHRoaXMuJHJvb3RTY29wZSA9ICRyb290U2NvcGU7XG5cdFx0XG5cdFx0Ly8gQWRtaW5DdHJsLnNldCR3YXRjaC5hcHBseSh0aGlzKTtcblx0XHRcblx0XHRPcmllbnRhdGlvbi5zb3VyY2VQYWdlID0gJ2FkbWluJztcblxuXHRcdCR3aW5kb3cub25vcmllbnRhdGlvbmNoYW5nZSA9ICgpID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodClcblx0XHRcdHRoaXMuc2Nyb2xsID0gT3JpZW50YXRpb24uc2Nyb2xsSGVpZ2h0O1xuXHRcdFx0JHNjb3BlLiRhcHBseSgpO1xuXHRcdH1cblxuXHRcdGlmIChkZXZpY2VEZXRlY3Rvci5kZXZpY2UgPT09ICdpcGhvbmUnKSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9ICdzY3JvbGwtaVBob25lJ1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHR9XG5cdH1cblxuXHRzdGF0aWMgc2V0JHdhdGNoKCkge1xuXHRcdHZhciB1bndhdGNoID0gdGhpcy51c2Vycy4kd2F0Y2goZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdGNvbnNvbGUubG9nKFwiZGF0YSBjaGFuZ2VkIVwiLCBkYXRhKTtcblx0XHR9KTtcblx0fVxuXHQvKipcblx0ICogQG5nZG9jIG1ldGhvZFxuXHQgKiBAbmFtZSBlbnJvbGxcblx0ICogQG1ldGhvZE9mIENvbmRpdGlvbkFwcC5jb250cm9sbGVyLkFkbWluQ3RybFxuXHQgKiBAZGVzY3JpcHRpb25cblx0ICogRW5yb2xsIG5ldyBhZG1pbiB1c2VyXG5cdCAqIEBwYXJhbSB7SFRNTEZvcm1FbGVtZW50fSBmb3JtIG5ldyBhZG1pbiB1c2VyIEhUTUwgZm9ybVxuXHQgKiBAcGFyYW0ge09iamVjdH0gJGV2ZW50IGtleSBwcmVzc2VkXG5cdCAqL1xuXG5cdGVucm9sbChmb3JtLCAkZXZlbnQpIHtcblx0XHRpZiAoJGV2ZW50LmtleUNvZGUgIT0gMTMgJiYgJGV2ZW50LmtleUNvZGUgIT0gMCkge1xuXHRcdFx0cmV0dXJuXG5cdFx0fVxuXG5cdFx0dGhpcy5hdXRoLiRjcmVhdGVVc2VyKHRoaXMudXNlcikudGhlbigodXNlckRhdGEpID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKFwiVXNlciBcIiArIHVzZXJEYXRhLnVpZCArIFwiIGNyZWF0ZWQgc3VjY2Vzc2Z1bGx5IVwiKTtcblxuXHRcdFx0dGhpcy5zaG93VG9hc3QoeyBuYW1lOiB0aGlzLnVzZXIubmFtZSB9KTtcblxuXHRcdFx0dGhpcy51c2VyLmVtYWlsID0gdGhpcy51c2VyLmVtYWlsLnRvTG93ZXJDYXNlKCk7XG5cblx0XHRcdHRoaXMudXNlcnMuJGFkZCh0aGlzLnVzZXIpO1xuXG5cdFx0XHRmb3JtLiRzZXRQcmlzdGluZSgpO1xuXHRcdFx0Zm9ybS4kc2V0VW50b3VjaGVkKCk7XG5cdFx0XHR0aGlzLnVzZXIgPSBhbmd1bGFyLmNvcHkodGhpcy5tYXN0ZXIpO1xuXG5cdFx0fSlcblx0XHRcdC5jYXRjaCgoZXJyb3IpID0+IHtcblx0XHRcdFx0dGhpcy5zaG93QWxlcnQoZXJyb3IpO1xuXHRcdFx0XHRjb25zb2xlLmVycm9yKFwiRXJyb3I6IFwiLCBlcnJvcik7XG5cdFx0XHR9KTtcblx0fVxuXHQvKipcblx0ICogQG5nZG9jIG1ldGhvZFxuXHQgKiBAbmFtZSBfcmVtb3ZlXG5cdCAqIEBtZXRob2RPZiBDb25kaXRpb25BcHAuY29udHJvbGxlci5BZG1pbkN0cmxcblx0ICogQGRlc2NyaXB0aW9uXG5cdCAqIFJlbW92ZSBlaXRoZXIgdGhlIHVzZXIgb3IgY3Jld1xuXHQgKiBAcGFyYW0ge09iamVjdHxzdHJpbmd9IGl0ZW1Ub1JlbW92ZSBUaGUgaXRlbSB0byByZW1vdmVcblx0ICovXG5cblx0X3JlbW92ZShpdGVtVG9SZW1vdmUpIHtcblxuXHRcdHN3aXRjaCAodGhpcy5zZWxlY3RlZEluZGV4KSB7XG5cdFx0XHRjYXNlIDE6XG5cdFx0XHRcdEFkbWluQ3RybC5yZW1vdmVVc2VyLmNhbGwodGhpcywgaXRlbVRvUmVtb3ZlKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIDI6XG5cdFx0XHRcdEFkbWluQ3RybC5yZW1vdmVDcmV3LmNhbGwodGhpcywgaXRlbVRvUmVtb3ZlKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cblx0fVxuXHQvKipcblx0ICogQG5nZG9jIG1ldGhvZFxuXHQgKiBAbmFtZSBfcmVtb3ZlXG5cdCAqIEBtZXRob2RPZiBDb25kaXRpb25BcHAuY29udHJvbGxlci5BZG1pbkN0cmxcblx0ICogQGRlc2NyaXB0aW9uXG5cdCAqIFJlbW92ZSBjcmV3XG5cdCAqIEBwYXJhbSB7T2JqZWN0fHN0cmluZ30gaXRlbVRvUmVtb3ZlIFRoZSBpdGVtIHRvIHJlbW92ZVxuXHQgKi9cblxuXHRzdGF0aWMgcmVtb3ZlQ3JldyhpdGVtVG9SZW1vdmUpIHtcblx0XHR2YXIgbnVtT2ZNYXRjaGVzO1xuXG5cdFx0dGhpcy5jbGllbnRzLiRsb2FkZWQoKS50aGVuKChjbGllbnRzKSA9PiB7XG5cdFx0XHRudW1PZk1hdGNoZXMgPSBjbGllbnRzLmZpbHRlcigoY3YsIGksIGEpID0+IHtcblx0XHRcdFx0aWYgKGN2LmNyZXcgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIGN2LmNyZXcgPT09IGl0ZW1Ub1JlbW92ZS5uYW1lXG5cdFx0XHR9KVxuXHRcdFx0XG5cdFx0XHQvL2lmIGEgcHJvamVjdCBkb2VzIG5vdCBleGlzdCByZW1vdmUgdGhlIHVzZXIuIEVsc2UgZGlzcGxheSBhbiBlcnJvclxuXHRcdFx0aWYgKG51bU9mTWF0Y2hlcy5sZW5ndGggPT0gMCkge1xuXHRcdFx0XHRpdGVtVG9SZW1vdmUuJHJlbW92ZSgpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRcdHRoaXMuc2hvd1RvYXN0KHsgbXNnOiBcIkNyZXcgcmVtb3ZlZCBzdWNjZXNzZnVsbHkhXCIgfSk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdFx0LmNhdGNoKChlcnJvcikgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5zaG93QWxlcnQoZXJyb3IpO1xuXHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIkVycm9yOiBcIiwgZXJyb3IpO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0dGhpcy5zaG93QWxlcnQoXCJQcm9qZWN0KHMpIGV4aXN0IHVuZGVyIHRoZSBjcmV3OiBcIiArIGl0ZW1Ub1JlbW92ZS5uYW1lICsgXCIsIGNhbm5vdCBkZWxldGUgY3Jld1wiKTtcblx0XHRcdFx0Y29uc29sZS53YXJuKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHR9XG5cdC8qKlxuXHQgKiBAbmdkb2MgbWV0aG9kXG5cdCAqIEBuYW1lIF9yZW1vdmVcblx0ICogQG1ldGhvZE9mIENvbmRpdGlvbkFwcC5jb250cm9sbGVyLkFkbWluQ3RybFxuXHQgKiBAZGVzY3JpcHRpb25cblx0ICogUmVtb3ZlIHVzZXJcblx0ICogQHBhcmFtIHtPYmplY3R8c3RyaW5nfSBpdGVtVG9SZW1vdmUgVGhlIGl0ZW0gdG8gcmVtb3ZlXG5cdCAqL1xuXG5cdHN0YXRpYyByZW1vdmVVc2VyKGl0ZW1Ub1JlbW92ZSkge1xuXHRcdHZhciBudW1PZk1hdGNoZXMgPSB1bmRlZmluZWQ7XG5cdFx0dGhpcy5qb2JzID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnam9iJykpO1xuXHRcdC8vIHRoaXMuY2xpZW50cyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ2NsaWVudHMnKSk7XG5cdFx0Ly9jaGVjayBpZiBhIHByb2plY3QgZXhpc3QgZmlyc3QgYmVmb3JlIHJlbW92aW5nIHVzZXJcblx0XHR0aGlzLmpvYnMuJGxvYWRlZCgpLnRoZW4oKGpvYnMpID0+IHtcblxuXHRcdFx0dGhpcy5jbGllbnRzLiRsb2FkZWQoKS50aGVuKChjbGllbnRzKSA9PiB7XG5cdFx0XHRcdG51bU9mTWF0Y2hlcyA9IGNsaWVudHMuZmlsdGVyKChjdiwgaSwgYSkgPT4ge1xuXHRcdFx0XHRcdGlmIChjdi50ZWNoSUQgPT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2Vcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0dmFyIGpvYiA9IF8uZmluZFdoZXJlKGpvYnMseyRpZDpjdi5mb3JlaWduS2V5fSk7XG5cblx0XHRcdFx0XHRyZXR1cm4gKGN2LnRlY2hJRCA9PT0gaXRlbVRvUmVtb3ZlLiRpZCAmJiBqb2Iuc3RhdHVzID09PSAnT3BlbicpXG5cdFx0XHRcdH0pXG5cdFx0XHRcblx0XHRcdFx0Ly9pZiBhIHByb2plY3QgZG9lcyBub3QgZXhpc3QgcmVtb3ZlIHRoZSB1c2VyLiBFbHNlIGRpc3BsYXkgYW4gZXJyb3Jcblx0XHRcdFx0aWYgKG51bU9mTWF0Y2hlcy5sZW5ndGggPT0gMCkge1xuXHRcdFx0XHRcdHRoaXMuYXV0aC4kcmVtb3ZlVXNlcihpdGVtVG9SZW1vdmUpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRcdFx0aXRlbVRvUmVtb3ZlLiRyZW1vdmUoKS50aGVuKChyZWYpID0+IHtcblx0XHRcdFx0XHRcdFx0Ly8gZGF0YSBoYXMgYmVlbiBkZWxldGVkIGxvY2FsbHkgYW5kIGluIHRoZSBkYXRhYmFzZVxuXHRcdFx0XHRcdFx0XHR0aGlzLnNob3dUb2FzdCh7IG1zZzogXCJVc2VyIHJlbW92ZWQgc3VjY2Vzc2Z1bGx5IVwiIH0pO1xuXHRcdFx0XHRcdFx0fSwgZnVuY3Rpb24gKGVycm9yKSB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKFwiRXJyb3I6XCIsIGVycm9yKTtcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHQuY2F0Y2goKGVycm9yKSA9PiB7XG5cdFx0XHRcdFx0XHRcdHRoaXMuc2hvd0FsZXJ0KGVycm9yKTtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIkVycm9yOiBcIiwgZXJyb3IpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0dGhpcy5zaG93QWxlcnQoXCJQcm9qZWN0KHMpIGV4aXN0IHVuZGVyIHRoZSBuYW1lOiBcIiArIGl0ZW1Ub1JlbW92ZS5uYW1lICsgXCIsIGNhbm5vdCBkZWxldGUgdXNlclwiKTtcblx0XHRcdFx0XHRjb25zb2xlLndhcm4oKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSlcblx0fVxuXHQvKipcblx0ICogQG5nZG9jIG1ldGhvZFxuXHQgKiBAbmFtZSBzaG93QWxlcnRcblx0ICogQG1ldGhvZE9mIENvbmRpdGlvbkFwcC5jb250cm9sbGVyLkFkbWluQ3RybFxuXHQgKiBAZGVzY3JpcHRpb25cblx0ICogc2hvdyBhbGVydCBkaWFsb2dcblx0ICogQHBhcmFtIHtzdHJpbmd9IGVycm9yIFRoZSBlcnJvciB0byBkaXNwbGF5XG5cdCAqL1xuXG5cdHNob3dBbGVydChlcnJvcikge1xuXHRcdGNvbnNvbGUubG9nKGVycm9yKTtcblx0XHR2YXIgYWxlcnQgPSB0aGlzLiRtZERpYWxvZy5hbGVydCgpXG5cdFx0XHQudGl0bGUoJ0Vycm9yJylcblx0XHRcdC5jb250ZW50KGVycm9yKVxuXHRcdFx0Lm9rKCdDbG9zZScpO1xuXG5cdFx0dGhpcy4kbWREaWFsb2cuc2hvdyhhbGVydCk7XG5cblxuXHR9XG5cdC8qKlxuXHQgKiBAbmdkb2MgbWV0aG9kXG5cdCAqIEBuYW1lIF9zaG93UmVhc3NpZ25EaWFsb2dcblx0ICogQG1ldGhvZE9mIENvbmRpdGlvbkFwcC5jb250cm9sbGVyLkFkbWluQ3RybFxuXHQgKiBAZGVzY3JpcHRpb25cblx0ICogc2hvdyByZWFzc2lnbiBkaWFsb2cuICBBbGxvd3MgdGhlIGFkbWluIHRvIHJlYXNzaWduIGFsbCBqb2JzIHRvIGFub3RoZXIgdXNlclxuXHQgKiBAcGFyYW0ge09iamVjdH0gJGV2ZW50IEJ1dHRvbiBjbGlja1xuXHQgKiBAcGFyYW0ge09iamVjdH0gb2JqIFRoZSB1c2VyIHRvIHJlYXNzaWduXG5cdCAqL1xuXG5cdF9zaG93UmVhc3NpZ25EaWFsb2codXNlclRvUmVhc3NpZ24sICRldmVudCkge1xuXHRcdHZhciB0aGF0ID0gdGhpcztcblxuXHRcdHRoaXMuJG1kRGlhbG9nLnNob3coe1xuXHRcdFx0dGFyZ2V0RXZlbnQ6ICRldmVudCxcblx0XHRcdGNsaWNrT3V0c2lkZVRvQ2xvc2U6IHRydWUsXG5cdFx0XHR0ZW1wbGF0ZVVybDogJ3ZpZXdzL3JlYXNzaWduUHJvamVjdHMuaHRtbCcsXG5cdFx0XHRjb250cm9sbGVyOiBmdW5jdGlvbiBEaWFsb2dDb250cm9sbGVyKCRtZERpYWxvZykge1xuXG5cdFx0XHRcdHRoYXQudXNlcnMuJGxvYWRlZCgpLnRoZW4oKHVzZXJzKSA9PiB7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2codXNlcnMpXG5cdFx0XHRcdFx0dGhpcy51c2VycyA9IHVzZXJzO1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHQvLyB0aGlzLnVzZXIgPSB1c2VyO1xuXHRcdFx0XHR0aGlzLmNsb3NlRGlhbG9nID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRcdCRtZERpYWxvZy5oaWRlKCk7XG5cdFx0XHRcdH07XG5cblx0XHRcdFx0dGhpcy5jaGFuZ2UgPSBmdW5jdGlvbiAodXNlcikge1xuXHRcdFx0XHRcdHRoYXQuY2xpZW50cy4kbG9hZGVkKCkudGhlbigoY2xpZW50cykgPT4ge1xuXHRcdFx0XHRcdFx0Y2xpZW50cy5tYXAoZnVuY3Rpb24gKGN2LCBpLCBhKSB7XG5cdFx0XHRcdFx0XHRcdGlmIChjdi50ZWNoSUQgPT09IHVzZXJUb1JlYXNzaWduLiRpZCkge1xuXHRcdFx0XHRcdFx0XHRcdC8vY2xpZW50IHRvIHVwZGF0ZSB0aGUgdGVjaCBmb3Jcblx0XHRcdFx0XHRcdFx0XHR2YXIgY2xpZW50ID0gdGhhdC4kZmlyZWJhc2VPYmplY3QodGhhdC5yZWYuY2hpbGQoJ2NsaWVudHMnKS5jaGlsZChjdi4kaWQpKVxuXHRcdFx0XHRcdFx0XHRcdGNsaWVudC4kbG9hZGVkKCkudGhlbigoY2xpZW50KSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhjbGllbnQudGVjaClcblxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xpZW50LnRlY2ggPSB1c2VyLm5hbWVcblx0XHRcdFx0XHRcdFx0XHRcdGNsaWVudC50ZWNoSUQgPSB1c2VyLiRpZFxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xpZW50LiRzYXZlKCkudGhlbigocmVmKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKFwiY2hhbmdlIHN1Y2Nlc3NmdWxcIilcblx0XHRcdFx0XHRcdFx0XHRcdFx0dGhhdC5zaG93VG9hc3QoeyBtc2c6IFwiUHJvamVjdHMgcmVhc3NpZ24gdG86IFwiICsgdXNlci5uYW1lIH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQkbWREaWFsb2cuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHRcdFx0fSwgZnVuY3Rpb24gKGVycm9yKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdCRtZERpYWxvZy5oaWRlKCk7XG5cdFx0XHRcdFx0XHRcdFx0XHRcdHRoYXQuc2hvd0FsZXJ0KGVycm9yKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIkVycm9yOlwiLCBlcnJvcik7XG5cdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9O1xuXHRcdFx0fSxcblx0XHRcdGNvbnRyb2xsZXJBczogJ3ZtJ1xuXHRcdH0pO1xuXHR9XG5cdC8qKlxuXHQgKiBAbmdkb2MgbWV0aG9kXG5cdCAqIEBuYW1lIHNob3dFZGl0RGlhbG9nXG5cdCAqIEBtZXRob2RPZiBDb25kaXRpb25BcHAuY29udHJvbGxlci5BZG1pbkN0cmxcblx0ICogQGRlc2NyaXB0aW9uXG5cdCAqIHNob3cgZWRpdCBkaWFsb2cuICBFZGl0IGFkbWluIHVzZXJcblx0ICogQHBhcmFtIHtPYmplY3R9ICRldmVudCBCdXR0b24gY2xpY2tcblx0ICogQHBhcmFtIHtPYmplY3R9IG9iaiBUaGUgdXNlciB0byBlZGl0XG5cdCAqL1xuXG5cdHNob3dFZGl0RGlhbG9nKCRldmVudCwgb2JqKSB7XG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXHRcdHZhciB0ZW1wbGF0ZVVybCA9IHVuZGVmaW5lZDtcblxuXHRcdGlmIChvYmouZGF0YS5uYW1lID09PSAnQWxsIENyZXdzJykge1xuXHRcdFx0cmV0dXJuXG5cdFx0fVxuXHRcdHZhciBlbnVtVGFiID0ge1xuXHRcdFx0ZWRpdFVzZXI6IDEsXG5cdFx0XHRlZGl0Q3JldzogMlxuXHRcdH1cblx0XHRzd2l0Y2ggKHRoaXMuc2VsZWN0ZWRJbmRleCkge1xuXHRcdFx0Y2FzZSAxOlxuXHRcdFx0XHR0ZW1wbGF0ZVVybCA9ICd2aWV3cy9lZGl0VXNlci5odG1sJ1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgMjpcblx0XHRcdFx0dGVtcGxhdGVVcmwgPSAndmlld3MvYWRkQ3Jldy5odG1sJ1xuXHRcdFx0XHRicmVhaztcblxuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0YnJlYWs7XG5cdFx0fVxuXHRcdHRoaXMuJG1kRGlhbG9nLnNob3coe1xuXHRcdFx0dGFyZ2V0RXZlbnQ6ICRldmVudCxcblx0XHRcdGNsaWNrT3V0c2lkZVRvQ2xvc2U6IHRydWUsXG5cdFx0XHRzY29wZTogdGhpcy4kc2NvcGUsICAgICAgICAvLyB1c2UgcGFyZW50IHNjb3BlIGluIHRlbXBsYXRlXG5cdFx0XHRwcmVzZXJ2ZVNjb3BlOiB0cnVlLCAgLy8gZG8gbm90IGZvcmdldCB0aGlzIGlmIHVzZSBwYXJlbnQgc2NvcGVcblx0XHRcdHRlbXBsYXRlVXJsOiB0ZW1wbGF0ZVVybCxcblx0XHRcdGNvbnRyb2xsZXI6IGZ1bmN0aW9uIERpYWxvZ0NvbnRyb2xsZXIoJHNjb3BlLCAkbWREaWFsb2cpIHtcblxuXG5cdFx0XHRcdGxldCBzZWFyY2gsIG1vZGVsO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly9pZGVudGlmZWQgd2hpY2ggbW9kZWwgaW5mb3JtYXRpb24gdG8gbG9hZFxuXHRcdFx0XHRzd2l0Y2ggKHRoYXQuc2VsZWN0ZWRJbmRleCkge1xuXHRcdFx0XHRcdGNhc2UgZW51bVRhYi5lZGl0VXNlcjogLy8gTG9hZCB1c2VyIGluZm9cblx0XHRcdFx0XHRcdHNlYXJjaCA9ICd1c2Vycydcblx0XHRcdFx0XHRcdG1vZGVsID0gJ3VzZXInXG5cdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRjYXNlIGVudW1UYWIuZWRpdENyZXc6IC8vIExvYWQgY3JldyBpbmZvXG5cdFx0XHRcdFx0XHRzZWFyY2ggPSAnY3Jld3MnXG5cdFx0XHRcdFx0XHRtb2RlbCA9ICdjcmV3J1xuXHRcdFx0XHRcdFx0dGhpcy5idG5OYW1lID0gZmFsc2UgLy8gZGlzcGxheSBBZGRcblx0XHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0dGhpc1ttb2RlbF0gPSB0aGF0LiRmaXJlYmFzZU9iamVjdCh0aGF0LnJlZi5jaGlsZChzZWFyY2gpLmNoaWxkKG9iai5kYXRhLiRpZCkpO1xuXG5cdFx0XHRcdHRoaXMudXBkYXRlID0gZnVuY3Rpb24gKGRhdGEsIGZvcm0pIHtcblxuXHRcdFx0XHRcdGZvcm0uJHNldFByaXN0aW5lKCk7XG5cdFx0XHRcdFx0Zm9ybS4kc2V0VW50b3VjaGVkKCk7XG5cdFx0XHRcdFx0JG1kRGlhbG9nLmhpZGUoKTtcblxuXHRcdFx0XHRcdHN3aXRjaCAodGhhdC5zZWxlY3RlZEluZGV4KSB7XG5cdFx0XHRcdFx0XHRjYXNlIGVudW1UYWIuZWRpdFVzZXI6XG5cdFx0XHRcdFx0XHRcdHVwZGF0ZVVzZXIoZGF0YSk7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSBlbnVtVGFiLmVkaXRDcmV3OlxuXHRcdFx0XHRcdFx0XHR1cGRhdGVDcmV3KGRhdGEpO1xuXHRcdFx0XHRcdFx0XHRicmVhaztcblxuXHRcdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdH07XG5cblx0XHRcdFx0ZnVuY3Rpb24gdXBkYXRlVXNlcih1c2VyKSB7XG5cdFx0XHRcdFx0aWYgKHVzZXIudGVtcENyZXcpIHtcblx0XHRcdFx0XHRcdGxldCB0ZW1wID0gdXNlci5jcmV3XG5cdFx0XHRcdFx0XHR1c2VyLmNyZXcgPSB1c2VyLnRlbXBDcmV3O1xuXHRcdFx0XHRcdFx0dXNlci50ZW1wQ3JldyA9IHRlbXA7XG5cdFx0XHRcdFx0XHR1c2VyLnRlbXBEYXRlID0gdGhhdC5EYXRlSVNPLnRvbW9ycm93XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0aWYgKHVzZXIubmV3cGFzc3dvcmQpIHtcblx0XHRcdFx0XHRcdHRoYXQuYXV0aC4kY2hhbmdlUGFzc3dvcmQoe1xuXHRcdFx0XHRcdFx0XHRlbWFpbDogdXNlci5lbWFpbCxcblx0XHRcdFx0XHRcdFx0b2xkUGFzc3dvcmQ6IHVzZXIucGFzc3dvcmQsXG5cdFx0XHRcdFx0XHRcdG5ld1Bhc3N3b3JkOiB1c2VyLm5ld3Bhc3N3b3JkXG5cdFx0XHRcdFx0XHR9KS50aGVuKCgpID0+IHtcblx0XHRcdFx0XHRcdFx0dXNlci5wYXNzd29yZCA9IHVzZXIubmV3cGFzc3dvcmQ7XG5cdFx0XHRcdFx0XHRcdGRlbGV0ZSB1c2VyLm5ld3Bhc3N3b3JkO1xuXHRcdFx0XHRcdFx0XHR1c2VyLiRzYXZlKCk7XG5cdFx0XHRcdFx0XHRcdHVzZXIuJGxvYWRlZCgpLnRoZW4oZnVuY3Rpb24gKHVzZXIpIHtcblx0XHRcdFx0XHRcdFx0XHR0aGF0LkxvY2FsRGF0YWJhc2UucHV0KCdhbGxvd2VkJywgdXNlcik7XG5cdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdHRoYXQuc2hvd1RvYXN0KHsgbXNnOiBcIlN1Y2Nlc3NmdWxseSBDaGFuZ2VkIERhdGFcIiB9KTtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coXCJQYXNzd29yZCBjaGFuZ2VkIHN1Y2Nlc3NmdWxseSFcIik7XG5cdFx0XHRcdFx0XHR9KS5jYXRjaCgoZXJyb3IpID0+IHtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihcIkVycm9yOiBcIiwgZXJyb3IpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdGRlbGV0ZSB1c2VyLm5ld3Bhc3N3b3JkO1xuXHRcdFx0XHRcdFx0dGhhdC5zaG93VG9hc3QoeyBtc2c6IFwiU3VjY2Vzc2Z1bGx5IENoYW5nZWQgRGF0YVwiIH0pO1xuXHRcdFx0XHRcdFx0dXNlci4kc2F2ZSgpO1xuXHRcdFx0XHRcdFx0dGhhdC4kcm9vdFNjb3BlLiRicm9hZGNhc3QoJ3N1Y2Nlc3NMb2dpbicsIHsgdXNlcjogdXNlciB9KTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRsZXQgbmV3VXNlciA9IHt9O1xuXHRcdFx0XHRcdF8uZXh0ZW5kT3duKG5ld1VzZXIsIHVzZXIpO1xuXHRcdFx0XHRcdGRlbGV0ZSBuZXdVc2VyLiQkY29uZjtcblx0XHRcdFx0XHR0aGF0LkxvY2FsRGF0YWJhc2UucHV0KCdhbGxvd2VkJywgbmV3VXNlcik7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRmdW5jdGlvbiB1cGRhdGVDcmV3KGNyZXcpIHtcblx0XHRcdFx0XHRjcmV3LiRzYXZlKCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHR0aGlzLmNsb3NlRGlhbG9nID0gKCkgPT4ge1xuXHRcdFx0XHRcdCRtZERpYWxvZy5oaWRlKCk7XG5cdFx0XHRcdH07XG5cblx0XHRcdFx0dGhpcy5yZW1vdmUgPSAoaXRlbVRvUmVtb3ZlKSA9PiB7XG5cdFx0XHRcdFx0dGhpcy5jbG9zZURpYWxvZygpO1xuXHRcdFx0XHRcdHRoYXQuX3JlbW92ZShpdGVtVG9SZW1vdmUpO1xuXG5cdFx0XHRcdH07XG5cblx0XHRcdFx0dGhpcy5zaG93UmVhc3NpZ25EaWFsb2cgPSAodXNlclRvUmVhc3NpZ24sIGZvcm0pID0+IHtcblx0XHRcdFx0XHR0aGlzLmNsb3NlRGlhbG9nKCk7XG5cdFx0XHRcdFx0dGhhdC5fc2hvd1JlYXNzaWduRGlhbG9nKHVzZXJUb1JlYXNzaWduLCBmb3JtKTtcblxuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdHRoaXMucmVtb3ZlVGVtcENyZXcgPSAodXNlcikgPT4ge1xuXHRcdFx0XHRcdHVzZXIuY3JldyA9IHVzZXIudGVtcENyZXc7XG5cdFx0XHRcdFx0dXNlci50ZW1wRGF0ZSA9IG51bGxcblx0XHRcdFx0XHR1c2VyLnRlbXBDcmV3ID0gbnVsbFxuXHRcdFx0XHR9XG5cdFx0XHR9LFxuXHRcdFx0Y29udHJvbGxlckFzOiAnbWREaWFsb2cnXG5cdFx0fSk7XG4gICAgfVxuXHQvKipcblx0ICogQG5nZG9jIG1ldGhvZFxuXHQgKiBAbmFtZSBzaG93QWRkRGlhbG9nXG5cdCAqIEBtZXRob2RPZiBDb25kaXRpb25BcHAuY29udHJvbGxlci5BZG1pbkN0cmxcblx0ICogQGRlc2NyaXB0aW9uXG5cdCAqIHNob3cgYWRkIGRpYWxvZy4gIEFkZCBhZG1pbiB1c2VyXG5cdCAqIEBwYXJhbSB7T2JqZWN0fSAkZXZlbnQgVGhlIGJ1dHRvbiBjbGlja2VkXG5cdCAqL1xuXG5cdHNob3dBZGREaWFsb2coJGV2ZW50KSB7XG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXG5cdFx0dmFyIHRlbXBsYXRlVXJsID0gdW5kZWZpbmVkO1xuXHRcdHN3aXRjaCAodGhpcy5zZWxlY3RlZEluZGV4KSB7XG5cdFx0XHRjYXNlIDE6XG5cblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlIDI6XG5cdFx0XHRcdHRlbXBsYXRlVXJsID0gJ3ZpZXdzL2FkZENyZXcuaHRtbCdcblx0XHRcdFx0YnJlYWs7XG5cblx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdGJyZWFrO1xuXHRcdH1cblx0XHR0aGlzLiRtZERpYWxvZy5zaG93KHtcblx0XHRcdHRhcmdldEV2ZW50OiAkZXZlbnQsXG5cdFx0XHRjbGlja091dHNpZGVUb0Nsb3NlOiB0cnVlLFxuXHRcdFx0c2NvcGU6IHRoaXMuJHNjb3BlLCAgICAgICAgLy8gdXNlIHBhcmVudCBzY29wZSBpbiB0ZW1wbGF0ZVxuXHRcdFx0cHJlc2VydmVTY29wZTogdHJ1ZSwgIC8vIGRvIG5vdCBmb3JnZXQgdGhpcyBpZiB1c2UgcGFyZW50IHNjb3BlXG5cdFx0XHQvLyBTaW5jZSBHcmVldGluZ0NvbnRyb2xsZXIgaXMgaW5zdGFudGlhdGVkIHdpdGggQ29udHJvbGxlckFzIHN5bnRheFxuXHRcdFx0Ly8gQU5EIHdlIGFyZSBwYXNzaW5nIHRoZSBwYXJlbnQgJyRzY29wZScgdG8gdGhlIGRpYWxvZywgd2UgTVVTVFxuXHRcdFx0Ly8gdXNlICd2bS48eHh4PicgaW4gdGhlIHRlbXBsYXRlIG1hcmt1cFxuXHRcdFx0dGVtcGxhdGVVcmw6IHRlbXBsYXRlVXJsLFxuXHRcdFx0Y29udHJvbGxlcjogZnVuY3Rpb24gRGlhbG9nQ29udHJvbGxlcigkc2NvcGUsICRtZERpYWxvZykge1xuXG5cdFx0XHRcdHRoaXMuYnRuTmFtZSA9IHRydWUgLy8gZGlzcGxheSBBZGRcblx0XHRcdFx0XG5cdFx0XHRcdHRoaXMuY2xvc2VEaWFsb2cgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0JG1kRGlhbG9nLmhpZGUoKTtcblx0XHRcdFx0fTtcblxuXHRcdFx0XHR0aGlzLmFkZCA9IGZ1bmN0aW9uIChpdGVtLCBmb3JtKSB7XG5cdFx0XHRcdFx0dGhhdC5jcmV3cy4kYWRkKGl0ZW0pO1xuXG5cdFx0XHRcdFx0Zm9ybS4kc2V0UHJpc3RpbmUoKTtcblx0XHRcdFx0XHRmb3JtLiRzZXRVbnRvdWNoZWQoKTtcblx0XHRcdFx0XHQkbWREaWFsb2cuaGlkZSgpO1xuXG5cblx0XHRcdFx0fTtcblx0XHRcdH0sXG5cdFx0XHRjb250cm9sbGVyQXM6ICdtZERpYWxvZydcblx0XHR9KTtcbiAgICB9XG5cdC8qKlxuXHQgKiBAbmdkb2MgbWV0aG9kXG5cdCAqIEBuYW1lIHNob3dUb2FzdFxuXHQgKiBAbWV0aG9kT2YgQ29uZGl0aW9uQXBwLmNvbnRyb2xsZXIuQWRtaW5DdHJsXG5cdCAqIEBkZXNjcmlwdGlvblxuXHQgKiBTaG93IHRvYXN0IG1lc3NhZ2UgYXQgdGhlIGJvdHRvbSBvZiB0aGUgc2NyZWVuXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSB0b2FzdCBUaGUgbWVzc2FnZSB0byBkaXNwbGF5XG5cdCAqL1xuXG5cdHNob3dUb2FzdCh0b2FzdCkge1xuXHRcdHZhciBtc2cgPSB0b2FzdC5tc2cgfHwgJ1VzZXI6ICcgKyB0b2FzdC5uYW1lICsgJyBoYXMgYmVlbiBlbnJvbGxlZCdcblx0XHR0aGlzLiRtZFRvYXN0LnNob3codGhpcy4kbWRUb2FzdC5zaW1wbGUoKS5jb250ZW50KG1zZykpO1xuXHR9XG59XG5leHBvcnQge0FkbWluQ3RybH07IiwiLyoqXG4gKiBBbGwgUmVwb3J0cyBDb250cm9sbGVyIGZvciBhbGxSZXBvcnRzLmh0bWxcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cbmNsYXNzIEFsbFJlcG9ydHNDdHJsIHtcblx0Y29uc3RydWN0b3IoJHN0YXRlLCAkc3RhdGVQYXJhbXMsICRmaXJlYmFzZUFycmF5LCBMb2FkUmVwb3J0c1NydiwgJHNjb3BlLCAkcm9vdFNjb3BlLCBMb2NhbERhdGFiYXNlLCBMb2FkaW5nUG9wdXApIHtcblx0XHR0aGlzLiRyb290U2NvcGUgPSAkcm9vdFNjb3BlO1xuXG5cdFx0Ly9jb3VudHMgdGhlIG51bWJlciBjb25kaXRpb24gcmVwb3J0cyBhZGRlZCB0byB0aGUgdGFiIGxpc3Rcblx0XHR0aGlzLmNvdW50T2ZDb25kaXRpb25SZXBvcnRzID0gMDtcblxuXHRcdC8vaGlkZXMgdGhlIGFkZCBidXR0b24gZm9yIGFkZGl0aW9uYWwgcmVwb3J0c1xuXHRcdHRoaXMuaGlkZUJ0biA9IHRydWU7XG5cblx0XHQvLyBhY2Nlc3MgdGhlIGRhdGEgdG8ga25vdyB3aGljaCByZXBvcnRzIHRvIGRpc3BsYXlcblx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXG5cdFx0XHRMb2FkUmVwb3J0c1Nydi5yZW1vdGUoJHNjb3BlKS50aGVuKChjbGllbnQpID0+IHtcblx0XHRcdFx0dGhpcy5jbGllbnQgPSBjbGllbnQ7XG5cblx0XHRcdFx0QWxsUmVwb3J0c0N0cmwubG9hZFRhYnMuY2FsbCh0aGlzKTtcblxuXHRcdFx0fSlcblx0XHR9IGVsc2Uge1xuXHRcdFx0TG9hZFJlcG9ydHNTcnYubG9jYWwoJHNjb3BlKS50aGVuKChjbGllbnQpID0+IHtcblx0XHRcdFx0Ly9zZXQgdGhlIHZpZXcgbW9kZWwgdG8gY2xpZW50IGRhdGEgZnJvbSBsb2NhbCBkYXRhYmFzZVxuXHRcdFx0XHR0aGlzLmNsaWVudCA9IGNsaWVudDtcblxuXHRcdFx0XHRBbGxSZXBvcnRzQ3RybC5sb2FkVGFicy5jYWxsKHRoaXMpO1xuXG5cdFx0XHRcdCRzY29wZS4kd2F0Y2hDb2xsZWN0aW9uKGZ1bmN0aW9uICgpIHsgcmV0dXJuIGNsaWVudCB9LCAobiwgbykgPT4ge1xuXHRcdFx0XHRcdGlmIChvICE9PSBuKSB7XG5cdFx0XHRcdFx0XHRMb2NhbERhdGFiYXNlLiRzYXZlKG4pXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XG5cblx0c3RhdGljIGxvYWRUYWJzKCkge1xuXHRcdHRoaXMudGFicyA9IFtcblx0XHRcdHtcblx0XHRcdFx0bGFiZWw6ICdDb25kaXRpb24gUmVwb3J0Jyxcblx0XHRcdFx0bmdpZjogdGhpcy5jbGllbnQuY29uZGl0aW9uLFxuXHRcdFx0XHRodG1sOiAndmlld3MvcmVwb3J0U2hlZXQuaHRtbCdcblx0XHRcdH0sIHtcblx0XHRcdFx0bGFiZWw6ICdEYW5nZXIgUmVwb3J0Jyxcblx0XHRcdFx0bmdpZjogdGhpcy5jbGllbnQuZGFuZ2VyLFxuXHRcdFx0XHRodG1sOiAndmlld3MvZGFuZ2VyU2hlZXQuaHRtbCdcblx0XHRcdH0sIHtcblx0XHRcdFx0bGFiZWw6ICdXYXRlciBSZXBvcnQnLFxuXHRcdFx0XHRuZ2lmOiB0aGlzLmNsaWVudC53YXRlcixcblx0XHRcdFx0aHRtbDogJ3ZpZXdzL3dhdGVyU2hlZXQuaHRtbCdcblx0XHRcdH1dXG5cblx0XHRpZiAodGhpcy5jbGllbnQuY29uZGl0aW9uRCkge1xuXHRcdFx0dGhpcy50YWJzLnNwbGljZSgxLCAwLCB7XG5cdFx0XHRcdGlkOiAnY3JlcG9ydCcsXG5cdFx0XHRcdGxhYmVsOiAnQ29uZGl0aW9uIFJlcG9ydCAxJyxcblx0XHRcdFx0bmdpZjogdGhpcy5jbGllbnQuY29uZGl0aW9uLFxuXHRcdFx0XHRodG1sOiAndmlld3MvcmVwb3J0U2hlZXQuMS5odG1sJ1xuXHRcdFx0fSlcblx0XHR9XG5cblx0XHRpZiAodGhpcy5jbGllbnQuY29uZGl0aW9uRykge1xuXHRcdFx0dGhpcy50YWJzLnNwbGljZSgyLCAwLCB7XG5cdFx0XHRcdGlkOiAnY3JlcG9ydCcsXG5cdFx0XHRcdGxhYmVsOiAnQ29uZGl0aW9uIFJlcG9ydCAyJyxcblx0XHRcdFx0bmdpZjogdGhpcy5jbGllbnQuY29uZGl0aW9uLFxuXHRcdFx0XHRodG1sOiAndmlld3MvcmVwb3J0U2hlZXQuMi5odG1sJ1xuXHRcdFx0fSlcblx0XHR9XG5cblx0XHR2YXIgY291bnRFeGlzdENvbmRpdGlvblJlcG9ydHMgPSBfLndoZXJlKHRoaXMudGFicywgeyBpZDogJ2NyZXBvcnQnIH0pXG5cdFx0dGhpcy5jb3VudE9mQ29uZGl0aW9uUmVwb3J0cyA9IGNvdW50RXhpc3RDb25kaXRpb25SZXBvcnRzLmxlbmd0aDtcblx0XHR2YXIgc2V0VGFiID0gXy5maW5kV2hlcmUodGhpcy50YWJzLCB7IG5naWY6IHRydWUgfSk7XG5cdFx0aWYgKHRoaXMuY291bnRPZkNvbmRpdGlvblJlcG9ydHMgPT0gMikge1xuXHRcdFx0dGhpcy5oaWRlQnRuID0gZmFsc2U7XG5cdFx0fVxuXG5cdFx0dGhpcy5zZWxlY3RUYWIoc2V0VGFiLmh0bWwpO1xuXHR9XG5cblx0YWRkQ29uZGl0aW9uUmVwb3J0KCkge1xuXHRcdGlmICh0aGlzLnNlbGVjdGVkSW5kZXggPSAwKSB7XG5cdFx0XHR0aGlzLnNlbGVjdGVkSW5kZXggPSB0aGlzLmN1cnJlbnRJbmRleDtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5zZWxlY3RlZEluZGV4ID0gdGhpcy5jdXJyZW50SW5kZXggKyB0aGlzLmNvdW50T2ZDb25kaXRpb25SZXBvcnRzO1xuXHRcdH1cblx0XHRcblx0XHQvLyB0aGlzLiRyb290U2NvcGUuJGJyb2FkY2FzdCgnbmV3Q29uZGl0aW9uUmVwb3J0Jyk7XG5cdFx0aWYgKCF0aGlzLnRhYnMpIHtcblx0XHRcdHJldHVyblxuXHRcdH1cblxuXHRcdGlmICghdGhpcy5jbGllbnQuY29uZGl0aW9uKSB7XG5cdFx0XHR0aGlzLnRhYnMuc3BsaWNlKDAsIDAsIHtcblx0XHRcdFx0bGFiZWw6ICdDb25kaXRpb24gUmVwb3J0Jyxcblx0XHRcdFx0bmdpZjogdHJ1ZSxcblx0XHRcdFx0aHRtbDogJ3ZpZXdzL3JlcG9ydFNoZWV0Lmh0bWwnXG5cdFx0XHR9KVxuXHRcdFx0dGhpcy5jbGllbnQuY29uZGl0aW9uID0gdHJ1ZTtcblx0XHRcdHJldHVyblxuXHRcdH1cblx0XHR0aGlzLmNvdW50T2ZDb25kaXRpb25SZXBvcnRzKys7XG5cblx0XHRpZiAodGhpcy5jb3VudE9mQ29uZGl0aW9uUmVwb3J0cyA8PSAyKSB7XG5cdFx0XHR0aGlzLnRhYnMuc3BsaWNlKHRoaXMuY291bnRPZkNvbmRpdGlvblJlcG9ydHMsIDAsIHtcblx0XHRcdFx0bGFiZWw6ICdDb25kaXRpb24gUmVwb3J0ICcgKyB0aGlzLmNvdW50T2ZDb25kaXRpb25SZXBvcnRzLFxuXHRcdFx0XHRuZ2lmOiB0cnVlLFxuXHRcdFx0XHRodG1sOiAndmlld3MvcmVwb3J0U2hlZXQuJyArIHRoaXMuY291bnRPZkNvbmRpdGlvblJlcG9ydHMgKyAnLmh0bWwnXG5cdFx0XHR9KVxuXHRcdH1cblxuXHRcdGlmICh0aGlzLmNvdW50T2ZDb25kaXRpb25SZXBvcnRzID09IDIpIHtcblxuXHRcdFx0dGhpcy5oaWRlQnRuID0gZmFsc2U7XG5cblx0XHR9XG5cdH1cblxuXHRhZGREYW5nZXJSZXBvcnQoKSB7XG5cdFx0dGhpcy50YWJzLnB1c2goe1xuXHRcdFx0bGFiZWw6ICdEYW5nZXIgUmVwb3J0Jyxcblx0XHRcdG5naWY6IHRydWUsXG5cdFx0XHRodG1sOiAndmlld3MvZGFuZ2VyU2hlZXQuaHRtbCdcblx0XHR9KTtcblxuXHRcdHRoaXMuY2xpZW50LmRhbmdlciA9IHRydWU7XG5cdH1cblxuXHRhZGRXYXRlclJlcG9ydCgpIHtcblx0XHR0aGlzLnRhYnMucHVzaCh7XG5cdFx0XHRsYWJlbDogJ1dhdGVyIFJlcG9ydCcsXG5cdFx0XHRuZ2lmOiB0cnVlLFxuXHRcdFx0aHRtbDogJ3ZpZXdzL3dhdGVyU2hlZXQuaHRtbCdcblx0XHR9KVxuXG5cdFx0dGhpcy5jbGllbnQud2F0ZXIgPSB0cnVlO1xuXHR9XG5cblx0c2VsZWN0VGFiKHRhYk5hbWUpIHtcblx0XHR0aGlzLnRhYkhUTUwgPSB0YWJOYW1lO1xuXHR9XG59XG5leHBvcnQge0FsbFJlcG9ydHNDdHJsfSIsIi8qKlxuICogQXBwIENvbnRyb2xsZXIgZm9yIGluZGV4Lmh0bWxcbiAqIEBwYXJhbSB7IXVpLnJvdXRlci4kc3RhZX0gJHN0YXRlXG4gKiBAcGFyYW0ge30gRmlyZUJhc2VBdXRoXG4gKiBAcGFyYW0ge30gTG9jYWxEYXRhYmFzZVxuICogQHBhcmFtIHshYW5ndWxhci4kcm9vdFNjb3BlfSAkcm9vdFNjb3BlXG4gKiBAcGFyYW0ge30gJGZpcmViYXNlQXJyYXlcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cblxuY2xhc3MgQXBwQ3RybCB7XG5cdGNvbnN0cnVjdG9yKCRzdGF0ZSwgRmlyZUJhc2VBdXRoLCBMb2NhbERhdGFiYXNlLCAkcm9vdFNjb3BlKSB7XG5cdFx0dGhpcy4kc3RhdGUgPSAkc3RhdGU7XG5cdFx0dGhpcy5tYXN0ZXIgPSB7IGVtYWlsOiAnJywgcGFzc3dvcmQ6ICcnIH07XG5cdFx0dGhpcy5hdXRoID0gRmlyZUJhc2VBdXRoXG5cdFx0dGhpcy5Mb2NhbERhdGFiYXNlID0gTG9jYWxEYXRhYmFzZTtcblx0XHR0aGlzLiRyb290U2NvcGUgPSAkcm9vdFNjb3BlO1xuXG5cdFx0dGhpcy51c2VyID0gdGhpcy5Mb2NhbERhdGFiYXNlLmdldCgnYWxsb3dlZCcpO1xuXG5cdFx0JHJvb3RTY29wZS4kb24oJ3N1Y2Nlc3NMb2dpbicsIChldnQsIGRhdGEpID0+IHtcblx0XHRcdHRoaXMudXNlciA9IHRoaXMuTG9jYWxEYXRhYmFzZS5nZXQoJ2FsbG93ZWQnKTtcblx0XHR9KVxuXHR9XG5cblx0bmF2KHNpdGUsICRldmVudCkge1xuXHRcdGlmIChzaXRlID09PSAnbG9nb3V0Jykge1xuXHRcdFx0Y29uc29sZS5sb2coc2l0ZSlcblx0XHRcdHRoaXMuYXV0aC4kdW5hdXRoKCk7XG5cdFx0XHR0aGlzLiRzdGF0ZS5nbygnbG9naW4nKTtcblx0XHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kdW5hdXRoKCk7XG5cdFx0XHR0aGlzLnVzZXIgPSAnJztcblx0XHRcdHJldHVyblxuXHRcdH1cblx0XHRpZiAoc2l0ZSA9PT0gJ2NyZWF0ZUpvYicpIHtcblx0XHRcdHRoaXMuJHN0YXRlLnRyYW5zaXRpb25UbyhzaXRlLCB7IGlkOiAnJyB9KTtcblxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLiRzdGF0ZS50cmFuc2l0aW9uVG8oc2l0ZSk7XG5cdFx0fVxuXG5cdH1cbn1cblxuXG5leHBvcnQge0FwcEN0cmx9O1xuXG4iLCIvKipcbiAqIENyZWF0ZSBKb2JzIENvbnRyb2xsZXIgY3JlYXRlSm9icy5odG1sXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cInVzZSBzdHJpY3RcIjtcbmNsYXNzIENyZWF0ZUpvYkN0cmwge1xuXHRjb25zdHJ1Y3RvcihGaXJlQmFzZVJlZiwgY3VycmVudEF1dGgsICRzY29wZSwgJGZpcmViYXNlQXJyYXksICRzdGF0ZSwgJG1kRGlhbG9nLCAkbG9jYXRpb24sXG5cdFx0JGFuY2hvclNjcm9sbCwgVG9hc3QsICRzdGF0ZVBhcmFtcywgJGZpcmViYXNlT2JqZWN0LCBMb2FkaW5nUG9wdXAsXG5cdFx0T3JpZW50YXRpb24sICR3aW5kb3csIGRldmljZURldGVjdG9yLCBQb2ludHNEYXRhLCBMb2NhbERhdGFiYXNlKSB7XG5cdFx0XG5cdFx0Ly9sb2FkIHN0YXRlc1xuXHRcdHRoaXMuc3RhdGVzID0gUG9pbnRzRGF0YS5zdGF0ZXM7XG5cdFx0XG5cdFx0Ly8gR2V0IGEgZGF0YWJhc2UgcmVmZXJlbmNlIFxuXHRcdHRoaXMucmVmID0gRmlyZUJhc2VSZWYucmVmO1xuXHRcdHRoaXMuJGZpcmViYXNlQXJyYXkgPSAkZmlyZWJhc2VBcnJheTtcblx0XHR0aGlzLiRtZERpYWxvZyA9ICRtZERpYWxvZztcblx0XHR2YXIgY3Jld3NMaXN0ID0gJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ2NyZXdzJykpO1xuXHRcdHRoaXMuJGZpcmViYXNlT2JqZWN0ID0gJGZpcmViYXNlT2JqZWN0O1xuXHRcdHRoaXMubWFzdGVyID0ge307XG5cdFx0dGhpcy4kc3RhdGUgPSAkc3RhdGU7XG5cdFx0dGhpcy4kbG9jYXRpb24gPSAkbG9jYXRpb247XG5cdFx0dGhpcy4kYW5jaG9yU2Nyb2xsID0gJGFuY2hvclNjcm9sbDtcblx0XHR0aGlzLlRvYXN0ID0gVG9hc3Q7XG5cdFx0dGhpcy5pZCA9ICRzdGF0ZVBhcmFtcy5pZFxuXHRcdHRoaXMuTG9hZGluZ1BvcHVwID0gTG9hZGluZ1BvcHVwO1xuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZSA9IExvY2FsRGF0YWJhc2U7XG5cdFx0XG5cdFx0Ly9jbGVhbiB0aGUgZm9ybVxuXHRcdHRoaXMuY2xlYW5Gb3JtKCk7XG5cblxuXHRcdGlmICh0aGlzLmlkKSB7XG5cdFx0XHR0aGlzLm5hbWUgPSBcIkVkaXQgSm9iXCI7XG5cdFx0XHR0aGlzLmJ0bk5hbWUgPSBcIlNhdmUgSm9iXCI7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMubmFtZSA9IFwiQ3JlYXRlIEpvYlwiO1xuXHRcdFx0dGhpcy5idG5OYW1lID0gXCJDcmVhdGUgSm9iXCI7XG5cdFx0XHQvL2RlZmF1bHQgc3RhdGUgZmllbGQgdG8gUEFcblx0XHRcdHRoaXMuY2xpZW50ID0geyBzdGF0ZTogJ1BBJyB9O1xuXHRcdH1cblxuXHRcdGNyZXdzTGlzdC4kbG9hZGVkKCkudGhlbigoaXRlbXMpID0+IHtcblx0XHRcdHRoaXMuY3Jld3MgPSBfLnJlamVjdChpdGVtcywgZnVuY3Rpb24gKHZhbHVlKSB7XG5cdFx0XHRcdHJldHVybiB2YWx1ZS5uYW1lID09PSBcIkFsbCBDcmV3c1wiO1xuXHRcdFx0fSk7XG5cdFx0fSk7XG5cblx0XHRpZiAodGhpcy5pZCkge1xuXHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuc2hvdygpO1xuXHRcdFx0Ly9saXN0IG9mIGFsbCBjbGllbnRzXG5cdFx0XHR2YXIgY2xpZW50cyA9ICRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdjbGllbnRzJykpO1xuXHRcdFx0Ly9sb29wIHRocm91Z2ggY2xpZW50cyBmb3IgYSBtYXRjaFxuXHRcdFx0Y2xpZW50cy4kbG9hZGVkKCkudGhlbigoY2xpZW50cykgPT4ge1xuXHRcdFx0XHRjbGllbnRzLm1hcCgoY3YsIGksIGEpID0+IHtcblx0XHRcdFx0XHRpZiAoY3YuZm9yZWlnbktleSA9PT0gdGhpcy5pZCkge1xuXHRcdFx0XHRcdFx0dmFyIGpvYiA9IHRoaXMuJGZpcmViYXNlT2JqZWN0KHRoaXMucmVmLmNoaWxkKCdqb2InKS5jaGlsZChjdi5mb3JlaWduS2V5KSk7XG5cdFx0XHRcdFx0XHRqb2IuJGxvYWRlZCgpLnRoZW4oKGpvYikgPT4ge1xuXHRcdFx0XHRcdFx0XHRjdi5qb2JEYXRlID0gbmV3IERhdGUoam9iLmpvYkRhdGUpO1xuXHRcdFx0XHRcdFx0XHQvL3B1bGwgdGhlIGNyZXcgZm9yIGNsaWVudCBmcm9tIEpPQiBrZXlcblx0XHRcdFx0XHRcdFx0Y3YuY3JldyA9IGpvYi5jcmV3XG5cdFx0XHRcdFx0XHRcdHRoaXMuY2xpZW50ID0gY3Y7XG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHR9KTtcblx0XHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdFx0T3JpZW50YXRpb24uc291cmNlUGFnZSA9ICdjcmVhdGVKb2InOyBcblx0XHRcblx0XHQvL3NldCBtZC1jYXJkLWNvbnRlbnQgY3NzIGhlaWdodCBwcm9wZXJ0eVxuXHRcdCR3aW5kb3cub25vcmllbnRhdGlvbmNoYW5nZSA9ICgpID0+IHtcblx0XHRcdHRoaXMuc2Nyb2xsID0gT3JpZW50YXRpb24uc2Nyb2xsSGVpZ2h0O1xuXHRcdFx0JHNjb3BlLiRhcHBseSgpO1xuXHRcdH1cblx0XHRcblx0XHQvL2ZpeCB0aGUgaGVpZ2h0IG9uIGlwaG9uZSAyNTBweFxuXHRcdGlmIChkZXZpY2VEZXRlY3Rvci5kZXZpY2UgPT09ICdpcGhvbmUnKSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9ICdzY3JvbGwtaVBob25lJ1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHR9XG5cdH1cblxuXHRxdWVyeShzZWFyY2hUZXh0KSB7XG5cdFx0Y29uc29sZS5sb2coc2VhcmNoVGV4dCk7XG5cdFx0cmV0dXJuIF8ud2hlcmUodGhpcy5zdGF0ZXMsIHsgYWJicmV2aWF0aW9uOiBzZWFyY2hUZXh0IH0pO1xuXHR9XG5cblx0Y3JlYXRlSm9iKGZvcm0pIHtcblx0XHQvL2FkZGluZyBuZXcgam9iXG5cdFx0aWYgKCF0aGlzLmlkKSB7XG5cdFx0XHRpZiAoIXRoaXMuY2xpZW50LmNvbmRpdGlvbiAmJiAhdGhpcy5jbGllbnQuZGFuZ2VyICYmICF0aGlzLmNsaWVudC53YXRlcikge1xuXHRcdFx0XHR2YXIgYWxlcnQgPSB0aGlzLiRtZERpYWxvZy5hbGVydCh7XG5cdFx0XHRcdFx0dGl0bGU6ICdFcnJvcicsXG5cdFx0XHRcdFx0Y29udGVudDogJ1lvdSBtdXN0IGNob29zZSBhIHJlcG9ydCB0byBjcmVhdGUnLFxuXHRcdFx0XHRcdG9rOiAnT2snXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHRoaXMuJG1kRGlhbG9nLnNob3coYWxlcnQpXG5cblx0XHRcdFx0cmV0dXJuXG5cdFx0XHR9XG5cdFx0XHR2YXIgY3Jld1RvQWRkID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnam9iJykpO1xuXHRcdFx0dmFyIGpvYiA9IHtcblx0XHRcdFx0bmFtZTogdGhpcy5jbGllbnQubmFtZSxcblx0XHRcdFx0Y3JldzogdGhpcy5jbGllbnQuY3Jldyxcblx0XHRcdFx0c3RhdHVzOiAnT3BlbicsXG5cdFx0XHRcdGpvYkRhdGU6IHRoaXMuY2xpZW50LmpvYkRhdGUudG9JU09TdHJpbmcoKVxuXHRcdFx0fVxuXHRcdFx0Y3Jld1RvQWRkLiRhZGQoam9iKS50aGVuKChyZWYpID0+IHtcblx0XHRcdFx0dmFyIGNsaWVudHMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdjbGllbnRzJykpO1xuXHRcdFx0XHR2YXIgam9iT3JkZXJzID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnam9iT3JkZXInKSk7XG5cdFx0XHRcdHZhciBpZCA9IHJlZi5rZXkoKTtcblx0XHRcdFx0dmFyIHVzZXIgPSB0aGlzLkxvY2FsRGF0YWJhc2UuZ2V0KCdhbGxvd2VkJyk7XG5cdFx0XHRcdHRoaXMuY2xpZW50LmZvcmVpZ25LZXkgPSBpZDtcblx0XHRcdFx0Ly9yZW1vdmUgY3JldyBmcm9tIGNsaWVudCBrZXkuXG5cdFx0XHRcdGRlbGV0ZSB0aGlzLmNsaWVudC5jcmV3O1xuXG5cdFx0XHRcdGNsaWVudHMuJGFkZCh0aGlzLmNsaWVudCkudGhlbigoKSA9PiB7XG5cdFx0XHRcdFx0am9iT3JkZXJzLiRsb2FkZWQoKS50aGVuKChqb2JPcmRlcikgPT4ge1xuXHRcdFx0XHRcdFx0aWYgKGpvYk9yZGVyLmxlbmd0aCAhPT0gMCkge1xuXHRcdFx0XHRcdFx0XHR2YXIgam9iID0gdGhpcy4kZmlyZWJhc2VPYmplY3QodGhpcy5yZWYuY2hpbGQoJ2pvYicpLmNoaWxkKGlkKSk7XG5cdFx0XHRcdFx0XHRcdC8vaW5pdGlhbCBsb2FkIG9mIGpvYk9yZGVyIHRhYmxlXG5cdFx0XHRcdFx0XHRcdGpvYi4kbG9hZGVkKCkudGhlbigoX2pvYikgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdGpvYk9yZGVyLiRhZGQoeyBmb3JlaWduS2V5OiBfam9iLiRpZCwgb3JkZXI6IGpvYk9yZGVyLmxlbmd0aCB9KTtcblx0XHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHRoaXMuVG9hc3Quc2hvd1RvYXN0KHsgbXNnOiAnSm9iIEFkZGVkJyB9KTtcblxuXG5cdFx0XHRcdGlmICh1c2VyLmNyZXcgIT09ICdBbGwgQ3Jld3MnKSB7XG5cdFx0XHRcdFx0dmFyIGNvbmZpcm0gPSB0aGlzLiRtZERpYWxvZy5jb25maXJtKHtcblx0XHRcdFx0XHRcdHRpdGxlOiAnU3VjY2VzcycsXG5cdFx0XHRcdFx0XHRjb250ZW50OiAnV291bGQgeW91IGxpa2UgdG8gb3BlbiB0aGUgbmV3IGpvYicsXG5cdFx0XHRcdFx0XHRvazogJ09wZW4gSm9iJyxcblx0XHRcdFx0XHRcdGNhbmNlbDogJ0NyZWF0ZSBOZXcgSm9iJ1xuXHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0dGhpcy4kbWREaWFsb2cuc2hvdyhjb25maXJtKS50aGVuKChkYXRhKSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLiRzdGF0ZS5nbygnYWxsUmVwb3J0cy5yZXBvcnQnLCB7IGlkOiBpZCB9KTtcblx0XHRcdFx0XHRcdHRoaXMuY2xlYW5Gb3JtKGZvcm0pO1xuXHRcdFx0XHRcdH0sIChkYXRhKSA9PiB7XG5cdFx0XHRcdFx0XHR0aGlzLmNsZWFuRm9ybShmb3JtKTtcblxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMuY2xlYW5Gb3JtKGZvcm0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdH0gXG5cdFx0Ly91cGRhdGluZyBhbiBleGlzdGluZyBqb2Jcblx0XHRlbHNlIHtcblx0XHRcdHZhciBjbGllbnQgPSB0aGlzLiRmaXJlYmFzZU9iamVjdCh0aGlzLnJlZi5jaGlsZCgnY2xpZW50cycpLmNoaWxkKHRoaXMuY2xpZW50LiRpZCkpO1xuXG5cdFx0XHR0aGlzLkxvYWRpbmdQb3B1cC5zaG93KCk7XG5cblx0XHRcdHZhciBuYW1lID0gdGhpcy5jbGllbnQubmFtZTtcblx0XHRcdHZhciBjcmV3ID0gdGhpcy5jbGllbnQuY3Jldztcblx0XHRcdHZhciBqb2JEYXRlID0gdGhpcy5jbGllbnQuam9iRGF0ZS50b0lTT1N0cmluZygpO1xuXHRcdFx0ZGVsZXRlIHRoaXMuY2xpZW50LmNyZXc7XG5cblx0XHRcdGNsaWVudC4kbG9hZGVkKCkudGhlbigoY2xpZW50KSA9PiB7XG5cdFx0XHRcdGFuZ3VsYXIuY29weSh0aGlzLmNsaWVudCwgY2xpZW50KTtcblx0XHRcdFx0Y2xpZW50LiRzYXZlKCkudGhlbigocmVmKSA9PiB7XG5cdFx0XHRcdFx0dmFyIGpvYiA9IHRoaXMuJGZpcmViYXNlT2JqZWN0KHRoaXMucmVmLmNoaWxkKCdqb2InKS5jaGlsZChjbGllbnQuZm9yZWlnbktleSkpO1xuXHRcdFx0XHRcdGpvYi4kbG9hZGVkKCkudGhlbigoam9iKSA9PiB7XG5cdFx0XHRcdFx0XHRqb2IubmFtZSA9IG5hbWU7XG5cdFx0XHRcdFx0XHRqb2IuY3JldyA9IGNyZXc7XG5cdFx0XHRcdFx0XHRqb2Iuam9iRGF0ZSA9IGpvYkRhdGU7XG5cdFx0XHRcdFx0XHRqb2IuJHNhdmUoKS50aGVuKCgpID0+IHtcblx0XHRcdFx0XHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLiRzdGF0ZS5nbygnam9icycpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fSlcblx0XHRcdFx0fSk7XG5cdFx0XHR9KTtcblx0XHR9XG5cdH1cblxuXHRjbGVhbkZvcm0oZm9ybSkge1xuXHRcdGlmIChmb3JtKSB7XG5cdFx0XHRmb3JtLiRzZXRQcmlzdGluZSgpO1xuXHRcdFx0Zm9ybS4kc2V0VW50b3VjaGVkKCk7XG5cdFx0XHR0aGlzLmNsaWVudCA9IGFuZ3VsYXIuY29weSh0aGlzLm1hc3Rlcik7XG5cdFx0XHR0aGlzLmNsaWVudCA9IHsgc3RhdGU6ICdQQScgfTtcblx0XHR9XG5cdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRvcCcpLnNjcm9sbEludG9WaWV3KCk7XG5cblx0fVxuXG5cdGNhbmNlbChmb3JtKSB7XG5cdFx0Zm9ybS4kc2V0UHJpc3RpbmUoKTtcblx0XHRmb3JtLiRzZXRVbnRvdWNoZWQoKTtcblx0XHR0aGlzLmNsaWVudCA9IGFuZ3VsYXIuY29weSh0aGlzLm1hc3Rlcik7XG5cdFx0dGhpcy4kc3RhdGUuZ28oJ2pvYnMnKTtcblx0fVxufVxuXG5leHBvcnQge0NyZWF0ZUpvYkN0cmx9IiwiLyoqXG4gKiBEYW5nZXIgU2hlZXQgQ29udHJvbGxlciBmb3IgaW5kZXguaHRtbFxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuO1xuY2xhc3MgRGFuZ2VyU2hlZXRDdHJsIHtcblx0Y29uc3RydWN0b3IoRmlyZUJhc2VSZWYsICRmaXJlYmFzZU9iamVjdCwgJHN0YXRlLCAvKmN1cnJlbnRBdXRoLCovICRzY29wZSwgJGZpcmViYXNlQXJyYXksICRzdGF0ZVBhcmFtcyxcblx0XHRMb2FkaW5nUG9wdXAsIExvYWRSZXBvcnRzU3J2LCBTaWduUGFkLCBMb2NhbERhdGFiYXNlLCBGaXJlQmFzZUF1dGgsICRyb290U2NvcGUsXG5cdFx0T3JpZW50YXRpb24sICR3aW5kb3csIGRldmljZURldGVjdG9yLCBKb2JTdGF0dXMsIFByaW50KSB7XG5cblx0XHQvL3VpLXJvdXRlciBzdGF0ZSBzZXJ2aWNlXG5cdFx0dGhpcy4kc3RhdGUgPSAkc3RhdGU7XG5cdFx0XG5cdFx0Ly8gR2V0IGEgZGF0YWJhc2UgcmVmZXJlbmNlIFxuXHRcdHRoaXMucmVmID0gRmlyZUJhc2VSZWYucmVmO1xuXHRcdFxuXHRcdC8vU2lnbmF0dXJlIFBhZFxuXHRcdHRoaXMuY2FudmFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignY2FudmFzJyk7XG5cdFx0dGhpcy5TaWduUGFkID0gU2lnblBhZDtcblxuXHRcdHRoaXMuTG9hZFJlcG9ydHNTcnYgPSBMb2FkUmVwb3J0c1NydjtcblxuXHRcdHRoaXMuRmlyZUJhc2VBdXRoID0gRmlyZUJhc2VBdXRoO1xuXG5cdFx0dGhpcy4kZmlyZWJhc2VBcnJheSA9ICRmaXJlYmFzZUFycmF5O1xuXG5cdFx0dGhpcy5Mb2FkaW5nUG9wdXAgPSBMb2FkaW5nUG9wdXBcblxuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZSA9IExvY2FsRGF0YWJhc2U7XG5cblx0XHR0aGlzLiRzY29wZSA9ICRzY29wZTtcblxuXHRcdHRoaXMuSm9iU3RhdHVzID0gSm9iU3RhdHVzO1xuXG5cdFx0dGhpcy5QcmludCA9IFByaW50O1xuXHRcdFxuXHRcdC8vZGlzcGxheSBsb2FkaW5nIHdpbmRvd1xuXHRcdExvYWRpbmdQb3B1cC5zaG93KCk7XG5cblx0XHR0aGlzLmxvYWREYXRhKCk7XG5cblx0XHRPcmllbnRhdGlvbi5zb3VyY2VQYWdlID0gJ3JlcG9ydCc7XG5cblx0XHQkd2luZG93Lm9ub3JpZW50YXRpb25jaGFuZ2UgPSAoKSA9PiB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHRcdHRoaXMubG9hZERhdGEoKVxuXHRcdH1cblxuXHRcdGlmIChkZXZpY2VEZXRlY3Rvci5kZXZpY2UgPT09ICdpcGhvbmUnKSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9ICdzY3JvbGwtaVBob25lJ1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHR9XG5cblx0XHR0aGlzLiRzY29wZS4kb24oJ2Nvbm5lY3RlZCcsICgpID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKCdjb25uZWN0ZWQnKTtcblx0XHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kc3luY1RvRmlyZWJhc2UoKTtcblx0XHR9KTtcblxuXHR9XG5cblx0c3RhdGljIGluaXRTaWduUGFkKCkge1xuXHRcdC8vSW5pdCBTaWduYXR1cmVcblx0XHR0aGlzLlNpZ25QYWQuaW5pdCh0aGlzLmNhbnZhcywgdGhpcy5jbGllbnQpO1xuXHRcdHRoaXMuU2lnblBhZC5yZXNpemVDYW52YXMoKTtcblx0XHR0aGlzLlNpZ25QYWQuZ2V0U2lnbmF0dXJlKCk7XG5cdH1cblxuXHRsb2FkRGF0YSgpIHtcblx0XHQvL2NoZWNrIGlmIHRoZSBzeXN0ZW0gaXMgb25saW5lXG5cdFx0aWYgKG5hdmlnYXRvci5vbkxpbmUpIHtcblxuXHRcdFx0dGhpcy5Mb2FkUmVwb3J0c1Nydi5yZW1vdGUodGhpcy4kc2NvcGUpLnRoZW4oKGNsaWVudCkgPT4ge1xuXHRcdFx0XHR0aGlzLmNsaWVudCA9IGNsaWVudDtcblxuXHRcdFx0XHR0aGlzLkxvYWRSZXBvcnRzU3J2LnJlbW90ZVVzZXJzKCkudGhlbigoZGF0YSkgPT4ge1xuXHRcdFx0XHRcdHRoaXMudXNlcnMgPSBkYXRhLnVzZXJzO1xuXHRcdFx0XHRcdGlmICghdGhpcy5jbGllbnQudGVjaCkge1xuXHRcdFx0XHRcdFx0dGhpcy5jbGllbnQudGVjaCA9IGRhdGEudGVjaC5uYW1lO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHRoaXMucHJpbnRKb2IgPSBkYXRhLnRlY2gucHJpbnRKb2I7XG5cdFx0XHRcdFx0Ly9oaWRlIGxvYWRpbmcgd2luZG93XG5cdFx0XHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHRcblx0XHRcdFx0Ly9zZXQgJHdhdGNoIG9uIHZtLmNsaWVudCBzY29wZS4gQW55IGNoYW5nZXMgdG8gdm0uY2xpZW50IGFyZSBzYXZlZFxuXHRcdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHdhdGNoKHRoaXMuY2xpZW50KTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHQvL0luaXQgU2lnbmF0dXJlXG5cdFx0XHRcdFx0RGFuZ2VyU2hlZXRDdHJsLmluaXRTaWduUGFkLmNhbGwodGhpcyk7XG5cdFx0XHRcdH0sIDEpO1xuXHRcdFx0fSlcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5Mb2FkUmVwb3J0c1Nydi5sb2NhbCh0aGlzLiRzY29wZSkudGhlbigoY2xpZW50KSA9PiB7XG5cdFx0XHRcdC8vc2V0IHRoZSB2aWV3IG1vZGVsIHRvIGNsaWVudCBkYXRhIGZyb20gbG9jYWwgZGF0YWJhc2Vcblx0XHRcdFx0dGhpcy5jbGllbnQgPSBjbGllbnQ7XG5cdFx0XHRcdFxuXHRcdFx0XHQvL2xvYWQgY3VycmVudCBhZG1pbiB1c2VyIGZyb20gbG9jYWwgZGF0YWJhc2Vcblx0XHRcdFx0dmFyIHVzZXIgPSB0aGlzLkxvY2FsRGF0YWJhc2UuJGdldEN1cnJlbnRBdXRoKClcblx0XHRcdFx0XG5cdFx0XHRcdC8vc2V0IHRoZSB0ZWNoIGJhc2VkIG9uIHRoZSBjdXJyZW50IHVzZXIncyBlbWFpbFxuXHRcdFx0XHR2YXIgdXNlcnMgPSB0aGlzLkxvY2FsRGF0YWJhc2UuZ2V0KCd1c2VycycpO1xuXHRcdFx0XHR0aGlzLnVzZXJzID0gdXNlcnM7XG5cblx0XHRcdFx0Ly9zZXQgdGhlIHRlY2hcblx0XHRcdFx0aWYgKCF0aGlzLmNsaWVudC50ZWNoKSB7XG5cdFx0XHRcdFx0dGhpcy5jbGllbnQudGVjaCA9IHVzZXIubmFtZTtcblx0XHRcdFx0XHR0aGlzLmNsaWVudC50ZWNoSUQgPSB1c2VyLiRpZDtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Ly9zZXQgJHdhdGNoIG9uIHZtLmNsaWVudCBzY29wZS4gQW55IGNoYW5nZXMgdG8gdm0uY2xpZW50IGFyZSBzYXZlZFxuXHRcdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHdhdGNoKHRoaXMuY2xpZW50KTtcblxuXHRcdFx0XHREYW5nZXJTaGVldEN0cmwuaW5pdFNpZ25QYWQuY2FsbCh0aGlzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vaGlkZSBsb2FkaW5nIHdpbmRvd1xuXHRcdFx0XHR0aGlzLkxvYWRpbmdQb3B1cC5oaWRlKCk7XG5cdFx0XHR9KVxuXHRcdH1cblx0fVxuXG5cdHNpZ25hdHVyZSgpIHtcblx0XHR0aGlzLlNpZ25QYWQuY2xlYXIoKTtcblx0fVxuXG5cdGNsb3NlKCkge1xuXHRcdHRoaXMuJHN0YXRlLmdvKCdqb2JzJyk7XG5cdFx0dGhpcy5QcmludC5yZXNldE5hbWUoKTtcblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHVud2F0Y2goKTtcblx0fVxuXG5cdGNvbXBsZXRlKCkge1xuXHRcdHRoaXMuSm9iU3RhdHVzLnNldFN0YXR1cyh0aGlzLmNsaWVudCwgJ0NvbXBsZXRlJyk7XG5cdFx0dGhpcy5jbG9zZSgpO1xuXHR9XG5cblx0cHJpbnQoKSB7XG5cdFx0dGhpcy5Kb2JTdGF0dXMuc2V0U3RhdHVzKHRoaXMuY2xpZW50LCAnUHJpbnRlZCcpO1xuXHRcdHRoaXMuUHJpbnQudGl0bGVOYW1lID0geyBuYW1lOiB0aGlzLmNsaWVudC5uYW1lLCByZXBvcnQ6ICdEYW5nZXIgUmVwb3J0JyB9O1xuXHRcdHRoaXMuUHJpbnQuZXhlYygpO1xuXHR9XG59XG5cbmV4cG9ydCB7RGFuZ2VyU2hlZXRDdHJsfSIsIi8qKlxuICogSm9icyBDb250cm9sbGVyIGpvYnMuaHRtbFxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5jbGFzcyBKb2JzQ3RybCB7XG5cdGNvbnN0cnVjdG9yKEZpcmVCYXNlUmVmLCAkZmlyZWJhc2VPYmplY3QsICRzdGF0ZSwgY3VycmVudEF1dGgsICRzY29wZSwgJGZpcmViYXNlQXJyYXksXG5cdFx0VG9hc3QsIExvY2FsRGF0YWJhc2UsICRtZERpYWxvZywgJHdpbmRvdywgT3JpZW50YXRpb24sIGRldmljZURldGVjdG9yLFxuXHRcdCRtZEJvdHRvbVNoZWV0LCBEYXRlSVNPLCAkaW50ZXJ2YWwsICRmaWx0ZXIpIHtcblx0XHRcblx0XHQvL2NvbnZlcnRzIGxvY2FsIGlzby1zdHJpbmcgdG8gaGFuZGxlZCB0aW1lem9uZVxuXHRcdHRoaXMuRGF0ZUlTTyA9IERhdGVJU09cblxuXHRcdHRoaXMuJHN0YXRlID0gJHN0YXRlO1xuXHRcdC8vIEdldCBhIGRhdGFiYXNlIHJlZmVyZW5jZSBcblx0XHR0aGlzLnJlZiA9IEZpcmVCYXNlUmVmLnJlZjtcblx0XHR0aGlzLiRmaXJlYmFzZUFycmF5ID0gJGZpcmViYXNlQXJyYXk7XG5cdFx0dGhpcy4kZmlyZWJhc2VPYmplY3QgPSAkZmlyZWJhc2VPYmplY3Q7XG5cdFx0dGhpcy5Ub2FzdCA9IFRvYXN0O1xuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZSA9IExvY2FsRGF0YWJhc2U7XG5cdFx0dGhpcy4kc2NvcGUgPSAkc2NvcGU7XG5cdFx0dGhpcy4kZmlsdGVyID0gJGZpbHRlcjtcblx0XHRcblx0XHQvL2luamVjdCAkbWRCb3R0b21TaGVldCBzZXJ2aWNlXG5cdFx0dGhpcy4kbWRCb3R0b21TaGVldCA9ICRtZEJvdHRvbVNoZWV0O1xuXG5cdFx0dGhpcy4kaW50ZXJ2YWwgPSAkaW50ZXJ2YWw7XG5cdFx0XG5cdFx0Ly9ob2xkcyBhIGxpc3Qgb2Ygc2VsZWN0ZWQgaXRlbXNcblx0XHR0aGlzLnNlbGVjdGVkTGlzdCA9IFtdO1xuXHRcdFxuXHRcdFxuXHRcdC8vIEdldCBzaWduZWQgaW4gdXNlcidzIGVtYWlsIGFkZHJlc3Ncblx0XHQvL2NoZWNrIHRvIHNlZSBpZiB0aGUgc3lzdGVtIGlzIG9ubGluZVxuXHRcdGlmIChuYXZpZ2F0b3Iub25MaW5lKSB7XG5cdFx0XHRpZiAoY3VycmVudEF1dGggIT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdHRoaXMuYWRtaW5FbWFpbCA9IGN1cnJlbnRBdXRoLnBhc3N3b3JkLmVtYWlsO1xuXHRcdFx0XHR0aGlzLmxvYWRKb2JzKGZhbHNlKTtcblx0XHRcdH1cblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5hZG1pbkVtYWlsID0gdGhpcy5Mb2NhbERhdGFiYXNlLiRnZXRDdXJyZW50QXV0aCgpLmVtYWlsO1xuXHRcdFx0dGhpcy5sb2FkSm9icyh0cnVlKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly9zZXQgYWxsb3dlZCBidXR0b24gaXRlbXNcblx0XHR0aGlzLnVzZXIgPSB0aGlzLkxvY2FsRGF0YWJhc2UuZ2V0KCdhbGxvd2VkJyk7XG5cdFx0aWYgKHRoaXMudXNlcikge1xuXHRcdFx0dGhpcy5lZGl0Sm9iID0gdGhpcy51c2VyLmVkaXRKb2I7XG5cdFx0XHR0aGlzLmRlbGV0ZUpvYiA9IHRoaXMudXNlci5kZWxldGVKb2I7XG5cdFx0XHR0aGlzLnByaW50Sm9iID0gdGhpcy51c2VyLnByaW50Sm9iO1xuXHRcdH1cblxuXHRcdHRoaXMuJG1kRGlhbG9nID0gJG1kRGlhbG9nO1xuXG5cdFx0Ly9zZXQgb3JpZW50YXRpb24gY3NzIGZpbGUgLSBcblx0XHRPcmllbnRhdGlvbi5zb3VyY2VQYWdlID0gJ2pvYidcblxuXHRcdCR3aW5kb3cub25vcmllbnRhdGlvbmNoYW5nZSA9ICgpID0+IHtcblx0XHRcdHRoaXMuc2Nyb2xsID0gT3JpZW50YXRpb24uc2Nyb2xsSGVpZ2h0O1xuXHRcdFx0JHNjb3BlLiRhcHBseSgpO1xuXHRcdH1cblxuXHRcdC8vc2V0IGZpeCBoZWlnaHQgZm9yIGlQaG9uZVxuXHRcdGlmIChkZXZpY2VEZXRlY3Rvci5kZXZpY2UgPT09ICdpcGhvbmUnKSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9ICdzY3JvbGwtaVBob25lJ1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHR9XG5cblx0XHQvL2NoZWNrIHRvIHNlZSBpZiB0aGUgdGVtcCBjcmV3IGRhdGUgaGFzIHBhc3NlZFxuXHRcdC8vIEpvYnNDdHJsLnRlbXBDcmV3LmNhbGwodGhpcyk7XG5cblx0XHQvL2NoZWNrIGV2ZXJ5IDFzIHRvIHNlZSBpZiB0ZW1wIGNyZXcgZGF0ZSBoYXMgcGFzc2VkXG5cdFx0Lyp0aGlzLmNoZWNrVGVtcENyZXcgPSB0aGlzLiRpbnRlcnZhbCgoKSA9PiB7XG5cdFx0XHRKb2JzQ3RybC50ZW1wQ3Jldy5jYWxsKHRoaXMpO1xuXHRcdH0sIDEwMDApOyovXG5cblxuXHRcdHRoaXMuJHNjb3BlLiRvbignY29ubmVjdGVkJywgKCkgPT4ge1xuXHRcdFx0Y29uc29sZS5sb2coJ2Nvbm5lY3RlZCcpO1xuXHRcdFx0dGhpcy5sb2FkSm9icyhmYWxzZSk7XG5cdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHN5bmNUb0ZpcmViYXNlKCk7XG5cdFx0fSk7XG5cdFx0XG5cdFx0Ly9zZXQgJHdhdGNoIG9uIHZtLnNlYXJjaCBzY29wZS4gQW55IGNoYW5nZXMgdG8gdm0uc2VhcmNoIGFyZSBzYXZlZFxuXHRcdHRoaXMudW53YXRjaCA9IHRoaXMuJHNjb3BlLiR3YXRjaENvbGxlY3Rpb24oKCkgPT4geyByZXR1cm4gdGhpcy5zZWFyY2ggfSwgKG4sIG8pID0+IHtcblx0XHRcdGlmIChvICE9PSBuKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKG4pO1xuXHRcdFx0XHR0aGlzLm9uU2VhcmNoSGFuZGxlcigpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHRyYWNrU2l0ZUxvY2F0aW9uID0geyBzaXRlOiAnam9icycgfVxuXG5cdH1cblxuXHRzdGF0aWMgdGVtcENyZXcoKSB7XG5cdFx0Ly91cGRhdGUgY3JldyBpZiB0aGUgdGVtcCBkYXRlIGhhcyBiZWVuIHBhc3NlZFxuXHRcdGlmICh0aGlzLnVzZXIgfHwgdGhpcy51c2VyLnRlbXBEYXRlKSB7XG5cdFx0XHRsZXQgcmVtb3RlVXNlciA9IHRoaXMuJGZpcmViYXNlT2JqZWN0KHRoaXMucmVmLmNoaWxkKCd1c2VycycpLmNoaWxkKHRoaXMudXNlci4kaWQpKTtcblx0XHRcdHJlbW90ZVVzZXIuJGxvYWRlZCgpLnRoZW4oKF9yZW1vdGVVc2VyKSA9PiB7XG5cdFx0XHRcdGxldCBjdXJEYXRlID0gdGhpcy5EYXRlSVNPLnRvZGF5XG5cdFx0XHRcdGxldCBpc0FmdGVyID0gbW9tZW50KGN1ckRhdGUpLmlzQWZ0ZXIoX3JlbW90ZVVzZXIudGVtcERhdGUsICdkYXknKTtcblx0XHRcdFx0bGV0IGlzU2FtZSA9IG1vbWVudChfcmVtb3RlVXNlci50ZW1wRGF0ZSkuaXNTYW1lKGN1ckRhdGUsICdkYXknKTtcblx0XHRcdFx0aWYgKF9yZW1vdGVVc2VyLnRlbXBEYXRlKSB7XG5cdFx0XHRcdFx0aWYgKGlzQWZ0ZXIgfHwgaXNTYW1lKSB7XG5cdFx0XHRcdFx0XHRfcmVtb3RlVXNlci5jcmV3ID0gdGhpcy51c2VyLnRlbXBDcmV3O1xuXHRcdFx0XHRcdFx0X3JlbW90ZVVzZXIudGVtcERhdGUgPSBudWxsXG5cdFx0XHRcdFx0XHRfcmVtb3RlVXNlci50ZW1wQ3JldyA9IG51bGxcblx0XHRcdFx0XHRcdF9yZW1vdGVVc2VyLiRzYXZlKCkudGhlbigoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdGxldCBuZXdVc2VyID0ge307XG5cdFx0XHRcdFx0XHRcdF8uZXh0ZW5kT3duKG5ld1VzZXIsIF9yZW1vdGVVc2VyKTtcblx0XHRcdFx0XHRcdFx0ZGVsZXRlIG5ld1VzZXIuJCRjb25mO1xuXHRcdFx0XHRcdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UucHV0KCdhbGxvd2VkJywgbmV3VXNlcik7XG5cdFx0XHRcdFx0XHRcdHRoaXMubG9hZEpvYnMoZmFsc2UpO1xuXHRcdFx0XHRcdFx0XHR0aGlzLiRpbnRlcnZhbC5jYW5jZWwodGhpcy5jaGVja1RlbXBDcmV3KTtcblx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLiRpbnRlcnZhbC5jYW5jZWwodGhpcy5jaGVja1RlbXBDcmV3KTtcblx0XHR9XG5cblx0fVxuXG5cdC8qKlxuXHQgKiBMb2FkIGpvYnMgZGF0YVxuXHQgKiBAdHlwZSB7Ym9vbGVhbn0gbG9jYWwgVHJ1ZSBpZiBsb2FkaW5nIGZyb20gbG9jYWwgZGF0YWJhc2Vcblx0ICovXG5cdGxvYWRKb2JzKGxvY2FsKSB7XG5cdFx0aWYgKCFsb2NhbCkgeyAvL29ubGluZSBydW4gdGhpcyBjb2RlXG5cdFx0XHRsZXQgdXNlcnMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCd1c2VycycpKTtcblx0XHRcdC8vYmluZCBjcmV3IG9iamVjdCB0byB0aGUgJHNjb3BlLmNyZXcgdG8gcHVsbCB0aGUgdmFsdWUgZm9yIGNyZXdcblx0XHRcdHVzZXJzLiRsb2FkZWQoKS50aGVuKCh1c2VycykgPT4ge1xuXG5cdFx0XHRcdHVzZXJzLm1hcCgoY3VycmVudFZhbHVlLCBpbmRleCwgYXJyYXkpID0+IHtcblx0XHRcdFx0XHR2YXIgam9ic0xpc3QgPSBbXTtcblx0XHRcdFx0XHR2YXIgam9icyA9IHVuZGVmaW5lZDtcblxuXHRcdFx0XHRcdGlmIChhbmd1bGFyLmxvd2VyY2FzZShjdXJyZW50VmFsdWUuZW1haWwpID09PSBhbmd1bGFyLmxvd2VyY2FzZSh0aGlzLmFkbWluRW1haWwpKSB7XG5cdFx0XHRcdFx0XHRsZXQgam9iT3JkZXIgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdqb2JPcmRlcicpKTtcblx0XHRcdFx0XHRcdGpvYnMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdqb2InKSk7XG5cdFx0XHRcdFx0XHRqb2JzLiRsb2FkZWQoKS50aGVuKChqb2JzKSA9PiB7XG5cblx0XHRcdFx0XHRcdFx0aWYgKGN1cnJlbnRWYWx1ZS5jcmV3ID09PSAnQWxsIENyZXdzJykge1xuXHRcdFx0XHRcdFx0XHRcdC8vZGlzcGxheSBhbGwgam9icyBmb3IgdGVjaHMgd2l0aCBjcmV3IHN0IHRvIEFsbCBDcmV3c1xuXG5cdFx0XHRcdFx0XHRcdFx0am9iT3JkZXIuJGxvYWRlZCgpLnRoZW4oKGpvYk9yZGVyKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRKb2JzQ3RybC5zb3J0Sm9icy5jYWxsKHRoaXMsIHsgam9iczogam9icywgam9iT3JkZXI6IGpvYk9yZGVyIH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0dGhpcy5qb2JzID0gdGhpcy5zb3J0QnlKb2JEYXRlO1xuXHRcdFx0XHRcdFx0XHRcdH0pXG5cblx0XHRcdFx0XHRcdFx0XHQvLyBpbml0IHNlYXJjaFxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuc2VhcmNoID0gJyc7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0Ly9kaXNwbGF5IHByaW50XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5wcmludGVkID0gdHJ1ZTtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIHsgLy9vbmx5IGhhdmUgYWNjZXNzIHRvIG9uZSBjcmV3XG5cdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHRcdFx0Ly9maWx0ZXIgam9icyBiYXNlZCBvbiBjcmV3XG5cdFx0XHRcdFx0XHRcdFx0am9ic0xpc3QgPSBqb2JzLmZpbHRlcihmdW5jdGlvbiAoY3YsIGksIGEpIHtcblx0XHRcdFx0XHRcdFx0XHRcdHJldHVybiBjdi5jcmV3ID09PSBjdXJyZW50VmFsdWUuY3Jld1xuXHRcdFx0XHRcdFx0XHRcdH0pO1xuXG5cdFx0XHRcdFx0XHRcdFx0aWYgKGpvYnNMaXN0Lmxlbmd0aCA9PSAwKSB7XG5cdFx0XHRcdFx0XHRcdFx0XHRyZXR1cm5cblx0XHRcdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdFx0XHQvL2RvIG5vdCBkaXNwbGF5IHByaW50XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5wcmludGVkID0gZmFsc2U7XG5cblx0XHRcdFx0XHRcdFx0XHRqb2JPcmRlci4kbG9hZGVkKCkudGhlbigoam9iT3JkZXIpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRcdEpvYnNDdHJsLnNvcnRKb2JzLmNhbGwodGhpcywgeyBqb2JzOiBqb2JzTGlzdCwgam9iT3JkZXI6IGpvYk9yZGVyIH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0Ly8gaW5pdCBzZWFyY2hcblx0XHRcdFx0XHRcdFx0XHRcdHRoaXMudG9kYXlTZWFyY2goKTtcblx0XHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblxuXHRcdFx0fSk7XG5cdFx0fSBlbHNlIHsgLy9vZmZsaW5lIHJ1biB0aGlzIGNvZGVcblx0XHRcdGxldCB1c2VycyA9IHRoaXMuTG9jYWxEYXRhYmFzZS5nZXQoJ3VzZXJzJyk7XG5cdFx0XHRsZXQgdXNlciA9IF8uZmluZFdoZXJlKHVzZXJzLCB7IGVtYWlsOiBhbmd1bGFyLmxvd2VyY2FzZSh0aGlzLmFkbWluRW1haWwpIH0pO1xuXHRcdFx0bGV0IGpvYnMgPSB0aGlzLkxvY2FsRGF0YWJhc2UuZ2V0KCdqb2JzJyk7XG5cdFx0XHRsZXQgam9iT3JkZXIgPSB0aGlzLkxvY2FsRGF0YWJhc2UuZ2V0KCdqb2JPcmRlcicpO1xuXG5cdFx0XHRKb2JzQ3RybC5sb2FkSm9iRGF0YS5jYWxsKHRoaXMsIHsgY3VycmVudFZhbHVlOiB1c2VyLCBqb2JzOiBqb2JzLCBqb2JPcmRlcjogam9iT3JkZXIgfSk7XG5cblx0XHR9XG5cdH1cblxuXHRzdGF0aWMgc29ydEpvYnMocGFyYW0pIHtcblx0XHQvL3NvcnQgam9icyBieSBqb2IgRGF0ZSBmaXJzdFxuXHRcdHRoaXMuc29ydEJ5Sm9iRGF0ZSA9IHBhcmFtLmpvYnMuc29ydChmdW5jdGlvbiAoYSwgYikge1xuXHRcdFx0dmFyIGQxID0gbmV3IERhdGUoYS5qb2JEYXRlKTtcblx0XHRcdHZhciBkMiA9IG5ldyBEYXRlKGIuam9iRGF0ZSk7XG5cdFx0XHRyZXR1cm4gZDIuZ2V0VGltZSgpIC0gZDEuZ2V0VGltZSgpO1xuXHRcdH0pO1xuXHRcdFx0XHRcdFx0XHRcdFx0XG5cdFx0Ly8gc29ydCBqb2JzIGJ5IGpvYiBvcmRlciBzZWNvbmRcblx0XHR2YXIgc29ydCA9IF8uc29ydEJ5KHRoaXMuc29ydEJ5Sm9iRGF0ZSwgKG9iaikgPT4ge1xuXHRcdFx0dmFyIGogPSBfLmZpbmRXaGVyZShwYXJhbS5qb2JPcmRlciwgeyBmb3JlaWduS2V5OiBvYmouJGlkIH0pO1xuXHRcdFx0aWYgKGopIHtcblx0XHRcdFx0cmV0dXJuIGoub3JkZXI7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHR0aGlzLmJhY2tVcEpvYnMgPSBzb3J0O1xuXG5cdFx0cmV0dXJuIHNvcnQ7XG5cdH1cblxuXHRzdGF0aWMgbG9hZEpvYkRhdGEocGFyYW0pIHtcblx0XHR2YXIgam9ic0xpc3QgPSBbXTtcblxuXHRcdGlmIChwYXJhbS5jdXJyZW50VmFsdWUuY3JldyA9PT0gJ0FsbCBDcmV3cycpIHtcblxuXHRcdFx0Ly9kaXNwbGF5IGFsbCBqb2JzIGZvciB0ZWNocyB3aXRoIGNyZXcgc2V0IHRvIEFsbCBDcmV3c1xuXHRcdFx0XG5cdFx0XHR0aGlzLmpvYnMgPSBKb2JzQ3RybC5zb3J0Sm9icyh7IGpvYnM6IHBhcmFtLmpvYnMsIGpvYk9yZGVyOiBwYXJhbS5qb2JPcmRlciB9KTs7XG5cdFx0XHRcblx0XHRcdC8vIGluaXQgc2VhcmNoXG5cdFx0XHR0aGlzLnNlYXJjaCA9ICcnO1xuXHRcdFx0XHRcdFx0XHRcdFxuXHRcdFx0Ly9kaXNwbGF5IHByaW50IGJ1dHRvblxuXHRcdFx0dGhpcy5wcmludGVkID0gdHJ1ZTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Ly9maWx0ZXIgam9icyBiYXNlZCBvbiBjcmV3XG5cdFx0XHRqb2JzTGlzdCA9IHBhcmFtLmpvYnMuZmlsdGVyKGZ1bmN0aW9uIChjdiwgaSwgYSkge1xuXHRcdFx0XHRyZXR1cm4gY3YuY3JldyA9PT0gcGFyYW0uY3VycmVudFZhbHVlLmNyZXdcblx0XHRcdH0pO1xuXG5cdFx0XHRpZiAoam9ic0xpc3QubGVuZ3RoID09IDApIHtcblx0XHRcdFx0cmV0dXJuXG5cdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XG5cdFx0XHQvL2RvIG5vdCBkaXNwbGF5IHByaW50IGJ1dHRvblxuXHRcdFx0dGhpcy5wcmludGVkID0gZmFsc2U7XG5cblx0XHRcdHRoaXMuam9icyA9IEpvYnNDdHJsLnNvcnRKb2JzLmNhbGwodGhpcywgeyBqb2JzOiBqb2JzTGlzdCwgam9iT3JkZXI6IHBhcmFtLmpvYk9yZGVyIH0pO1xuXHRcdFx0XG5cdFx0XHQvLyBzZXQgc2VhcmNoIHRvIHRvZGF5J3MgZGF0ZVxuXHRcdFx0Ly8gdGhpcy5zZWFyY2ggPSB0aGlzLnRvZGF5LnN1YnN0cmluZygwLDEwKVxuXHRcdFx0dGhpcy50b2RheVNlYXJjaCgpO1xuXHRcdH1cblx0fVxuXG5cdG9wZW4oam9iKSB7XG5cblx0XHR0aGlzLiRzdGF0ZS5nbygnYWxsUmVwb3J0cy5yZXBvcnQnLCB7IGlkOiBqb2IuJGlkIH0pO1xuXG5cdH1cblxuXHRlZGl0KGpvYiwgJGV2ZW50KSB7XG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXHRcdGlmICh0aGlzLnNlbGVjdGVkTGlzdC5sZW5ndGggPiAxKSB7XG5cdFx0XHR0aGlzLiRtZERpYWxvZy5zaG93KHtcblx0XHRcdFx0dGFyZ2V0RXZlbnQ6ICRldmVudCxcblx0XHRcdFx0Y2xpY2tPdXRzaWRlVG9DbG9zZTogdHJ1ZSxcblx0XHRcdFx0dGVtcGxhdGVVcmw6IFwidmlld3MvY2hhbmdlSm9iQ3Jldy5odG1sXCIsXG5cdFx0XHRcdGNvbnRyb2xsZXI6IGZ1bmN0aW9uIERpYWxvZ0NvbnRyb2xsZXIoJHNjb3BlLCAkbWREaWFsb2cpIHtcblxuXHRcdFx0XHRcdHRoaXMudXBkYXRlID0gKGNyZXcsIGZvcm0pID0+IHtcblx0XHRcdFx0XHRcdHZhciBqb2JzID0gdGhhdC4kZmlyZWJhc2VBcnJheSh0aGF0LnJlZi5jaGlsZCgnam9iJykpO1xuXHRcdFx0XHRcdFx0am9icy4kbG9hZGVkKCkudGhlbigoam9icykgPT4ge1xuXHRcdFx0XHRcdFx0XHR0aGF0LnNlbGVjdGVkTGlzdC5mb3JFYWNoKCh2YWwsIGlkeCwgYXJyKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIGpvYiA9IF8uZmluZFdoZXJlKGpvYnMsIHsgJGlkOiB2YWwuJGlkIH0pXG5cdFx0XHRcdFx0XHRcdFx0dmFyIGpvYkluZGV4ID0gXy5maW5kSW5kZXgoam9icywgeyAkaWQ6IHZhbC4kaWQgfSlcblx0XHRcdFx0XHRcdFx0XHRqb2IuY3JldyA9IGNyZXc7XG5cdFx0XHRcdFx0XHRcdFx0am9icy4kc2F2ZShqb2JJbmRleCk7XG5cdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdGZvcm0uJHNldFByaXN0aW5lKCk7XG5cdFx0XHRcdFx0XHRcdGZvcm0uJHNldFVudG91Y2hlZCgpO1xuXHRcdFx0XHRcdFx0XHR0aGF0LnNlbGVjdGVkTGlzdCA9IFtdO1xuXHRcdFx0XHRcdFx0XHQkbWREaWFsb2cuaGlkZSgpO1xuXG5cdFx0XHRcdFx0XHR9KVxuXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHZhciBjcmV3cyA9IHRoYXQuJGZpcmViYXNlQXJyYXkodGhhdC5yZWYuY2hpbGQoJ2NyZXdzJykpO1xuXHRcdFx0XHRcdGNyZXdzLiRsb2FkZWQoKS50aGVuKChpdGVtcykgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jcmV3cyA9IF8ucmVqZWN0KGl0ZW1zLCAodmFsdWUpID0+IHtcblx0XHRcdFx0XHRcdFx0cmV0dXJuIHZhbHVlLm5hbWUgPT09IFwiQWxsIENyZXdzXCI7XG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9KVxuXG5cdFx0XHRcdFx0dGhpcy5jbG9zZURpYWxvZyA9ICgpID0+IHtcblx0XHRcdFx0XHRcdHRoYXQuc2VsZWN0ZWRMaXN0ID0gW107XG5cdFx0XHRcdFx0XHQkbWREaWFsb2cuaGlkZSgpO1xuXHRcdFx0XHRcdH07XG5cdFx0XHRcdH0sXG5cdFx0XHRcdGNvbnRyb2xsZXJBczogJ21kRGlhbG9nJ1xuXHRcdFx0fSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMuJHN0YXRlLmdvKCdjcmVhdGVKb2InLCB7IGlkOiBqb2IuJGlkIH0pO1xuXHRcdH1cblx0fVxuXG5cdGRlbGV0ZShqb2IpIHtcblx0XHR2YXIgY2xpZW50cyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ2NsaWVudHMnKSk7XG5cdFx0dmFyIGpvYnNUb1JlbW92ZSA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ2pvYicpKTtcblxuXHRcdHZhciBjb25maXJtID0gdGhpcy4kbWREaWFsb2cuY29uZmlybSgpXG5cdFx0XHQudGl0bGUoJ1dhcm4nKVxuXHRcdFx0LmNvbnRlbnQoJ1RoaXMgZGF0YSB3aWxsIGJlIGRlbGV0ZWQgcGVybWFuZW50bHkuJylcblx0XHRcdC5vaygnWUVTIERFTEVURScpXG5cdFx0XHQuY2FuY2VsKCdDYW5jZWwnKTtcblxuXG5cblx0XHR0aGlzLiRtZERpYWxvZ1xuXHRcdFx0LnNob3coY29uZmlybSkudGhlbihyZW1vdmVKb2IuYmluZCh0aGlzKSwgKCkgPT4ge1xuXHRcdFx0XHR0aGlzLmpvYiA9IHVuZGVmaW5lZDtcblx0XHRcdFx0dGhpcy5zZWxlY3RlZExpc3QgPSBbXTtcblx0XHRcdH0pO1xuXG5cdFx0ZnVuY3Rpb24gcmVtb3ZlSm9iKCkge1xuXHRcdFx0dGhpcy5zZWxlY3RlZExpc3QgPSBbXTtcblxuXHRcdFx0Y2xpZW50cy4kbG9hZGVkKCkudGhlbigoaXRlbSkgPT4ge1xuXHRcdFx0XHRpdGVtLm1hcCgoY3YsIGksIGEpID0+IHtcblxuXHRcdFx0XHRcdGlmIChjdi5mb3JlaWduS2V5ID09PSBqb2IuJGlkKSB7XG5cdFx0XHRcdFx0XHRjbGllbnRzLiRyZW1vdmUoaSkudGhlbigoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdC8vIHRoaXMubG9hZEpvYnMoKTtcblx0XHRcdFx0XHRcdFx0am9ic1RvUmVtb3ZlLiRsb2FkZWQoKS50aGVuKChfaXRlbSkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdF9pdGVtLm1hcCgoX2N2LCBfaSkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0aWYgKF9jdi4kaWQgPT09IGpvYi4kaWQpIHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0am9ic1RvUmVtb3ZlLiRyZW1vdmUoX2kpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZhciBqb2JPcmRlcnMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdqb2JPcmRlcicpKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRqb2JPcmRlcnMuJGxvYWRlZCgpLnRoZW4oKF9qb2JPcmRlcnMpID0+IHtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdF9qb2JPcmRlcnMubWFwKChqb2JPcmRlciwgam9iT3JkZXJJbmRleCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpZiAoam9iT3JkZXIuZm9yZWlnbktleSA9PT0gam9iLiRpZCkge1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdGpvYk9yZGVycy4kcmVtb3ZlKGpvYk9yZGVySW5kZXgpO1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR9KTtcblxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHRoaXMuam9iID0gbnVsbDtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLmxvYWRKb2JzKGZhbHNlLCB0cnVlKTtcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR0aGlzLlRvYXN0LnNob3dUb2FzdCh7IG1zZzogJ0pvYiByZW1vdmVkJyB9KTtcblxuXHRcdFx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRcdFx0fSk7Ly9qb2JzVG9SZW1vdmVcblx0XHRcdFx0XHRcdH0pOy8vY2xpZW50c1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7Ly9pdGVtXG5cdFx0XHR9KTtcblx0XHRcdHRoaXMuc2VsZWN0ZWRMaXN0ID0gW107XG5cdFx0fS8vcmVtb3ZlSm9iXG5cdH0vL2RlbGV0ZVxuXG5cdHNldFNlbGVjdGVkSm9iKCRldmVudCwgam9iKSB7XG5cdFx0dmFyIGlzTWF0Y2ggPSBmYWxzZTtcblx0XHRcblx0XHQvL3NldCB2bS5qb2IgZm9yIHRoZSBsYXN0IHNlbGVjdGVkIGl0ZW1cblx0XHQvL25vdCB1c2VkIHdoZW4gbXVsdGlwbGUgaXRlbXMgYXJlIHNlbGVjdGVkXG5cdFx0dGhpcy5qb2IgPSBqb2I7XG5cdFx0Ly9jaGVjayB0byBzZWUgaWYgdGhlIGpvYiBpcyBhbHJlYWR5IHNlbGVjdGVkLlxuXHRcdC8vaWYgc2VsZWN0ZWQgZGVzZWxlY3Qgam9iXG5cdFx0dGhpcy5zZWxlY3RlZExpc3QuZm9yRWFjaCgodmFsLCBpZHgsIGFycikgPT4ge1xuXHRcdFx0aWYgKHZhbC4kaWQgPT09IGpvYi4kaWQpIHtcblx0XHRcdFx0dGhpcy5zZWxlY3RlZExpc3Quc3BsaWNlKGlkeCwgMSk7XG5cdFx0XHRcdGlzTWF0Y2ggPSB0cnVlO1xuXHRcdFx0fVxuXHRcdH0pXG5cblx0XHRpZiAoIWlzTWF0Y2gpIHtcdFxuXHRcdFx0Ly8gcHVzaCBzZWxlY3RlZCBqb2IgaXN0IG9udG8gdGhlIHNlbGVjdGVkTGlzdCBhcnJheVxuXHRcdFx0dGhpcy5zZWxlY3RlZExpc3QucHVzaChqb2IpO1xuXHRcdH1cblxuXG5cdH1cblxuXHRkaXNhYmxlT3B0aW9ucygpIHtcblx0XHRpZiAodGhpcy5zZWxlY3RlZExpc3QubGVuZ3RoID09IDAgfHwgdGhpcy5zZWxlY3RlZExpc3QubGVuZ3RoID4gMSkge1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXHRcdGVsc2Uge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0fVxuXG5cdHNldFNlbGVjdGVkQ2xhc3Moam9iKSB7XG5cdFx0dmFyIG1hdGNoID0gXy5maW5kV2hlcmUodGhpcy5zZWxlY3RlZExpc3QsIHsgJGlkOiBqb2IuJGlkIH0pO1xuXG5cdFx0cmV0dXJuIChtYXRjaCkgPyB0cnVlIDogZmFsc2Vcblx0fVxuXG5cdGdldCB0b21vcnJvdygpIHtcblx0XHRyZXR1cm4gdGhpcy5EYXRlSVNPLnRvbW9ycm93O1xuXHR9XG5cblx0Z2V0IHRvZGF5KCkge1xuXHRcdHJldHVybiB0aGlzLkRhdGVJU08udG9kYXk7XG5cdH1cblxuXHRnZXQgdG9kYXlTZXRDbGFzcygpIHtcblx0XHQvLyByZXR1cm4gJ3tqb2JEYXRlOicgKyB0aGlzLnRvZGF5LnN1YnN0cmluZygwLCAxMCkgKyAnLHN0YXR1czohcHJpbnRlZH0nXG5cdFx0cmV0dXJuIHRoaXMudG9kYXkuc3Vic3RyaW5nKDAsIDEwKSArICcsIXByaW50ZWQnXG5cdH1cblxuXHR0b2RheVNlYXJjaCgpIHtcblx0XHQvLyB0aGlzLnNlYXJjaCA9ICd7am9iRGF0ZTonICsgdGhpcy50b2RheS5zdWJzdHJpbmcoMCwgMTApICsgJyxzdGF0dXM6IXByaW50ZWR9J1xuXHRcdHRoaXMuc2VhcmNoID0gdGhpcy50b2RheS5zdWJzdHJpbmcoMCwgMTApICsgJywhcHJpbnRlZCdcblxuXHR9XG5cblx0Z2V0IHRvbW9ycm93U2V0Q2xhc3MoKSB7XG5cdFx0Ly8gcmV0dXJuICd7am9iRGF0ZTonICsgdGhpcy50b21vcnJvdy5zdWJzdHJpbmcoMCwgMTApICsgJyxzdGF0dXM6IXByaW50ZWR9J1xuXHRcdHJldHVybiB0aGlzLnRvbW9ycm93LnN1YnN0cmluZygwLCAxMCkgKyAnLCFwcmludGVkJ1xuXHR9XG5cblx0dG9tb3Jyb3dTZWFyY2goKSB7XG5cdFx0Ly8gdGhpcy5zZWFyY2ggPSAne2pvYkRhdGU6JyArIHRoaXMudG9tb3Jyb3cuc3Vic3RyaW5nKDAsIDEwKSArICcsc3RhdHVzOiFwcmludGVkfSdcblx0XHR0aGlzLnNlYXJjaCA9IHRoaXMudG9tb3Jyb3cuc3Vic3RyaW5nKDAsIDEwKSArICcsIXByaW50ZWQnXG5cblx0fVxuXG5cdG9uU29ydEhhbmRsZXIoJGl0ZW0sICRwYXJ0RnJvbSwgJHBhcnRUbykge1xuXHRcdC8vcHVsbCB0aGUgc29ydCBvcmRlciBmb3IgZWFjaCBqb2Jcblx0XHR2YXIgam9iT3JkZXJzID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnam9iT3JkZXInKSk7XG5cblx0XHRqb2JPcmRlcnMuJGxvYWRlZCgpLnRoZW4oKGpvYk9yZGVyKSA9PiB7XG5cblx0XHRcdC8vaW5pdGlhbCBsb2FkIG9mIGpvYk9yZGVyIHRhYmxlXG5cdFx0XHRpZiAoam9iT3JkZXIubGVuZ3RoID09PSAwKSB7XG5cblx0XHRcdFx0dmFyIGpvYnMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdqb2InKSk7XG5cdFx0XHRcdGpvYnMuJGxvYWRlZCgpLnRoZW4oKF9qb2JzKSA9PiB7XG5cdFx0XHRcdFx0X2pvYnMuZm9yRWFjaCgoX2pvYiwgaW5kZXgpID0+IHtcblx0XHRcdFx0XHRcdGpvYk9yZGVyLiRhZGQoeyBmb3JlaWduS2V5OiBfam9iLiRpZCwgb3JkZXI6IGluZGV4IH0pO1xuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vdXBkYXRlICBqb2JPcmRlciBmb3IgZmlyc3Qgc29ydCBhdHRlbXB0XG5cdFx0XHRcdGpvYk9yZGVyLmZvckVhY2goKHYsIGkpID0+IHtcblx0XHRcdFx0XHR2YXIgaW5kZXggPSBfLmZpbmRJbmRleCgkcGFydFRvLCB7ICRpZDogdi5mb3JlaWduS2V5IH0pO1xuXHRcdFx0XHRcdC8vc2F2ZSBpbmRleCBmb3IgRlJPTVxuXHRcdFx0XHRcdGpvYk9yZGVyW2ldLm9yZGVyID0gaW5kZXg7XG5cdFx0XHRcdFx0am9iT3JkZXIuJHNhdmUoaSk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0am9iT3JkZXIuZm9yRWFjaCgodiwgaSkgPT4ge1xuXHRcdFx0XHRcdHZhciBpbmRleCA9IF8uZmluZEluZGV4KCRwYXJ0VG8sIHsgJGlkOiB2LmZvcmVpZ25LZXkgfSk7XG5cdFx0XHRcdFx0aWYgKGluZGV4ICE9PSAtMSkge1x0XG5cdFx0XHRcdFx0XHQvL3NhdmUgaW5kZXggZm9yIEZST01cblx0XHRcdFx0XHRcdGpvYk9yZGVyW2ldLm9yZGVyID0gaW5kZXg7XG5cdFx0XHRcdFx0XHRqb2JPcmRlci4kc2F2ZShpKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0b25TZWFyY2hIYW5kbGVyKCkge1xuXG5cdFx0dmFyIHNlYXJjaFZhbHVlID0gdGhpcy5zZWFyY2g7XG5cdFx0dmFyIGNyZXdzID0gdGhpcy5Mb2NhbERhdGFiYXNlLmdldCgnY3Jld3MnKTtcblx0XHR2YXIgc3RhdHVzZXMgPSBbeyBuYW1lOiAnb3BlbicgfSwgeyBuYW1lOiAncHJpbnRlZCcgfSwgeyBuYW1lOiAnY29tcGxldGUnIH1dXG5cdFx0dmFyIG9wZXJhdG9ycyA9IFsnISddO1xuXHRcdGlmIChzZWFyY2hWYWx1ZSkge1xuXG5cdFx0XHRpZiAoc2VhcmNoVmFsdWUuc2VhcmNoKCcsJykgPiAwKSB7XG5cdFx0XHRcdC8vdmFyaWFibGUgZm9yIHN0b3JpbmcgdGhlIG5ldyBvYmplY3Rcblx0XHRcdFx0bGV0IHNlYXJjaFZhbHVlT2JqZWN0ID0ge307XG5cdFx0XHRcdGxldCBzZWFyY2hQYXJhbXMgPSBzZWFyY2hWYWx1ZS5zcGxpdCgnLCcpO1xuXHRcdFx0XHRzZWFyY2hQYXJhbXMubWFwKCh2LCBpLCBhKSA9PiB7XG5cblx0XHRcdFx0XHR2YXIgaXNEYXRlID0gIShuZXcgRGF0ZSh2LnJlcGxhY2UoJyAnLCAnJykpLnRvRGF0ZVN0cmluZygpID09ICdJbnZhbGlkIERhdGUnKTtcblx0XHRcdFx0XHR2YXIgaXNDcmV3ID0gXy5zb21lKGNyZXdzLCBmdW5jdGlvbiAoY3Jldykge1xuXHRcdFx0XHRcdFx0cmV0dXJuIGNyZXcubmFtZS50b1VwcGVyQ2FzZSgpID09PSB2LnRvVXBwZXJDYXNlKClcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR2YXIgaXNTdGF0dXMgPSBfLnNvbWUoc3RhdHVzZXMsIGZ1bmN0aW9uIChzdGF0dXMpIHtcblx0XHRcdFx0XHRcdHZhciBfc3RhdHVzXG5cdFx0XHRcdFx0XHRvcGVyYXRvcnMubWFwKGZ1bmN0aW9uIChvcHMsIGksIGEpIHtcblx0XHRcdFx0XHRcdFx0X3N0YXR1cyA9IHYucmVwbGFjZShvcHMsICcnKTtcblx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRyZXR1cm4gX3N0YXR1cy50b1VwcGVyQ2FzZSgpID09PSBzdGF0dXMubmFtZS50b1VwcGVyQ2FzZSgpXG5cdFx0XHRcdFx0fSk7XG5cblx0XHRcdFx0XHRpZiAoaXNEYXRlKSB7XG5cdFx0XHRcdFx0XHRzZWFyY2hWYWx1ZU9iamVjdC5qb2JEYXRlID0gdjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAoaXNDcmV3KSB7XG5cdFx0XHRcdFx0XHRzZWFyY2hWYWx1ZU9iamVjdC5jcmV3ID0gdjtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAoaXNTdGF0dXMpIHtcblx0XHRcdFx0XHRcdHNlYXJjaFZhbHVlT2JqZWN0LnN0YXR1cyA9IHY7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGVsc2Uge1xuXHRcdFx0XHRcdFx0c2VhcmNoVmFsdWVPYmplY3QubmFtZSA9IHY7XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0dHJ5IHtcblx0XHRcdFx0XHRcdHRoaXMuam9icyA9IHRoaXMuJGZpbHRlcignZmlsdGVyJykodGhpcy5iYWNrVXBKb2JzLCBzZWFyY2hWYWx1ZU9iamVjdCk7XG5cblx0XHRcdFx0XHR9IGNhdGNoIChlcnJvcikge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2coZXJyb3IpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHR9KVxuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHR0aGlzLmpvYnMgPSB0aGlzLiRmaWx0ZXIoJ2ZpbHRlcicpKHRoaXMuc29ydEJ5Sm9iRGF0ZSwgc2VhcmNoVmFsdWUpXG5cdFx0XHR9XG5cblxuXHRcdH0gZWxzZSB7IC8vbm8gc2VhcmNoIGl0ZW0gXG5cblx0XHRcdHRoaXMuam9icyA9IHRoaXMuc29ydEJ5Sm9iRGF0ZVxuXHRcdH1cblx0fVxuXG5cdHNhdmVTZWFyY2goc2VhcmNoVmFsdWUpIHtcblx0XHR2YXIgc2VhcmNoZXMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdzZWFyY2hlcycpKVxuXHRcdHNlYXJjaGVzLiRsb2FkZWQoKS50aGVuKGZ1bmN0aW9uIChzZWFyY2hlcykge1xuXHRcdFx0c2VhcmNoZXMuJGFkZChzZWFyY2hWYWx1ZSk7XG5cdFx0fSlcblx0fVxuXG5cbn1cbmV4cG9ydCB7Sm9ic0N0cmx9IiwiLyoqXG4gKiBMb2dpbiBDb250cm9sbGVyIGZvciBsb2dpbi5odG1sXG4gKiBAcGFyYW0geyFmaXJlYmFzZS4kZmlyZWJhc2VBdXRofSAkZmlyZWJhc2VBdXRoIFxuICogQHBhcmFtIHshYW5ndWxhci4kbWREaWFsb2d9ICRtZERpYWxvZ1xuICogQHBhcmFtIHshdWkucm91dGVyLiRzdGFlfSAkc3RhdGVcbiAqIEBwYXJhbSB7fSBGaXJlQmFzZVJlZlxuICogQHBhcmFtIHt9IEZpcmVCYXNlQXV0aFxuICogQHBhcmFtIHt9IExvYWRpbmdQb3B1cFxuICogQHBhcmFtIHt9IExvY2FsRGF0YWJhc2VcbiAqIEBwYXJhbSB7IWFuZ3VsYXIuJHJvb3RTY29wZX0gJHJvb3RTY29wZVxuICogQHBhcmFtIHt9ICRmaXJlYmFzZUFycmF5XG4gKiBAcGFyYW0geyFhbmd1bGFyLlNjb3BlfSAkc2NvcGVcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cblxuY2xhc3MgTG9naW5DdHJsIHtcblx0Y29uc3RydWN0b3IoJGZpcmViYXNlQXV0aCwgJG1kRGlhbG9nLCAkc3RhdGUsIEZpcmVCYXNlUmVmLCBGaXJlQmFzZUF1dGgsXG5cdFx0TG9hZGluZ1BvcHVwLCBMb2NhbERhdGFiYXNlLCAkcm9vdFNjb3BlLCAkZmlyZWJhc2VBcnJheSwgJHNjb3BlLCBEYXRlSVNPLCAkZmlyZWJhc2VPYmplY3QpIHtcblx0XHR0aGlzLiRtZERpYWxvZyA9ICRtZERpYWxvZztcblx0XHR0aGlzLiRzdGF0ZSA9ICRzdGF0ZTtcblx0XHR0aGlzLm1hc3RlciA9IHsgZW1haWw6ICcnLCBwYXNzd29yZDogJycgfTtcblx0XHR0aGlzLmF1dGggPSBGaXJlQmFzZUF1dGhcblx0XHR0aGlzLkxvYWRpbmdQb3B1cCA9IExvYWRpbmdQb3B1cDtcblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UgPSBMb2NhbERhdGFiYXNlO1xuXHRcdHRoaXMuJHJvb3RTY29wZSA9ICRyb290U2NvcGU7XG5cdFx0dGhpcy5yZWYgPSBGaXJlQmFzZVJlZi5yZWY7XG5cdFx0dGhpcy4kZmlyZWJhc2VBcnJheSA9ICRmaXJlYmFzZUFycmF5XG5cdFx0dGhpcy4kc2NvcGUgPSAkc2NvcGU7XG5cdFx0dGhpcy5EYXRlSVNPID0gRGF0ZUlTTztcblx0XHR0aGlzLiRmaXJlYmFzZU9iamVjdCA9ICRmaXJlYmFzZU9iamVjdDtcblx0XHR0aGlzLnVzZXIgPSB7fTtcblx0XHRcblx0XHQvKipcblx0XHQgKiBAdHlwZSB7b2JqZWN0fSBcblx0XHQgKi9cblx0XHR2YXIgdXNlciA9IHRoaXMuTG9jYWxEYXRhYmFzZS5nZXQoJ2FsbG93ZWQnKTtcblx0XHRpZiAodXNlcikge1xuXHRcdFx0dGhpcy5jcmVhdGVKb2IgPSB1c2VyLmNyZWF0ZUpvYjtcblx0XHRcdHRoaXMuYWRtaW5zdHJhdGlvbiA9IHVzZXIuYWRtaW5zdHJhdGlvbjtcblx0XHRcdHRoaXMudXNlci5lbWFpbCA9IHVzZXIuZW1haWw7XG5cdFx0XHR0aGlzLnVzZXIucGFzc3dvcmQgPSB1c2VyLnBhc3N3b3JkO1xuXHRcdH1cblxuXHRcdHZhciB0cmFjayA9IHRoaXMuTG9jYWxEYXRhYmFzZS4kdHJhY2tTaXRlTG9jYXRpb247XG5cdFx0aWYgKHRyYWNrKSB7XG5cdFx0XHRpZiAodHJhY2suc2l0ZSAmJiAhdHJhY2suJGlkKSB7XG5cdFx0XHRcdHRoaXMuJHN0YXRlLmdvKHRyYWNrLnNpdGUgKTtcblx0XHRcdH0gZWxzZXtcblx0XHRcdFx0dGhpcy4kc3RhdGUuZ28odHJhY2suc2l0ZSAsIHsgaWQ6IHRyYWNrLiRpZCB9KTtcblx0XHRcdH1cblx0XHR9XG5cblxuXHR9XG5cblx0c2hvd0FsZXJ0KGVycm9yLCBmb3JtKSB7XG5cdFx0LyoqXG5cdFx0ICogQHR5cGUgeyFhbmd1bGFyLiRtZERpYWxvZ31cblx0XHQgKi9cblx0XHR2YXIgYWxlcnQgPSB0aGlzLiRtZERpYWxvZy5hbGVydCgpXG5cdFx0XHQudGl0bGUoJ0F1dGhlbnRpY2F0aW9uIGZhaWxlZDonKVxuXHRcdFx0LmNvbnRlbnQoZXJyb3IuY29kZSlcblx0XHRcdC5vaygnQ2xvc2UnKTtcblxuXHRcdHRoaXMuJG1kRGlhbG9nLnNob3coYWxlcnQpO1xuXHR9XG5cblx0Y2FuY2VsKGZvcm0pIHtcblx0XHR0aGlzLnVzZXIgPSBhbmd1bGFyLmNvcHkodGhpcy5tYXN0ZXIpO1xuXHRcdGZvcm0uJHNldFByaXN0aW5lKCk7XG5cdFx0Zm9ybS4kc2V0VW50b3VjaGVkKCk7XG5cblx0fVxuXG5cdGxvZ2luKCRldmVudCwgZm9ybSkge1xuXG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXG5cdFx0aWYgKCRldmVudC5rZXlDb2RlICE9IDEzICYmICRldmVudC5rZXlDb2RlICE9IDApIHtcblx0XHRcdHJldHVyblxuXHRcdH1cblxuXHRcdHRoaXMuTG9hZGluZ1BvcHVwLnNob3coKTtcblxuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZS5pbml0YWxMb2FkKCk7XG5cblx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXHRcdFx0dGhpcy5hdXRoLiRhdXRoV2l0aFBhc3N3b3JkKHRoaXMudXNlcilcblx0XHRcdFx0LnRoZW4ob25TdWNjZXNzLmJpbmQodGhpcyksIG9uUmVqZWN0LmJpbmQodGhpcykpO1xuXHRcdFx0XG5cdFx0XHQvL3NldCB0aGUgY3VycmVudCBhdXRoIGxvY2FsbHlcblx0XHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kYXV0aFdpdGhQYXNzd29yZCh0aGlzLnVzZXIpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJGF1dGhXaXRoUGFzc3dvcmQodGhpcy51c2VyKVxuXHRcdFx0XHQudGhlbihvblN1Y2Nlc3MuYmluZCh0aGlzKSwgb25SZWplY3QuYmluZCh0aGlzKSlcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBvblN1Y2Nlc3MoYXV0aERhdGEpIHtcblx0XHRcdHZhciB1c2VycywgdXNlciwgZW1haWw7XG5cblx0XHRcdGlmIChuYXZpZ2F0b3Iub25MaW5lKSB7XG5cdFx0XHRcdHVzZXJzID0gdGhhdC4kZmlyZWJhc2VBcnJheSh0aGF0LnJlZi5jaGlsZCgndXNlcnMnKSk7XG5cblx0XHRcdFx0dXNlcnMuJGxvYWRlZCgpLnRoZW4oKHVzZXJzKSA9PiB7XG5cdFx0XHRcdFx0dmFyIHVzZXIgPSBfLmZpbmRXaGVyZSh1c2VycywgeyBlbWFpbDogYXV0aERhdGEucGFzc3dvcmQuZW1haWwgfSk7XG5cdFx0XHRcdFx0aWYgKHVzZXIudGVtcERhdGUpIHtcblxuXHRcdFx0XHRcdFx0bGV0IHJlbW90ZVVzZXIgPSB0aGlzLiRmaXJlYmFzZU9iamVjdCh0aGlzLnJlZi5jaGlsZCgndXNlcnMnKS5jaGlsZCh1c2VyLiRpZCkpO1xuXHRcdFx0XHRcdFx0cmVtb3RlVXNlci4kbG9hZGVkKCkudGhlbigoX3JlbW90ZVVzZXIpID0+IHtcblx0XHRcdFx0XHRcdFx0bGV0IGN1ckRhdGUgPSB0aGlzLkRhdGVJU08udG9kYXlcblx0XHRcdFx0XHRcdFx0bGV0IGlzQWZ0ZXIgPSBtb21lbnQoY3VyRGF0ZSkuaXNBZnRlcihfcmVtb3RlVXNlci50ZW1wRGF0ZSwgJ2RheScpO1xuXHRcdFx0XHRcdFx0XHRsZXQgaXNTYW1lID0gbW9tZW50KF9yZW1vdGVVc2VyLnRlbXBEYXRlKS5pc1NhbWUoY3VyRGF0ZSwgJ2RheScpO1xuXG5cdFx0XHRcdFx0XHRcdGlmIChpc0FmdGVyIHx8IGlzU2FtZSkge1xuXHRcdFx0XHRcdFx0XHRcdF9yZW1vdGVVc2VyLmNyZXcgPSB1c2VyLnRlbXBDcmV3O1xuXHRcdFx0XHRcdFx0XHRcdF9yZW1vdGVVc2VyLnRlbXBEYXRlID0gbnVsbFxuXHRcdFx0XHRcdFx0XHRcdF9yZW1vdGVVc2VyLnRlbXBDcmV3ID0gbnVsbFxuXHRcdFx0XHRcdFx0XHRcdF9yZW1vdGVVc2VyLiRzYXZlKCkudGhlbigoKSA9PiB7XG5cdFx0XHRcdFx0XHRcdFx0XHRsZXQgbmV3VXNlciA9IHt9O1xuXHRcdFx0XHRcdFx0XHRcdFx0Xy5leHRlbmRPd24obmV3VXNlciwgX3JlbW90ZVVzZXIpO1xuXHRcdFx0XHRcdFx0XHRcdFx0ZGVsZXRlIG5ld1VzZXIuJCRjb25mO1xuXHRcdFx0XHRcdFx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLnB1dCgnYWxsb3dlZCcsIG5ld1VzZXIpO1xuXHRcdFx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLnB1dCgnYWxsb3dlZCcsIHVzZXIpO1xuXHRcdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdH0pXG5cdFx0XHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLnB1dCgnYWxsb3dlZCcsIHVzZXIpO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHRoYXQuJHJvb3RTY29wZS4kYnJvYWRjYXN0KCdzdWNjZXNzTG9naW4nLCB7IHVzZXI6IHVzZXIgfSk7XG5cblx0XHRcdFx0XHRjb25zb2xlLmxvZyh1c2VyKTtcblx0XHRcdFx0fSlcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHVzZXJzID0gdGhhdC5Mb2NhbERhdGFiYXNlLmdldCgndXNlcnMnKTtcblx0XHRcdFx0ZW1haWwgPSBhdXRoRGF0YS5lbWFpbC50b0xvd2VyQ2FzZSgpO1xuXHRcdFx0XHR1c2VyID0gXy5maW5kV2hlcmUodXNlcnMsIHsgZW1haWw6IGVtYWlsIH0pO1xuXHRcdFx0XHR0aGF0LkxvY2FsRGF0YWJhc2UucHV0KCdhbGxvd2VkJywgdXNlcik7XG5cdFx0XHRcdHRoYXQuJHJvb3RTY29wZS4kYnJvYWRjYXN0KCdzdWNjZXNzTG9naW4nLCB7IHVzZXI6IHVzZXIgfSk7XG5cblx0XHRcdH1cblxuXHRcdFx0Y29uc29sZS5sb2coXCJMb2dnZWQgaW4gYXM6XCIsIGF1dGhEYXRhKTtcblx0XHRcdExvZ2luQ3RybC5jbGVhckZvcm0uYmluZCh0aGF0LCBmb3JtKTtcblx0XHRcdHRoYXQuTG9hZGluZ1BvcHVwLmhpZGUoKTtcblx0XHRcdHRoYXQuTG9jYWxEYXRhYmFzZS4kc2V0QXV0aCgndHJ1ZScpO1xuXHRcdFx0dGhhdC4kc3RhdGUuZ28oJ2pvYnMnKTtcblx0XHR9XG5cblx0XHRmdW5jdGlvbiBvblJlamVjdChlcnJvcikge1xuXHRcdFx0dGhhdC5zaG93QWxlcnQoZXJyb3IpO1xuXHRcdFx0Y29uc29sZS5lcnJvcihcIkF1dGhlbnRpY2F0aW9uIGZhaWxlZDpcIiwgZXJyb3IpO1xuXHRcdFx0TG9naW5DdHJsLmNsZWFyRm9ybS5iaW5kKHRoYXQsIGZvcm0pO1xuXHRcdFx0dGhhdC5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0dGhhdC5Mb2NhbERhdGFiYXNlLiRzZXRBdXRoKCdmYWxzZScpO1xuXHRcdH1cblxuXG5cdH1cblxuXHRzdGF0aWMgY2xlYXJGb3JtKGZvcm0pIHtcblx0XHR0aGlzLnVzZXIgPSBhbmd1bGFyLmNvcHkodGhpcy5tYXN0ZXIpO1xuXHRcdGZvcm0uJHNldFByaXN0aW5lKCk7XG5cdFx0Zm9ybS4kc2V0VW50b3VjaGVkKCk7XG5cblx0fVxufVxuXG5cbmV4cG9ydCB7TG9naW5DdHJsfTtcblxuIiwiLyoqXG4gKiBSZXBvcnQgQ29udHJvbGxlciByZXBvcnRTaGVldC5odG1sXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cInVzZSBzdHJpY3RcIjtcbmNsYXNzIFJlcG9ydEN0cmwge1xuXHRjb25zdHJ1Y3RvcihGaXJlQmFzZVJlZiwgLypjdXJyZW50QXV0aCovICRzY29wZSwgJGZpcmViYXNlQXJyYXksXG5cdFx0UG9pbnRzRGF0YSwgJHN0YXRlLCBMb2FkUmVwb3J0c1NydiwgU2lnblBhZCwgTG9jYWxEYXRhYmFzZSwgRmlyZUJhc2VBdXRoLFxuXHRcdExvYWRpbmdQb3B1cCwgT3JpZW50YXRpb24sICR3aW5kb3csIGRldmljZURldGVjdG9yLCAkZmlyZWJhc2VPYmplY3QsXG5cdFx0Sm9iU3RhdHVzLCBQcmludCwgJGludGVydmFsKSB7XG5cblx0XHR0aGlzLiRzY29wZSA9ICRzY29wZTtcblx0XHRcdFx0XG5cdFx0Ly8gR2V0IGEgZGF0YWJhc2UgcmVmZXJlbmNlIFxuXHRcdHRoaXMucmVmID0gRmlyZUJhc2VSZWYucmVmO1xuXHRcdFxuXHRcdC8vc3RhdGUgc2VydmljZVxuXHRcdHRoaXMuJHN0YXRlID0gJHN0YXRlO1xuXHRcdFxuXHRcdC8vU2lnbmF0dXJlIFBhZFxuXHRcdHRoaXMuY2FudmFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignY2FudmFzJyk7XG5cdFx0dGhpcy5TaWduUGFkID0gU2lnblBhZDtcblxuXHRcdHRoaXMuTG9hZFJlcG9ydHNTcnYgPSBMb2FkUmVwb3J0c1NydjtcblxuXHRcdHRoaXMuRmlyZUJhc2VBdXRoID0gRmlyZUJhc2VBdXRoO1xuXG5cdFx0dGhpcy4kZmlyZWJhc2VBcnJheSA9ICRmaXJlYmFzZUFycmF5O1xuXG5cdFx0dGhpcy4kZmlyZWJhc2VPYmplY3QgPSAkZmlyZWJhc2VPYmplY3RcblxuXHRcdHRoaXMuTG9hZGluZ1BvcHVwID0gTG9hZGluZ1BvcHVwXG5cblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UgPSBMb2NhbERhdGFiYXNlO1xuXG5cdFx0dGhpcy5Kb2JTdGF0dXMgPSBKb2JTdGF0dXM7XG5cblx0XHR0aGlzLlByaW50ID0gUHJpbnQ7XG5cdFx0XG5cdFx0Ly9kaXNwbGF5IGxvYWRpbmcgd2luZG93XG5cdFx0TG9hZGluZ1BvcHVwLnNob3coKTtcblxuXHRcdHRoaXMubG9hZERhdGEoKTtcblxuXHRcdE9yaWVudGF0aW9uLnNvdXJjZVBhZ2UgPSAncmVwb3J0JzsgXG5cdFx0XG5cdFx0Ly91cGRhdGUgdGhlIG9yaWVudGF0aW9uLiAgU2V0IGhlaWdodCBzY3JvbGwgPSBQb3J0cmFpdCBNb2RlIG9yICBzY3JvbGxMRyA9IExhbmRzY2FwZVxuXHRcdCR3aW5kb3cub25vcmllbnRhdGlvbmNoYW5nZSA9ICgpID0+IHtcblx0XHRcdHRoaXMuc2Nyb2xsID0gT3JpZW50YXRpb24uc2Nyb2xsSGVpZ2h0O1xuXHRcdFx0Ly9yZXNpemUgc2lnbmF0dXJlIGFyZWFcblx0XHRcdC8vIFJlcG9ydEN0cmwuaW5pdFNpZ25QYWQuY2FsbCh0aGlzKTtcblx0XHRcdHRoaXMubG9hZERhdGEoKTtcblx0XHRcdC8vICRzY29wZS4kYXBwbHkoKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly9zZXQgYSBmaXggaGVpZ2h0IGZvciBkZXZpY2Ugb2YgaXBob25lXG5cdFx0aWYgKGRldmljZURldGVjdG9yLmRldmljZSA9PT0gJ2lwaG9uZScpIHtcblx0XHRcdHRoaXMuc2Nyb2xsID0gJ3Njcm9sbC1pUGhvbmUnXG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMuc2Nyb2xsID0gT3JpZW50YXRpb24uc2Nyb2xsSGVpZ2h0O1xuXHRcdH1cblx0XHRcblx0XHQvL0xvYWQgdGhlIFBvaW50IG9mIFZpc3VhbCBJbnNwZWN0aW9uIGluZm9ybWF0aW9uXG5cdFx0dGhpcy5oZWFkZXJzID0gUG9pbnRzRGF0YS5wb2ludHM7XG5cblx0XHQvL2xvYWQgYXZhaWxhYmxlIG9wdGlvbnNcblx0XHR0aGlzLm9wdGlvbnMgPSBbJ1MgLSBTYXRpc2ZhY3RvcnknLCAnWCAtIFVuc2F0aXNmYWN0b3J5JywgJ04vQSAtIE5vdCBBcHBsaWNhYmxlJ107XG5cblx0XHR0aGlzLiRzY29wZS4kb24oJ2Nvbm5lY3RlZCcsICgpID0+IHtcblx0XHRcdGNvbnNvbGUubG9nKCdjb25uZWN0ZWQnKTtcblx0XHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kc3luY1RvRmlyZWJhc2UoKTtcblx0XHR9KTtcblxuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kdHJhY2tTaXRlTG9jYXRpb24gPSB7IHNpdGU6ICdhbGxSZXBvcnRzLnJlcG9ydCcsICRpZDogdGhpcy4kc3RhdGUucGFyYW1zLmlkIH1cblx0fVxuXG5cdHN0YXRpYyBpbml0U2lnblBhZCgpIHtcblx0XHQvL0luaXQgU2lnbmF0dXJlXG5cdFx0XG5cdFx0dGhpcy5TaWduUGFkLmluaXQodGhpcy5jYW52YXMsIHRoaXMuY2xpZW50KTtcblx0XHR0aGlzLlNpZ25QYWQucmVzaXplQ2FudmFzKCk7XG5cdFx0dGhpcy5TaWduUGFkLmdldFNpZ25hdHVyZSgpO1xuXHR9XG5cblx0bG9hZERhdGEoKSB7XG5cdFx0XHRcdFxuXHRcdC8vY2hlY2sgaWYgdGhlIHN5c3RlbSBpcyBvbmxpbmVcblx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXHRcdFx0Ly9hY2Nlc3MgY3VycmVudCBzaWduZWQgaW4gdXNlciBkYXRhXG5cdFx0XHRsZXQgY3VycmVudEF1dGggPSB0aGlzLkZpcmVCYXNlQXV0aC4kZ2V0QXV0aCgpO1xuXHRcdFx0XG5cdFx0XHQvL2xvYWQgdGhlIGNsaWVudCBkYXRhXG5cdFx0XHR0aGlzLkxvYWRSZXBvcnRzU3J2LnJlbW90ZSh0aGlzLiRzY29wZSkudGhlbigoY2xpZW50KSA9PiB7XG5cdFx0XHRcdHRoaXMuY2xpZW50ID0gY2xpZW50O1xuXHRcdFx0XG5cdFx0XHRcdC8vc2V0IHRoZSB0ZWNoIGJhc2VkIG9uIHRoZSBjdXJyZW50IHVzZXIncyBlbWFpbFxuXHRcdFx0XHR2YXIgdXNlcnMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCd1c2VycycpKTtcblx0XHRcdFx0dGhpcy51c2VycyA9IHVzZXJzO1xuXHRcdFx0XHR1c2Vycy4kbG9hZGVkKCkudGhlbigodXNlcnMpID0+IHtcblx0XHRcdFx0XHR2YXIgdXNlciA9IF8uZmluZFdoZXJlKHVzZXJzLCB7IGVtYWlsOiBjdXJyZW50QXV0aC5wYXNzd29yZC5lbWFpbCB9KTtcblx0XHRcdFx0XHRpZiAoIXRoaXMuY2xpZW50LnRlY2gpIHtcblx0XHRcdFx0XHRcdHRoaXMuY2xpZW50LnRlY2ggPSB1c2VyLm5hbWU7XG5cdFx0XHRcdFx0XHR0aGlzLmNsaWVudC50ZWNoSUQgPSB1c2VyLiRpZDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5wcmludEpvYiA9IHVzZXIucHJpbnRKb2I7XG5cdFx0XHRcdFx0dGhpcy5Kb2JTdGF0dXMuZGF0ZSh0aGlzLmNsaWVudCkudGhlbigoZGF0ZSkgPT4ge1xuXHRcdFx0XHRcdFx0dGhpcy5jb21wbGV0ZWREYXRlID0gZGF0ZTtcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHQvL2hpZGUgbG9hZGluZyB3aW5kb3dcblx0XHRcdFx0XHR0aGlzLkxvYWRpbmdQb3B1cC5oaWRlKCk7XG5cdFx0XHRcdH0pXG5cdFx0XHRcdFxuXHRcdFx0XHQvKipcblx0XHRcdFx0ICpzZXQgJHdhdGNoIHRvIGFkZCBhbmQgcmVtb3ZlIGNsYXNzZXMgdG8gaGlkZSBjb2x1bW5zXG5cdFx0XHRcdCAqL1xuXHRcdFx0XHR0aGlzLmhpZGVPblByaW50KCk7XG5cblx0XHRcdFx0Ly90ZW1wIGZpeCBmb3Igc2lnbmF0dXJlIGlzc3VlXG5cdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRcdC8vSW5pdCBTaWduYXR1cmVcblx0XHRcdFx0XHRSZXBvcnRDdHJsLmluaXRTaWduUGFkLmNhbGwodGhpcyk7XG5cdFx0XHRcdH0sIDEpO1xuXG5cdFx0XHRcdC8vc2V0ICR3YXRjaCBvbiB2bS5jbGllbnQgc2NvcGUuIEFueSBjaGFuZ2VzIHRvIHZtLmNsaWVudCBhcmUgc2F2ZWRcblx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLiR3YXRjaCh0aGlzLmNsaWVudCk7XG5cblx0XHRcdH0pXG5cdFx0fSBlbHNlIHtcblx0XHRcdC8qKlxuXHRcdFx0ICogbG9hZCBkYXRhIGZyb20gbG9jYWwgZGF0YWJhc2Vcblx0XHRcdCAqL1xuXHRcdFx0dGhpcy5Mb2FkUmVwb3J0c1Nydi5sb2NhbCgpLnRoZW4oKGNsaWVudCkgPT4ge1xuXHRcdFx0XHQvL3NldCB0aGUgdmlldyBtb2RlbCB0byBjbGllbnQgZGF0YSBmcm9tIGxvY2FsIGRhdGFiYXNlXG5cdFx0XHRcdHRoaXMuY2xpZW50ID0gY2xpZW50O1xuXHRcdFx0XHRcblx0XHRcdFx0Ly9sb2FkIGN1cnJlbnQgYWRtaW4gdXNlciBmcm9tIGxvY2FsIGRhdGFiYXNlXG5cdFx0XHRcdHZhciB1c2VyID0gdGhpcy5Mb2NhbERhdGFiYXNlLiRnZXRDdXJyZW50QXV0aCgpXG5cdFx0XHRcdFxuXHRcdFx0XHQvL3NldCB0aGUgdGVjaCBiYXNlZCBvbiB0aGUgY3VycmVudCB1c2VyJ3MgZW1haWxcblx0XHRcdFx0dmFyIHVzZXJzID0gdGhpcy5Mb2NhbERhdGFiYXNlLmdldCgndXNlcnMnKTtcblx0XHRcdFx0dGhpcy51c2VycyA9IHVzZXJzO1xuXG5cdFx0XHRcdGlmICghdGhpcy5jbGllbnQudGVjaCkge1xuXHRcdFx0XHRcdHRoaXMuY2xpZW50LnRlY2ggPSB1c2VyLm5hbWU7XG5cdFx0XHRcdFx0dGhpcy5jbGllbnQudGVjaElEID0gdXNlci4kaWQ7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHR0aGlzLnByaW50Sm9iID0gdXNlci5wcmludEpvYjtcblx0XHRcdFx0dGhpcy5Kb2JTdGF0dXMuZGF0ZSh0aGlzLmNsaWVudCkudGhlbigoZGF0ZSkgPT4ge1xuXHRcdFx0XHRcdHRoaXMuY29tcGxldGVkRGF0ZSA9IGRhdGU7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHQvL3NldCAkd2F0Y2ggb24gdm0uY2xpZW50IHNjb3BlLiBBbnkgY2hhbmdlcyB0byB2bS5jbGllbnQgYXJlIHNhdmVkXG5cdFx0XHRcdHRoaXMuTG9jYWxEYXRhYmFzZS4kd2F0Y2godGhpcy5jbGllbnQpO1xuXHRcdFx0XHRcblx0XHRcdFx0Ly9Jbml0IFNpZ25hdHVyZVxuXHRcdFx0XHRSZXBvcnRDdHJsLmluaXRTaWduUGFkLmNhbGwodGhpcyk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvL2hpZGUgbG9hZGluZyB3aW5kb3dcblx0XHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0fSlcblx0XHR9XG5cdH1cblxuXHRzZXRUZWNoSUQodGVjaCkge1xuXHRcdHZhciB0ZWNocyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ3VzZXJzJykpO1xuXHRcdHRlY2hzLiRsb2FkZWQoKS50aGVuKCh0ZWNocyk9PiB7XG5cdFx0XHR2YXIgX3RlY2ggPSBfLmZpbmRXaGVyZSh0ZWNocywgeyBuYW1lOiB0ZWNoIH0pO1xuXHRcdFx0dGhpcy5jbGllbnQudGVjaElEID0gX3RlY2guJGlkO1xuXHRcdH0pXG5cdH1cblxuXHRzaWduYXR1cmUoKSB7XG5cdFx0dGhpcy5TaWduUGFkLmNsZWFyKCk7XG5cdH1cblxuXHRjbG9zZSgpIHsvKiogZnVuY3Rpb24gZm9yIGNsb3NlIGJ1dHRvbiAqL1xuXHRcdHRoaXMuJHN0YXRlLmdvKCdqb2JzJyk7XG5cdFx0dGhpcy5QcmludC5yZXNldE5hbWUoKTtcblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHVud2F0Y2goKTtcblx0fVxuXG5cdGNvbXBsZXRlKCkge1xuXHRcdHRoaXMuSm9iU3RhdHVzLnNldFN0YXR1cyh0aGlzLmNsaWVudCwgJ0NvbXBsZXRlJyk7XG5cdFx0dGhpcy5jbG9zZSgpO1xuXHR9XG5cblx0cHJpbnQoKSB7XG5cdFx0dGhpcy5Kb2JTdGF0dXMuc2V0U3RhdHVzKHRoaXMuY2xpZW50LCAnUHJpbnRlZCcpO1xuXHRcdHRoaXMuUHJpbnQudGl0bGVOYW1lID0geyBuYW1lOiB0aGlzLmNsaWVudC5uYW1lLCByZXBvcnQ6ICdDb25kaXRpb25hbCBSZXBvcnQnIH07XG5cdFx0dGhpcy5QcmludC5leGVjKCk7XG5cdH1cblxuXHRoaWRlT25QcmludCgpIHtcblxuXHRcdHRoaXMuJHNjb3BlLiR3YXRjaENvbGxlY3Rpb24oKCkgPT4geyByZXR1cm4gdGhpcy5jbGllbnQgfSxcblx0XHRcdChuLCBvKSA9PiB7XG5cdFx0XHRcdFxuXHRcdFx0XHQvL2xpc3Qgb2YgY29uZGl0aW9uIGhlYWRlcnMgdG8gcmV2aWV3XG5cdFx0XHRcdHZhciBsaXN0T2ZDb25kaXRpb25zID0gW1xuXHRcdFx0XHRcdHsgbmFtZTogJ2NvbmRpdGlvbkEnLCBoaWRlOiAnaGlkZUEnIH0sXG5cdFx0XHRcdFx0eyBuYW1lOiAnY29uZGl0aW9uQicsIGhpZGU6ICdoaWRlQicgfSxcblx0XHRcdFx0XHR7IG5hbWU6ICdjb25kaXRpb25DJywgaGlkZTogJ2hpZGVDJyB9LFxuXHRcdFx0XHRcdHsgbmFtZTogJ2NvbmRpdGlvbkQnLCBoaWRlOiAnaGlkZUQnIH0sXG5cdFx0XHRcdFx0eyBuYW1lOiAnY29uZGl0aW9uRScsIGhpZGU6ICdoaWRlRScgfSxcblx0XHRcdFx0XHR7IG5hbWU6ICdjb25kaXRpb25GJywgaGlkZTogJ2hpZGVGJyB9LFxuXHRcdFx0XHRcdHsgbmFtZTogJ2NvbmRpdGlvbkcnLCBoaWRlOiAnaGlkZUcnIH0sXG5cdFx0XHRcdFx0eyBuYW1lOiAnY29uZGl0aW9uSCcsIGhpZGU6ICdoaWRlSCcgfSxcblx0XHRcdFx0XHR7IG5hbWU6ICdjb25kaXRpb25JJywgaGlkZTogJ2hpZGVJJyB9XG5cblx0XHRcdFx0XTtcblx0XHRcdFx0Ly9jaGVja3MgdG8gc2VlIGlmIHRoZSAgY29uZGl0aW9uIGhlYWRlciBleGlzdC5cblx0XHRcdFx0Ly9kb2VzIG5vdCBwcmludCBjb2x1bW4gd2l0aCBubyBjb25kaXRpb24gaGVhZGVyIFxuXHRcdFx0XHRsaXN0T2ZDb25kaXRpb25zLmZvckVhY2goKGVsZW1lbnQpID0+IHtcblx0XHRcdFx0XHRpZiAobltlbGVtZW50Lm5hbWVdKSB7XG5cdFx0XHRcdFx0XHQobltlbGVtZW50Lm5hbWVdLmxlbmd0aCA+IDApID8gdGhpc1tlbGVtZW50LmhpZGVdID0gZmFsc2UgOiB0aGlzW2VsZW1lbnQuaGlkZV0gPSB0cnVlXG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXNbZWxlbWVudC5oaWRlXSA9IHRydWU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vaGlkZXMgdGhlIHJpZ2h0IGJvcmRlciBpZiBjb2x1bW4gQiBhbmQgQyBhcmUgaGlkZGVuXG5cdFx0XHRcdHRoaXMuYm9yZGVyQSA9ICh0aGlzLmhpZGVCICYmIHRoaXMuaGlkZUMpO1xuXHRcdFx0XHR0aGlzLmJvcmRlckQgPSAodGhpcy5oaWRlRSAmJiB0aGlzLmhpZGVGKTtcblx0XHRcdFx0dGhpcy5ib3JkZXJHID0gKHRoaXMuaGlkZUggJiYgdGhpcy5oaWRlSSk7XG5cblx0XHRcdFx0XG5cdFx0XHRcdC8vaGlkZXMgdGhlIHJpZ2h0IGJvcmRlciBpZiBjb2x1bW4gQyBhcmUgaGlkZGVuXG5cdFx0XHRcdHRoaXMuYm9yZGVyQiA9ICh0aGlzLmhpZGVDKTtcblx0XHRcdFx0dGhpcy5ib3JkZXJFID0gKHRoaXMuaGlkZUYpO1xuXHRcdFx0XHR0aGlzLmJvcmRlckggPSAodGhpcy5oaWRlSSk7XG5cblxuXHRcdFx0fSk7XG5cdH1cbn1cblxuZXhwb3J0IHtSZXBvcnRDdHJsfSIsIi8qKlxuICogU2lkZSBOYXYgQ29udHJvbGxlciBmb3IgaW5kZXguaHRtbFxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuY2xhc3MgU2lkZU5hdkN0cmx7XG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0XG5cdH1cbn1cblxuZXhwb3J0e1NpZGVOYXZDdHJsfSIsIi8qKlxuICogV2F0ZXIgU2hlZXQgQ29udHJvbGxlciBmb3IgaW5kZXguaHRtbFxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuXG5jbGFzcyBXYXRlclNoZWV0Q3RybCB7XG5cdGNvbnN0cnVjdG9yKEZpcmVCYXNlUmVmLCAkZmlyZWJhc2VPYmplY3QsICRzdGF0ZSwgJHNjb3BlLCAkZmlyZWJhc2VBcnJheSxcblx0XHRMb2FkUmVwb3J0c1NydiwgU2lnblBhZCwgTG9jYWxEYXRhYmFzZSwgTG9hZGluZ1BvcHVwLFxuXHRcdE9yaWVudGF0aW9uLCAkd2luZG93LCBkZXZpY2VEZXRlY3RvciwgRmlyZUJhc2VBdXRoLCBKb2JTdGF0dXMsIFByaW50KSB7XG5cdFx0XG5cdFx0Ly91aS1yb3V0ZXIgc3RhdGUgc2VydmljZVxuXHRcdHRoaXMuJHN0YXRlID0gJHN0YXRlO1xuXHRcdFxuXHRcdC8vIEdldCBhIGRhdGFiYXNlIHJlZmVyZW5jZSBcblx0XHR0aGlzLnJlZiA9IEZpcmVCYXNlUmVmLnJlZjtcblxuXHRcdC8vU2lnbmF0dXJlIFBhZFxuXHRcdHRoaXMuY2FudmFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignY2FudmFzJyk7XG5cdFx0dGhpcy5TaWduUGFkID0gU2lnblBhZDtcblxuXHRcdHRoaXMuTG9hZFJlcG9ydHNTcnYgPSBMb2FkUmVwb3J0c1NydjtcblxuXHRcdHRoaXMuRmlyZUJhc2VBdXRoID0gRmlyZUJhc2VBdXRoO1xuXG5cdFx0dGhpcy4kZmlyZWJhc2VBcnJheSA9ICRmaXJlYmFzZUFycmF5O1xuXG5cdFx0dGhpcy5Mb2FkaW5nUG9wdXAgPSBMb2FkaW5nUG9wdXBcblxuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZSA9IExvY2FsRGF0YWJhc2U7XG5cblx0XHR0aGlzLiRzY29wZSA9ICRzY29wZTtcblxuXHRcdHRoaXMuSm9iU3RhdHVzID0gSm9iU3RhdHVzO1xuXG5cdFx0dGhpcy5QcmludCA9IFByaW50O1xuXHRcdFxuXHRcdC8vZGlzcGxheSBsb2FkaW5nIHdpbmRvd1xuXHRcdExvYWRpbmdQb3B1cC5zaG93KCk7XG5cblx0XHR0aGlzLmxvYWREYXRhKCk7XG5cblx0XHRPcmllbnRhdGlvbi5zb3VyY2VQYWdlID0gJ3JlcG9ydCc7XG5cblx0XHQkd2luZG93Lm9ub3JpZW50YXRpb25jaGFuZ2UgPSAoKSA9PiB7XG5cdFx0XHR0aGlzLnNjcm9sbCA9IE9yaWVudGF0aW9uLnNjcm9sbEhlaWdodDtcblx0XHRcdHRoaXMubG9hZERhdGEoKTtcblx0XHR9XG5cblx0XHRpZiAoZGV2aWNlRGV0ZWN0b3IuZGV2aWNlID09PSAnaXBob25lJykge1xuXHRcdFx0dGhpcy5zY3JvbGwgPSAnc2Nyb2xsLWlQaG9uZSdcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5zY3JvbGwgPSBPcmllbnRhdGlvbi5zY3JvbGxIZWlnaHQ7XG5cdFx0fVxuXG5cdFx0dGhpcy4kc2NvcGUuJG9uKCdjb25uZWN0ZWQnLCAoKSA9PiB7XG5cdFx0XHRjb25zb2xlLmxvZygnY29ubmVjdGVkJyk7XG5cdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHN5bmNUb0ZpcmViYXNlKCk7XG5cdFx0fSk7XG5cdH1cblxuXHRzdGF0aWMgaW5pdFNpZ25QYWQoKSB7XG5cdFx0Ly9Jbml0IFNpZ25hdHVyZVxuXHRcdHRoaXMuU2lnblBhZC5pbml0KHRoaXMuY2FudmFzLCB0aGlzLmNsaWVudCk7XG5cdFx0dGhpcy5TaWduUGFkLnJlc2l6ZUNhbnZhcygpO1xuXHRcdHRoaXMuU2lnblBhZC5nZXRTaWduYXR1cmUoKTtcblx0fVxuXG5cdGxvYWREYXRhKCkge1xuXHRcdC8vY2hlY2sgaWYgdGhlIHN5c3RlbSBpcyBvbmxpbmVcblx0XHRpZiAobmF2aWdhdG9yLm9uTGluZSkge1xuXG5cdFx0XHR0aGlzLkxvYWRSZXBvcnRzU3J2LnJlbW90ZSh0aGlzLiRzY29wZSkudGhlbigoY2xpZW50KSA9PiB7XG5cdFx0XHRcdHRoaXMuY2xpZW50ID0gY2xpZW50O1xuXG5cdFx0XHRcdHRoaXMuTG9hZFJlcG9ydHNTcnYucmVtb3RlVXNlcnMoKS50aGVuKChkYXRhKSA9PiB7XG5cdFx0XHRcdFx0dGhpcy51c2VycyA9IGRhdGEudXNlcnM7XG5cdFx0XHRcdFx0aWYgKCF0aGlzLmNsaWVudC50ZWNoKSB7XG5cdFx0XHRcdFx0XHR0aGlzLmNsaWVudC50ZWNoID0gZGF0YS50ZWNoLm5hbWU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHRoaXMucHJpbnRKb2IgPSBkYXRhLnRlY2gucHJpbnRKb2I7XG5cdFx0XHRcdFx0Ly9oaWRlIGxvYWRpbmcgd2luZG93XG5cdFx0XHRcdFx0dGhpcy5Mb2FkaW5nUG9wdXAuaGlkZSgpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHRcblx0XHRcdFx0Ly9zZXQgJHdhdGNoIG9uIHZtLmNsaWVudCBzY29wZS4gQW55IGNoYW5nZXMgdG8gdm0uY2xpZW50IGFyZSBzYXZlZFxuXHRcdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHdhdGNoKHRoaXMuY2xpZW50KTtcblxuXHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHQvL0luaXQgU2lnbmF0dXJlXG5cdFx0XHRcdFx0V2F0ZXJTaGVldEN0cmwuaW5pdFNpZ25QYWQuY2FsbCh0aGlzKTtcblx0XHRcdFx0fSwgMSk7XG5cdFx0XHR9KVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLkxvYWRSZXBvcnRzU3J2LmxvY2FsKHRoaXMuJHNjb3BlKS50aGVuKChjbGllbnQpID0+IHtcblx0XHRcdFx0Ly9zZXQgdGhlIHZpZXcgbW9kZWwgdG8gY2xpZW50IGRhdGEgZnJvbSBsb2NhbCBkYXRhYmFzZVxuXHRcdFx0XHR0aGlzLmNsaWVudCA9IGNsaWVudDtcblxuXHRcdFx0XHQvL2xvYWQgY3VycmVudCBhZG1pbiB1c2VyIGZyb20gbG9jYWwgZGF0YWJhc2Vcblx0XHRcdFx0dmFyIHVzZXIgPSB0aGlzLkxvY2FsRGF0YWJhc2UuJGdldEN1cnJlbnRBdXRoKCk7XG5cdFx0XHRcdFxuXHRcdFx0XHQvL3NldCB0aGUgdGVjaCBiYXNlZCBvbiB0aGUgY3VycmVudCB1c2VyJ3MgZW1haWxcblx0XHRcdFx0dmFyIHVzZXJzID0gdGhpcy5Mb2NhbERhdGFiYXNlLmdldCgndXNlcnMnKTtcblx0XHRcdFx0dGhpcy51c2VycyA9IHVzZXJzO1xuXG5cdFx0XHRcdC8vc2V0IHRoZSB0ZWNoXG5cdFx0XHRcdGlmICghdGhpcy5jbGllbnQudGVjaCkge1xuXHRcdFx0XHRcdHRoaXMuY2xpZW50LnRlY2ggPSB1c2VyLm5hbWU7XG5cdFx0XHRcdFx0dGhpcy5jbGllbnQudGVjaElEID0gdXNlci4kaWQ7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdC8vc2V0ICR3YXRjaCBvbiB2bS5jbGllbnQgc2NvcGUuIEFueSBjaGFuZ2VzIHRvIHZtLmNsaWVudCBhcmUgc2F2ZWRcblx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLiR3YXRjaCh0aGlzLmNsaWVudCk7XG5cblx0XHRcdFx0V2F0ZXJTaGVldEN0cmwuaW5pdFNpZ25QYWQuY2FsbCh0aGlzKTtcblx0XHRcdFx0XG5cdFx0XHRcdC8vaGlkZSBsb2FkaW5nIHdpbmRvd1xuXHRcdFx0XHR0aGlzLkxvYWRpbmdQb3B1cC5oaWRlKCk7XG5cdFx0XHR9KVxuXHRcdH1cblx0fVxuXG5cdHNpZ25hdHVyZSgpIHtcblx0XHR0aGlzLlNpZ25QYWQuY2xlYXIoKTtcblx0fVxuXG5cdGNsb3NlKCkge1xuXHRcdHRoaXMuJHN0YXRlLmdvKCdqb2JzJyk7XG5cdFx0dGhpcy5QcmludC5yZXNldE5hbWUoKTtcblx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHVud2F0Y2goKTtcblx0fVxuXG5cdGNvbXBsZXRlKCkge1xuXHRcdHRoaXMuSm9iU3RhdHVzLnNldFN0YXR1cyh0aGlzLmNsaWVudCwgJ0NvbXBsZXRlJyk7XG5cdFx0dGhpcy5jbG9zZSgpO1xuXHR9XG5cblx0cHJpbnQoKSB7XG5cdFx0dGhpcy5Kb2JTdGF0dXMuc2V0U3RhdHVzKHRoaXMuY2xpZW50LCAnUHJpbnRlZCcpO1xuXHRcdHRoaXMuUHJpbnQudGl0bGVOYW1lID0geyBuYW1lOiB0aGlzLmNsaWVudC5uYW1lLCByZXBvcnQ6ICdXYXRlciBSZXBvcnQnIH07XG5cdFx0dGhpcy5QcmludC5leGVjKCk7XG5cblx0fVxufVxuZXhwb3J0IHtXYXRlclNoZWV0Q3RybH0iLCIvKipcbiAqIExvZ2luIENvbnRyb2xsZXIgZm9yIGxvZ2luLmh0bWxcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cbmNsYXNzIERhdGVJU09TcnYge1xuXHRjb25zdHJ1Y3RvcigpIHtcblx0XHQvL2NvbnZlcnRzIGxvY2FsIGlzby1zdHJpbmcgdG8gaGFuZGxlZCB0aW1lem9uZVxuXHRcdHRoaXMudHpvZmZzZXQgPSAobmV3IERhdGUoKSkuZ2V0VGltZXpvbmVPZmZzZXQoKSAqIDYwMDAwOyAvL29mZnNldCBpbiBtaWxsaXNlY29uZHNcblx0XHR0aGlzLmxvY2FsSVNPVGltZSA9IChuZXcgRGF0ZShEYXRlLm5vdygpIC0gdGhpcy50em9mZnNldCkpLnRvSVNPU3RyaW5nKCkuc2xpY2UoMCwgLTEpO1xuXHR9XG5cblx0Z2V0IHRvZGF5KCkge1xuXHRcdHJldHVybiB0aGlzLmxvY2FsSVNPVGltZTtcblx0fVxuXG5cdGdldCB0b21vcnJvdygpIHtcblx0XHQvL2NoZWNrIHRvIHNlZSBpZiB0b2RheSBpcyBGcmlkYXlcblx0XHQvL2lmIGZyaWRheSBzZXQgdG9tb3Jyb3cgYnV0dG9uIHRvIE1vbmRheVxuXHRcdHZhciB0b2RheSA9IG5ldyBEYXRlKCkuZ2V0RGF5KClcblx0XHRzd2l0Y2ggKHRvZGF5KSB7XG5cdFx0XHRjYXNlIDU6XG5cdFx0XHRcdHJldHVybiBtb21lbnQobmV3IERhdGUodGhpcy5sb2NhbElTT1RpbWUpKS5hZGQoMywgJ2RheXMnKS50b0lTT1N0cmluZygpO1xuXHRcdFx0XHRicmVhaztcblx0XHRcdGNhc2UgNjpcblx0XHRcdFx0cmV0dXJuIG1vbWVudChuZXcgRGF0ZSh0aGlzLmxvY2FsSVNPVGltZSkpLmFkZCgyLCAnZGF5cycpLnRvSVNPU3RyaW5nKCk7XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSA3OlxuXHRcdFx0XHRyZXR1cm4gbW9tZW50KG5ldyBEYXRlKHRoaXMubG9jYWxJU09UaW1lKSkuYWRkKDEsICdkYXlzJykudG9JU09TdHJpbmcoKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRyZXR1cm4gbW9tZW50KG5ldyBEYXRlKHRoaXMubG9jYWxJU09UaW1lKSkuYWRkKDEsICdkYXlzJykudG9JU09TdHJpbmcoKTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0fVxuXHR9XG59XG5cbmV4cG9ydCB7RGF0ZUlTT1Nydn0iLCIvKipcbiAqIEZpcmVCYXNlIEF1dGggU2VydmljZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5jbGFzcyBGaXJlQmFzZUF1dGhTcnZ7XG5cdGNvbnN0cnVjdG9yKEZpcmVCYXNlUmVmLCAkZmlyZWJhc2VBdXRoKXtcblx0XHR0aGlzLkZpcmVCYXNlUmVmID0gRmlyZUJhc2VSZWYucmVmO1xuXHRcdHJldHVybiAkZmlyZWJhc2VBdXRoKHRoaXMuRmlyZUJhc2VSZWYpO1xuXHR9XG5cdFxuXHQvLyBnZXQgcmVmKCkge1xuXHQvLyBcdHJldHVybiB0aGlzLkZpcmVCYXNlUmVmO1xuXHQvLyB9XG59XG5cbmV4cG9ydHtGaXJlQmFzZUF1dGhTcnZ9IiwiLyoqXG4gKiBKb2IgU3RhdHVzIFNlcnZpY2UgXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5jbGFzcyBKb2JTdGF0dXNTcnYge1xuXHRjb25zdHJ1Y3RvcihGaXJlQmFzZVJlZiwgJGZpcmViYXNlT2JqZWN0LCAkcSwgTG9jYWxEYXRhYmFzZSkge1xuXHRcdC8vIEdldCBhIGRhdGFiYXNlIHJlZmVyZW5jZSBcblx0XHR0aGlzLnJlZiA9IEZpcmVCYXNlUmVmLnJlZjtcblx0XHR0aGlzLiRmaXJlYmFzZU9iamVjdCA9ICRmaXJlYmFzZU9iamVjdDtcblx0XHR0aGlzLiRxID0gJHE7XG5cdFx0dGhpcy5Mb2NhbERhdGFiYXNlID0gTG9jYWxEYXRhYmFzZTtcblx0fVxuXG5cdHNldFN0YXR1cyhjbGllbnQsIHN0YXR1cykge1xuXHRcdGlmIChuYXZpZ2F0b3Iub25MaW5lKSB7XG5cdFx0XHR2YXIgam9iID0gdGhpcy4kZmlyZWJhc2VPYmplY3QodGhpcy5yZWYuY2hpbGQoJ2pvYicpLmNoaWxkKGNsaWVudC5mb3JlaWduS2V5KSk7XG5cdFx0XHRqb2IuJGxvYWRlZCgpLnRoZW4oKGpvYikgPT4ge1xuXHRcdFx0XHRpZiAoc3RhdHVzID09PSAnQ29tcGxldGUnKSB7XG5cdFx0XHRcdFx0am9iLmNvbXBsZXRlZERhdGUgPSBuZXcgRGF0ZSgpLnRvSVNPU3RyaW5nKCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0am9iLnN0YXR1cyA9IHN0YXR1cztcblx0XHRcdFx0am9iLiRzYXZlKCk7XG5cdFx0XHR9KVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLkxvY2FsRGF0YWJhc2UuJHNhdmUoY2xpZW50LCB7aXNKb2I6dHJ1ZSxzdGF0dXM6c3RhdHVzfSk7XG5cdFx0fVxuXHR9XG5cblx0ZGF0ZShjbGllbnQpIHtcblx0XHRyZXR1cm4gdGhpcy4kcSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0XHR2YXIgam9iID0gdGhpcy4kZmlyZWJhc2VPYmplY3QodGhpcy5yZWYuY2hpbGQoJ2pvYicpLmNoaWxkKGNsaWVudC5mb3JlaWduS2V5KSk7XG5cdFx0XHRqb2IuJGxvYWRlZCgpLnRoZW4oKGpvYikgPT4ge1xuXHRcdFx0XHRyZXNvbHZlKG1vbWVudChqb2IuY29tcGxldGVkRGF0ZSkuZm9ybWF0KCdkZGQsIE1NTSBEbyBZWVlZJykpO1xuXHRcdFx0fSlcblx0XHR9KVxuXHR9XG59XG5leHBvcnQge0pvYlN0YXR1c1Nydn0iLCIvKipcbiAqIFNldCBBZGRyZXNzIFNlcnZpY2VcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cblxuY2xhc3MgTG9hZFJlcG9ydHNTcnYge1xuXHRjb25zdHJ1Y3RvcihGaXJlQmFzZVJlZiwgJHN0YXRlLCAkc3RhdGVQYXJhbXMsICRmaXJlYmFzZUFycmF5LFxuXHRcdCRmaXJlYmFzZU9iamVjdCwgJHEsIExvY2FsRGF0YWJhc2UsICRyb290U2NvcGUsIEZpcmVCYXNlQXV0aCkge1xuXHRcdHRoaXMuRmlyZUJhc2VSZWYgPSBGaXJlQmFzZVJlZjtcblx0XHR0aGlzLiRzdGF0ZSA9ICRzdGF0ZTtcblx0XHR0aGlzLiRzdGF0ZVBhcmFtcyA9ICRzdGF0ZVBhcmFtcztcblx0XHR0aGlzLiRmaXJlYmFzZUFycmF5ID0gJGZpcmViYXNlQXJyYXk7XG5cdFx0dGhpcy4kZmlyZWJhc2VPYmplY3QgPSAkZmlyZWJhc2VPYmplY3Q7XG5cdFx0dGhpcy4kcSA9ICRxO1xuXHRcdHRoaXMuTG9jYWxEYXRhYmFzZSA9IExvY2FsRGF0YWJhc2U7XG5cdFx0dGhpcy5GaXJlQmFzZUF1dGggPSBGaXJlQmFzZUF1dGg7XG5cdFx0Ly8gR2V0IGEgZGF0YWJhc2UgcmVmZXJlbmNlIFxuXHRcdHRoaXMucmVmID0gRmlyZUJhc2VSZWYucmVmO1xuXHR9XG5cblx0cmVtb3RlKCRzY29wZSkge1xuXG5cdFx0cmV0dXJuIHRoaXMuJHEoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFxuXHRcdFx0Ly9jbGllbnQgbmFtZVxuXHRcdFx0dmFyIGlkID0gdGhpcy4kc3RhdGVQYXJhbXMuaWQ7XG5cdFx0XG5cdFx0XHQvL2dldCB0aGUgY2xpZW50IGRhdGFcblx0XHRcdHZhciBjbGllbnRzID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnY2xpZW50cycpKTtcblxuXHRcdFx0Ly9sb2FkIHRoZSBjbGllbnQgZGF0YVxuXHRcdFx0Y2xpZW50cy4kbG9hZGVkKCkudGhlbigoZGF0YSkgPT4ge1xuXG5cdFx0XHRcdGRhdGEubWFwKChjdiwgaSwgYSkgPT4ge1xuXG5cdFx0XHRcdFx0aWYgKGN2LmZvcmVpZ25LZXkgPT09IGlkKSB7XG5cdFx0XHRcdFx0XHQvL2dldCB0aGUgY2xpZW50IGRhdGEgaS5lLiBuYW1lLCBhZGRlcnNzLCBjaXR5LCBzdGF0ZVxuXHRcdFx0XHRcdFx0dmFyIGNsaWVudCA9IHRoaXMuJGZpcmViYXNlT2JqZWN0KHRoaXMucmVmLmNoaWxkKCdjbGllbnRzJykuY2hpbGQoY3YuJGlkKSk7XG5cblx0XHRcdFx0XHRcdGNsaWVudC4kYmluZFRvKCRzY29wZSwgJ2NsaWVudCcpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHRcdFx0XHRyZXNvbHZlKCRzY29wZS5jbGllbnQpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0XHRcbi8qXHRcdFx0XHRcdFx0ZGVidWdnZXI7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdHRoaXMudW53YXRjaCA9IGNsaWVudC4kd2F0Y2goKGRhdGEpPT4ge1xuXHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhkYXRhKTtcblx0XHRcdFx0XHRcdFx0ZGVidWdnZXJcblx0XHRcdFx0XHRcdFx0dmFyIGNsaWVudCA9IHRoaXMuJGZpcmViYXNlT2JqZWN0KHRoaXMucmVmLmNoaWxkKCdjbGllbnRzJykuY2hpbGQoZGF0YS5rZXkpKTtcblx0XHRcdFx0XHRcdFx0Y2xpZW50LiRsb2FkZWQoKS50aGVuKChjbGllbnQpPT57XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5Mb2NhbERhdGFiYXNlLiRzYXZlKGNsaWVudClcblx0XHRcdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhcImRhdGEgY2hhbmdlZCFcIixjbGllbnQpO1xuXHRcdFx0XHRcdFx0XHR9KVxuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHR9KTsqL1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblx0XHRcdH0pXG5cdFx0fSk7XG5cdH1cblx0XG5cdHJlbW90ZVN0b3BXYXRjaCgpe1xuXHRcdHRoaXMudW53YXRjaCgpO1xuXHR9XG5cblx0cmVtb3RlVXNlcnMoKSB7XG5cdFx0cmV0dXJuIHRoaXMuJHEoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0dmFyIGN1cnJlbnRBdXRoID0gdGhpcy5GaXJlQmFzZUF1dGguJGdldEF1dGgoKTtcblx0XHRcdHZhciB1c2VycyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ3VzZXJzJykpO1xuXHRcdFx0dGhpcy51c2VycyA9IHVzZXJzXG5cdFx0XHR1c2Vycy4kbG9hZGVkKCkudGhlbigodXNlcnMpID0+IHtcblx0XHRcdFx0dmFyIHVzZXIgPSBfLmZpbmRXaGVyZSh1c2VycywgeyBlbWFpbDogY3VycmVudEF1dGgucGFzc3dvcmQuZW1haWwgfSk7XG5cdFx0XHRcdHJlc29sdmUoeyB1c2VyczogdXNlcnMsIHRlY2g6IHVzZXIgfSk7XG5cblx0XHRcdH0pXG5cdFx0fSk7XG5cdH1cblxuXHRsb2NhbCgpIHtcblx0XHRyZXR1cm4gdGhpcy4kcSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0XHRcblx0XHRcdC8vY2xpZW50IG5hbWVcblx0XHRcdHZhciBpZCA9IHRoaXMuJHN0YXRlUGFyYW1zLmlkO1xuXG5cdFx0XHR2YXIgY2xpZW50cyA9IHRoaXMuTG9jYWxEYXRhYmFzZS5nZXQoJ2NsaWVudHMnKTtcblxuXHRcdFx0Ly9sb2FkIHRoZSBjbGllbnQgZGF0YVxuXHRcdFx0Y2xpZW50cy5tYXAoKGN2LCBpLCBhKSA9PiB7XG5cblx0XHRcdFx0aWYgKGN2LmZvcmVpZ25LZXkgPT09IGlkKSB7XG5cdFx0XHRcdFx0Ly9nZXQgdGhlIGNsaWVudCBkYXRhIGkuZS4gbmFtZSwgYWRkZXJzcywgY2l0eSwgc3RhdGVcblx0XHRcdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0XHRcdHJlc29sdmUoY3YpXG5cdFx0XHRcdFx0fSwgMCk7XG5cblxuXG5cdFx0XHRcdH1cblx0XHRcdH0pXG5cdFx0fSlcblxuXHR9XG59XG5cbmV4cG9ydCB7TG9hZFJlcG9ydHNTcnZ9OyIsIi8qKlxuICogTG9hZGluZyBQb3B1cCBTZXJ2aWNlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cInVzZSBzdHJpY3RcIjtcbmNsYXNzIExvYWRpbmdQb3B1cFNydiB7XG5cdGNvbnN0cnVjdG9yKCRtZERpYWxvZywgJHEsICR0aW1lb3V0KSB7XG5cdFx0dGhpcy4kbWREaWFsb2cgPSAkbWREaWFsb2dcblx0XHR0aGlzLiRxID0gJHE7XG5cdFx0dGhpcy4kdGltZW91dCA9ICR0aW1lb3V0O1xuXHR9XG5cblx0c2hvdygpIHtcblxuXHRcdHRoaXMuJG1kRGlhbG9nLnNob3coe1xuXHRcdFx0Y2xpY2tPdXRzaWRlVG9DbG9zZTogZmFsc2UsXG5cdFx0XHR0ZW1wbGF0ZTogJzxtZC1kaWFsb2cgYXJpYS1sYWJlbD1cIkxvYWRpbmcgZGlhbG9nXCI+ICcgK1xuXHRcdFx0JzxtZC1kaWFsb2ctY29udGVudCBsYXlvdXQ9XCJyb3dcIiBsYXlvdXQtYWxpZ249XCJjZW50ZXIgY2VudGVyXCI+JyArXG5cdFx0XHQnPGRpdiBzdHlsZT1cImZvbnQtc2l6ZTozMHB4XCI+TG9hZGluZzwvZGl2PicgK1xuXHRcdFx0JzxtZC1wcm9ncmVzcy1jaXJjdWxhciBtZC1tb2RlPVwiaW5kZXRlcm1pbmF0ZVwiIG1kLWRpYW1ldGVyPVwiMjVcIj48L21kLXByb2dyZXNzLWNpcmN1bGFyPicgK1xuXHRcdFx0JzwvbWQtZGlhbG9nLWNvbnRlbnQ+PC9tZC1kaWFsb2c+Jyxcblx0XHR9KTtcblx0fVxuXG5cdGhpZGUoKSB7XG5cdFx0dGhpcy4kdGltZW91dCgoKSA9PiB7XG5cdFx0XHR0aGlzLiRtZERpYWxvZy5oaWRlKCk7XG5cdFx0fSwgMClcblx0fVxufVxuZXhwb3J0IHtMb2FkaW5nUG9wdXBTcnZ9IiwiLyoqXG4gKiBMb2NhbCBEYXRhYmFzZSBTZXJ2aWNlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cInVzZSBzdHJpY3RcIjtcbmNsYXNzIExvY2FsRGF0YWJhc2VTcnYge1xuXHRjb25zdHJ1Y3RvcihDYWNoZUZhY3RvcnksIEZpcmVCYXNlUmVmLCAkZmlyZWJhc2VBcnJheSwgJGZpcmViYXNlT2JqZWN0LFxuXHRcdCRxLCAkcm9vdFNjb3BlLCAkaW50ZXJ2YWwsIERhdGVJU08pIHtcblx0XHQvL2dldCBmaXJlYmFzZSByZWZlcmVuY2Vcblx0XHR0aGlzLnJlZiA9IEZpcmVCYXNlUmVmLnJlZjtcblx0XHRcblx0XHQvL3NldCAkZmlyZWJhc2VBcnJheVxuXHRcdHRoaXMuJGZpcmViYXNlQXJyYXkgPSAkZmlyZWJhc2VBcnJheTtcblx0XHRcblx0XHQvL3NldCAkZmlyZWJhc2VBcnJheVxuXHRcdHRoaXMuJGZpcmViYXNlT2JqZWN0ID0gJGZpcmViYXNlT2JqZWN0O1xuXHRcdC8vc2V0ICRxIGxpYmFyeSBcblx0XHR0aGlzLiRxID0gJHFcblxuXHRcdHRoaXMuJHJvb3RTY29wZSA9ICRyb290U2NvcGU7XG5cblx0XHR0aGlzLkRhdGVJU08gPSBEYXRlSVNPO1xuXG5cdFx0dGhpcy51bndhdGNoO1xuXHRcdFxuXHRcdC8vIENoZWNrIHRvIG1ha2Ugc3VyZSB0aGUgY2FjaGUgZG9lc24ndCBhbHJlYWR5IGV4aXN0XG5cdFx0aWYgKCFDYWNoZUZhY3RvcnkuZ2V0KCdjci1jYWNoZScpKSB7XG5cdFx0XHR0aGlzLmNvbmRpdGlvblJlcG9ydENhY2hlID0gQ2FjaGVGYWN0b3J5KCdjci1jYWNoZScpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLmNvbmRpdGlvblJlcG9ydENhY2hlID0gQ2FjaGVGYWN0b3J5LmNyZWF0ZUNhY2hlKCdjci1jYWNoZScsIHtcblx0XHRcdFx0c3RvcmFnZVByZWZpeDogJ25nLWNhY2hlJyxcblx0XHRcdFx0Ly8gc3RvcmFnZU1vZGU6ICdzZXNzaW9uU3RvcmFnZSdcblx0XHRcdH0pXG5cdFx0fVxuXHR9XG5cblx0aW5pdGFsTG9hZCgpIHtcblxuXHRcdHZhciB1c2VycyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ3VzZXJzJykpXG5cdFx0dXNlcnMuJGxvYWRlZCgpLnRoZW4oKHVzZXJzKSA9PiB7XG5cdFx0XHR0aGlzLmNvbmRpdGlvblJlcG9ydENhY2hlLnB1dCgndXNlcnMnLCB1c2Vycyk7XG5cdFx0fSk7XG5cblx0XHR2YXIgam9icyA9IHRoaXMuJGZpcmViYXNlQXJyYXkodGhpcy5yZWYuY2hpbGQoJ2pvYicpKVxuXHRcdGpvYnMuJGxvYWRlZCgpLnRoZW4oKGpvYnMpID0+IHtcblx0XHRcdHRoaXMuY29uZGl0aW9uUmVwb3J0Q2FjaGUucHV0KCdqb2JzJywgam9icyk7XG5cdFx0fSk7XG5cblx0XHR2YXIgam9iT3JkZXIgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdqb2JPcmRlcicpKVxuXHRcdGpvYk9yZGVyLiRsb2FkZWQoKS50aGVuKChqb2JPcmRlcikgPT4ge1xuXHRcdFx0dGhpcy5jb25kaXRpb25SZXBvcnRDYWNoZS5wdXQoJ2pvYk9yZGVyJywgam9iT3JkZXIpO1xuXHRcdH0pO1xuXG5cdFx0dmFyIGNyZXdzID0gdGhpcy4kZmlyZWJhc2VBcnJheSh0aGlzLnJlZi5jaGlsZCgnY3Jld3MnKSlcblx0XHRjcmV3cy4kbG9hZGVkKCkudGhlbigoY3Jld3MpID0+IHtcblx0XHRcdHRoaXMuY29uZGl0aW9uUmVwb3J0Q2FjaGUucHV0KCdjcmV3cycsIGNyZXdzKTtcblx0XHR9KVxuXG5cdFx0dmFyIGNsaWVudHMgPSB0aGlzLiRmaXJlYmFzZUFycmF5KHRoaXMucmVmLmNoaWxkKCdjbGllbnRzJykpXG5cdFx0Y2xpZW50cy4kbG9hZGVkKCkudGhlbigoY2xpZW50cykgPT4ge1xuXHRcdFx0LyoqXG5cdFx0XHQqIHRlbXAgYXJyYXkgdG8gbG9hZCBpbnRvIGxvY2Fsc3RvcmFnZVxuXHRcdFx0KiBAdHlwZSB7YXJyYXkuPG9iamVjdHM+fVxuXHRcdFx0Ki9cblx0XHRcdHZhciBfY2xpZW50cyA9IFtdO1xuXHRcdFx0LyoqXG5cdFx0XHQqIHRyYWNrcyB3aGVuIHRvIHN0YXJ0IHB1c2hpbmcgaXRlbXMgXG5cdFx0XHQqIGludG8gdGhlIF9jbGllbnRzIGFycmF5XG5cdFx0XHQqIEB0eXBlIHtib29sZWFufSBzdGFydEFycmF5UHVzaFxuXHRcdFx0Ki9cblx0XHRcdHZhciBzdGFydEFycmF5UHVzaCA9IGZhbHNlO1xuXHRcdFx0LyoqXG5cdFx0XHQqIGpvYnMgYXJyYXlcblx0XHRcdCogQHR5cGUge2FycmF5LjxvYmplY3RzPn0gam9ic1xuXHRcdFx0Ki9cblx0XHRcdHZhciBqb2JzID0gdGhpcy5nZXQoJ2pvYnMnKTtcblx0XHRcdC8qKlxuXHRcdFx0KiBqb2IgdGhhdCBjb250YWlucyBhIGpvYiBkYXRlIGFmdGVyIDMgZGF5cyBhZ29cblx0XHRcdCogQHR5cGUge29iamVjdH0gam9ic1xuXHRcdFx0Ki9cblx0XHRcdHZhciBqb2IgPSBfLmZpbmQoam9icywgKGpvYikgPT4ge1xuXHRcdFx0XHQvL3B1bGwgam9icyBncmVhdGVyIHRoYW4gdG9kYXlcblx0XHRcdFx0dmFyIHRocmVlRGF5c0FnbyA9IG1vbWVudChuZXcgRGF0ZSh0aGlzLkRhdGVJU08udG9kYXkpKS5zdWJ0cmFjdCgzLCAnZGF5cycpO1xuXHRcdFx0XHR2YXIgX2pvYkRhdGUgPSBtb21lbnQoam9iLmpvYkRhdGUpO1xuXHRcdFx0XHRyZXR1cm4gbW9tZW50KHRocmVlRGF5c0FnbykuaXNCZWZvcmUoX2pvYkRhdGUsICdkYXknKTtcblx0XHRcdH0pO1xuXG5cdFx0XHQvL2xvb3AgdGhyb3VnaCBjbGllbnRcblx0XHRcdGZvciAodmFyIGluZGV4ID0gMDsgaW5kZXggPCBjbGllbnRzLmxlbmd0aDsgaW5kZXgrKykge1xuXHRcdFx0XHQvL3B1bGwgdGhlIGl0ZW0gZnJvbSB0aGUgY2xpZW50cyBhcnJheVxuXHRcdFx0XHR2YXIgZWxlbWVudCA9IGNsaWVudHNbaW5kZXhdO1xuXHRcdFx0XHQvL3B1c2ggdGhlIGl0ZW0gaW50byB0aGUgdGVtcCBhcnJheVxuXHRcdFx0XHRpZiAoc3RhcnRBcnJheVB1c2gpIHtcblx0XHRcdFx0XHRfY2xpZW50cy5wdXNoKGVsZW1lbnQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHQvL3NldCBzdGFydEFycmF5UHVzaCB0byB0cnVlIGlmIGpvYi4kaWQgbWF0Y2hlcyBcblx0XHRcdFx0Ly90aGUgZWxlbWVudCBqb2IgaWQgdW5kZXIgZm9yZWlnbktleVxuXHRcdFx0XHRpZiAoZWxlbWVudC5mb3JlaWduS2V5ID09PSBqb2IuJGlkKSB7XG5cdFx0XHRcdFx0c3RhcnRBcnJheVB1c2ggPSB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdHRoaXMuY29uZGl0aW9uUmVwb3J0Q2FjaGUucHV0KCdjbGllbnRzJywgX2NsaWVudHMpO1xuXG5cdFx0fSlcblxuXHR9XG5cblx0cHV0KGlkLCBkYXRhKSB7XG5cdFx0dGhpcy5jb25kaXRpb25SZXBvcnRDYWNoZS5wdXQoaWQsIGRhdGEpXG5cdH1cblxuXHRyZW1vdmUoaWQpIHtcblx0XHR0aGlzLmNvbmRpdGlvblJlcG9ydENhY2hlLnJlbW92ZShpZClcblx0fVxuXG5cdGdldChpZCkge1xuXHRcdHJldHVybiB0aGlzLmNvbmRpdGlvblJlcG9ydENhY2hlLmdldChpZCk7XG5cdH1cblxuXHQkYXV0aFdpdGhQYXNzd29yZCh1c2VyKSB7XG5cdFx0dmFyIHRoYXQgPSB0aGlzO1xuXG5cdFx0cmV0dXJuIHRoaXMuJHEoKHJlcywgcmVqKSA9PiB7XG5cdFx0XHR2YXIgdXNlcnMgPSB0aGlzLmdldCgndXNlcnMnKTtcblx0XHRcdHZhciByZXN1bHQgPSBfLmZpbmRXaGVyZSh1c2VycywgeyBlbWFpbDogdXNlci5lbWFpbC50b0xvd2VyQ2FzZSgpLCBwYXNzd29yZDogdXNlci5wYXNzd29yZCB9KTtcblx0XHRcdHRoaXMuJHNldEN1cnJlbnRBdXRoKHJlc3VsdCk7XG5cdFx0XHRpZiAocmVzdWx0KSB7XG5cdFx0XHRcdHJlcyhyZXN1bHQpO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmVqKHsgY29kZTogXCJFcnJvciB3aXRoIGNyZWRlbnRpYWxzXCIgfSk7XG5cdFx0XHR9XG5cblx0XHR9KTtcblxuXHR9XG5cblx0JHNldEN1cnJlbnRBdXRoKHVzZXIpIHtcblx0XHRjb25zb2xlLmxvZyh1c2VyKTtcblx0XHR0aGlzLnB1dCgnY3VycmVudEF1dGgnLCB1c2VyKTtcblx0fVxuXG5cdCRnZXRDdXJyZW50QXV0aCgpIHtcblx0XHRyZXR1cm4gdGhpcy5nZXQoJ2N1cnJlbnRBdXRoJylcblx0fVxuXG5cdCRnZXRBdXRoKCkge1xuXHRcdHJldHVybiB0aGlzLiRxKChyZXMsIHJlaikgPT4ge1xuXHRcdFx0dmFyIHNlc3Npb24gPSB0aGlzLmdldCgnc2Vzc2lvbicpXG5cblx0XHRcdGlmIChzZXNzaW9uICE9ICdmYWxzZScpIHtcblx0XHRcdFx0cmVzKHRydWUpXG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhzZXNzaW9uKTtcblx0XHRcdFx0cmVqKGZhbHNlKVxuXHRcdFx0fVxuXG5cdFx0fSlcblx0fVxuXG5cdCRzZXRBdXRoKGRhdGEpIHtcblx0XHR0aGlzLnB1dCgnc2Vzc2lvbicsIGRhdGEpO1xuXHR9XG5cblx0JHVuYXV0aCgpIHtcblx0XHR0aGlzLiRzZXRBdXRoKCdmYWxzZScpO1xuXHRcdHRoaXMucmVtb3ZlKCdhbGxvd2VkJyk7XG5cdFx0dGhpcy5yZW1vdmUoJ2N1cnJlbnRBdXRoJyk7XG5cdFx0dGhpcy5yZW1vdmUoJ3RyYWNrJylcblx0fVxuXG5cdCRzYXZlKGNsaWVudCwgcGFyYW0pIHtcblx0XHQvL2lmIHBhcmFtLmlzSm9iID0gdHJ1ZVxuXHRcdC8vdXBkYXRlcyB0aGUgam9icyBjYWNoZVxuXHRcdGlmIChwYXJhbSAmJiBwYXJhbS5pc0pvYikge1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBMaXN0IG9mIGpvYnMgZnJvbSBjYWNoZVxuXHRcdFx0ICogQHR5cGV7YXJyYXkuPG9iamVjdHM+fSBqb2JzIFxuXHRcdFx0ICovXG5cdFx0XHRsZXQgam9icyA9IHRoaXMuZ2V0KCdqb2JzJyk7XG5cdFx0XHQvKipcblx0XHRcdCAqIHRoZSBpbmRleCBvZiBqb2IgaW4gdGhlIGpvYnMgYXJyYXlcblx0XHRcdCAqIEB0eXBle251bWJlcn0gaW5kZXhcblx0XHRcdCAqL1xuXHRcdFx0bGV0IGluZGV4ID0gXy5maW5kSW5kZXgoam9icywgeyAkaWQ6IGNsaWVudC5mb3JlaWduS2V5IH0pO1xuXHRcdFx0LyoqXG5cdFx0XHQgKiB0aGUgam9iIG9iamVjdCBpbiB0aGUgam9icyBhcnJheVxuXHRcdFx0ICogQHR5cGV7b2JqZWN0fSBqb2Jcblx0XHRcdCAqL1xuXHRcdFx0bGV0IGpvYiA9IF8uZmluZFdoZXJlKGpvYnMsIHsgJGlkOiBjbGllbnQuZm9yZWlnbktleSB9KTtcblx0XHRcdGlmIChwYXJhbS5zdGF0dXMgPT09ICdDb21wbGV0ZScpIHtcblx0XHRcdFx0am9iLmNvbXBsZXRlZERhdGUgPSBuZXcgRGF0ZSgpLnRvSVNPU3RyaW5nKCk7XG5cdFx0XHR9XG5cdFx0XHQvL3NldCBqb2Igc3RhdHVzIHRvIHRoZSBuZXcgc3RhdHVzXG5cdFx0XHRqb2Iuc3RhdHVzID0gcGFyYW0uc3RhdHVzO1xuXHRcdFx0ZGVidWdnZXI7XG5cdFx0XHQvL3VwZGF0ZSB0aGUgam9iIGluIHRoZSBqb2JzIGFycmF5XG5cdFx0XHRqb2JzLnNwbGljZShpbmRleCwgMSwgam9iKTtcblx0XHRcdC8vdXBkYXRlIGpvYnNcblx0XHRcdHRoaXMucHV0KCdqb2JzJywgam9icyk7XG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLmdldCgnam9icycpKTtcblx0XHRcdHJldHVyblxuXHRcdH1cblxuXG5cdFx0bGV0IGNsaWVudHMgPSB0aGlzLmdldCgnY2xpZW50cycpO1xuXHRcdGxldCBpbmRleCA9IF8uZmluZEluZGV4KGNsaWVudHMsIHsgJGlkOiBjbGllbnQuJGlkIH0pO1xuXHRcdC8vIGlmIG5vIG1hdGNoIHRoZW4gYWRkIHRoZSBuZXcgcmVjb3JkIHRvIHRoZSBlbmQgb2YgYXJyYXlcblx0XHRpZiAoaW5kZXggPT09IC0xKSB7XG5cdFx0XHRjbGllbnRzLnB1c2goY2xpZW50KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0Y2xpZW50cy5zcGxpY2UoaW5kZXgsIDEsIGNsaWVudCk7XG5cdFx0fVxuXG5cdFx0dGhpcy5wdXQoJ2NsaWVudHMnLCBjbGllbnRzKTtcblx0fVxuXG5cdCR3YXRjaChjbGllbnQpIHtcblx0XHR0aGlzLnVud2F0Y2ggPSB0aGlzLiRyb290U2NvcGUuJHdhdGNoQ29sbGVjdGlvbihmdW5jdGlvbiAoKSB7IHJldHVybiBjbGllbnQgfSwgKG4sIG8pID0+IHtcblx0XHRcdGlmIChvICE9PSBuKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKG4pO1xuXHRcdFx0XHRuLiRkaXJ0eSA9IHRydWU7XG5cdFx0XHRcdHRoaXMuJHNhdmUobik7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdH1cblxuXHQkdW53YXRjaCgpIHtcblx0XHR0aGlzLnVud2F0Y2goKTtcblx0fVxuXG5cdCRzeW5jVG9GaXJlYmFzZSgpIHtcblx0XHR2YXIgY2xpZW50cyA9IHRoaXMuZ2V0KCdjbGllbnRzJyk7XG5cdFx0dmFyIGpvYnMgPSB0aGlzLmdldCgnam9icycpO1xuXHRcdHZhciBjbGllbnRzVG9VcGRhdGUgPSBfLmZpbHRlcihjbGllbnRzLCB7ICRkaXJ0eTogdHJ1ZSB9KTtcblx0XHR2YXIgam9ic1RvVXBkYXRlID0gXy5maWx0ZXIoam9icywgeyAkZGlydHk6IHRydWUgfSk7XG5cblx0XHRjbGllbnRzVG9VcGRhdGUuZm9yRWFjaCgodiwgaWR4LCBhcnIpID0+IHtcblx0XHRcdHZhciBjbGllbnQgPSB0aGlzLiRmaXJlYmFzZU9iamVjdCh0aGlzLnJlZi5jaGlsZCgnY2xpZW50cycpLmNoaWxkKHYuJGlkKSk7XG5cdFx0XHRkZWxldGUgdi4kZGlydHk7XG5cdFx0XHRmb3IgKHZhciBrZXkgaW4gdikge1xuXHRcdFx0XHRpZiAodi5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG5cdFx0XHRcdFx0Y2xpZW50W2tleV0gPSB2W2tleV07XG5cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0Y2xpZW50LiRzYXZlKCkudGhlbigoKSA9PiB7XG5cdFx0XHRcdHRoaXMuaW5pdGFsTG9hZCgpO1xuXHRcdFx0fSlcblxuXHRcdH0pXG5cblx0XHRqb2JzVG9VcGRhdGUuZm9yRWFjaCgodiwgaWR4LCBhcnIpID0+IHtcblx0XHRcdHZhciBqb2IgPSB0aGlzLiRmaXJlYmFzZU9iamVjdCh0aGlzLnJlZi5jaGlsZCgnam9icycpLmNoaWxkKHYuZm9yZWlnbktleSkpO1xuXHRcdFx0ZGVsZXRlIHYuJGRpcnR5O1xuXHRcdFx0Zm9yICh2YXIga2V5IGluIHYpIHtcblx0XHRcdFx0aWYgKHYuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuXHRcdFx0XHRcdGpvYltrZXldID0gdltrZXldO1xuXG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdGpvYi4kc2F2ZSgpLnRoZW4oKCkgPT4ge1xuXHRcdFx0XHR0aGlzLmluaXRhbExvYWQoKTtcblx0XHRcdH0pXG5cblx0XHR9KVxuXG5cdFx0dGhpcy5pbml0YWxMb2FkKCk7XG5cdH1cblxuXHRzZXQgJHRyYWNrU2l0ZUxvY2F0aW9uKHBhcmFtKSB7XG5cdFx0LyoqXG5cdFx0ICogbG9jYXRpb24gYW5kIGlkXG5cdFx0ICogQHR5cGUge29iamVjdC48c2l0ZSxpZD59IHx8IHtzdHJpbmd9XG5cdFx0ICovXG5cdFx0dGhpcy5wdXQoJ3RyYWNrJywgcGFyYW0pO1xuXHR9XG5cblx0Z2V0ICR0cmFja1NpdGVMb2NhdGlvbigpIHtcblx0XHRyZXR1cm4gdGhpcy5nZXQoJ3RyYWNrJyk7XG5cdH1cbn1cbmV4cG9ydCB7TG9jYWxEYXRhYmFzZVNydn1cbiIsIi8qKlxuICogT3JpZW50YXRpb24gU2VydmljZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuY2xhc3MgT3JpZW50YXRpb25TcnYge1xuXHRjb25zdHJ1Y3Rvcigkd2luZG93LCAkcm9vdFNjb3BlLCAkcSkge1xuXHRcdHRoaXMuJHdpbmRvdyA9ICR3aW5kb3c7XG5cdFx0dGhpcy4kcm9vdFNjb3BlID0gJHJvb3RTY29wZTtcblx0fVxuXG5cdHNldCBzb3VyY2VQYWdlKHNvdXJjZSkge1xuXHRcdHRoaXMuc291cmNlID0gc291cmNlO1xuXHR9XG5cblx0Z2V0IHNjcm9sbEhlaWdodCgpIHtcblx0XHR2YXIgc2Nyb2xsID0gdW5kZWZpbmVkO1xuXHRcdHZhciBsYW5kc2NhcGUgPSB1bmRlZmluZWQ7XG5cdFx0dmFyIHBvcnRyYWl0ID0gdW5kZWZpbmVkO1xuXHRcdHN3aXRjaCAodGhpcy5zb3VyY2UpIHtcblx0XHRcdGNhc2UgJ3JlcG9ydCc6XG5cdFx0XHRcdGxhbmRzY2FwZSA9ICdyZXBvcnQtbGFuZHNjYXBlJ1xuXHRcdFx0XHRwb3J0cmFpdCA9ICdyZXBvcnQtcG9ydHJhaXQnXG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnam9iJzpcblx0XHRcdFx0bGFuZHNjYXBlID0gJ2pvYi1sYW5kc2NhcGUnXG5cdFx0XHRcdHBvcnRyYWl0ID0gJ2pvYi1wb3J0cmFpdCdcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRjYXNlICdjcmVhdGVKb2InOlxuXHRcdFx0XHRsYW5kc2NhcGUgPSAnY3JlYXRlLWpvYi1sYW5kc2NhcGUnXG5cdFx0XHRcdHBvcnRyYWl0ID0gJ2NyZWF0ZS1qb2ItcG9ydHJhaXQnXG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSAnYWRtaW4nOlxuXHRcdFx0XHRsYW5kc2NhcGUgPSAnYWRtaW4tbGFuZHNjYXBlJ1xuXHRcdFx0XHRwb3J0cmFpdCA9ICdhZG1pbi1wb3J0cmFpdCdcblx0XHRcdFx0YnJlYWs7XG5cblx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdGxhbmRzY2FwZSA9ICdsYW5kc2NhcGUnXG5cdFx0XHRcdHBvcnRyYWl0ID0gJ3BvcnRyYWl0J1xuXHRcdFx0XHRicmVhaztcblx0XHR9XG5cblx0XHRpZiAodGhpcy4kd2luZG93Lm9yaWVudGF0aW9uID09PSAwIHx8IHRoaXMuJHdpbmRvdy5vcmllbnRhdGlvbiA9PSAxODApIHtcblx0XHRcdHNjcm9sbCA9IHBvcnRyYWl0O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRzY3JvbGwgPSBsYW5kc2NhcGU7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHNjcm9sbDtcblx0fVxufVxuZXhwb3J0IHtPcmllbnRhdGlvblNydn0iLCIvKipcbiAqIFBvaW50cyBEYXRhIFNlcnZpY2VcbiAqIEBjb25zdHJ1Y3RvclxuICogQG5nSW5qZWN0XG4gKi9cblwidXNlIHN0cmljdFwiO1xuY2xhc3MgUG9pbnRzRGF0YVNydiB7XG5cdGNvbnN0cnVjdG9yKCkge1xuXG5cdH1cblxuXHRnZXQgcG9pbnRzKCkge1xuXHRcdHJldHVybiBQb2ludHNEYXRhU3J2LnBvaW50cygpXG5cdH1cblxuXHRnZXQgc3RhdGVzKCkge1xuXHRcdHJldHVybiBbXG5cdFx0XHR7IG5hbWU6ICdBTEFCQU1BJywgYWJicmV2aWF0aW9uOiAnQUwnIH0sXG5cdFx0XHR7IG5hbWU6ICdBTEFTS0EnLCBhYmJyZXZpYXRpb246ICdBSycgfSxcblx0XHRcdHsgbmFtZTogJ0FNRVJJQ0FOIFNBTU9BJywgYWJicmV2aWF0aW9uOiAnQVMnIH0sXG5cdFx0XHR7IG5hbWU6ICdBUklaT05BJywgYWJicmV2aWF0aW9uOiAnQVonIH0sXG5cdFx0XHR7IG5hbWU6ICdBUktBTlNBUycsIGFiYnJldmlhdGlvbjogJ0FSJyB9LFxuXHRcdFx0eyBuYW1lOiAnQ0FMSUZPUk5JQScsIGFiYnJldmlhdGlvbjogJ0NBJyB9LFxuXHRcdFx0eyBuYW1lOiAnQ09MT1JBRE8nLCBhYmJyZXZpYXRpb246ICdDTycgfSxcblx0XHRcdHsgbmFtZTogJ0NPTk5FQ1RJQ1VUJywgYWJicmV2aWF0aW9uOiAnQ1QnIH0sXG5cdFx0XHR7IG5hbWU6ICdERUxBV0FSRScsIGFiYnJldmlhdGlvbjogJ0RFJyB9LFxuXHRcdFx0eyBuYW1lOiAnRElTVFJJQ1QgT0YgQ09MVU1CSUEnLCBhYmJyZXZpYXRpb246ICdEQycgfSxcblx0XHRcdHsgbmFtZTogJ0ZFREVSQVRFRCBTVEFURVMgT0YgTUlDUk9ORVNJQScsIGFiYnJldmlhdGlvbjogJ0ZNJyB9LFxuXHRcdFx0eyBuYW1lOiAnRkxPUklEQScsIGFiYnJldmlhdGlvbjogJ0ZMJyB9LFxuXHRcdFx0eyBuYW1lOiAnR0VPUkdJQScsIGFiYnJldmlhdGlvbjogJ0dBJyB9LFxuXHRcdFx0eyBuYW1lOiAnR1VBTScsIGFiYnJldmlhdGlvbjogJ0dVJyB9LFxuXHRcdFx0eyBuYW1lOiAnSEFXQUlJJywgYWJicmV2aWF0aW9uOiAnSEknIH0sXG5cdFx0XHR7IG5hbWU6ICdJREFITycsIGFiYnJldmlhdGlvbjogJ0lEJyB9LFxuXHRcdFx0eyBuYW1lOiAnSUxMSU5PSVMnLCBhYmJyZXZpYXRpb246ICdJTCcgfSxcblx0XHRcdHsgbmFtZTogJ0lORElBTkEnLCBhYmJyZXZpYXRpb246ICdJTicgfSxcblx0XHRcdHsgbmFtZTogJ0lPV0EnLCBhYmJyZXZpYXRpb246ICdJQScgfSxcblx0XHRcdHsgbmFtZTogJ0tBTlNBUycsIGFiYnJldmlhdGlvbjogJ0tTJyB9LFxuXHRcdFx0eyBuYW1lOiAnS0VOVFVDS1knLCBhYmJyZXZpYXRpb246ICdLWScgfSxcblx0XHRcdHsgbmFtZTogJ0xPVUlTSUFOQScsIGFiYnJldmlhdGlvbjogJ0xBJyB9LFxuXHRcdFx0eyBuYW1lOiAnTUFJTkUnLCBhYmJyZXZpYXRpb246ICdNRScgfSxcblx0XHRcdHsgbmFtZTogJ01BUlNIQUxMIElTTEFORFMnLCBhYmJyZXZpYXRpb246ICdNSCcgfSxcblx0XHRcdHsgbmFtZTogJ01BUllMQU5EJywgYWJicmV2aWF0aW9uOiAnTUQnIH0sXG5cdFx0XHR7IG5hbWU6ICdNQVNTQUNIVVNFVFRTJywgYWJicmV2aWF0aW9uOiAnTUEnIH0sXG5cdFx0XHR7IG5hbWU6ICdNSUNISUdBTicsIGFiYnJldmlhdGlvbjogJ01JJyB9LFxuXHRcdFx0eyBuYW1lOiAnTUlOTkVTT1RBJywgYWJicmV2aWF0aW9uOiAnTU4nIH0sXG5cdFx0XHR7IG5hbWU6ICdNSVNTSVNTSVBQSScsIGFiYnJldmlhdGlvbjogJ01TJyB9LFxuXHRcdFx0eyBuYW1lOiAnTUlTU09VUkknLCBhYmJyZXZpYXRpb246ICdNTycgfSxcblx0XHRcdHsgbmFtZTogJ01PTlRBTkEnLCBhYmJyZXZpYXRpb246ICdNVCcgfSxcblx0XHRcdHsgbmFtZTogJ05FQlJBU0tBJywgYWJicmV2aWF0aW9uOiAnTkUnIH0sXG5cdFx0XHR7IG5hbWU6ICdORVZBREEnLCBhYmJyZXZpYXRpb246ICdOVicgfSxcblx0XHRcdHsgbmFtZTogJ05FVyBIQU1QU0hJUkUnLCBhYmJyZXZpYXRpb246ICdOSCcgfSxcblx0XHRcdHsgbmFtZTogJ05FVyBKRVJTRVknLCBhYmJyZXZpYXRpb246ICdOSicgfSxcblx0XHRcdHsgbmFtZTogJ05FVyBNRVhJQ08nLCBhYmJyZXZpYXRpb246ICdOTScgfSxcblx0XHRcdHsgbmFtZTogJ05FVyBZT1JLJywgYWJicmV2aWF0aW9uOiAnTlknIH0sXG5cdFx0XHR7IG5hbWU6ICdOT1JUSCBDQVJPTElOQScsIGFiYnJldmlhdGlvbjogJ05DJyB9LFxuXHRcdFx0eyBuYW1lOiAnTk9SVEggREFLT1RBJywgYWJicmV2aWF0aW9uOiAnTkQnIH0sXG5cdFx0XHR7IG5hbWU6ICdOT1JUSEVSTiBNQVJJQU5BIElTTEFORFMnLCBhYmJyZXZpYXRpb246ICdNUCcgfSxcblx0XHRcdHsgbmFtZTogJ09ISU8nLCBhYmJyZXZpYXRpb246ICdPSCcgfSxcblx0XHRcdHsgbmFtZTogJ09LTEFIT01BJywgYWJicmV2aWF0aW9uOiAnT0snIH0sXG5cdFx0XHR7IG5hbWU6ICdPUkVHT04nLCBhYmJyZXZpYXRpb246ICdPUicgfSxcblx0XHRcdHsgbmFtZTogJ1BBTEFVJywgYWJicmV2aWF0aW9uOiAnUFcnIH0sXG5cdFx0XHR7IG5hbWU6ICdQRU5OU1lMVkFOSUEnLCBhYmJyZXZpYXRpb246ICdQQScgfSxcblx0XHRcdHsgbmFtZTogJ1BVRVJUTyBSSUNPJywgYWJicmV2aWF0aW9uOiAnUFInIH0sXG5cdFx0XHR7IG5hbWU6ICdSSE9ERSBJU0xBTkQnLCBhYmJyZXZpYXRpb246ICdSSScgfSxcblx0XHRcdHsgbmFtZTogJ1NPVVRIIENBUk9MSU5BJywgYWJicmV2aWF0aW9uOiAnU0MnIH0sXG5cdFx0XHR7IG5hbWU6ICdTT1VUSCBEQUtPVEEnLCBhYmJyZXZpYXRpb246ICdTRCcgfSxcblx0XHRcdHsgbmFtZTogJ1RFTk5FU1NFRScsIGFiYnJldmlhdGlvbjogJ1ROJyB9LFxuXHRcdFx0eyBuYW1lOiAnVEVYQVMnLCBhYmJyZXZpYXRpb246ICdUWCcgfSxcblx0XHRcdHsgbmFtZTogJ1VUQUgnLCBhYmJyZXZpYXRpb246ICdVVCcgfSxcblx0XHRcdHsgbmFtZTogJ1ZFUk1PTlQnLCBhYmJyZXZpYXRpb246ICdWVCcgfSxcblx0XHRcdHsgbmFtZTogJ1ZJUkdJTiBJU0xBTkRTJywgYWJicmV2aWF0aW9uOiAnVkknIH0sXG5cdFx0XHR7IG5hbWU6ICdWSVJHSU5JQScsIGFiYnJldmlhdGlvbjogJ1ZBJyB9LFxuXHRcdFx0eyBuYW1lOiAnV0FTSElOR1RPTicsIGFiYnJldmlhdGlvbjogJ1dBJyB9LFxuXHRcdFx0eyBuYW1lOiAnV0VTVCBWSVJHSU5JQScsIGFiYnJldmlhdGlvbjogJ1dWJyB9LFxuXHRcdFx0eyBuYW1lOiAnV0lTQ09OU0lOJywgYWJicmV2aWF0aW9uOiAnV0knIH0sXG5cdFx0XHR7IG5hbWU6ICdXWU9NSU5HJywgYWJicmV2aWF0aW9uOiAnV1knIH1cblx0XHRdO1xuXHR9XG5cblx0c3RhdGljIHBvaW50cygpIHtcblx0XHRyZXR1cm4gW3tcblx0XHRcdG5hbWU6ICdBbmltYWwgUHJldmVudGlvbiAoQ2FwKScsXG5cdFx0XHRtb2RlbDogJ0FuaW1hbFByZXZlbnRpb24nXG5cdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnU3BhcmsgQXJyZXN0b3IgKENhcCknLFxuXHRcdFx0XHRtb2RlbDogJ1NwYXJrJ1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnQ3Jvd24vU3BsYXNoJyxcblx0XHRcdFx0bW9kZWw6ICdDcm93bidcblx0XHRcdH0sIHtcblx0XHRcdFx0bmFtZTogJ0V4dGVyaW9yIFN0cnVjdHVyZScsXG5cdFx0XHRcdG1vZGVsOiAnRXh0ZXJpb3InXG5cdFx0XHR9LCB7XG5cdFx0XHRcdG5hbWU6ICdIZWlnaHQgT2YgQ2hpbW5leScsXG5cdFx0XHRcdG1vZGVsOiAnSGVpZ2h0J1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnRmxhc2hpbmcvQ3JpY2tldCcsXG5cdFx0XHRcdG1vZGVsOiAnRmxhc2hpbmcnXG5cdFx0XHR9LCB7XG5cdFx0XHRcdG5hbWU6ICdMaW5pbmcvRmx1ZScsXG5cdFx0XHRcdG1vZGVsOiAnTGluaW5nJ1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnQ2xlYXJhbmNlIFRvIENvbWJ1c3RpYmxlcycsXG5cdFx0XHRcdG1vZGVsOiAnQ2xlYXJhbmNlJ1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnRmlyZWJveCBBcmVhJyxcblx0XHRcdFx0bW9kZWw6ICdGaXJlYm94J1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnU3RhY2tQaXBlIE9yIFN0b3ZlUGlwZScsXG5cdFx0XHRcdG1vZGVsOiAnU3RhY2tQaXBlJ1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnRGFtcGVyIE9wZXJhdGlvbicsXG5cdFx0XHRcdG1vZGVsOiAnRGFtcGVyJ1xuXHRcdFx0fSwge1xuXHRcdFx0XHRuYW1lOiAnRHJhZnQgVGVzdCcsXG5cdFx0XHRcdG1vZGVsOiAnRHJhZnQnXG5cdFx0XHR9XTtcblx0fVxufVxuXG5leHBvcnQge1BvaW50c0RhdGFTcnZ9IiwiLyoqXG4gKiBQcmludCBTZXJ2aWNlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cInVzZSBzdHJpY3RcIjtcbmNsYXNzIFByaW50U3J2IHtcblx0Y29uc3RydWN0b3IoKSB7XG5cblx0fVxuXHRcblx0LyoqXG5cdCAqIHNldCB0aXRsZSBuYW1lIHVzZWQgZm9yIHNhdmluZyB0aGUgZG9jdW1lbnRcblx0ICogQHBhcmFtIHtvYmplY3R9IGZpbGVcblx0ICovXG5cblx0c2V0IHRpdGxlTmFtZShmaWxlKSB7XG5cdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcigndGl0bGUnKS5pbm5lckhUTUwgPSBmaWxlLm5hbWUgKyAnICcgKyBmaWxlLnJlcG9ydDtcblx0fVxuXG5cdHJlc2V0TmFtZSgpIHtcblx0XHRkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCd0aXRsZScpLmlubmVySFRNTCA9ICdXZWxscyBSZXBvcnRzJztcblx0fVxuXG5cdGV4ZWMoKSB7XG5cdFx0d2luZG93LnByaW50KCk7XG5cdH1cbn1cblxuZXhwb3J0IHtQcmludFNydn0iLCIvKiBnbG9iYWwgU2lnbmF0dXJlUGFkICovXG4vKipcbiAqIFNpZ25hdHVyZSBQYWQgU2VydmljZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAbmdJbmplY3RcbiAqL1xuXG5jbGFzcyBTaWduUGFkU3J2IHtcblx0Y29uc3RydWN0b3IoJHJvb3RTY29wZSkge1xuXHRcdHRoaXMuJHJvb3RTY29wZSA9ICRyb290U2NvcGU7XG5cdH1cblxuXHRpbml0KGNhbnZhcywgY2xpZW50KSB7XG5cdFx0dGhpcy5jYW52YXMgPSBjYW52YXM7XG5cdFx0dGhpcy5jbGllbnQgPSBjbGllbnQ7XG5cdFx0dGhpcy5zaWduYXR1cmVQYWQgPSBuZXcgU2lnbmF0dXJlUGFkKGNhbnZhcywge1xuXHRcdFx0b25FbmQ6ICgpID0+IHtcblx0XHRcdFx0dGhpcy5zYXZlKGNsaWVudClcblx0XHRcdFx0dGhpcy4kcm9vdFNjb3BlLiRhcHBseSgpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0cmVzaXplQ2FudmFzKCkge1xuXHRcdC8vIFdoZW4gem9vbWVkIG91dCB0byBsZXNzIHRoYW4gMTAwJSwgZm9yIHNvbWUgdmVyeSBzdHJhbmdlIHJlYXNvbixcblx0XHQvLyBzb21lIGJyb3dzZXJzIHJlcG9ydCBkZXZpY2VQaXhlbFJhdGlvIGFzIGxlc3MgdGhhbiAxXG5cdFx0Ly8gYW5kIG9ubHkgcGFydCBvZiB0aGUgY2FudmFzIGlzIGNsZWFyZWQgdGhlbi5cblx0XHR2YXIgcmF0aW8gPSBNYXRoLm1heCh3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyB8fCAxLCAxKTtcblx0XHR0aGlzLmNhbnZhcy53aWR0aCA9IHRoaXMuY2FudmFzLm9mZnNldFdpZHRoICogcmF0aW87XG5cdFx0dGhpcy5jYW52YXMuaGVpZ2h0ID0gdGhpcy5jYW52YXMub2Zmc2V0SGVpZ2h0ICogcmF0aW87XG5cdFx0dGhpcy5jYW52YXMuZ2V0Q29udGV4dChcIjJkXCIpLnNjYWxlKHJhdGlvLCByYXRpbyk7XG5cdH1cblxuXHRzYXZlKGNsaWVudCkge1xuXHRcdHZhciBkYXRhID0gdGhpcy5zaWduYXR1cmVQYWQudG9EYXRhVVJMKCk7XG5cdFx0Y2xpZW50LnNpZ25hdHVyZSA9IGRhdGE7XG5cdFx0Y29uc29sZS5sb2coZGF0YSk7XG5cdH1cblxuXHRjbGVhcigpIHtcblx0XHR0aGlzLnNpZ25hdHVyZVBhZC5jbGVhcigpO1xuXHRcdHRoaXMuY2xpZW50LnNpZ25hdHVyZSA9ICcnO1xuXHR9XG5cblx0Z2V0U2lnbmF0dXJlKCkge1xuXHRcdGlmICh0aGlzLmNsaWVudC5zaWduYXR1cmUpIHtcblx0XHRcdHRoaXMuc2lnbmF0dXJlUGFkLmZyb21EYXRhVVJMKHRoaXMuY2xpZW50LnNpZ25hdHVyZSk7XG5cdFx0fVxuXHR9XG5cblx0Y2xlYXJSZXNpemVDYW52YXMoKSB7XG5cdFx0dGhpcy5zaWduYXR1cmVQYWQub2ZmKCk7XG5cdFx0dGhpcy5jYW52YXMud2lkdGggPSBudWxsO1xuXHRcdHRoaXMuY2FudmFzLmhlaWdodCA9IG51bGw7XG5cdH1cbn1cbmV4cG9ydCB7U2lnblBhZFNydn0iLCIvKipcbiAqIFRvYXN0IE5vdGlmaWNhdGlvbiBTZXJ2aWNlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBuZ0luamVjdFxuICovXG5cbmNsYXNzIFRvYXN0U3J2e1xuXHRjb25zdHJ1Y3RvcigkbWRUb2FzdCl7XG5cdFx0dGhpcy4kbWRUb2FzdCA9ICRtZFRvYXN0O1xuXHR9XG5cdFxuXHRzaG93VG9hc3QodG9hc3QpIHtcblx0XHR2YXIgbXNnID0gdG9hc3QubXNnIHx8ICdVc2VyOiAnICsgdG9hc3QubmFtZSArICcgaGFzIGJlZW4gZW5yb2xsZWQnXG5cdFx0dGhpcy4kbWRUb2FzdC5zaG93KHRoaXMuJG1kVG9hc3Quc2ltcGxlKCkuY29udGVudChtc2cpKTtcblx0fVxufVxuZXhwb3J0e1RvYXN0U3J2fVxuIl19
