/**
 * Login Controller for login.html
 * @constructor
 * @ngInject
 */
class DateISOSrv {
	constructor() {
		//converts local iso-string to handled timezone
		this.tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
		this.localISOTime = (new Date(Date.now() - this.tzoffset)).toISOString().slice(0, -1);
	}

	get today() {
		return this.localISOTime;
	}

	get tomorrow() {
		//check to see if today is Friday
		//if friday set tomorrow button to Monday
		var today = new Date().getDay()
		switch (today) {
			case 5:
				return moment(new Date(this.localISOTime)).add(3, 'days').toISOString();
				break;
			case 6:
				return moment(new Date(this.localISOTime)).add(2, 'days').toISOString();
				break;
			case 7:
				return moment(new Date(this.localISOTime)).add(1, 'days').toISOString();
				break;
			default:
				return moment(new Date(this.localISOTime)).add(1, 'days').toISOString();
				break;
		}
	}
}

export {DateISOSrv}