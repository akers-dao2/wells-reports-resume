/**
 * Print Service
 * @constructor
 * @ngInject
 */
"use strict";
class PrintSrv {
	constructor() {

	}
	
	/**
	 * set title name used for saving the document
	 * @param {object} file
	 */

	set titleName(file) {
		document.querySelector('title').innerHTML = file.name + ' ' + file.report;
	}

	resetName() {
		document.querySelector('title').innerHTML = 'Wells Reports';
	}

	exec() {
		window.print();
	}
}

export {PrintSrv}