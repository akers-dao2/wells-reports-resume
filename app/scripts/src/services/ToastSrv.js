/**
 * Toast Notification Service
 * @constructor
 * @ngInject
 */

class ToastSrv{
	constructor($mdToast){
		this.$mdToast = $mdToast;
	}
	
	showToast(toast) {
		var msg = toast.msg || 'User: ' + toast.name + ' has been enrolled'
		this.$mdToast.show(this.$mdToast.simple().content(msg));
	}
}
export{ToastSrv}
