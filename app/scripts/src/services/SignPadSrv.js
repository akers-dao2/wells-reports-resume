/* global SignaturePad */
/**
 * Signature Pad Service
 * @constructor
 * @ngInject
 */

class SignPadSrv {
	constructor($rootScope) {
		this.$rootScope = $rootScope;
	}

	init(canvas, client) {
		this.canvas = canvas;
		this.client = client;
		this.signaturePad = new SignaturePad(canvas, {
			onEnd: () => {
				this.save(client)
				this.$rootScope.$apply();
			}
		});
	}

	resizeCanvas() {
		// When zoomed out to less than 100%, for some very strange reason,
		// some browsers report devicePixelRatio as less than 1
		// and only part of the canvas is cleared then.
		var ratio = Math.max(window.devicePixelRatio || 1, 1);
		this.canvas.width = this.canvas.offsetWidth * ratio;
		this.canvas.height = this.canvas.offsetHeight * ratio;
		this.canvas.getContext("2d").scale(ratio, ratio);
	}

	save(client) {
		var data = this.signaturePad.toDataURL();
		client.signature = data;
		console.log(data);
	}

	clear() {
		this.signaturePad.clear();
		this.client.signature = '';
	}

	getSignature() {
		if (this.client.signature) {
			this.signaturePad.fromDataURL(this.client.signature);
		}
	}

	clearResizeCanvas() {
		this.signaturePad.off();
		this.canvas.width = null;
		this.canvas.height = null;
	}
}
export {SignPadSrv}