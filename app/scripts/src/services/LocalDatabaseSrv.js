/**
 * Local Database Service
 * @constructor
 * @ngInject
 */
"use strict";
class LocalDatabaseSrv {
	constructor(CacheFactory, FireBaseRef, $firebaseArray, $firebaseObject,
		$q, $rootScope, $interval, DateISO) {
		//get firebase reference
		this.ref = FireBaseRef.ref;
		
		//set $firebaseArray
		this.$firebaseArray = $firebaseArray;
		
		//set $firebaseArray
		this.$firebaseObject = $firebaseObject;
		//set $q libary 
		this.$q = $q

		this.$rootScope = $rootScope;

		this.DateISO = DateISO;

		this.unwatch;
		
		// Check to make sure the cache doesn't already exist
		if (!CacheFactory.get('cr-cache')) {
			this.conditionReportCache = CacheFactory('cr-cache');
		} else {
			this.conditionReportCache = CacheFactory.createCache('cr-cache', {
				storagePrefix: 'ng-cache',
				// storageMode: 'sessionStorage'
			})
		}
	}

	initalLoad() {

		var users = this.$firebaseArray(this.ref.child('users'))
		users.$loaded().then((users) => {
			this.conditionReportCache.put('users', users);
		});

		var jobs = this.$firebaseArray(this.ref.child('job'))
		jobs.$loaded().then((jobs) => {
			this.conditionReportCache.put('jobs', jobs);
		});

		var jobOrder = this.$firebaseArray(this.ref.child('jobOrder'))
		jobOrder.$loaded().then((jobOrder) => {
			this.conditionReportCache.put('jobOrder', jobOrder);
		});

		var crews = this.$firebaseArray(this.ref.child('crews'))
		crews.$loaded().then((crews) => {
			this.conditionReportCache.put('crews', crews);
		})

		var clients = this.$firebaseArray(this.ref.child('clients'))
		clients.$loaded().then((clients) => {
			/**
			* temp array to load into localstorage
			* @type {array.<objects>}
			*/
			var _clients = [];
			/**
			* tracks when to start pushing items 
			* into the _clients array
			* @type {boolean} startArrayPush
			*/
			var startArrayPush = false;
			/**
			* jobs array
			* @type {array.<objects>} jobs
			*/
			var jobs = this.get('jobs');
			/**
			* job that contains a job date after 3 days ago
			* @type {object} jobs
			*/
			var job = _.find(jobs, (job) => {
				//pull jobs greater than today
				var threeDaysAgo = moment(new Date(this.DateISO.today)).subtract(3, 'days');
				var _jobDate = moment(job.jobDate);
				return moment(threeDaysAgo).isBefore(_jobDate, 'day');
			});

			//loop through client
			for (var index = 0; index < clients.length; index++) {
				//pull the item from the clients array
				var element = clients[index];
				//push the item into the temp array
				if (startArrayPush) {
					_clients.push(element);
				}
				
				//set startArrayPush to true if job.$id matches 
				//the element job id under foreignKey
				if (element.foreignKey === job.$id) {
					startArrayPush = true;
				}
			}

			this.conditionReportCache.put('clients', _clients);

		})

	}

	put(id, data) {
		this.conditionReportCache.put(id, data)
	}

	remove(id) {
		this.conditionReportCache.remove(id)
	}

	get(id) {
		return this.conditionReportCache.get(id);
	}

	$authWithPassword(user) {
		var that = this;

		return this.$q((res, rej) => {
			var users = this.get('users');
			var result = _.findWhere(users, { email: user.email.toLowerCase(), password: user.password });
			this.$setCurrentAuth(result);
			if (result) {
				res(result);
			} else {
				rej({ code: "Error with credentials" });
			}

		});

	}

	$setCurrentAuth(user) {
		console.log(user);
		this.put('currentAuth', user);
	}

	$getCurrentAuth() {
		return this.get('currentAuth')
	}

	$getAuth() {
		return this.$q((res, rej) => {
			var session = this.get('session')

			if (session != 'false') {
				res(true)
			} else {
				console.log(session);
				rej(false)
			}

		})
	}

	$setAuth(data) {
		this.put('session', data);
	}

	$unauth() {
		this.$setAuth('false');
		this.remove('allowed');
		this.remove('currentAuth');
		this.remove('track')
	}

	$save(client, param) {
		//if param.isJob = true
		//updates the jobs cache
		if (param && param.isJob) {
			/**
			 * List of jobs from cache
			 * @type{array.<objects>} jobs 
			 */
			let jobs = this.get('jobs');
			/**
			 * the index of job in the jobs array
			 * @type{number} index
			 */
			let index = _.findIndex(jobs, { $id: client.foreignKey });
			/**
			 * the job object in the jobs array
			 * @type{object} job
			 */
			let job = _.findWhere(jobs, { $id: client.foreignKey });
			if (param.status === 'Complete') {
				job.completedDate = new Date().toISOString();
			}
			//set job status to the new status
			job.status = param.status;
			debugger;
			//update the job in the jobs array
			jobs.splice(index, 1, job);
			//update jobs
			this.put('jobs', jobs);
			console.log(this.get('jobs'));
			return
		}


		let clients = this.get('clients');
		let index = _.findIndex(clients, { $id: client.$id });
		// if no match then add the new record to the end of array
		if (index === -1) {
			clients.push(client);
		} else {
			clients.splice(index, 1, client);
		}

		this.put('clients', clients);
	}

	$watch(client) {
		this.unwatch = this.$rootScope.$watchCollection(function () { return client }, (n, o) => {
			if (o !== n) {
				console.log(n);
				n.$dirty = true;
				this.$save(n);
			}
		});
	}

	$unwatch() {
		this.unwatch();
	}

	$syncToFirebase() {
		var clients = this.get('clients');
		var jobs = this.get('jobs');
		var clientsToUpdate = _.filter(clients, { $dirty: true });
		var jobsToUpdate = _.filter(jobs, { $dirty: true });

		clientsToUpdate.forEach((v, idx, arr) => {
			var client = this.$firebaseObject(this.ref.child('clients').child(v.$id));
			delete v.$dirty;
			for (var key in v) {
				if (v.hasOwnProperty(key)) {
					client[key] = v[key];

				}
			}
			client.$save().then(() => {
				this.initalLoad();
			})

		})

		jobsToUpdate.forEach((v, idx, arr) => {
			var job = this.$firebaseObject(this.ref.child('jobs').child(v.foreignKey));
			delete v.$dirty;
			for (var key in v) {
				if (v.hasOwnProperty(key)) {
					job[key] = v[key];

				}
			}
			job.$save().then(() => {
				this.initalLoad();
			})

		})

		this.initalLoad();
	}

	set $trackSiteLocation(param) {
		/**
		 * location and id
		 * @type {object.<site,id>} || {string}
		 */
		this.put('track', param);
	}

	get $trackSiteLocation() {
		return this.get('track');
	}
}
export {LocalDatabaseSrv}
