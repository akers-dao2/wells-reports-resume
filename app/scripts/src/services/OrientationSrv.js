/**
 * Orientation Service
 * @constructor
 * @ngInject
 */
class OrientationSrv {
	constructor($window, $rootScope, $q) {
		this.$window = $window;
		this.$rootScope = $rootScope;
	}

	set sourcePage(source) {
		this.source = source;
	}

	get scrollHeight() {
		var scroll = undefined;
		var landscape = undefined;
		var portrait = undefined;
		switch (this.source) {
			case 'report':
				landscape = 'report-landscape'
				portrait = 'report-portrait'
				break;
			case 'job':
				landscape = 'job-landscape'
				portrait = 'job-portrait'
				break;
			case 'createJob':
				landscape = 'create-job-landscape'
				portrait = 'create-job-portrait'
				break;
			case 'admin':
				landscape = 'admin-landscape'
				portrait = 'admin-portrait'
				break;

			default:
				landscape = 'landscape'
				portrait = 'portrait'
				break;
		}

		if (this.$window.orientation === 0 || this.$window.orientation == 180) {
			scroll = portrait;
		} else {
			scroll = landscape;
		}

		return scroll;
	}
}
export {OrientationSrv}