/**
 * Job Status Service 
 * @constructor
 * @ngInject
 */
class JobStatusSrv {
	constructor(FireBaseRef, $firebaseObject, $q, LocalDatabase) {
		// Get a database reference 
		this.ref = FireBaseRef.ref;
		this.$firebaseObject = $firebaseObject;
		this.$q = $q;
		this.LocalDatabase = LocalDatabase;
	}

	setStatus(client, status) {
		if (navigator.onLine) {
			var job = this.$firebaseObject(this.ref.child('job').child(client.foreignKey));
			job.$loaded().then((job) => {
				if (status === 'Complete') {
					job.completedDate = new Date().toISOString();
				}
				job.status = status;
				job.$save();
			})
		} else {
			this.LocalDatabase.$save(client, {isJob:true,status:status});
		}
	}

	date(client) {
		return this.$q((resolve, reject) => {
			var job = this.$firebaseObject(this.ref.child('job').child(client.foreignKey));
			job.$loaded().then((job) => {
				resolve(moment(job.completedDate).format('ddd, MMM Do YYYY'));
			})
		})
	}
}
export {JobStatusSrv}