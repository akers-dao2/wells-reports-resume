/**
 * Loading Popup Service
 * @constructor
 * @ngInject
 */
"use strict";
class LoadingPopupSrv {
	constructor($mdDialog, $q, $timeout) {
		this.$mdDialog = $mdDialog
		this.$q = $q;
		this.$timeout = $timeout;
	}

	show() {

		this.$mdDialog.show({
			clickOutsideToClose: false,
			template: '<md-dialog aria-label="Loading dialog"> ' +
			'<md-dialog-content layout="row" layout-align="center center">' +
			'<div style="font-size:30px">Loading</div>' +
			'<md-progress-circular md-mode="indeterminate" md-diameter="25"></md-progress-circular>' +
			'</md-dialog-content></md-dialog>',
		});
	}

	hide() {
		this.$timeout(() => {
			this.$mdDialog.hide();
		}, 0)
	}
}
export {LoadingPopupSrv}