/**
 * FireBase Auth Service
 * @constructor
 * @ngInject
 */
"use strict";
class FireBaseAuthSrv{
	constructor(FireBaseRef, $firebaseAuth){
		this.FireBaseRef = FireBaseRef.ref;
		return $firebaseAuth(this.FireBaseRef);
	}
	
	// get ref() {
	// 	return this.FireBaseRef;
	// }
}

export{FireBaseAuthSrv}