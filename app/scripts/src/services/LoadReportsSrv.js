/**
 * Set Address Service
 * @constructor
 * @ngInject
 */

class LoadReportsSrv {
	constructor(FireBaseRef, $state, $stateParams, $firebaseArray,
		$firebaseObject, $q, LocalDatabase, $rootScope, FireBaseAuth) {
		this.FireBaseRef = FireBaseRef;
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$firebaseArray = $firebaseArray;
		this.$firebaseObject = $firebaseObject;
		this.$q = $q;
		this.LocalDatabase = LocalDatabase;
		this.FireBaseAuth = FireBaseAuth;
		// Get a database reference 
		this.ref = FireBaseRef.ref;
	}

	remote($scope) {

		return this.$q((resolve, reject) => {
		
			//client name
			var id = this.$stateParams.id;
		
			//get the client data
			var clients = this.$firebaseArray(this.ref.child('clients'));

			//load the client data
			clients.$loaded().then((data) => {

				data.map((cv, i, a) => {

					if (cv.foreignKey === id) {
						//get the client data i.e. name, adderss, city, state
						var client = this.$firebaseObject(this.ref.child('clients').child(cv.$id));

						client.$bindTo($scope, 'client').then(() => {
							resolve($scope.client);
						});
						
/*						debugger;
						
						this.unwatch = client.$watch((data)=> {
							console.log(data);
							debugger
							var client = this.$firebaseObject(this.ref.child('clients').child(data.key));
							client.$loaded().then((client)=>{
								this.LocalDatabase.$save(client)
								console.log("data changed!",client);
							})
							
							
						});*/
					}
				})
			})
		});
	}
	
	remoteStopWatch(){
		this.unwatch();
	}

	remoteUsers() {
		return this.$q((resolve, reject) => {
			var currentAuth = this.FireBaseAuth.$getAuth();
			var users = this.$firebaseArray(this.ref.child('users'));
			this.users = users
			users.$loaded().then((users) => {
				var user = _.findWhere(users, { email: currentAuth.password.email });
				resolve({ users: users, tech: user });

			})
		});
	}

	local() {
		return this.$q((resolve, reject) => {
			
			//client name
			var id = this.$stateParams.id;

			var clients = this.LocalDatabase.get('clients');

			//load the client data
			clients.map((cv, i, a) => {

				if (cv.foreignKey === id) {
					//get the client data i.e. name, adderss, city, state
					setTimeout(() => {
						resolve(cv)
					}, 0);



				}
			})
		})

	}
}

export {LoadReportsSrv};