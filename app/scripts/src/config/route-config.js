"use strict";
var config = (function config() {
	function config($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdGestureProvider, CacheFactoryProvider) {
		$mdGestureProvider.skipClickHijack();

		angular.extend(CacheFactoryProvider.defaults, {
			maxAge: 15 * 60 * 1000,
			storageMode: 'localStorage'
		});

		$stateProvider.state('login', {
			url: '/login',
			templateUrl: 'views/login.html',
			controller: 'LoginCtrl as vm'

		})
			.state('logOut', {
				url: '/logOut',
				templateUrl: 'views/login.html',
				controller: function (FireBaseAuth) {
					FireBaseAuth.$unauth();
				}
			})
			.state('jobs', {
				url: '/jobs',
				templateUrl: 'views/jobs.html',
				controller: 'JobsCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}


					}]
				}
			})
			.state('admin', {
				url: '/admin',
				templateUrl: 'views/admin.html',
				controller: 'AdminCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", function (Auth) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						return Auth.$requireAuth();
					}]
				}
			})
			.state('createJob', {
				url: '/createJob/:id',
				templateUrl: 'views/createJobs.html',
				controller: 'CreateJobCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}
					}]
				}
			})
			.state('allReports', {
				url: '/allReports',
				templateUrl: 'views/allReports.html',
				controller: "AllReportsCtrl as vm",
				// controller: 'WaterSheetCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}
					}]
				}
			})
			.state('allReports.report', {
				url: '/report/:id',
				templateUrl: 'views/reportSheet.html',
				controller: 'ReportCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}
					}]
				}
			})
			.state('allReports.dangerSheet', {
				url: '/dangerSheet/:id',
				templateUrl: 'views/dangerSheet.html',
				controller: 'DangerSheetCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}
					}]
				}
			})
			.state('allReports.waterSheet', {
				url: '/waterSheet/:id',
				templateUrl: 'views/waterSheet.html',
				controller: 'WaterSheetCtrl as vm',
				resolve: {
					// controller will not be loaded until $requireAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					"currentAuth": ["FireBaseAuth", "LocalDatabase", function (Auth, LocalDatabase) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						if (navigator.onLine) {
							return Auth.$requireAuth();
						} else {
							return LocalDatabase.$getAuth();
						}
					}]
				}
			})


		$urlRouterProvider.otherwise('login');

		$mdThemingProvider.theme('default')
			.primaryPalette('light-blue')
			.accentPalette('orange');
	}

	config.$inject = ['$stateProvider', '$urlRouterProvider', '$mdThemingProvider', '$mdGestureProvider', 'CacheFactoryProvider']

	return config
})();
export {config}