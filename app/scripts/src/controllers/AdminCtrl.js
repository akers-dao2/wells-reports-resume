/**
 * @ngdoc controller
 * @name ConditionApp.controller.AdminCtrl
 * @description
 * Admin Controller admin.html
 * @requires currentAuth
 * @requires FireBaseRef
 * @requires $firebaseArray
 * @requires FireBaseAuth
 * @requires $mdDialog
 * @requires $mdToast
 * @requires $scope
 * @requires $firebaseObject
 * @requires $window
 * @requires Orientation
 * @requires deviceDetector
 * @requires LocalDatabase
 * @requires $rootScope
 * @requires DateISO
 * @ngInject
 *  
 */

class AdminCtrl {
	constructor(currentAuth, FireBaseRef, $firebaseArray, FireBaseAuth, $mdDialog, $mdToast,
		$scope, $firebaseObject, $window, Orientation, deviceDetector, LocalDatabase, $rootScope, DateISO) {

		//converts local iso-string to handled timezone
		this.DateISO = DateISO
		
		// Get a database reference 
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.users = $firebaseArray(this.ref.child('users'));
		this.crews = $firebaseArray(this.ref.child('crews'));
		this.clients = $firebaseArray(this.ref.child('clients'));
		this.$mdDialog = $mdDialog;
		this.$mdToast = $mdToast;
		this.$scope = $scope;
		this.auth = FireBaseAuth;
		this.master = { email: '', password: '', name: '' };
		this.$firebaseObject = $firebaseObject;
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;
		
		// AdminCtrl.set$watch.apply(this);
		
		Orientation.sourcePage = 'admin';

		$window.onorientationchange = () => {
			console.log(Orientation.scrollHeight)
			this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		}

		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone'
		} else {
			this.scroll = Orientation.scrollHeight;
		}
	}

	static set$watch() {
		var unwatch = this.users.$watch(function (data) {
			console.log("data changed!", data);
		});
	}
	/**
	 * @ngdoc method
	 * @name enroll
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * Enroll new admin user
	 * @param {HTMLFormElement} form new admin user HTML form
	 * @param {Object} $event key pressed
	 */

	enroll(form, $event) {
		if ($event.keyCode != 13 && $event.keyCode != 0) {
			return
		}

		this.auth.$createUser(this.user).then((userData) => {
			console.log("User " + userData.uid + " created successfully!");

			this.showToast({ name: this.user.name });

			this.user.email = this.user.email.toLowerCase();

			this.users.$add(this.user);

			form.$setPristine();
			form.$setUntouched();
			this.user = angular.copy(this.master);

		})
			.catch((error) => {
				this.showAlert(error);
				console.error("Error: ", error);
			});
	}
	/**
	 * @ngdoc method
	 * @name _remove
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * Remove either the user or crew
	 * @param {Object|string} itemToRemove The item to remove
	 */

	_remove(itemToRemove) {

		switch (this.selectedIndex) {
			case 1:
				AdminCtrl.removeUser.call(this, itemToRemove);
				break;
			case 2:
				AdminCtrl.removeCrew.call(this, itemToRemove);
				break;
			default:
				break;
		}

	}
	/**
	 * @ngdoc method
	 * @name _remove
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * Remove crew
	 * @param {Object|string} itemToRemove The item to remove
	 */

	static removeCrew(itemToRemove) {
		var numOfMatches;

		this.clients.$loaded().then((clients) => {
			numOfMatches = clients.filter((cv, i, a) => {
				if (cv.crew == undefined) {
					return false
				}
				return cv.crew === itemToRemove.name
			})
			
			//if a project does not exist remove the user. Else display an error
			if (numOfMatches.length == 0) {
				itemToRemove.$remove().then(() => {
					this.showToast({ msg: "Crew removed successfully!" });
				})
					.catch((error) => {
						this.showAlert(error);
						console.error("Error: ", error);
					});
			} else {
				this.showAlert("Project(s) exist under the crew: " + itemToRemove.name + ", cannot delete crew");
				console.warn();
			}
		});


	}
	/**
	 * @ngdoc method
	 * @name _remove
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * Remove user
	 * @param {Object|string} itemToRemove The item to remove
	 */

	static removeUser(itemToRemove) {
		var numOfMatches = undefined;
		this.jobs = this.$firebaseArray(this.ref.child('job'));
		// this.clients = this.$firebaseArray(this.ref.child('clients'));
		//check if a project exist first before removing user
		this.jobs.$loaded().then((jobs) => {

			this.clients.$loaded().then((clients) => {
				numOfMatches = clients.filter((cv, i, a) => {
					if (cv.techID == undefined) {
						return false
					}
					
					var job = _.findWhere(jobs,{$id:cv.foreignKey});

					return (cv.techID === itemToRemove.$id && job.status === 'Open')
				})
			
				//if a project does not exist remove the user. Else display an error
				if (numOfMatches.length == 0) {
					this.auth.$removeUser(itemToRemove).then(() => {
						itemToRemove.$remove().then((ref) => {
							// data has been deleted locally and in the database
							this.showToast({ msg: "User removed successfully!" });
						}, function (error) {
							console.log("Error:", error);
						});
					})
						.catch((error) => {
							this.showAlert(error);
							console.error("Error: ", error);
						});
				} else {
					this.showAlert("Project(s) exist under the name: " + itemToRemove.name + ", cannot delete user");
					console.warn();
				}
			});
		})
	}
	/**
	 * @ngdoc method
	 * @name showAlert
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * show alert dialog
	 * @param {string} error The error to display
	 */

	showAlert(error) {
		console.log(error);
		var alert = this.$mdDialog.alert()
			.title('Error')
			.content(error)
			.ok('Close');

		this.$mdDialog.show(alert);


	}
	/**
	 * @ngdoc method
	 * @name _showReassignDialog
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * show reassign dialog.  Allows the admin to reassign all jobs to another user
	 * @param {Object} $event Button click
	 * @param {Object} obj The user to reassign
	 */

	_showReassignDialog(userToReassign, $event) {
		var that = this;

		this.$mdDialog.show({
			targetEvent: $event,
			clickOutsideToClose: true,
			templateUrl: 'views/reassignProjects.html',
			controller: function DialogController($mdDialog) {

				that.users.$loaded().then((users) => {
					console.log(users)
					this.users = users;
				});

				// this.user = user;
				this.closeDialog = function () {
					$mdDialog.hide();
				};

				this.change = function (user) {
					that.clients.$loaded().then((clients) => {
						clients.map(function (cv, i, a) {
							if (cv.techID === userToReassign.$id) {
								//client to update the tech for
								var client = that.$firebaseObject(that.ref.child('clients').child(cv.$id))
								client.$loaded().then((client) => {
									console.log(client.tech)

									client.tech = user.name
									client.techID = user.$id
									client.$save().then((ref) => {
										console.log("change successful")
										that.showToast({ msg: "Projects reassign to: " + user.name });
										$mdDialog.hide();
									}, function (error) {
										$mdDialog.hide();
										that.showAlert(error);
										console.error("Error:", error);
									});
								})
							}
						})
					});
				};
			},
			controllerAs: 'vm'
		});
	}
	/**
	 * @ngdoc method
	 * @name showEditDialog
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * show edit dialog.  Edit admin user
	 * @param {Object} $event Button click
	 * @param {Object} obj The user to edit
	 */

	showEditDialog($event, obj) {
		var that = this;
		var templateUrl = undefined;

		if (obj.data.name === 'All Crews') {
			return
		}
		var enumTab = {
			editUser: 1,
			editCrew: 2
		}
		switch (this.selectedIndex) {
			case 1:
				templateUrl = 'views/editUser.html'
				break;
			case 2:
				templateUrl = 'views/addCrew.html'
				break;

			default:
				break;
		}
		this.$mdDialog.show({
			targetEvent: $event,
			clickOutsideToClose: true,
			scope: this.$scope,        // use parent scope in template
			preserveScope: true,  // do not forget this if use parent scope
			templateUrl: templateUrl,
			controller: function DialogController($scope, $mdDialog) {


				let search, model;
				
				//identifed which model information to load
				switch (that.selectedIndex) {
					case enumTab.editUser: // Load user info
						search = 'users'
						model = 'user'
						break;
					case enumTab.editCrew: // Load crew info
						search = 'crews'
						model = 'crew'
						this.btnName = false // display Add
						break;

					default:
						break;
				}

				this[model] = that.$firebaseObject(that.ref.child(search).child(obj.data.$id));

				this.update = function (data, form) {

					form.$setPristine();
					form.$setUntouched();
					$mdDialog.hide();

					switch (that.selectedIndex) {
						case enumTab.editUser:
							updateUser(data);
							break;
						case enumTab.editCrew:
							updateCrew(data);
							break;

						default:
							break;
					}

				};

				function updateUser(user) {
					if (user.tempCrew) {
						let temp = user.crew
						user.crew = user.tempCrew;
						user.tempCrew = temp;
						user.tempDate = that.DateISO.tomorrow
					}

					if (user.newpassword) {
						that.auth.$changePassword({
							email: user.email,
							oldPassword: user.password,
							newPassword: user.newpassword
						}).then(() => {
							user.password = user.newpassword;
							delete user.newpassword;
							user.$save();
							user.$loaded().then(function (user) {
								that.LocalDatabase.put('allowed', user);
							})
							that.showToast({ msg: "Successfully Changed Data" });
							console.log("Password changed successfully!");
						}).catch((error) => {
							console.error("Error: ", error);
						});
					} else {
						delete user.newpassword;
						that.showToast({ msg: "Successfully Changed Data" });
						user.$save();
						that.$rootScope.$broadcast('successLogin', { user: user });
					}

					let newUser = {};
					_.extendOwn(newUser, user);
					delete newUser.$$conf;
					that.LocalDatabase.put('allowed', newUser);
				}

				function updateCrew(crew) {
					crew.$save();
				}

				this.closeDialog = () => {
					$mdDialog.hide();
				};

				this.remove = (itemToRemove) => {
					this.closeDialog();
					that._remove(itemToRemove);

				};

				this.showReassignDialog = (userToReassign, form) => {
					this.closeDialog();
					that._showReassignDialog(userToReassign, form);

				};

				this.removeTempCrew = (user) => {
					user.crew = user.tempCrew;
					user.tempDate = null
					user.tempCrew = null
				}
			},
			controllerAs: 'mdDialog'
		});
    }
	/**
	 * @ngdoc method
	 * @name showAddDialog
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * show add dialog.  Add admin user
	 * @param {Object} $event The button clicked
	 */

	showAddDialog($event) {
		var that = this;

		var templateUrl = undefined;
		switch (this.selectedIndex) {
			case 1:

				break;
			case 2:
				templateUrl = 'views/addCrew.html'
				break;

			default:
				break;
		}
		this.$mdDialog.show({
			targetEvent: $event,
			clickOutsideToClose: true,
			scope: this.$scope,        // use parent scope in template
			preserveScope: true,  // do not forget this if use parent scope
			// Since GreetingController is instantiated with ControllerAs syntax
			// AND we are passing the parent '$scope' to the dialog, we MUST
			// use 'vm.<xxx>' in the template markup
			templateUrl: templateUrl,
			controller: function DialogController($scope, $mdDialog) {

				this.btnName = true // display Add
				
				this.closeDialog = function () {
					$mdDialog.hide();
				};

				this.add = function (item, form) {
					that.crews.$add(item);

					form.$setPristine();
					form.$setUntouched();
					$mdDialog.hide();


				};
			},
			controllerAs: 'mdDialog'
		});
    }
	/**
	 * @ngdoc method
	 * @name showToast
	 * @methodOf ConditionApp.controller.AdminCtrl
	 * @description
	 * Show toast message at the bottom of the screen
	 * @param {string} toast The message to display
	 */

	showToast(toast) {
		var msg = toast.msg || 'User: ' + toast.name + ' has been enrolled'
		this.$mdToast.show(this.$mdToast.simple().content(msg));
	}
}
export {AdminCtrl};