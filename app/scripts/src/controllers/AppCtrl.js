/**
 * App Controller for index.html
 * @param {!ui.router.$stae} $state
 * @param {} FireBaseAuth
 * @param {} LocalDatabase
 * @param {!angular.$rootScope} $rootScope
 * @param {} $firebaseArray
 * @constructor
 * @ngInject
 */

class AppCtrl {
	constructor($state, FireBaseAuth, LocalDatabase, $rootScope) {
		this.$state = $state;
		this.master = { email: '', password: '' };
		this.auth = FireBaseAuth
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;

		this.user = this.LocalDatabase.get('allowed');

		$rootScope.$on('successLogin', (evt, data) => {
			this.user = this.LocalDatabase.get('allowed');
		})
	}

	nav(site, $event) {
		if (site === 'logout') {
			console.log(site)
			this.auth.$unauth();
			this.$state.go('login');
			this.LocalDatabase.$unauth();
			this.user = '';
			return
		}
		if (site === 'createJob') {
			this.$state.transitionTo(site, { id: '' });

		} else {
			this.$state.transitionTo(site);
		}

	}
}


export {AppCtrl};

