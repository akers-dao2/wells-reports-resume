/**
 * Login Controller for login.html
 * @param {!firebase.$firebaseAuth} $firebaseAuth 
 * @param {!angular.$mdDialog} $mdDialog
 * @param {!ui.router.$stae} $state
 * @param {} FireBaseRef
 * @param {} FireBaseAuth
 * @param {} LoadingPopup
 * @param {} LocalDatabase
 * @param {!angular.$rootScope} $rootScope
 * @param {} $firebaseArray
 * @param {!angular.Scope} $scope
 * @constructor
 * @ngInject
 */

class LoginCtrl {
	constructor($firebaseAuth, $mdDialog, $state, FireBaseRef, FireBaseAuth,
		LoadingPopup, LocalDatabase, $rootScope, $firebaseArray, $scope, DateISO, $firebaseObject) {
		this.$mdDialog = $mdDialog;
		this.$state = $state;
		this.master = { email: '', password: '' };
		this.auth = FireBaseAuth
		this.LoadingPopup = LoadingPopup;
		this.LocalDatabase = LocalDatabase;
		this.$rootScope = $rootScope;
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray
		this.$scope = $scope;
		this.DateISO = DateISO;
		this.$firebaseObject = $firebaseObject;
		this.user = {};
		
		/**
		 * @type {object} 
		 */
		var user = this.LocalDatabase.get('allowed');
		if (user) {
			this.createJob = user.createJob;
			this.adminstration = user.adminstration;
			this.user.email = user.email;
			this.user.password = user.password;
		}

		var track = this.LocalDatabase.$trackSiteLocation;
		if (track) {
			if (track.site && !track.$id) {
				this.$state.go(track.site );
			} else{
				this.$state.go(track.site , { id: track.$id });
			}
		}


	}

	showAlert(error, form) {
		/**
		 * @type {!angular.$mdDialog}
		 */
		var alert = this.$mdDialog.alert()
			.title('Authentication failed:')
			.content(error.code)
			.ok('Close');

		this.$mdDialog.show(alert);
	}

	cancel(form) {
		this.user = angular.copy(this.master);
		form.$setPristine();
		form.$setUntouched();

	}

	login($event, form) {

		var that = this;

		if ($event.keyCode != 13 && $event.keyCode != 0) {
			return
		}

		this.LoadingPopup.show();

		this.LocalDatabase.initalLoad();

		if (navigator.onLine) {
			this.auth.$authWithPassword(this.user)
				.then(onSuccess.bind(this), onReject.bind(this));
			
			//set the current auth locally
			this.LocalDatabase.$authWithPassword(this.user);
		} else {
			this.LocalDatabase.$authWithPassword(this.user)
				.then(onSuccess.bind(this), onReject.bind(this))
		}

		function onSuccess(authData) {
			var users, user, email;

			if (navigator.onLine) {
				users = that.$firebaseArray(that.ref.child('users'));

				users.$loaded().then((users) => {
					var user = _.findWhere(users, { email: authData.password.email });
					if (user.tempDate) {

						let remoteUser = this.$firebaseObject(this.ref.child('users').child(user.$id));
						remoteUser.$loaded().then((_remoteUser) => {
							let curDate = this.DateISO.today
							let isAfter = moment(curDate).isAfter(_remoteUser.tempDate, 'day');
							let isSame = moment(_remoteUser.tempDate).isSame(curDate, 'day');

							if (isAfter || isSame) {
								_remoteUser.crew = user.tempCrew;
								_remoteUser.tempDate = null
								_remoteUser.tempCrew = null
								_remoteUser.$save().then(() => {
									let newUser = {};
									_.extendOwn(newUser, _remoteUser);
									delete newUser.$$conf;
									this.LocalDatabase.put('allowed', newUser);
								})
							} else {
								this.LocalDatabase.put('allowed', user);
							}

						})
					} else {

						this.LocalDatabase.put('allowed', user);
					}

					that.$rootScope.$broadcast('successLogin', { user: user });

					console.log(user);
				})
			} else {
				users = that.LocalDatabase.get('users');
				email = authData.email.toLowerCase();
				user = _.findWhere(users, { email: email });
				that.LocalDatabase.put('allowed', user);
				that.$rootScope.$broadcast('successLogin', { user: user });

			}

			console.log("Logged in as:", authData);
			LoginCtrl.clearForm.bind(that, form);
			that.LoadingPopup.hide();
			that.LocalDatabase.$setAuth('true');
			that.$state.go('jobs');
		}

		function onReject(error) {
			that.showAlert(error);
			console.error("Authentication failed:", error);
			LoginCtrl.clearForm.bind(that, form);
			that.LoadingPopup.hide();
			that.LocalDatabase.$setAuth('false');
		}


	}

	static clearForm(form) {
		this.user = angular.copy(this.master);
		form.$setPristine();
		form.$setUntouched();

	}
}


export {LoginCtrl};

