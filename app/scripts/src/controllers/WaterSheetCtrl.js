/**
 * Water Sheet Controller for index.html
 * @constructor
 * @ngInject
 */

class WaterSheetCtrl {
	constructor(FireBaseRef, $firebaseObject, $state, $scope, $firebaseArray,
		LoadReportsSrv, SignPad, LocalDatabase, LoadingPopup,
		Orientation, $window, deviceDetector, FireBaseAuth, JobStatus, Print) {
		
		//ui-router state service
		this.$state = $state;
		
		// Get a database reference 
		this.ref = FireBaseRef.ref;

		//Signature Pad
		this.canvas = document.querySelector('canvas');
		this.SignPad = SignPad;

		this.LoadReportsSrv = LoadReportsSrv;

		this.FireBaseAuth = FireBaseAuth;

		this.$firebaseArray = $firebaseArray;

		this.LoadingPopup = LoadingPopup

		this.LocalDatabase = LocalDatabase;

		this.$scope = $scope;

		this.JobStatus = JobStatus;

		this.Print = Print;
		
		//display loading window
		LoadingPopup.show();

		this.loadData();

		Orientation.sourcePage = 'report';

		$window.onorientationchange = () => {
			this.scroll = Orientation.scrollHeight;
			this.loadData();
		}

		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone'
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		this.$scope.$on('connected', () => {
			console.log('connected');
			this.LocalDatabase.$syncToFirebase();
		});
	}

	static initSignPad() {
		//Init Signature
		this.SignPad.init(this.canvas, this.client);
		this.SignPad.resizeCanvas();
		this.SignPad.getSignature();
	}

	loadData() {
		//check if the system is online
		if (navigator.onLine) {

			this.LoadReportsSrv.remote(this.$scope).then((client) => {
				this.client = client;

				this.LoadReportsSrv.remoteUsers().then((data) => {
					this.users = data.users;
					if (!this.client.tech) {
						this.client.tech = data.tech.name;
					}
					this.printJob = data.tech.printJob;
					//hide loading window
					this.LoadingPopup.hide();
				})
				
				//set $watch on vm.client scope. Any changes to vm.client are saved
				this.LocalDatabase.$watch(this.client);

				setTimeout(() => {
					//Init Signature
					WaterSheetCtrl.initSignPad.call(this);
				}, 1);
			})
		} else {
			this.LoadReportsSrv.local(this.$scope).then((client) => {
				//set the view model to client data from local database
				this.client = client;

				//load current admin user from local database
				var user = this.LocalDatabase.$getCurrentAuth();
				
				//set the tech based on the current user's email
				var users = this.LocalDatabase.get('users');
				this.users = users;

				//set the tech
				if (!this.client.tech) {
					this.client.tech = user.name;
					this.client.techID = user.$id;
				}
				
				//set $watch on vm.client scope. Any changes to vm.client are saved
				this.LocalDatabase.$watch(this.client);

				WaterSheetCtrl.initSignPad.call(this);
				
				//hide loading window
				this.LoadingPopup.hide();
			})
		}
	}

	signature() {
		this.SignPad.clear();
	}

	close() {
		this.$state.go('jobs');
		this.Print.resetName();
		this.LocalDatabase.$unwatch();
	}

	complete() {
		this.JobStatus.setStatus(this.client, 'Complete');
		this.close();
	}

	print() {
		this.JobStatus.setStatus(this.client, 'Printed');
		this.Print.titleName = { name: this.client.name, report: 'Water Report' };
		this.Print.exec();

	}
}
export {WaterSheetCtrl}