/**
 * All Reports Controller for allReports.html
 * @constructor
 * @ngInject
 */
class AllReportsCtrl {
	constructor($state, $stateParams, $firebaseArray, LoadReportsSrv, $scope, $rootScope, LocalDatabase, LoadingPopup) {
		this.$rootScope = $rootScope;

		//counts the number condition reports added to the tab list
		this.countOfConditionReports = 0;

		//hides the add button for additional reports
		this.hideBtn = true;

		// access the data to know which reports to display
		if (navigator.onLine) {

			LoadReportsSrv.remote($scope).then((client) => {
				this.client = client;

				AllReportsCtrl.loadTabs.call(this);

			})
		} else {
			LoadReportsSrv.local($scope).then((client) => {
				//set the view model to client data from local database
				this.client = client;

				AllReportsCtrl.loadTabs.call(this);

				$scope.$watchCollection(function () { return client }, (n, o) => {
					if (o !== n) {
						LocalDatabase.$save(n)
					}
				});

			});
		}
	}

	static loadTabs() {
		this.tabs = [
			{
				label: 'Condition Report',
				ngif: this.client.condition,
				html: 'views/reportSheet.html'
			}, {
				label: 'Danger Report',
				ngif: this.client.danger,
				html: 'views/dangerSheet.html'
			}, {
				label: 'Water Report',
				ngif: this.client.water,
				html: 'views/waterSheet.html'
			}]

		if (this.client.conditionD) {
			this.tabs.splice(1, 0, {
				id: 'creport',
				label: 'Condition Report 1',
				ngif: this.client.condition,
				html: 'views/reportSheet.1.html'
			})
		}

		if (this.client.conditionG) {
			this.tabs.splice(2, 0, {
				id: 'creport',
				label: 'Condition Report 2',
				ngif: this.client.condition,
				html: 'views/reportSheet.2.html'
			})
		}

		var countExistConditionReports = _.where(this.tabs, { id: 'creport' })
		this.countOfConditionReports = countExistConditionReports.length;
		var setTab = _.findWhere(this.tabs, { ngif: true });
		if (this.countOfConditionReports == 2) {
			this.hideBtn = false;
		}

		this.selectTab(setTab.html);
	}

	addConditionReport() {
		if (this.selectedIndex = 0) {
			this.selectedIndex = this.currentIndex;
		} else {
			this.selectedIndex = this.currentIndex + this.countOfConditionReports;
		}
		
		// this.$rootScope.$broadcast('newConditionReport');
		if (!this.tabs) {
			return
		}

		if (!this.client.condition) {
			this.tabs.splice(0, 0, {
				label: 'Condition Report',
				ngif: true,
				html: 'views/reportSheet.html'
			})
			this.client.condition = true;
			return
		}
		this.countOfConditionReports++;

		if (this.countOfConditionReports <= 2) {
			this.tabs.splice(this.countOfConditionReports, 0, {
				label: 'Condition Report ' + this.countOfConditionReports,
				ngif: true,
				html: 'views/reportSheet.' + this.countOfConditionReports + '.html'
			})
		}

		if (this.countOfConditionReports == 2) {

			this.hideBtn = false;

		}
	}

	addDangerReport() {
		this.tabs.push({
			label: 'Danger Report',
			ngif: true,
			html: 'views/dangerSheet.html'
		});

		this.client.danger = true;
	}

	addWaterReport() {
		this.tabs.push({
			label: 'Water Report',
			ngif: true,
			html: 'views/waterSheet.html'
		})

		this.client.water = true;
	}

	selectTab(tabName) {
		this.tabHTML = tabName;
	}
}
export {AllReportsCtrl}