/**
 * Report Controller reportSheet.html
 * @constructor
 * @ngInject
 */
"use strict";
class ReportCtrl {
	constructor(FireBaseRef, /*currentAuth*/ $scope, $firebaseArray,
		PointsData, $state, LoadReportsSrv, SignPad, LocalDatabase, FireBaseAuth,
		LoadingPopup, Orientation, $window, deviceDetector, $firebaseObject,
		JobStatus, Print, $interval) {

		this.$scope = $scope;
				
		// Get a database reference 
		this.ref = FireBaseRef.ref;
		
		//state service
		this.$state = $state;
		
		//Signature Pad
		this.canvas = document.querySelector('canvas');
		this.SignPad = SignPad;

		this.LoadReportsSrv = LoadReportsSrv;

		this.FireBaseAuth = FireBaseAuth;

		this.$firebaseArray = $firebaseArray;

		this.$firebaseObject = $firebaseObject

		this.LoadingPopup = LoadingPopup

		this.LocalDatabase = LocalDatabase;

		this.JobStatus = JobStatus;

		this.Print = Print;
		
		//display loading window
		LoadingPopup.show();

		this.loadData();

		Orientation.sourcePage = 'report'; 
		
		//update the orientation.  Set height scroll = Portrait Mode or  scrollLG = Landscape
		$window.onorientationchange = () => {
			this.scroll = Orientation.scrollHeight;
			//resize signature area
			// ReportCtrl.initSignPad.call(this);
			this.loadData();
			// $scope.$apply();
		}
		
		//set a fix height for device of iphone
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone'
		} else {
			this.scroll = Orientation.scrollHeight;
		}
		
		//Load the Point of Visual Inspection information
		this.headers = PointsData.points;

		//load available options
		this.options = ['S - Satisfactory', 'X - Unsatisfactory', 'N/A - Not Applicable'];

		this.$scope.$on('connected', () => {
			console.log('connected');
			this.LocalDatabase.$syncToFirebase();
		});

		this.LocalDatabase.$trackSiteLocation = { site: 'allReports.report', $id: this.$state.params.id }
	}

	static initSignPad() {
		//Init Signature
		
		this.SignPad.init(this.canvas, this.client);
		this.SignPad.resizeCanvas();
		this.SignPad.getSignature();
	}

	loadData() {
				
		//check if the system is online
		if (navigator.onLine) {
			//access current signed in user data
			let currentAuth = this.FireBaseAuth.$getAuth();
			
			//load the client data
			this.LoadReportsSrv.remote(this.$scope).then((client) => {
				this.client = client;
			
				//set the tech based on the current user's email
				var users = this.$firebaseArray(this.ref.child('users'));
				this.users = users;
				users.$loaded().then((users) => {
					var user = _.findWhere(users, { email: currentAuth.password.email });
					if (!this.client.tech) {
						this.client.tech = user.name;
						this.client.techID = user.$id;
					}
					this.printJob = user.printJob;
					this.JobStatus.date(this.client).then((date) => {
						this.completedDate = date;
					});
					//hide loading window
					this.LoadingPopup.hide();
				})
				
				/**
				 *set $watch to add and remove classes to hide columns
				 */
				this.hideOnPrint();

				//temp fix for signature issue
				setTimeout(() => {
					//Init Signature
					ReportCtrl.initSignPad.call(this);
				}, 1);

				//set $watch on vm.client scope. Any changes to vm.client are saved
				this.LocalDatabase.$watch(this.client);

			})
		} else {
			/**
			 * load data from local database
			 */
			this.LoadReportsSrv.local().then((client) => {
				//set the view model to client data from local database
				this.client = client;
				
				//load current admin user from local database
				var user = this.LocalDatabase.$getCurrentAuth()
				
				//set the tech based on the current user's email
				var users = this.LocalDatabase.get('users');
				this.users = users;

				if (!this.client.tech) {
					this.client.tech = user.name;
					this.client.techID = user.$id;
				}

				this.printJob = user.printJob;
				this.JobStatus.date(this.client).then((date) => {
					this.completedDate = date;
				});
					
				//set $watch on vm.client scope. Any changes to vm.client are saved
				this.LocalDatabase.$watch(this.client);
				
				//Init Signature
				ReportCtrl.initSignPad.call(this);
				
				//hide loading window
				this.LoadingPopup.hide();
			})
		}
	}

	setTechID(tech) {
		var techs = this.$firebaseArray(this.ref.child('users'));
		techs.$loaded().then((techs)=> {
			var _tech = _.findWhere(techs, { name: tech });
			this.client.techID = _tech.$id;
		})
	}

	signature() {
		this.SignPad.clear();
	}

	close() {/** function for close button */
		this.$state.go('jobs');
		this.Print.resetName();
		this.LocalDatabase.$unwatch();
	}

	complete() {
		this.JobStatus.setStatus(this.client, 'Complete');
		this.close();
	}

	print() {
		this.JobStatus.setStatus(this.client, 'Printed');
		this.Print.titleName = { name: this.client.name, report: 'Conditional Report' };
		this.Print.exec();
	}

	hideOnPrint() {

		this.$scope.$watchCollection(() => { return this.client },
			(n, o) => {
				
				//list of condition headers to review
				var listOfConditions = [
					{ name: 'conditionA', hide: 'hideA' },
					{ name: 'conditionB', hide: 'hideB' },
					{ name: 'conditionC', hide: 'hideC' },
					{ name: 'conditionD', hide: 'hideD' },
					{ name: 'conditionE', hide: 'hideE' },
					{ name: 'conditionF', hide: 'hideF' },
					{ name: 'conditionG', hide: 'hideG' },
					{ name: 'conditionH', hide: 'hideH' },
					{ name: 'conditionI', hide: 'hideI' }

				];
				//checks to see if the  condition header exist.
				//does not print column with no condition header 
				listOfConditions.forEach((element) => {
					if (n[element.name]) {
						(n[element.name].length > 0) ? this[element.hide] = false : this[element.hide] = true
					} else {
						this[element.hide] = true;
					}
				});
				
				//hides the right border if column B and C are hidden
				this.borderA = (this.hideB && this.hideC);
				this.borderD = (this.hideE && this.hideF);
				this.borderG = (this.hideH && this.hideI);

				
				//hides the right border if column C are hidden
				this.borderB = (this.hideC);
				this.borderE = (this.hideF);
				this.borderH = (this.hideI);


			});
	}
}

export {ReportCtrl}