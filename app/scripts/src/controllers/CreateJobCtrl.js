/**
 * Create Jobs Controller createJobs.html
 * @constructor
 * @ngInject
 */
"use strict";
class CreateJobCtrl {
	constructor(FireBaseRef, currentAuth, $scope, $firebaseArray, $state, $mdDialog, $location,
		$anchorScroll, Toast, $stateParams, $firebaseObject, LoadingPopup,
		Orientation, $window, deviceDetector, PointsData, LocalDatabase) {
		
		//load states
		this.states = PointsData.states;
		
		// Get a database reference 
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.$mdDialog = $mdDialog;
		var crewsList = $firebaseArray(this.ref.child('crews'));
		this.$firebaseObject = $firebaseObject;
		this.master = {};
		this.$state = $state;
		this.$location = $location;
		this.$anchorScroll = $anchorScroll;
		this.Toast = Toast;
		this.id = $stateParams.id
		this.LoadingPopup = LoadingPopup;
		this.LocalDatabase = LocalDatabase;
		
		//clean the form
		this.cleanForm();


		if (this.id) {
			this.name = "Edit Job";
			this.btnName = "Save Job";
		} else {
			this.name = "Create Job";
			this.btnName = "Create Job";
			//default state field to PA
			this.client = { state: 'PA' };
		}

		crewsList.$loaded().then((items) => {
			this.crews = _.reject(items, function (value) {
				return value.name === "All Crews";
			});
		});

		if (this.id) {
			this.LoadingPopup.show();
			//list of all clients
			var clients = $firebaseArray(this.ref.child('clients'));
			//loop through clients for a match
			clients.$loaded().then((clients) => {
				clients.map((cv, i, a) => {
					if (cv.foreignKey === this.id) {
						var job = this.$firebaseObject(this.ref.child('job').child(cv.foreignKey));
						job.$loaded().then((job) => {
							cv.jobDate = new Date(job.jobDate);
							//pull the crew for client from JOB key
							cv.crew = job.crew
							this.client = cv;
						})
					}

				});
				this.LoadingPopup.hide();
			});
		}

		Orientation.sourcePage = 'createJob'; 
		
		//set md-card-content css height property
		$window.onorientationchange = () => {
			this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		}
		
		//fix the height on iphone 250px
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone'
		} else {
			this.scroll = Orientation.scrollHeight;
		}
	}

	query(searchText) {
		console.log(searchText);
		return _.where(this.states, { abbreviation: searchText });
	}

	createJob(form) {
		//adding new job
		if (!this.id) {
			if (!this.client.condition && !this.client.danger && !this.client.water) {
				var alert = this.$mdDialog.alert({
					title: 'Error',
					content: 'You must choose a report to create',
					ok: 'Ok'
				});

				this.$mdDialog.show(alert)

				return
			}
			var crewToAdd = this.$firebaseArray(this.ref.child('job'));
			var job = {
				name: this.client.name,
				crew: this.client.crew,
				status: 'Open',
				jobDate: this.client.jobDate.toISOString()
			}
			crewToAdd.$add(job).then((ref) => {
				var clients = this.$firebaseArray(this.ref.child('clients'));
				var jobOrders = this.$firebaseArray(this.ref.child('jobOrder'));
				var id = ref.key();
				var user = this.LocalDatabase.get('allowed');
				this.client.foreignKey = id;
				//remove crew from client key.
				delete this.client.crew;

				clients.$add(this.client).then(() => {
					jobOrders.$loaded().then((jobOrder) => {
						if (jobOrder.length !== 0) {
							var job = this.$firebaseObject(this.ref.child('job').child(id));
							//initial load of jobOrder table
							job.$loaded().then((_job) => {
								jobOrder.$add({ foreignKey: _job.$id, order: jobOrder.length });
							});
						}
					});
				});

				this.Toast.showToast({ msg: 'Job Added' });


				if (user.crew !== 'All Crews') {
					var confirm = this.$mdDialog.confirm({
						title: 'Success',
						content: 'Would you like to open the new job',
						ok: 'Open Job',
						cancel: 'Create New Job'
					});

					this.$mdDialog.show(confirm).then((data) => {
						this.$state.go('allReports.report', { id: id });
						this.cleanForm(form);
					}, (data) => {
						this.cleanForm(form);

					});
				} else {
					this.cleanForm(form);
				}
			})
		} 
		//updating an existing job
		else {
			var client = this.$firebaseObject(this.ref.child('clients').child(this.client.$id));

			this.LoadingPopup.show();

			var name = this.client.name;
			var crew = this.client.crew;
			var jobDate = this.client.jobDate.toISOString();
			delete this.client.crew;

			client.$loaded().then((client) => {
				angular.copy(this.client, client);
				client.$save().then((ref) => {
					var job = this.$firebaseObject(this.ref.child('job').child(client.foreignKey));
					job.$loaded().then((job) => {
						job.name = name;
						job.crew = crew;
						job.jobDate = jobDate;
						job.$save().then(() => {
							this.LoadingPopup.hide();
							this.$state.go('jobs');
						});
					})
				});
			});
		}
	}

	cleanForm(form) {
		if (form) {
			form.$setPristine();
			form.$setUntouched();
			this.client = angular.copy(this.master);
			this.client = { state: 'PA' };
		}
		document.querySelector('.top').scrollIntoView();

	}

	cancel(form) {
		form.$setPristine();
		form.$setUntouched();
		this.client = angular.copy(this.master);
		this.$state.go('jobs');
	}
}

export {CreateJobCtrl}