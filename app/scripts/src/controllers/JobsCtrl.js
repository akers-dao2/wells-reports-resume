/**
 * Jobs Controller jobs.html
 * @constructor
 * @ngInject
 */
"use strict";
class JobsCtrl {
	constructor(FireBaseRef, $firebaseObject, $state, currentAuth, $scope, $firebaseArray,
		Toast, LocalDatabase, $mdDialog, $window, Orientation, deviceDetector,
		$mdBottomSheet, DateISO, $interval, $filter) {
		
		//converts local iso-string to handled timezone
		this.DateISO = DateISO

		this.$state = $state;
		// Get a database reference 
		this.ref = FireBaseRef.ref;
		this.$firebaseArray = $firebaseArray;
		this.$firebaseObject = $firebaseObject;
		this.Toast = Toast;
		this.LocalDatabase = LocalDatabase;
		this.$scope = $scope;
		this.$filter = $filter;
		
		//inject $mdBottomSheet service
		this.$mdBottomSheet = $mdBottomSheet;

		this.$interval = $interval;
		
		//holds a list of selected items
		this.selectedList = [];
		
		
		// Get signed in user's email address
		//check to see if the system is online
		if (navigator.onLine) {
			if (currentAuth != undefined) {
				this.adminEmail = currentAuth.password.email;
				this.loadJobs(false);
			}
		} else {
			this.adminEmail = this.LocalDatabase.$getCurrentAuth().email;
			this.loadJobs(true);
		}
		
		//set allowed button items
		this.user = this.LocalDatabase.get('allowed');
		if (this.user) {
			this.editJob = this.user.editJob;
			this.deleteJob = this.user.deleteJob;
			this.printJob = this.user.printJob;
		}

		this.$mdDialog = $mdDialog;

		//set orientation css file - 
		Orientation.sourcePage = 'job'

		$window.onorientationchange = () => {
			this.scroll = Orientation.scrollHeight;
			$scope.$apply();
		}

		//set fix height for iPhone
		if (deviceDetector.device === 'iphone') {
			this.scroll = 'scroll-iPhone'
		} else {
			this.scroll = Orientation.scrollHeight;
		}

		//check to see if the temp crew date has passed
		// JobsCtrl.tempCrew.call(this);

		//check every 1s to see if temp crew date has passed
		/*this.checkTempCrew = this.$interval(() => {
			JobsCtrl.tempCrew.call(this);
		}, 1000);*/


		this.$scope.$on('connected', () => {
			console.log('connected');
			this.loadJobs(false);
			this.LocalDatabase.$syncToFirebase();
		});
		
		//set $watch on vm.search scope. Any changes to vm.search are saved
		this.unwatch = this.$scope.$watchCollection(() => { return this.search }, (n, o) => {
			if (o !== n) {
				console.log(n);
				this.onSearchHandler();
			}
		});


		this.LocalDatabase.$trackSiteLocation = { site: 'jobs' }

	}

	static tempCrew() {
		//update crew if the temp date has been passed
		if (this.user || this.user.tempDate) {
			let remoteUser = this.$firebaseObject(this.ref.child('users').child(this.user.$id));
			remoteUser.$loaded().then((_remoteUser) => {
				let curDate = this.DateISO.today
				let isAfter = moment(curDate).isAfter(_remoteUser.tempDate, 'day');
				let isSame = moment(_remoteUser.tempDate).isSame(curDate, 'day');
				if (_remoteUser.tempDate) {
					if (isAfter || isSame) {
						_remoteUser.crew = this.user.tempCrew;
						_remoteUser.tempDate = null
						_remoteUser.tempCrew = null
						_remoteUser.$save().then(() => {
							let newUser = {};
							_.extendOwn(newUser, _remoteUser);
							delete newUser.$$conf;
							this.LocalDatabase.put('allowed', newUser);
							this.loadJobs(false);
							this.$interval.cancel(this.checkTempCrew);
						})
					}
				}
			})
		} else {
			this.$interval.cancel(this.checkTempCrew);
		}

	}

	/**
	 * Load jobs data
	 * @type {boolean} local True if loading from local database
	 */
	loadJobs(local) {
		if (!local) { //online run this code
			let users = this.$firebaseArray(this.ref.child('users'));
			//bind crew object to the $scope.crew to pull the value for crew
			users.$loaded().then((users) => {

				users.map((currentValue, index, array) => {
					var jobsList = [];
					var jobs = undefined;

					if (angular.lowercase(currentValue.email) === angular.lowercase(this.adminEmail)) {
						let jobOrder = this.$firebaseArray(this.ref.child('jobOrder'));
						jobs = this.$firebaseArray(this.ref.child('job'));
						jobs.$loaded().then((jobs) => {

							if (currentValue.crew === 'All Crews') {
								//display all jobs for techs with crew st to All Crews

								jobOrder.$loaded().then((jobOrder) => {
									JobsCtrl.sortJobs.call(this, { jobs: jobs, jobOrder: jobOrder });
									this.jobs = this.sortByJobDate;
								})

								// init search
								this.search = '';
					
								//display print
								this.printed = true;
							} else { //only have access to one crew
								
								//filter jobs based on crew
								jobsList = jobs.filter(function (cv, i, a) {
									return cv.crew === currentValue.crew
								});

								if (jobsList.length == 0) {
									return
								}

								//do not display print
								this.printed = false;

								jobOrder.$loaded().then((jobOrder) => {
									JobsCtrl.sortJobs.call(this, { jobs: jobsList, jobOrder: jobOrder });
									// init search
									this.todaySearch();
								})
							}
						})
					}
				})

			});
		} else { //offline run this code
			let users = this.LocalDatabase.get('users');
			let user = _.findWhere(users, { email: angular.lowercase(this.adminEmail) });
			let jobs = this.LocalDatabase.get('jobs');
			let jobOrder = this.LocalDatabase.get('jobOrder');

			JobsCtrl.loadJobData.call(this, { currentValue: user, jobs: jobs, jobOrder: jobOrder });

		}
	}

	static sortJobs(param) {
		//sort jobs by job Date first
		this.sortByJobDate = param.jobs.sort(function (a, b) {
			var d1 = new Date(a.jobDate);
			var d2 = new Date(b.jobDate);
			return d2.getTime() - d1.getTime();
		});
									
		// sort jobs by job order second
		var sort = _.sortBy(this.sortByJobDate, (obj) => {
			var j = _.findWhere(param.jobOrder, { foreignKey: obj.$id });
			if (j) {
				return j.order;
			}
		});

		this.backUpJobs = sort;

		return sort;
	}

	static loadJobData(param) {
		var jobsList = [];

		if (param.currentValue.crew === 'All Crews') {

			//display all jobs for techs with crew set to All Crews
			
			this.jobs = JobsCtrl.sortJobs({ jobs: param.jobs, jobOrder: param.jobOrder });;
			
			// init search
			this.search = '';
								
			//display print button
			this.printed = true;
		} else {
			//filter jobs based on crew
			jobsList = param.jobs.filter(function (cv, i, a) {
				return cv.crew === param.currentValue.crew
			});

			if (jobsList.length == 0) {
				return
			}
								
			//do not display print button
			this.printed = false;

			this.jobs = JobsCtrl.sortJobs.call(this, { jobs: jobsList, jobOrder: param.jobOrder });
			
			// set search to today's date
			// this.search = this.today.substring(0,10)
			this.todaySearch();
		}
	}

	open(job) {

		this.$state.go('allReports.report', { id: job.$id });

	}

	edit(job, $event) {
		var that = this;
		if (this.selectedList.length > 1) {
			this.$mdDialog.show({
				targetEvent: $event,
				clickOutsideToClose: true,
				templateUrl: "views/changeJobCrew.html",
				controller: function DialogController($scope, $mdDialog) {

					this.update = (crew, form) => {
						var jobs = that.$firebaseArray(that.ref.child('job'));
						jobs.$loaded().then((jobs) => {
							that.selectedList.forEach((val, idx, arr) => {
								var job = _.findWhere(jobs, { $id: val.$id })
								var jobIndex = _.findIndex(jobs, { $id: val.$id })
								job.crew = crew;
								jobs.$save(jobIndex);
							})
							form.$setPristine();
							form.$setUntouched();
							that.selectedList = [];
							$mdDialog.hide();

						})

					}
					var crews = that.$firebaseArray(that.ref.child('crews'));
					crews.$loaded().then((items) => {
						this.crews = _.reject(items, (value) => {
							return value.name === "All Crews";
						});
					})

					this.closeDialog = () => {
						that.selectedList = [];
						$mdDialog.hide();
					};
				},
				controllerAs: 'mdDialog'
			});
		} else {
			this.$state.go('createJob', { id: job.$id });
		}
	}

	delete(job) {
		var clients = this.$firebaseArray(this.ref.child('clients'));
		var jobsToRemove = this.$firebaseArray(this.ref.child('job'));

		var confirm = this.$mdDialog.confirm()
			.title('Warn')
			.content('This data will be deleted permanently.')
			.ok('YES DELETE')
			.cancel('Cancel');



		this.$mdDialog
			.show(confirm).then(removeJob.bind(this), () => {
				this.job = undefined;
				this.selectedList = [];
			});

		function removeJob() {
			this.selectedList = [];

			clients.$loaded().then((item) => {
				item.map((cv, i, a) => {

					if (cv.foreignKey === job.$id) {
						clients.$remove(i).then(() => {
							// this.loadJobs();
							jobsToRemove.$loaded().then((_item) => {
								_item.map((_cv, _i) => {
									if (_cv.$id === job.$id) {
										jobsToRemove.$remove(_i).then(() => {
											var jobOrders = this.$firebaseArray(this.ref.child('jobOrder'));
											jobOrders.$loaded().then((_jobOrders) => {
												_jobOrders.map((jobOrder, jobOrderIndex) => {
													if (jobOrder.foreignKey === job.$id) {
														jobOrders.$remove(jobOrderIndex);
													}
												});
											});

											this.job = null;
											this.loadJobs(false, true);
											this.Toast.showToast({ msg: 'Job removed' });

										});
									}
								});
							});//jobsToRemove
						});//clients
					}
				});//item
			});
			this.selectedList = [];
		}//removeJob
	}//delete

	setSelectedJob($event, job) {
		var isMatch = false;
		
		//set vm.job for the last selected item
		//not used when multiple items are selected
		this.job = job;
		//check to see if the job is already selected.
		//if selected deselect job
		this.selectedList.forEach((val, idx, arr) => {
			if (val.$id === job.$id) {
				this.selectedList.splice(idx, 1);
				isMatch = true;
			}
		})

		if (!isMatch) {	
			// push selected job ist onto the selectedList array
			this.selectedList.push(job);
		}


	}

	disableOptions() {
		if (this.selectedList.length == 0 || this.selectedList.length > 1) {
			return true;
		}
		else {
			return false;
		}
	}

	setSelectedClass(job) {
		var match = _.findWhere(this.selectedList, { $id: job.$id });

		return (match) ? true : false
	}

	get tomorrow() {
		return this.DateISO.tomorrow;
	}

	get today() {
		return this.DateISO.today;
	}

	get todaySetClass() {
		// return '{jobDate:' + this.today.substring(0, 10) + ',status:!printed}'
		return this.today.substring(0, 10) + ',!printed'
	}

	todaySearch() {
		// this.search = '{jobDate:' + this.today.substring(0, 10) + ',status:!printed}'
		this.search = this.today.substring(0, 10) + ',!printed'

	}

	get tomorrowSetClass() {
		// return '{jobDate:' + this.tomorrow.substring(0, 10) + ',status:!printed}'
		return this.tomorrow.substring(0, 10) + ',!printed'
	}

	tomorrowSearch() {
		// this.search = '{jobDate:' + this.tomorrow.substring(0, 10) + ',status:!printed}'
		this.search = this.tomorrow.substring(0, 10) + ',!printed'

	}

	onSortHandler($item, $partFrom, $partTo) {
		//pull the sort order for each job
		var jobOrders = this.$firebaseArray(this.ref.child('jobOrder'));

		jobOrders.$loaded().then((jobOrder) => {

			//initial load of jobOrder table
			if (jobOrder.length === 0) {

				var jobs = this.$firebaseArray(this.ref.child('job'));
				jobs.$loaded().then((_jobs) => {
					_jobs.forEach((_job, index) => {
						jobOrder.$add({ foreignKey: _job.$id, order: index });
					});
				});
				
				//update  jobOrder for first sort attempt
				jobOrder.forEach((v, i) => {
					var index = _.findIndex($partTo, { $id: v.foreignKey });
					//save index for FROM
					jobOrder[i].order = index;
					jobOrder.$save(i);
				});
			} else {
				jobOrder.forEach((v, i) => {
					var index = _.findIndex($partTo, { $id: v.foreignKey });
					if (index !== -1) {	
						//save index for FROM
						jobOrder[i].order = index;
						jobOrder.$save(i);
					}
				});
			}
		});
	}

	onSearchHandler() {

		var searchValue = this.search;
		var crews = this.LocalDatabase.get('crews');
		var statuses = [{ name: 'open' }, { name: 'printed' }, { name: 'complete' }]
		var operators = ['!'];
		if (searchValue) {

			if (searchValue.search(',') > 0) {
				//variable for storing the new object
				let searchValueObject = {};
				let searchParams = searchValue.split(',');
				searchParams.map((v, i, a) => {

					var isDate = !(new Date(v.replace(' ', '')).toDateString() == 'Invalid Date');
					var isCrew = _.some(crews, function (crew) {
						return crew.name.toUpperCase() === v.toUpperCase()
					});
					var isStatus = _.some(statuses, function (status) {
						var _status
						operators.map(function (ops, i, a) {
							_status = v.replace(ops, '');
						})
						return _status.toUpperCase() === status.name.toUpperCase()
					});

					if (isDate) {
						searchValueObject.jobDate = v;
					}
					else if (isCrew) {
						searchValueObject.crew = v;
					}
					else if (isStatus) {
						searchValueObject.status = v;
					}
					else {
						searchValueObject.name = v;
					}

					try {
						this.jobs = this.$filter('filter')(this.backUpJobs, searchValueObject);

					} catch (error) {
						console.log(error);
					}

				})
			} else {

				this.jobs = this.$filter('filter')(this.sortByJobDate, searchValue)
			}


		} else { //no search item 

			this.jobs = this.sortByJobDate
		}
	}

	saveSearch(searchValue) {
		var searches = this.$firebaseArray(this.ref.child('searches'))
		searches.$loaded().then(function (searches) {
			searches.$add(searchValue);
		})
	}


}
export {JobsCtrl}