/// <reference path="../../../typings/underscore/underscore.d.ts"/>
/* global Firebase, SignaturePad */
/// <reference path="../../../typings/angularjs/angular.d.ts"/>


var app = angular.module('ConditionApp', ['firebase', 'ngMaterial', 'ngMessages', 'ui.router', 'angular-cache',
	'ng.deviceDetector', 'angularMoment', 'angular-sortable-view','ngWebworker', 'ngIOS9UIWebViewPatch']);

import {config} from './config/route-config.js'
app.config(config);


app.run(function ($rootScope, $state, FireBaseRef) {

	$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		
		
	});

	$rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		document.querySelector('ui-view').scrollIntoView();

	});

	$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
		// We can catch the error thrown when the $requireAuth promise is rejected
		// and redirect the user back to the home page
		console.log(error)
		if (error === "AUTH_REQUIRED") {
			$state.go("login");
		}
	});

	var connectedRef = new Firebase("https://wells-report-resume.firebaseio.com/.info/connected");
	connectedRef.on("value", function (snap) {
		if (snap.val() === true) {
			$rootScope.$broadcast('connected');
		} else {
			$rootScope.$broadcast('disconnected');
		}
	});

})

/**
 * Controllers
 */

import {AppCtrl} from './controllers/AppCtrl.js'
app.controller('AppCtrl', AppCtrl);

import {LoginCtrl} from './controllers/LoginCtrl.js'
app.controller('LoginCtrl', LoginCtrl);

import {AdminCtrl} from './controllers/AdminCtrl.js'
app.controller('AdminCtrl', AdminCtrl);

import {AllReportsCtrl} from './controllers/AllReportsCtrl.js'
app.controller('AllReportsCtrl', AllReportsCtrl);

import {ReportCtrl} from './controllers/ReportCtrl.js'
app.controller('ReportCtrl', ReportCtrl);

import {JobsCtrl} from './controllers/JobsCtrl.js'
app.controller('JobsCtrl', JobsCtrl);

import {CreateJobCtrl} from './controllers/CreateJobCtrl.js'
app.controller('CreateJobCtrl', CreateJobCtrl);

import {DangerSheetCtrl} from './controllers/DangerSheetCtrl.js'
app.controller('DangerSheetCtrl', DangerSheetCtrl);

import {WaterSheetCtrl} from './controllers/WaterSheetCtrl.js'
app.controller('WaterSheetCtrl', WaterSheetCtrl);

import {SideNavCtrl} from './controllers/SideNavCtrl.js'
app.controller('SideNavCtrl', SideNavCtrl);


/**
 *Services 
 */

import {SignPadSrv} from './services/SignPadSrv.js'
app.service('SignPad', SignPadSrv);

import {ToastSrv} from './services/ToastSrv.js'
app.service('Toast', ToastSrv);

import {LoadReportsSrv} from './services/LoadReportsSrv.js'
app.service('LoadReportsSrv', LoadReportsSrv);

import {FireBaseAuthSrv} from './services/FireBaseAuthSrv.js'
app.service('FireBaseAuth', FireBaseAuthSrv);

import {LoadingPopupSrv} from './services/LoadingPopupSrv.js'
app.service('LoadingPopup', LoadingPopupSrv);

import {LocalDatabaseSrv} from './services/LocalDatabaseSrv.js'
app.service('LocalDatabase', LocalDatabaseSrv);

import {PointsDataSrv} from './services/PointsDataSrv.js'
app.service('PointsData', PointsDataSrv);

import {OrientationSrv} from './services/OrientationSrv.js'
app.service('Orientation', OrientationSrv);

import {JobStatusSrv} from './services/JobStatusSrv.js'
app.service('JobStatus', JobStatusSrv);

import {PrintSrv} from './services/PrintSrv.js'
app.service('Print', PrintSrv);

import {DateISOSrv} from './services/DateISOSrv.js'
app.service('DateISO', DateISOSrv);

/**
 * Firebase URL
 */
app.constant('FireBaseRef', {
	ref: new Firebase("https://wells-report-resume.firebaseio.com") 
})

app.filter('jobFilter', function ($filter) {

	// In the return function, we must pass in a single parameter which will be the data we will work on.
	// We have the ability to support multiple other parameters that can be passed into the filter optionally
	return function (arrayToParse, searchValue) {

		var output;

		if (searchValue) {

			if (searchValue.search('{') == 0 && searchValue.search('}') > 0) {
				//split the search text by comma to push array
				let searchParams = searchValue.split(',');
				//variable for storing the new object
				let searchValueObject = {};
				//loop through searchParams to split each array based colon
				searchParams.map((v, i, a) => {
					let searchParam = v.split(':');
					let key, value;
					//remove { or }
					
					try {
						key = searchParam[0].replace(/[{}]/, '');
						value = searchParam[1].replace(/[{}]/, '');
					} catch (error) {

					}
					
					//create a new object with key and value
					searchValueObject[key] = value;
				});

				try {
					output = $filter('filter')(arrayToParse, searchValueObject);
				} catch (error) {

				}



			} else {

				output = $filter('filter')(arrayToParse, searchValue)
			}
		} else {

			output = $filter('filter')(arrayToParse, searchValue)
		}

		return output;

	}

});