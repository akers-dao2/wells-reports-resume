NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "ConditionApp.controller.AdminCtrl",
      "shortName": "ConditionApp.controller.AdminCtrl",
      "type": "controller",
      "moduleName": "ConditionApp.controller",
      "shortDescription": "Admin Controller admin.html",
      "keywords": "$event $firebasearray $firebaseobject $mddialog $mdtoast $rootscope $scope $window _remove _showreassigndialog add admin adminctrl alert allows api bottom button click clicked conditionapp controller crew currentauth dateiso devicedetector dialog display edit enroll error firebaseauth firebaseref form html item itemtoremove jobs key localdatabase message method obj orientation pressed reassign remove screen showadddialog showalert showeditdialog showtoast toast user"
    }
  ],
  "apis": {
    "api": true
  },
  "__file": "_FAKE_DEST_/js/docs-setup.js",
  "__options": {
    "startPage": "/api",
    "scripts": [
      "js/angular.min.js",
      "js/angular-animate.min.js",
      "js/marked.js"
    ],
    "styles": [],
    "title": "API Documentation",
    "html5Mode": true,
    "editExample": true,
    "navTemplate": false,
    "navContent": "",
    "navTemplateData": {},
    "loadDefaults": {
      "angular": true,
      "angularAnimate": true,
      "marked": true
    }
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "js/angular.min.js",
    "js/angular-animate.min.js",
    "js/marked.js"
  ]
};