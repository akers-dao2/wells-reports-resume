var gulp = require('gulp');
var server = require('gulp-server-livereload');
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var uglify = require('gulp-uglify');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var ngAnnotate = require('gulp-ng-annotate');
var minify = require('gulp-minify');
var manifest = require('gulp-manifest');
var gulpDocs = require('gulp-ngdocs');

var paths = {
  scripts: ['app/scripts/src/**/*.js']
}

gulp.task('serve', function () {
  gulp.src('app')
    .pipe(server({
      livereload: true,
      directoryListing: false,
      open: true,
      host: '0.0.0.0'
    }));
});

gulp.task("scripts", function () {
  return gulp.src(paths.scripts[0])
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(ngAnnotate())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("app/scripts/lib"));
});

gulp.task("min", function () {
  return gulp.src("app/scripts/lib/bundle.js")
    .pipe(uglify())
    .pipe(minify())
    .pipe(gulp.dest("app/scripts/lib/"))
})

gulp.task('modules',['scripts'], function () {
  browserify({
    entries: 'app/scripts/src/app.js',
    debug: true
  })
    .transform(babelify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest("app/scripts/lib"));
});

gulp.task('ngdocs', [], function () {
  return gulp.src('app/scripts/src/**/*.js')
    .pipe(gulpDocs.process())
    .pipe(gulp.dest('app/docs'));
});

gulp.task('build',['scripts','manifest','modules'])

gulp.task('manifest',['modules'], function () {
  gulp.src(['app/scripts/lib/bundle.js',
    'app/styles/**/*', 'app/views/*', 'app/bower_components/*/*.min.js',
    'app/bower_components/*/*/*.min.js', 'app/bower_components/firebase/firebase.js',
    'app/index.html','app/bower_components/angular/angular.webkitassign.js','app/scripts/lib/services/Worker-LoadDatabase.js'],
    { base: 'app/' })
    .pipe(manifest({
      hash: true,
      preferOnline: true,
      network: ['*'],
      filename: 'app.appcache',
      exclude: 'app.appcache'
    }))
    .pipe(gulp.dest('app'));
});

// Rerun the task when a file changes 
gulp.task('watch', function () {
  gulp.watch(paths.scripts, ['scripts']);
});

gulp.task('default', ['watch', 'scripts']);