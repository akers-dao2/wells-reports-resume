##Wells Reports

---------
	
Application for generating Chimney Inspection Reports

**Installation:**

You need node.js and npm. At the root of the project type:

```node
npm install
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. Angularjs (https://angularjs.org) use for MVVM
1. Angular Material Design (https://material.angularjs.org/latest/)
1. HTML5 (App Cache, LocalStorage, FileSystem API, drag-n-drop)
1. FireBase (https://www.firebase.com)
1. ES2015 (Watchify, Babel)


----------
**Example:**

- Offers CRUD for inspection reports
- Signing of reports
- Printing Reports
- Real-Time Data Save
- Offline Mode


![wells_reporting_recording.gif](wells_reporting_recording.gif)